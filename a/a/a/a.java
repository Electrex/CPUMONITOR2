package a.a.a;

import android.os.Looper;

public final class a
{
  private static boolean a = false;
  private static int b = 65535;
  private static a c = null;
  private static boolean d = true;
  
  private static void a(int paramInt, String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder;
    if ((a) && ((paramInt & b) == paramInt) && (c == null))
    {
      localStringBuilder = new StringBuilder("[libsuperuser][").append(paramString1).append("]");
      if ((paramString2.startsWith("[")) || (paramString2.startsWith(" "))) {
        break label74;
      }
    }
    label74:
    for (String str = " ";; str = "")
    {
      localStringBuilder.append(str).append(paramString2);
      return;
    }
  }
  
  public static void a(String paramString)
  {
    a(1, "G", paramString);
  }
  
  public static boolean a()
  {
    return (a) && (d);
  }
  
  public static void b(String paramString)
  {
    a(2, "C", paramString);
  }
  
  public static boolean b()
  {
    return (Looper.myLooper() != null) && (Looper.myLooper() == Looper.getMainLooper());
  }
  
  public static void c(String paramString)
  {
    a(4, "O", paramString);
  }
  
  public static abstract interface a {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/a/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */