package a.a.a;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public final class d
  extends Thread
{
  private String a = null;
  private BufferedReader b = null;
  private List<String> c = null;
  private a d = null;
  
  public d(String paramString, InputStream paramInputStream, List<String> paramList)
  {
    this.a = paramString;
    this.b = new BufferedReader(new InputStreamReader(paramInputStream));
    this.c = paramList;
  }
  
  public final void run()
  {
    try
    {
      for (;;)
      {
        String str = this.b.readLine();
        if (str == null) {
          break;
        }
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = this.a;
        arrayOfObject[1] = str;
        a.c(String.format("[%s] %s", arrayOfObject));
        if (this.c != null) {
          this.c.add(str);
        }
        a locala = this.d;
        if (locala == null) {}
      }
      return;
    }
    catch (IOException localIOException1)
    {
      try
      {
        this.b.close();
        return;
      }
      catch (IOException localIOException2) {}
    }
  }
  
  public static abstract interface a {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/a/a/a/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */