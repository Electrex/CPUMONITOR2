package org.apache.commons.io;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Stack;

public class FilenameUtils
{
  public static final char EXTENSION_SEPARATOR = '.';
  public static final String EXTENSION_SEPARATOR_STR = Character.toString('.');
  private static final char OTHER_SEPARATOR = '\\';
  private static final char SYSTEM_SEPARATOR = File.separatorChar;
  private static final char UNIX_SEPARATOR = '/';
  private static final char WINDOWS_SEPARATOR = '\\';
  
  static
  {
    if (isSystemWindows())
    {
      OTHER_SEPARATOR = '/';
      return;
    }
  }
  
  public static String concat(String paramString1, String paramString2)
  {
    int i = getPrefixLength(paramString2);
    if (i < 0) {}
    do
    {
      return null;
      if (i > 0) {
        return normalize(paramString2);
      }
    } while (paramString1 == null);
    int j = paramString1.length();
    if (j == 0) {
      return normalize(paramString2);
    }
    if (isSeparator(paramString1.charAt(j - 1))) {
      return normalize(paramString1 + paramString2);
    }
    return normalize(paramString1 + '/' + paramString2);
  }
  
  public static boolean directoryContains(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      throw new IllegalArgumentException("Directory must not be null");
    }
    if (paramString2 == null) {}
    while (IOCase.SYSTEM.checkEquals(paramString1, paramString2)) {
      return false;
    }
    return IOCase.SYSTEM.checkStartsWith(paramString2, paramString1);
  }
  
  private static String doGetFullPath(String paramString, boolean paramBoolean)
  {
    if (paramString == null) {
      paramString = null;
    }
    int i;
    do
    {
      return paramString;
      i = getPrefixLength(paramString);
      if (i < 0) {
        return null;
      }
      if (i < paramString.length()) {
        break;
      }
    } while (!paramBoolean);
    return getPrefix(paramString);
    int j = indexOfLastSeparator(paramString);
    if (j < 0) {
      return paramString.substring(0, i);
    }
    if (paramBoolean) {}
    for (int k = 1;; k = 0)
    {
      int m = k + j;
      if (m == 0) {
        m++;
      }
      return paramString.substring(0, m);
    }
  }
  
  private static String doGetPath(String paramString, int paramInt)
  {
    if (paramString == null) {}
    int i;
    do
    {
      return null;
      i = getPrefixLength(paramString);
    } while (i < 0);
    int j = indexOfLastSeparator(paramString);
    int k = j + paramInt;
    if ((i >= paramString.length()) || (j < 0) || (i >= k)) {
      return "";
    }
    return paramString.substring(i, k);
  }
  
  private static String doNormalize(String paramString, char paramChar, boolean paramBoolean)
  {
    if (paramString == null) {
      paramString = null;
    }
    int i;
    do
    {
      return paramString;
      i = paramString.length();
    } while (i == 0);
    int j = getPrefixLength(paramString);
    if (j < 0) {
      return null;
    }
    char[] arrayOfChar = new char[i + 2];
    paramString.getChars(0, paramString.length(), arrayOfChar, 0);
    if (paramChar == SYSTEM_SEPARATOR) {}
    for (int k = OTHER_SEPARATOR;; k = SYSTEM_SEPARATOR) {
      for (int m = 0; m < arrayOfChar.length; m++) {
        if (arrayOfChar[m] == k) {
          arrayOfChar[m] = paramChar;
        }
      }
    }
    int i1;
    int n;
    if (arrayOfChar[(i - 1)] != paramChar)
    {
      int i6 = i + 1;
      arrayOfChar[i] = paramChar;
      i1 = i6;
      n = 0;
    }
    for (;;)
    {
      for (int i2 = j + 1; i2 < i1; i2++) {
        if ((arrayOfChar[i2] == paramChar) && (arrayOfChar[(i2 - 1)] == paramChar))
        {
          System.arraycopy(arrayOfChar, i2, arrayOfChar, i2 - 1, i1 - i2);
          i1--;
          i2--;
        }
      }
      for (int i3 = j + 1; i3 < i1; i3++) {
        if ((arrayOfChar[i3] == paramChar) && (arrayOfChar[(i3 - 1)] == '.') && ((i3 == j + 1) || (arrayOfChar[(i3 - 2)] == paramChar)))
        {
          if (i3 == i1 - 1) {
            n = 1;
          }
          System.arraycopy(arrayOfChar, i3 + 1, arrayOfChar, i3 - 1, i1 - i3);
          i1 -= 2;
          i3--;
        }
      }
      int i4 = j + 2;
      if (i4 < i1)
      {
        int i5;
        if ((arrayOfChar[i4] == paramChar) && (arrayOfChar[(i4 - 1)] == '.') && (arrayOfChar[(i4 - 2)] == '.') && ((i4 == j + 2) || (arrayOfChar[(i4 - 3)] == paramChar)))
        {
          if (i4 == j + 2) {
            return null;
          }
          if (i4 == i1 - 1) {
            n = 1;
          }
          i5 = i4 - 4;
          label385:
          if (i5 < j) {
            break label449;
          }
          if (arrayOfChar[i5] != paramChar) {
            break label443;
          }
          System.arraycopy(arrayOfChar, i4 + 1, arrayOfChar, i5 + 1, i1 - i4);
          i1 -= i4 - i5;
        }
        for (i4 = i5 + 1;; i4 = j + 1)
        {
          i4++;
          break;
          label443:
          i5--;
          break label385;
          label449:
          System.arraycopy(arrayOfChar, i4 + 1, arrayOfChar, j, i1 - i4);
          i1 -= i4 + 1 - j;
        }
      }
      if (i1 <= 0) {
        return "";
      }
      if (i1 <= j) {
        return new String(arrayOfChar, 0, i1);
      }
      if ((n != 0) && (paramBoolean)) {
        return new String(arrayOfChar, 0, i1);
      }
      return new String(arrayOfChar, 0, i1 - 1);
      n = 1;
      i1 = i;
    }
  }
  
  public static boolean equals(String paramString1, String paramString2)
  {
    return equals(paramString1, paramString2, false, IOCase.SENSITIVE);
  }
  
  public static boolean equals(String paramString1, String paramString2, boolean paramBoolean, IOCase paramIOCase)
  {
    if ((paramString1 == null) || (paramString2 == null)) {
      return (paramString1 == null) && (paramString2 == null);
    }
    if (paramBoolean)
    {
      paramString1 = normalize(paramString1);
      paramString2 = normalize(paramString2);
      if ((paramString1 == null) || (paramString2 == null)) {
        throw new NullPointerException("Error normalizing one or both of the file names");
      }
    }
    if (paramIOCase == null) {
      paramIOCase = IOCase.SENSITIVE;
    }
    return paramIOCase.checkEquals(paramString1, paramString2);
  }
  
  public static boolean equalsNormalized(String paramString1, String paramString2)
  {
    return equals(paramString1, paramString2, true, IOCase.SENSITIVE);
  }
  
  public static boolean equalsNormalizedOnSystem(String paramString1, String paramString2)
  {
    return equals(paramString1, paramString2, true, IOCase.SYSTEM);
  }
  
  public static boolean equalsOnSystem(String paramString1, String paramString2)
  {
    return equals(paramString1, paramString2, false, IOCase.SYSTEM);
  }
  
  public static String getBaseName(String paramString)
  {
    return removeExtension(getName(paramString));
  }
  
  public static String getExtension(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    int i = indexOfExtension(paramString);
    if (i == -1) {
      return "";
    }
    return paramString.substring(i + 1);
  }
  
  public static String getFullPath(String paramString)
  {
    return doGetFullPath(paramString, true);
  }
  
  public static String getFullPathNoEndSeparator(String paramString)
  {
    return doGetFullPath(paramString, false);
  }
  
  public static String getName(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return paramString.substring(1 + indexOfLastSeparator(paramString));
  }
  
  public static String getPath(String paramString)
  {
    return doGetPath(paramString, 1);
  }
  
  public static String getPathNoEndSeparator(String paramString)
  {
    return doGetPath(paramString, 0);
  }
  
  public static String getPrefix(String paramString)
  {
    if (paramString == null) {}
    int i;
    do
    {
      return null;
      i = getPrefixLength(paramString);
    } while (i < 0);
    if (i > paramString.length()) {
      return paramString + '/';
    }
    return paramString.substring(0, i);
  }
  
  public static int getPrefixLength(String paramString)
  {
    int j;
    if (paramString == null) {
      j = -1;
    }
    boolean bool1;
    do
    {
      int i;
      char c1;
      boolean bool2;
      do
      {
        do
        {
          return j;
          i = paramString.length();
          j = 0;
        } while (i == 0);
        c1 = paramString.charAt(0);
        if (c1 == ':') {
          return -1;
        }
        if (i != 1) {
          break;
        }
        if (c1 == '~') {
          return 2;
        }
        bool2 = isSeparator(c1);
        j = 0;
      } while (!bool2);
      return 1;
      if (c1 == '~')
      {
        int i2 = paramString.indexOf('/', 1);
        int i3 = paramString.indexOf('\\', 1);
        if ((i2 == -1) && (i3 == -1)) {
          return i + 1;
        }
        if (i2 == -1) {
          i2 = i3;
        }
        if (i3 == -1) {
          i3 = i2;
        }
        return 1 + Math.min(i2, i3);
      }
      char c2 = paramString.charAt(1);
      if (c2 == ':')
      {
        int i1 = Character.toUpperCase(c1);
        if ((i1 >= 65) && (i1 <= 90))
        {
          if ((i == 2) || (!isSeparator(paramString.charAt(2)))) {
            return 2;
          }
          return 3;
        }
        return -1;
      }
      if ((isSeparator(c1)) && (isSeparator(c2)))
      {
        int k = paramString.indexOf('/', 2);
        int m = paramString.indexOf('\\', 2);
        if (((k == -1) && (m == -1)) || (k == 2) || (m == 2)) {
          return -1;
        }
        if (k == -1) {}
        for (int n = m;; n = k)
        {
          if (m == -1) {
            m = n;
          }
          return 1 + Math.min(n, m);
        }
      }
      bool1 = isSeparator(c1);
      j = 0;
    } while (!bool1);
    return 1;
  }
  
  public static int indexOfExtension(String paramString)
  {
    if (paramString == null) {}
    int i;
    do
    {
      return -1;
      i = paramString.lastIndexOf('.');
    } while (indexOfLastSeparator(paramString) > i);
    return i;
  }
  
  public static int indexOfLastSeparator(String paramString)
  {
    if (paramString == null) {
      return -1;
    }
    return Math.max(paramString.lastIndexOf('/'), paramString.lastIndexOf('\\'));
  }
  
  public static boolean isExtension(String paramString1, String paramString2)
  {
    if (paramString1 == null) {}
    do
    {
      return false;
      if ((paramString2 != null) && (paramString2.length() != 0)) {
        break;
      }
    } while (indexOfExtension(paramString1) != -1);
    return true;
    return getExtension(paramString1).equals(paramString2);
  }
  
  public static boolean isExtension(String paramString, Collection<String> paramCollection)
  {
    if (paramString == null) {
      return false;
    }
    if ((paramCollection == null) || (paramCollection.isEmpty())) {
      return indexOfExtension(paramString) == -1;
    }
    String str = getExtension(paramString);
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext()) {
      if (str.equals((String)localIterator.next())) {
        return true;
      }
    }
    return false;
  }
  
  public static boolean isExtension(String paramString, String[] paramArrayOfString)
  {
    if (paramString == null) {}
    for (;;)
    {
      return false;
      if ((paramArrayOfString == null) || (paramArrayOfString.length == 0))
      {
        if (indexOfExtension(paramString) == -1) {
          return true;
        }
      }
      else
      {
        String str = getExtension(paramString);
        int i = paramArrayOfString.length;
        for (int j = 0; j < i; j++) {
          if (str.equals(paramArrayOfString[j])) {
            return true;
          }
        }
      }
    }
  }
  
  private static boolean isSeparator(char paramChar)
  {
    return (paramChar == '/') || (paramChar == '\\');
  }
  
  static boolean isSystemWindows()
  {
    return SYSTEM_SEPARATOR == '\\';
  }
  
  public static String normalize(String paramString)
  {
    return doNormalize(paramString, SYSTEM_SEPARATOR, true);
  }
  
  public static String normalize(String paramString, boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (char c = '/';; c = '\\') {
      return doNormalize(paramString, c, true);
    }
  }
  
  public static String normalizeNoEndSeparator(String paramString)
  {
    return doNormalize(paramString, SYSTEM_SEPARATOR, false);
  }
  
  public static String normalizeNoEndSeparator(String paramString, boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (char c = '/';; c = '\\') {
      return doNormalize(paramString, c, false);
    }
  }
  
  public static String removeExtension(String paramString)
  {
    if (paramString == null) {
      paramString = null;
    }
    int i;
    do
    {
      return paramString;
      i = indexOfExtension(paramString);
    } while (i == -1);
    return paramString.substring(0, i);
  }
  
  public static String separatorsToSystem(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    if (isSystemWindows()) {
      return separatorsToWindows(paramString);
    }
    return separatorsToUnix(paramString);
  }
  
  public static String separatorsToUnix(String paramString)
  {
    if ((paramString == null) || (paramString.indexOf('\\') == -1)) {
      return paramString;
    }
    return paramString.replace('\\', '/');
  }
  
  public static String separatorsToWindows(String paramString)
  {
    if ((paramString == null) || (paramString.indexOf('/') == -1)) {
      return paramString;
    }
    return paramString.replace('/', '\\');
  }
  
  static String[] splitOnTokens(String paramString)
  {
    if ((paramString.indexOf('?') == -1) && (paramString.indexOf('*') == -1)) {
      return new String[] { paramString };
    }
    char[] arrayOfChar = paramString.toCharArray();
    ArrayList localArrayList = new ArrayList();
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    if (i < arrayOfChar.length)
    {
      if ((arrayOfChar[i] == '?') || (arrayOfChar[i] == '*'))
      {
        if (localStringBuilder.length() != 0)
        {
          localArrayList.add(localStringBuilder.toString());
          localStringBuilder.setLength(0);
        }
        if (arrayOfChar[i] == '?') {
          localArrayList.add("?");
        }
      }
      for (;;)
      {
        i++;
        break;
        if ((localArrayList.isEmpty()) || ((i > 0) && (!((String)localArrayList.get(-1 + localArrayList.size())).equals("*"))))
        {
          localArrayList.add("*");
          continue;
          localStringBuilder.append(arrayOfChar[i]);
        }
      }
    }
    if (localStringBuilder.length() != 0) {
      localArrayList.add(localStringBuilder.toString());
    }
    return (String[])localArrayList.toArray(new String[localArrayList.size()]);
  }
  
  public static boolean wildcardMatch(String paramString1, String paramString2)
  {
    return wildcardMatch(paramString1, paramString2, IOCase.SENSITIVE);
  }
  
  public static boolean wildcardMatch(String paramString1, String paramString2, IOCase paramIOCase)
  {
    if ((paramString1 == null) && (paramString2 == null)) {
      return true;
    }
    if ((paramString1 == null) || (paramString2 == null)) {
      return false;
    }
    if (paramIOCase == null) {
      paramIOCase = IOCase.SENSITIVE;
    }
    String[] arrayOfString = splitOnTokens(paramString2);
    Stack localStack = new Stack();
    int i = 0;
    int j = 0;
    int k = 0;
    label51:
    if (localStack.size() > 0)
    {
      int[] arrayOfInt = (int[])localStack.pop();
      int n = arrayOfInt[0];
      int i1 = arrayOfInt[1];
      k = 1;
      i = n;
      j = i1;
    }
    label92:
    if (i < arrayOfString.length) {
      if (arrayOfString[i].equals("?"))
      {
        j++;
        if (j > paramString1.length()) {
          break label261;
        }
        k = 0;
      }
    }
    for (;;)
    {
      i++;
      break label92;
      if (arrayOfString[i].equals("*"))
      {
        if (i == -1 + arrayOfString.length)
        {
          j = paramString1.length();
          k = 1;
        }
      }
      else
      {
        if (k != 0)
        {
          j = paramIOCase.checkIndexOf(paramString1, j, arrayOfString[i]);
          if (j == -1) {
            break label261;
          }
          m = paramIOCase.checkIndexOf(paramString1, j + 1, arrayOfString[i]);
          if (m >= 0) {
            localStack.push(new int[] { i, m });
          }
        }
        while (paramIOCase.checkRegionMatches(paramString1, j, arrayOfString[i]))
        {
          int m;
          j += arrayOfString[i].length();
          k = 0;
          break;
        }
        label261:
        if ((i == arrayOfString.length) && (j == paramString1.length())) {
          break;
        }
        if (localStack.size() > 0) {
          break label51;
        }
        return false;
      }
      k = 1;
    }
  }
  
  public static boolean wildcardMatchOnSystem(String paramString1, String paramString2)
  {
    return wildcardMatch(paramString1, paramString2, IOCase.SYSTEM);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/FilenameUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */