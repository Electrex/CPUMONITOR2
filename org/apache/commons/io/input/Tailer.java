package org.apache.commons.io.input;

import java.io.File;
import java.io.RandomAccessFile;

public class Tailer
  implements Runnable
{
  private static final int DEFAULT_BUFSIZE = 4096;
  private static final int DEFAULT_DELAY_MILLIS = 1000;
  private static final String RAF_MODE = "r";
  private final long delayMillis;
  private final boolean end;
  private final File file;
  private final byte[] inbuf;
  private final TailerListener listener;
  private final boolean reOpen;
  private volatile boolean run = true;
  
  public Tailer(File paramFile, TailerListener paramTailerListener)
  {
    this(paramFile, paramTailerListener, 1000L);
  }
  
  public Tailer(File paramFile, TailerListener paramTailerListener, long paramLong)
  {
    this(paramFile, paramTailerListener, paramLong, false);
  }
  
  public Tailer(File paramFile, TailerListener paramTailerListener, long paramLong, boolean paramBoolean)
  {
    this(paramFile, paramTailerListener, paramLong, paramBoolean, 4096);
  }
  
  public Tailer(File paramFile, TailerListener paramTailerListener, long paramLong, boolean paramBoolean, int paramInt)
  {
    this(paramFile, paramTailerListener, paramLong, paramBoolean, false, paramInt);
  }
  
  public Tailer(File paramFile, TailerListener paramTailerListener, long paramLong, boolean paramBoolean1, boolean paramBoolean2)
  {
    this(paramFile, paramTailerListener, paramLong, paramBoolean1, paramBoolean2, 4096);
  }
  
  public Tailer(File paramFile, TailerListener paramTailerListener, long paramLong, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    this.file = paramFile;
    this.delayMillis = paramLong;
    this.end = paramBoolean1;
    this.inbuf = new byte[paramInt];
    this.listener = paramTailerListener;
    paramTailerListener.init(this);
    this.reOpen = paramBoolean2;
  }
  
  public static Tailer create(File paramFile, TailerListener paramTailerListener)
  {
    return create(paramFile, paramTailerListener, 1000L, false);
  }
  
  public static Tailer create(File paramFile, TailerListener paramTailerListener, long paramLong)
  {
    return create(paramFile, paramTailerListener, paramLong, false);
  }
  
  public static Tailer create(File paramFile, TailerListener paramTailerListener, long paramLong, boolean paramBoolean)
  {
    return create(paramFile, paramTailerListener, paramLong, paramBoolean, 4096);
  }
  
  public static Tailer create(File paramFile, TailerListener paramTailerListener, long paramLong, boolean paramBoolean, int paramInt)
  {
    Tailer localTailer = new Tailer(paramFile, paramTailerListener, paramLong, paramBoolean, paramInt);
    Thread localThread = new Thread(localTailer);
    localThread.setDaemon(true);
    localThread.start();
    return localTailer;
  }
  
  public static Tailer create(File paramFile, TailerListener paramTailerListener, long paramLong, boolean paramBoolean1, boolean paramBoolean2)
  {
    return create(paramFile, paramTailerListener, paramLong, paramBoolean1, paramBoolean2, 4096);
  }
  
  public static Tailer create(File paramFile, TailerListener paramTailerListener, long paramLong, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    Tailer localTailer = new Tailer(paramFile, paramTailerListener, paramLong, paramBoolean1, paramBoolean2, paramInt);
    Thread localThread = new Thread(localTailer);
    localThread.setDaemon(true);
    localThread.start();
    return localTailer;
  }
  
  private long readLines(RandomAccessFile paramRandomAccessFile)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    long l1 = paramRandomAccessFile.getFilePointer();
    int i = 0;
    for (long l2 = l1; this.run; l2 = paramRandomAccessFile.getFilePointer())
    {
      int j = paramRandomAccessFile.read(this.inbuf);
      if (j == -1) {
        break;
      }
      int k = 0;
      if (k < j)
      {
        int m = this.inbuf[k];
        switch (m)
        {
        case 11: 
        case 12: 
        default: 
          if (i != 0)
          {
            this.listener.handle(localStringBuilder.toString());
            localStringBuilder.setLength(0);
            l1 = 1L + (l2 + k);
            i = 0;
          }
          localStringBuilder.append((char)m);
        }
        for (;;)
        {
          k++;
          break;
          this.listener.handle(localStringBuilder.toString());
          localStringBuilder.setLength(0);
          l1 = 1L + (l2 + k);
          i = 0;
          continue;
          if (i != 0) {
            localStringBuilder.append('\r');
          }
          i = 1;
        }
      }
    }
    paramRandomAccessFile.seek(l1);
    return l1;
  }
  
  public long getDelay()
  {
    return this.delayMillis;
  }
  
  public File getFile()
  {
    return this.file;
  }
  
  /* Error */
  public void run()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: lconst_0
    //   3: lstore_2
    //   4: lconst_0
    //   5: lstore 4
    //   7: aload_0
    //   8: getfield 49	org/apache/commons/io/input/Tailer:run	Z
    //   11: istore 8
    //   13: iload 8
    //   15: ifeq +118 -> 133
    //   18: aload_1
    //   19: ifnonnull +114 -> 133
    //   22: new 99	java/io/RandomAccessFile
    //   25: dup
    //   26: aload_0
    //   27: getfield 51	org/apache/commons/io/input/Tailer:file	Ljava/io/File;
    //   30: ldc 15
    //   32: invokespecial 139	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   35: astore 30
    //   37: aload 30
    //   39: astore_1
    //   40: aload_1
    //   41: ifnonnull +50 -> 91
    //   44: aload_0
    //   45: getfield 53	org/apache/commons/io/input/Tailer:delayMillis	J
    //   48: invokestatic 142	java/lang/Thread:sleep	(J)V
    //   51: goto -44 -> 7
    //   54: astore 31
    //   56: goto -49 -> 7
    //   59: astore 32
    //   61: aload_0
    //   62: getfield 59	org/apache/commons/io/input/Tailer:listener	Lorg/apache/commons/io/input/TailerListener;
    //   65: invokeinterface 145 1 0
    //   70: goto -30 -> 40
    //   73: astore 7
    //   75: aload_0
    //   76: getfield 59	org/apache/commons/io/input/Tailer:listener	Lorg/apache/commons/io/input/TailerListener;
    //   79: aload 7
    //   81: invokeinterface 148 2 0
    //   86: aload_1
    //   87: invokestatic 154	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   90: return
    //   91: aload_0
    //   92: getfield 55	org/apache/commons/io/input/Tailer:end	Z
    //   95: ifeq +33 -> 128
    //   98: aload_0
    //   99: getfield 51	org/apache/commons/io/input/Tailer:file	Ljava/io/File;
    //   102: invokevirtual 159	java/io/File:length	()J
    //   105: lstore_2
    //   106: invokestatic 164	java/lang/System:currentTimeMillis	()J
    //   109: lstore 4
    //   111: aload_1
    //   112: lload_2
    //   113: invokevirtual 127	java/io/RandomAccessFile:seek	(J)V
    //   116: goto -109 -> 7
    //   119: astore 6
    //   121: aload_1
    //   122: invokestatic 154	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   125: aload 6
    //   127: athrow
    //   128: lconst_0
    //   129: lstore_2
    //   130: goto -24 -> 106
    //   133: aload_0
    //   134: getfield 49	org/apache/commons/io/input/Tailer:run	Z
    //   137: ifeq +205 -> 342
    //   140: aload_0
    //   141: getfield 51	org/apache/commons/io/input/Tailer:file	Ljava/io/File;
    //   144: lload 4
    //   146: invokestatic 170	org/apache/commons/io/FileUtils:isFileNewer	(Ljava/io/File;J)Z
    //   149: istore 9
    //   151: aload_0
    //   152: getfield 51	org/apache/commons/io/input/Tailer:file	Ljava/io/File;
    //   155: invokevirtual 159	java/io/File:length	()J
    //   158: lstore 10
    //   160: lload 10
    //   162: lload_2
    //   163: lcmp
    //   164: ifge +73 -> 237
    //   167: aload_0
    //   168: getfield 59	org/apache/commons/io/input/Tailer:listener	Lorg/apache/commons/io/input/TailerListener;
    //   171: invokeinterface 173 1 0
    //   176: new 99	java/io/RandomAccessFile
    //   179: dup
    //   180: aload_0
    //   181: getfield 51	org/apache/commons/io/input/Tailer:file	Ljava/io/File;
    //   184: ldc 15
    //   186: invokespecial 139	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   189: astore 12
    //   191: aload_1
    //   192: invokestatic 154	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   195: lconst_0
    //   196: lstore_2
    //   197: aload 12
    //   199: astore_1
    //   200: goto -67 -> 133
    //   203: astore 21
    //   205: lload_2
    //   206: lstore 22
    //   208: aload_1
    //   209: astore 14
    //   211: lload 22
    //   213: lstore 15
    //   215: aload_0
    //   216: getfield 59	org/apache/commons/io/input/Tailer:listener	Lorg/apache/commons/io/input/TailerListener;
    //   219: invokeinterface 145 1 0
    //   224: lload 15
    //   226: lstore 17
    //   228: aload 14
    //   230: astore_1
    //   231: lload 17
    //   233: lstore_2
    //   234: goto -101 -> 133
    //   237: lload 10
    //   239: lload_2
    //   240: lcmp
    //   241: ifle +73 -> 314
    //   244: aload_0
    //   245: aload_1
    //   246: invokespecial 175	org/apache/commons/io/input/Tailer:readLines	(Ljava/io/RandomAccessFile;)J
    //   249: lstore_2
    //   250: invokestatic 164	java/lang/System:currentTimeMillis	()J
    //   253: lstore 4
    //   255: aload_0
    //   256: getfield 67	org/apache/commons/io/input/Tailer:reOpen	Z
    //   259: ifeq +7 -> 266
    //   262: aload_1
    //   263: invokestatic 154	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   266: aload_0
    //   267: getfield 53	org/apache/commons/io/input/Tailer:delayMillis	J
    //   270: invokestatic 142	java/lang/Thread:sleep	(J)V
    //   273: aload_0
    //   274: getfield 49	org/apache/commons/io/input/Tailer:run	Z
    //   277: ifeq +151 -> 428
    //   280: aload_0
    //   281: getfield 67	org/apache/commons/io/input/Tailer:reOpen	Z
    //   284: ifeq +144 -> 428
    //   287: new 99	java/io/RandomAccessFile
    //   290: dup
    //   291: aload_0
    //   292: getfield 51	org/apache/commons/io/input/Tailer:file	Ljava/io/File;
    //   295: ldc 15
    //   297: invokespecial 139	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   300: astore 27
    //   302: aload 27
    //   304: lload_2
    //   305: invokevirtual 127	java/io/RandomAccessFile:seek	(J)V
    //   308: aload 27
    //   310: astore_1
    //   311: goto -178 -> 133
    //   314: iload 9
    //   316: ifeq -61 -> 255
    //   319: aload_1
    //   320: lconst_0
    //   321: invokevirtual 127	java/io/RandomAccessFile:seek	(J)V
    //   324: aload_0
    //   325: aload_1
    //   326: invokespecial 175	org/apache/commons/io/input/Tailer:readLines	(Ljava/io/RandomAccessFile;)J
    //   329: lstore_2
    //   330: invokestatic 164	java/lang/System:currentTimeMillis	()J
    //   333: lstore 24
    //   335: lload 24
    //   337: lstore 4
    //   339: goto -84 -> 255
    //   342: aload_1
    //   343: invokestatic 154	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   346: return
    //   347: astore 20
    //   349: aload 12
    //   351: astore_1
    //   352: aload 20
    //   354: astore 6
    //   356: goto -235 -> 121
    //   359: astore 6
    //   361: aload 14
    //   363: astore_1
    //   364: goto -243 -> 121
    //   367: astore 29
    //   369: aload 27
    //   371: astore_1
    //   372: aload 29
    //   374: astore 6
    //   376: goto -255 -> 121
    //   379: astore 19
    //   381: aload 12
    //   383: astore_1
    //   384: aload 19
    //   386: astore 7
    //   388: goto -313 -> 75
    //   391: astore 7
    //   393: aload 14
    //   395: astore_1
    //   396: goto -321 -> 75
    //   399: astore 28
    //   401: aload 27
    //   403: astore_1
    //   404: aload 28
    //   406: astore 7
    //   408: goto -333 -> 75
    //   411: astore 26
    //   413: goto -140 -> 273
    //   416: astore 13
    //   418: aload 12
    //   420: astore 14
    //   422: lconst_0
    //   423: lstore 15
    //   425: goto -210 -> 215
    //   428: aload_1
    //   429: astore 27
    //   431: goto -123 -> 308
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	434	0	this	Tailer
    //   1	428	1	localObject1	Object
    //   3	327	2	l1	long
    //   5	333	4	l2	long
    //   119	7	6	localObject2	Object
    //   354	1	6	localObject3	Object
    //   359	1	6	localObject4	Object
    //   374	1	6	localObject5	Object
    //   73	7	7	localException1	Exception
    //   386	1	7	localObject6	Object
    //   391	1	7	localException2	Exception
    //   406	1	7	localObject7	Object
    //   11	3	8	bool1	boolean
    //   149	166	9	bool2	boolean
    //   158	80	10	l3	long
    //   189	230	12	localRandomAccessFile1	RandomAccessFile
    //   416	1	13	localFileNotFoundException1	java.io.FileNotFoundException
    //   209	212	14	localObject8	Object
    //   213	211	15	l4	long
    //   226	6	17	l5	long
    //   379	6	19	localException3	Exception
    //   347	6	20	localObject9	Object
    //   203	1	21	localFileNotFoundException2	java.io.FileNotFoundException
    //   206	6	22	l6	long
    //   333	3	24	l7	long
    //   411	1	26	localInterruptedException1	InterruptedException
    //   300	130	27	localObject10	Object
    //   399	6	28	localException4	Exception
    //   367	6	29	localObject11	Object
    //   35	3	30	localRandomAccessFile2	RandomAccessFile
    //   54	1	31	localInterruptedException2	InterruptedException
    //   59	1	32	localFileNotFoundException3	java.io.FileNotFoundException
    // Exception table:
    //   from	to	target	type
    //   44	51	54	java/lang/InterruptedException
    //   22	37	59	java/io/FileNotFoundException
    //   7	13	73	java/lang/Exception
    //   22	37	73	java/lang/Exception
    //   44	51	73	java/lang/Exception
    //   61	70	73	java/lang/Exception
    //   91	106	73	java/lang/Exception
    //   106	116	73	java/lang/Exception
    //   133	160	73	java/lang/Exception
    //   167	176	73	java/lang/Exception
    //   176	191	73	java/lang/Exception
    //   244	255	73	java/lang/Exception
    //   255	266	73	java/lang/Exception
    //   266	273	73	java/lang/Exception
    //   273	302	73	java/lang/Exception
    //   319	335	73	java/lang/Exception
    //   7	13	119	finally
    //   22	37	119	finally
    //   44	51	119	finally
    //   61	70	119	finally
    //   75	86	119	finally
    //   91	106	119	finally
    //   106	116	119	finally
    //   133	160	119	finally
    //   167	176	119	finally
    //   176	191	119	finally
    //   244	255	119	finally
    //   255	266	119	finally
    //   266	273	119	finally
    //   273	302	119	finally
    //   319	335	119	finally
    //   176	191	203	java/io/FileNotFoundException
    //   191	195	347	finally
    //   215	224	359	finally
    //   302	308	367	finally
    //   191	195	379	java/lang/Exception
    //   215	224	391	java/lang/Exception
    //   302	308	399	java/lang/Exception
    //   266	273	411	java/lang/InterruptedException
    //   191	195	416	java/io/FileNotFoundException
  }
  
  public void stop()
  {
    this.run = false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/input/Tailer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */