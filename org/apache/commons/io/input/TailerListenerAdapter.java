package org.apache.commons.io.input;

public class TailerListenerAdapter
  implements TailerListener
{
  public void fileNotFound() {}
  
  public void fileRotated() {}
  
  public void handle(Exception paramException) {}
  
  public void handle(String paramString) {}
  
  public void init(Tailer paramTailer) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/input/TailerListenerAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */