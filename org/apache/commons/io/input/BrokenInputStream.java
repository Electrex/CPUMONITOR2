package org.apache.commons.io.input;

import java.io.IOException;
import java.io.InputStream;

public class BrokenInputStream
  extends InputStream
{
  private final IOException exception;
  
  public BrokenInputStream()
  {
    this(new IOException("Broken input stream"));
  }
  
  public BrokenInputStream(IOException paramIOException)
  {
    this.exception = paramIOException;
  }
  
  public int available()
  {
    throw this.exception;
  }
  
  public void close()
  {
    throw this.exception;
  }
  
  public int read()
  {
    throw this.exception;
  }
  
  public void reset()
  {
    throw this.exception;
  }
  
  public long skip(long paramLong)
  {
    throw this.exception;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/input/BrokenInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */