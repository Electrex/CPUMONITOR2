package org.apache.commons.io.input;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;

public class CharSequenceInputStream
  extends InputStream
{
  private final ByteBuffer bbuf;
  private final CharBuffer cbuf;
  private final CharsetEncoder encoder;
  private int mark;
  
  public CharSequenceInputStream(CharSequence paramCharSequence, String paramString)
  {
    this(paramCharSequence, paramString, 2048);
  }
  
  public CharSequenceInputStream(CharSequence paramCharSequence, String paramString, int paramInt)
  {
    this(paramCharSequence, Charset.forName(paramString), paramInt);
  }
  
  public CharSequenceInputStream(CharSequence paramCharSequence, Charset paramCharset)
  {
    this(paramCharSequence, paramCharset, 2048);
  }
  
  public CharSequenceInputStream(CharSequence paramCharSequence, Charset paramCharset, int paramInt)
  {
    this.encoder = paramCharset.newEncoder().onMalformedInput(CodingErrorAction.REPLACE).onUnmappableCharacter(CodingErrorAction.REPLACE);
    this.bbuf = ByteBuffer.allocate(paramInt);
    this.bbuf.flip();
    this.cbuf = CharBuffer.wrap(paramCharSequence);
    this.mark = -1;
  }
  
  private void fillBuffer()
  {
    this.bbuf.compact();
    CoderResult localCoderResult = this.encoder.encode(this.cbuf, this.bbuf, true);
    if (localCoderResult.isError()) {
      localCoderResult.throwException();
    }
    this.bbuf.flip();
  }
  
  public int available()
  {
    return this.cbuf.remaining();
  }
  
  public void close() {}
  
  public void mark(int paramInt)
  {
    try
    {
      this.mark = this.cbuf.position();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public boolean markSupported()
  {
    return true;
  }
  
  public int read()
  {
    do
    {
      if (this.bbuf.hasRemaining()) {
        return 0xFF & this.bbuf.get();
      }
      fillBuffer();
    } while ((this.bbuf.hasRemaining()) || (this.cbuf.hasRemaining()));
    return -1;
  }
  
  public int read(byte[] paramArrayOfByte)
  {
    return read(paramArrayOfByte, 0, paramArrayOfByte.length);
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramArrayOfByte == null) {
      throw new NullPointerException("Byte array is null");
    }
    if ((paramInt2 < 0) || (paramInt1 + paramInt2 > paramArrayOfByte.length)) {
      throw new IndexOutOfBoundsException("Array Size=" + paramArrayOfByte.length + ", offset=" + paramInt1 + ", length=" + paramInt2);
    }
    int i = 0;
    if (paramInt2 == 0) {}
    label198:
    do
    {
      return i;
      boolean bool1 = this.bbuf.hasRemaining();
      i = 0;
      if (!bool1)
      {
        boolean bool2 = this.cbuf.hasRemaining();
        i = 0;
        if (!bool2) {
          return -1;
        }
      }
      do
      {
        for (;;)
        {
          if (paramInt2 <= 0) {
            break label198;
          }
          if (!this.bbuf.hasRemaining()) {
            break;
          }
          int j = Math.min(this.bbuf.remaining(), paramInt2);
          this.bbuf.get(paramArrayOfByte, paramInt1, j);
          paramInt1 += j;
          paramInt2 -= j;
          i += j;
        }
        fillBuffer();
      } while ((this.bbuf.hasRemaining()) || (this.cbuf.hasRemaining()));
    } while ((i != 0) || (this.cbuf.hasRemaining()));
    return -1;
  }
  
  public void reset()
  {
    try
    {
      if (this.mark != -1)
      {
        this.cbuf.position(this.mark);
        this.mark = -1;
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public long skip(long paramLong)
  {
    for (int i = 0; (paramLong > 0L) && (this.cbuf.hasRemaining()); i++)
    {
      this.cbuf.get();
      paramLong -= 1L;
    }
    return i;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/input/CharSequenceInputStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */