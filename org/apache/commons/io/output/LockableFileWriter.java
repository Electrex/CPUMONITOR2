package org.apache.commons.io.output;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

public class LockableFileWriter
  extends Writer
{
  private static final String LCK = ".lck";
  private final File lockFile;
  private final Writer out;
  
  public LockableFileWriter(File paramFile)
  {
    this(paramFile, false, null);
  }
  
  public LockableFileWriter(File paramFile, String paramString)
  {
    this(paramFile, paramString, false, null);
  }
  
  public LockableFileWriter(File paramFile, String paramString1, boolean paramBoolean, String paramString2)
  {
    this(paramFile, Charsets.toCharset(paramString1), paramBoolean, paramString2);
  }
  
  public LockableFileWriter(File paramFile, Charset paramCharset)
  {
    this(paramFile, paramCharset, false, null);
  }
  
  public LockableFileWriter(File paramFile, Charset paramCharset, boolean paramBoolean, String paramString)
  {
    File localFile1 = paramFile.getAbsoluteFile();
    if (localFile1.getParentFile() != null) {
      FileUtils.forceMkdir(localFile1.getParentFile());
    }
    if (localFile1.isDirectory()) {
      throw new IOException("File specified is a directory");
    }
    if (paramString == null) {
      paramString = System.getProperty("java.io.tmpdir");
    }
    File localFile2 = new File(paramString);
    FileUtils.forceMkdir(localFile2);
    testLockDir(localFile2);
    this.lockFile = new File(localFile2, localFile1.getName() + ".lck");
    createLock();
    this.out = initWriter(localFile1, paramCharset, paramBoolean);
  }
  
  public LockableFileWriter(File paramFile, boolean paramBoolean)
  {
    this(paramFile, paramBoolean, null);
  }
  
  public LockableFileWriter(File paramFile, boolean paramBoolean, String paramString)
  {
    this(paramFile, Charset.defaultCharset(), paramBoolean, paramString);
  }
  
  public LockableFileWriter(String paramString)
  {
    this(paramString, false, null);
  }
  
  public LockableFileWriter(String paramString, boolean paramBoolean)
  {
    this(paramString, paramBoolean, null);
  }
  
  public LockableFileWriter(String paramString1, boolean paramBoolean, String paramString2)
  {
    this(new File(paramString1), paramBoolean, paramString2);
  }
  
  private void createLock()
  {
    try
    {
      if (!this.lockFile.createNewFile()) {
        throw new IOException("Can't write file, lock " + this.lockFile.getAbsolutePath() + " exists");
      }
    }
    finally {}
    this.lockFile.deleteOnExit();
  }
  
  /* Error */
  private Writer initWriter(File paramFile, Charset paramCharset, boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 128	java/io/File:exists	()Z
    //   4: istore 4
    //   6: new 130	java/io/FileOutputStream
    //   9: dup
    //   10: aload_1
    //   11: invokevirtual 118	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   14: iload_3
    //   15: invokespecial 132	java/io/FileOutputStream:<init>	(Ljava/lang/String;Z)V
    //   18: astore 5
    //   20: new 134	java/io/OutputStreamWriter
    //   23: dup
    //   24: aload 5
    //   26: aload_2
    //   27: invokestatic 137	org/apache/commons/io/Charsets:toCharset	(Ljava/nio/charset/Charset;)Ljava/nio/charset/Charset;
    //   30: invokespecial 140	java/io/OutputStreamWriter:<init>	(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V
    //   33: astore 6
    //   35: aload 6
    //   37: areturn
    //   38: astore 7
    //   40: aconst_null
    //   41: astore 5
    //   43: aconst_null
    //   44: invokestatic 146	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Writer;)V
    //   47: aload 5
    //   49: invokestatic 149	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/OutputStream;)V
    //   52: aload_0
    //   53: getfield 89	org/apache/commons/io/output/LockableFileWriter:lockFile	Ljava/io/File;
    //   56: invokestatic 153	org/apache/commons/io/FileUtils:deleteQuietly	(Ljava/io/File;)Z
    //   59: pop
    //   60: iload 4
    //   62: ifne +8 -> 70
    //   65: aload_1
    //   66: invokestatic 153	org/apache/commons/io/FileUtils:deleteQuietly	(Ljava/io/File;)Z
    //   69: pop
    //   70: aload 7
    //   72: athrow
    //   73: astore 10
    //   75: aconst_null
    //   76: astore 5
    //   78: aconst_null
    //   79: invokestatic 146	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Writer;)V
    //   82: aload 5
    //   84: invokestatic 149	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/OutputStream;)V
    //   87: aload_0
    //   88: getfield 89	org/apache/commons/io/output/LockableFileWriter:lockFile	Ljava/io/File;
    //   91: invokestatic 153	org/apache/commons/io/FileUtils:deleteQuietly	(Ljava/io/File;)Z
    //   94: pop
    //   95: iload 4
    //   97: ifne +8 -> 105
    //   100: aload_1
    //   101: invokestatic 153	org/apache/commons/io/FileUtils:deleteQuietly	(Ljava/io/File;)Z
    //   104: pop
    //   105: aload 10
    //   107: athrow
    //   108: astore 10
    //   110: goto -32 -> 78
    //   113: astore 7
    //   115: goto -72 -> 43
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	118	0	this	LockableFileWriter
    //   0	118	1	paramFile	File
    //   0	118	2	paramCharset	Charset
    //   0	118	3	paramBoolean	boolean
    //   4	92	4	bool	boolean
    //   18	65	5	localFileOutputStream	java.io.FileOutputStream
    //   33	3	6	localOutputStreamWriter	java.io.OutputStreamWriter
    //   38	33	7	localIOException1	IOException
    //   113	1	7	localIOException2	IOException
    //   73	33	10	localRuntimeException1	RuntimeException
    //   108	1	10	localRuntimeException2	RuntimeException
    // Exception table:
    //   from	to	target	type
    //   6	20	38	java/io/IOException
    //   6	20	73	java/lang/RuntimeException
    //   20	35	108	java/lang/RuntimeException
    //   20	35	113	java/io/IOException
  }
  
  private void testLockDir(File paramFile)
  {
    if (!paramFile.exists()) {
      throw new IOException("Could not find lockDir: " + paramFile.getAbsolutePath());
    }
    if (!paramFile.canWrite()) {
      throw new IOException("Could not write to lockDir: " + paramFile.getAbsolutePath());
    }
  }
  
  public void close()
  {
    try
    {
      this.out.close();
      return;
    }
    finally
    {
      this.lockFile.delete();
    }
  }
  
  public void flush()
  {
    this.out.flush();
  }
  
  public void write(int paramInt)
  {
    this.out.write(paramInt);
  }
  
  public void write(String paramString)
  {
    this.out.write(paramString);
  }
  
  public void write(String paramString, int paramInt1, int paramInt2)
  {
    this.out.write(paramString, paramInt1, paramInt2);
  }
  
  public void write(char[] paramArrayOfChar)
  {
    this.out.write(paramArrayOfChar);
  }
  
  public void write(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    this.out.write(paramArrayOfChar, paramInt1, paramInt2);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/output/LockableFileWriter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */