package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;

public class FalseFileFilter
  implements Serializable, IOFileFilter
{
  public static final IOFileFilter FALSE;
  public static final IOFileFilter INSTANCE;
  
  static
  {
    FalseFileFilter localFalseFileFilter = new FalseFileFilter();
    FALSE = localFalseFileFilter;
    INSTANCE = localFalseFileFilter;
  }
  
  public boolean accept(File paramFile)
  {
    return false;
  }
  
  public boolean accept(File paramFile, String paramString)
  {
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/filefilter/FalseFileFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */