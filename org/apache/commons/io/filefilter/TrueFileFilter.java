package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;

public class TrueFileFilter
  implements Serializable, IOFileFilter
{
  public static final IOFileFilter INSTANCE;
  public static final IOFileFilter TRUE;
  
  static
  {
    TrueFileFilter localTrueFileFilter = new TrueFileFilter();
    TRUE = localTrueFileFilter;
    INSTANCE = localTrueFileFilter;
  }
  
  public boolean accept(File paramFile)
  {
    return true;
  }
  
  public boolean accept(File paramFile, String paramString)
  {
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/filefilter/TrueFileFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */