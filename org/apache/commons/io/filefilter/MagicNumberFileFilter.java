package org.apache.commons.io.filefilter;

import java.io.Serializable;

public class MagicNumberFileFilter
  extends AbstractFileFilter
  implements Serializable
{
  private static final long serialVersionUID = -547733176983104172L;
  private final long byteOffset;
  private final byte[] magicNumbers;
  
  public MagicNumberFileFilter(String paramString)
  {
    this(paramString, 0L);
  }
  
  public MagicNumberFileFilter(String paramString, long paramLong)
  {
    if (paramString == null) {
      throw new IllegalArgumentException("The magic number cannot be null");
    }
    if (paramString.length() == 0) {
      throw new IllegalArgumentException("The magic number must contain at least one byte");
    }
    if (paramLong < 0L) {
      throw new IllegalArgumentException("The offset cannot be negative");
    }
    this.magicNumbers = paramString.getBytes();
    this.byteOffset = paramLong;
  }
  
  public MagicNumberFileFilter(byte[] paramArrayOfByte)
  {
    this(paramArrayOfByte, 0L);
  }
  
  public MagicNumberFileFilter(byte[] paramArrayOfByte, long paramLong)
  {
    if (paramArrayOfByte == null) {
      throw new IllegalArgumentException("The magic number cannot be null");
    }
    if (paramArrayOfByte.length == 0) {
      throw new IllegalArgumentException("The magic number must contain at least one byte");
    }
    if (paramLong < 0L) {
      throw new IllegalArgumentException("The offset cannot be negative");
    }
    this.magicNumbers = new byte[paramArrayOfByte.length];
    System.arraycopy(paramArrayOfByte, 0, this.magicNumbers, 0, paramArrayOfByte.length);
    this.byteOffset = paramLong;
  }
  
  /* Error */
  public boolean accept(java.io.File paramFile)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +71 -> 72
    //   4: aload_1
    //   5: invokevirtual 65	java/io/File:isFile	()Z
    //   8: ifeq +64 -> 72
    //   11: aload_1
    //   12: invokevirtual 68	java/io/File:canRead	()Z
    //   15: ifeq +57 -> 72
    //   18: aload_0
    //   19: getfield 43	org/apache/commons/io/filefilter/MagicNumberFileFilter:magicNumbers	[B
    //   22: arraylength
    //   23: newarray <illegal type>
    //   25: astore 5
    //   27: new 70	java/io/RandomAccessFile
    //   30: dup
    //   31: aload_1
    //   32: ldc 72
    //   34: invokespecial 75	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   37: astore_3
    //   38: aload_3
    //   39: aload_0
    //   40: getfield 45	org/apache/commons/io/filefilter/MagicNumberFileFilter:byteOffset	J
    //   43: invokevirtual 79	java/io/RandomAccessFile:seek	(J)V
    //   46: aload_3
    //   47: aload 5
    //   49: invokevirtual 83	java/io/RandomAccessFile:read	([B)I
    //   52: istore 7
    //   54: aload_0
    //   55: getfield 43	org/apache/commons/io/filefilter/MagicNumberFileFilter:magicNumbers	[B
    //   58: arraylength
    //   59: istore 8
    //   61: iload 7
    //   63: iload 8
    //   65: if_icmpeq +9 -> 74
    //   68: aload_3
    //   69: invokestatic 89	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   72: iconst_0
    //   73: ireturn
    //   74: aload_0
    //   75: getfield 43	org/apache/commons/io/filefilter/MagicNumberFileFilter:magicNumbers	[B
    //   78: aload 5
    //   80: invokestatic 95	java/util/Arrays:equals	([B[B)Z
    //   83: istore 9
    //   85: aload_3
    //   86: invokestatic 89	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   89: iload 9
    //   91: ireturn
    //   92: astore 4
    //   94: aconst_null
    //   95: astore_3
    //   96: aload_3
    //   97: invokestatic 89	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   100: iconst_0
    //   101: ireturn
    //   102: astore_2
    //   103: aconst_null
    //   104: astore_3
    //   105: aload_3
    //   106: invokestatic 89	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   109: aload_2
    //   110: athrow
    //   111: astore_2
    //   112: goto -7 -> 105
    //   115: astore 6
    //   117: goto -21 -> 96
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	120	0	this	MagicNumberFileFilter
    //   0	120	1	paramFile	java.io.File
    //   102	8	2	localObject1	Object
    //   111	1	2	localObject2	Object
    //   37	69	3	localRandomAccessFile	java.io.RandomAccessFile
    //   92	1	4	localIOException1	java.io.IOException
    //   25	54	5	arrayOfByte	byte[]
    //   115	1	6	localIOException2	java.io.IOException
    //   52	14	7	i	int
    //   59	7	8	j	int
    //   83	7	9	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   18	38	92	java/io/IOException
    //   18	38	102	finally
    //   38	61	111	finally
    //   74	85	111	finally
    //   38	61	115	java/io/IOException
    //   74	85	115	java/io/IOException
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(super.toString());
    localStringBuilder.append("(");
    localStringBuilder.append(new String(this.magicNumbers));
    localStringBuilder.append(",");
    localStringBuilder.append(this.byteOffset);
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/filefilter/MagicNumberFileFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */