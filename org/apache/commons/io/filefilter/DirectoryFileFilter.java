package org.apache.commons.io.filefilter;

import java.io.File;
import java.io.Serializable;

public class DirectoryFileFilter
  extends AbstractFileFilter
  implements Serializable
{
  public static final IOFileFilter DIRECTORY;
  public static final IOFileFilter INSTANCE;
  
  static
  {
    DirectoryFileFilter localDirectoryFileFilter = new DirectoryFileFilter();
    DIRECTORY = localDirectoryFileFilter;
    INSTANCE = localDirectoryFileFilter;
  }
  
  public boolean accept(File paramFile)
  {
    return paramFile.isDirectory();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/filefilter/DirectoryFileFilter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */