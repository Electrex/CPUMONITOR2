package org.apache.commons.io;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class FileSystemUtils
{
  private static final String DF;
  private static final int INIT_PROBLEM = -1;
  private static final FileSystemUtils INSTANCE;
  private static final int OS = 0;
  private static final int OTHER = 0;
  private static final int POSIX_UNIX = 3;
  private static final int UNIX = 2;
  private static final int WINDOWS = 1;
  
  static
  {
    int i = 3;
    INSTANCE = new FileSystemUtils();
    String str1 = "df";
    String str2;
    try
    {
      str2 = System.getProperty("os.name");
      if (str2 == null) {
        throw new IOException("os.name not found");
      }
    }
    catch (Exception localException)
    {
      i = -1;
    }
    for (;;)
    {
      OS = i;
      DF = str1;
      return;
      String str3 = str2.toLowerCase(Locale.ENGLISH);
      if (str3.indexOf("windows") != -1)
      {
        i = 1;
      }
      else
      {
        if ((str3.indexOf("linux") == -1) && (str3.indexOf("mpe/ix") == -1) && (str3.indexOf("freebsd") == -1) && (str3.indexOf("irix") == -1) && (str3.indexOf("digital unix") == -1) && (str3.indexOf("unix") == -1) && (str3.indexOf("mac os x") == -1))
        {
          if ((str3.indexOf("sun os") == -1) && (str3.indexOf("sunos") == -1) && (str3.indexOf("solaris") == -1))
          {
            if (str3.indexOf("hp-ux") != -1) {
              continue;
            }
            int j = str3.indexOf("aix");
            if (j != -1) {
              continue;
            }
            i = 0;
          }
        }
        else
        {
          i = 2;
          continue;
        }
        str1 = "/usr/xpg4/bin/df";
      }
    }
  }
  
  @Deprecated
  public static long freeSpace(String paramString)
  {
    return INSTANCE.freeSpaceOS(paramString, OS, false, -1L);
  }
  
  public static long freeSpaceKb()
  {
    return freeSpaceKb(-1L);
  }
  
  public static long freeSpaceKb(long paramLong)
  {
    return freeSpaceKb(new File(".").getAbsolutePath(), paramLong);
  }
  
  public static long freeSpaceKb(String paramString)
  {
    return freeSpaceKb(paramString, -1L);
  }
  
  public static long freeSpaceKb(String paramString, long paramLong)
  {
    return INSTANCE.freeSpaceOS(paramString, OS, true, paramLong);
  }
  
  long freeSpaceOS(String paramString, int paramInt, boolean paramBoolean, long paramLong)
  {
    if (paramString == null) {
      throw new IllegalArgumentException("Path must not be empty");
    }
    switch (paramInt)
    {
    default: 
      throw new IllegalStateException("Exception caught when determining operating system");
    case 1: 
      if (paramBoolean) {
        return freeSpaceWindows(paramString, paramLong) / 1024L;
      }
      return freeSpaceWindows(paramString, paramLong);
    case 2: 
      return freeSpaceUnix(paramString, paramBoolean, false, paramLong);
    case 3: 
      return freeSpaceUnix(paramString, paramBoolean, true, paramLong);
    }
    throw new IllegalStateException("Unsupported operating system");
  }
  
  long freeSpaceUnix(String paramString, boolean paramBoolean1, boolean paramBoolean2, long paramLong)
  {
    if (paramString.length() == 0) {
      throw new IllegalArgumentException("Path must not be empty");
    }
    String str = "-";
    if (paramBoolean1) {
      str = str + "k";
    }
    if (paramBoolean2) {
      str = str + "P";
    }
    String[] arrayOfString1;
    if (str.length() > 1)
    {
      String[] arrayOfString2 = new String[3];
      arrayOfString2[0] = DF;
      arrayOfString2[1] = str;
      arrayOfString2[2] = paramString;
      arrayOfString1 = arrayOfString2;
    }
    List localList;
    for (;;)
    {
      localList = performCommand(arrayOfString1, 3, paramLong);
      if (localList.size() >= 2) {
        break;
      }
      throw new IOException("Command line '" + DF + "' did not return info as expected for path '" + paramString + "'- response was " + localList);
      arrayOfString1 = new String[2];
      arrayOfString1[0] = DF;
      arrayOfString1[1] = paramString;
    }
    StringTokenizer localStringTokenizer1 = new StringTokenizer((String)localList.get(1), " ");
    if (localStringTokenizer1.countTokens() < 4) {
      if ((localStringTokenizer1.countTokens() != 1) || (localList.size() < 3)) {}
    }
    for (StringTokenizer localStringTokenizer2 = new StringTokenizer((String)localList.get(2), " ");; localStringTokenizer2 = localStringTokenizer1)
    {
      localStringTokenizer2.nextToken();
      localStringTokenizer2.nextToken();
      return parseBytes(localStringTokenizer2.nextToken(), paramString);
      throw new IOException("Command line '" + DF + "' did not return data as expected for path '" + paramString + "'- check path is valid");
      localStringTokenizer1.nextToken();
    }
  }
  
  long freeSpaceWindows(String paramString, long paramLong)
  {
    String str1 = FilenameUtils.normalize(paramString, false);
    if ((str1.length() > 0) && (str1.charAt(0) != '"')) {}
    for (String str2 = "\"" + str1 + "\"";; str2 = str1)
    {
      String[] arrayOfString = new String[3];
      arrayOfString[0] = "cmd.exe";
      arrayOfString[1] = "/C";
      arrayOfString[2] = ("dir /a /-c " + str2);
      List localList = performCommand(arrayOfString, Integer.MAX_VALUE, paramLong);
      for (int i = -1 + localList.size(); i >= 0; i--)
      {
        String str3 = (String)localList.get(i);
        if (str3.length() > 0) {
          return parseDir(str3, str2);
        }
      }
      throw new IOException("Command line 'dir /-c' did not return any info for path '" + str2 + "'");
    }
  }
  
  Process openProcess(String[] paramArrayOfString)
  {
    return Runtime.getRuntime().exec(paramArrayOfString);
  }
  
  long parseBytes(String paramString1, String paramString2)
  {
    long l;
    try
    {
      l = Long.parseLong(paramString1);
      if (l < 0L) {
        throw new IOException("Command line '" + DF + "' did not find free space in response for path '" + paramString2 + "'- check path is valid");
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new IOExceptionWithCause("Command line '" + DF + "' did not return numeric data as expected for path '" + paramString2 + "'- check path is valid", localNumberFormatException);
    }
    return l;
  }
  
  long parseDir(String paramString1, String paramString2)
  {
    int i = 0;
    int j = -1 + paramString1.length();
    int m;
    int k;
    if (j >= 0) {
      if (Character.isDigit(paramString1.charAt(j)))
      {
        m = j + 1;
        k = j;
      }
    }
    for (;;)
    {
      label37:
      if (k >= 0)
      {
        char c = paramString1.charAt(k);
        if ((Character.isDigit(c)) || (c == ',') || (c == '.')) {}
      }
      for (int n = k + 1;; n = 0)
      {
        if (k < 0)
        {
          throw new IOException("Command line 'dir /-c' did not return valid info for path '" + paramString2 + "'");
          j--;
          break;
          k--;
          break label37;
        }
        StringBuilder localStringBuilder = new StringBuilder(paramString1.substring(n, m));
        while (i < localStringBuilder.length())
        {
          if ((localStringBuilder.charAt(i) == ',') || (localStringBuilder.charAt(i) == '.'))
          {
            int i1 = i - 1;
            localStringBuilder.deleteCharAt(i);
            i = i1;
          }
          i++;
        }
        return parseBytes(localStringBuilder.toString(), paramString2);
      }
      k = j;
      m = 0;
    }
  }
  
  /* Error */
  List<String> performCommand(String[] paramArrayOfString, int paramInt, long paramLong)
  {
    // Byte code:
    //   0: new 281	java/util/ArrayList
    //   3: dup
    //   4: bipush 20
    //   6: invokespecial 284	java/util/ArrayList:<init>	(I)V
    //   9: astore 5
    //   11: lload_3
    //   12: invokestatic 290	org/apache/commons/io/ThreadMonitor:start	(J)Ljava/lang/Thread;
    //   15: astore 17
    //   17: aload_0
    //   18: aload_1
    //   19: invokevirtual 292	org/apache/commons/io/FileSystemUtils:openProcess	([Ljava/lang/String;)Ljava/lang/Process;
    //   22: astore 18
    //   24: aload 18
    //   26: astore 13
    //   28: aload 13
    //   30: invokevirtual 298	java/lang/Process:getInputStream	()Ljava/io/InputStream;
    //   33: astore 19
    //   35: aload 19
    //   37: astore 14
    //   39: aload 13
    //   41: invokevirtual 302	java/lang/Process:getOutputStream	()Ljava/io/OutputStream;
    //   44: astore 20
    //   46: aload 20
    //   48: astore 15
    //   50: aload 13
    //   52: invokevirtual 305	java/lang/Process:getErrorStream	()Ljava/io/InputStream;
    //   55: astore 21
    //   57: aload 21
    //   59: astore 16
    //   61: new 307	java/io/BufferedReader
    //   64: dup
    //   65: new 309	java/io/InputStreamReader
    //   68: dup
    //   69: aload 14
    //   71: invokespecial 312	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   74: invokespecial 315	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   77: astore 7
    //   79: aload 7
    //   81: invokevirtual 318	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   84: astore 22
    //   86: aload 22
    //   88: ifnull +43 -> 131
    //   91: aload 5
    //   93: invokeinterface 171 1 0
    //   98: iload_2
    //   99: if_icmpge +32 -> 131
    //   102: aload 5
    //   104: aload 22
    //   106: getstatic 56	java/util/Locale:ENGLISH	Ljava/util/Locale;
    //   109: invokevirtual 62	java/lang/String:toLowerCase	(Ljava/util/Locale;)Ljava/lang/String;
    //   112: invokevirtual 321	java/lang/String:trim	()Ljava/lang/String;
    //   115: invokeinterface 325 2 0
    //   120: pop
    //   121: aload 7
    //   123: invokevirtual 318	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   126: astore 22
    //   128: goto -42 -> 86
    //   131: aload 13
    //   133: invokevirtual 328	java/lang/Process:waitFor	()I
    //   136: pop
    //   137: aload 17
    //   139: invokestatic 332	org/apache/commons/io/ThreadMonitor:stop	(Ljava/lang/Thread;)V
    //   142: aload 13
    //   144: invokevirtual 335	java/lang/Process:exitValue	()I
    //   147: ifeq +154 -> 301
    //   150: new 41	java/io/IOException
    //   153: dup
    //   154: new 150	java/lang/StringBuilder
    //   157: dup
    //   158: ldc_w 337
    //   161: invokespecial 174	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   164: aload 13
    //   166: invokevirtual 335	java/lang/Process:exitValue	()I
    //   169: invokevirtual 339	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   172: ldc_w 341
    //   175: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   178: aload_1
    //   179: invokestatic 347	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
    //   182: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   185: invokevirtual 160	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   188: invokespecial 46	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   191: athrow
    //   192: astore 6
    //   194: aload 16
    //   196: astore 8
    //   198: aload 15
    //   200: astore 9
    //   202: aload 14
    //   204: astore 10
    //   206: aload 13
    //   208: astore 11
    //   210: new 254	org/apache/commons/io/IOExceptionWithCause
    //   213: dup
    //   214: new 150	java/lang/StringBuilder
    //   217: dup
    //   218: ldc_w 349
    //   221: invokespecial 174	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   224: aload_1
    //   225: invokestatic 347	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
    //   228: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   231: ldc_w 351
    //   234: invokevirtual 155	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   237: lload_3
    //   238: invokevirtual 354	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   241: invokevirtual 160	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   244: aload 6
    //   246: invokespecial 259	org/apache/commons/io/IOExceptionWithCause:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   249: athrow
    //   250: astore 12
    //   252: aload 11
    //   254: astore 13
    //   256: aload 10
    //   258: astore 14
    //   260: aload 9
    //   262: astore 15
    //   264: aload 8
    //   266: astore 16
    //   268: aload 14
    //   270: invokestatic 359	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   273: aload 15
    //   275: invokestatic 362	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/OutputStream;)V
    //   278: aload 16
    //   280: invokestatic 359	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   283: aload 7
    //   285: invokestatic 364	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Reader;)V
    //   288: aload 13
    //   290: ifnull +8 -> 298
    //   293: aload 13
    //   295: invokevirtual 367	java/lang/Process:destroy	()V
    //   298: aload 12
    //   300: athrow
    //   301: aload 5
    //   303: invokeinterface 371 1 0
    //   308: ifeq +36 -> 344
    //   311: new 41	java/io/IOException
    //   314: dup
    //   315: new 150	java/lang/StringBuilder
    //   318: dup
    //   319: ldc_w 373
    //   322: invokespecial 174	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   325: aload_1
    //   326: invokestatic 347	java/util/Arrays:asList	([Ljava/lang/Object;)Ljava/util/List;
    //   329: invokevirtual 181	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   332: invokevirtual 160	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   335: invokespecial 46	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   338: athrow
    //   339: astore 12
    //   341: goto -73 -> 268
    //   344: aload 14
    //   346: invokestatic 359	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   349: aload 15
    //   351: invokestatic 362	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/OutputStream;)V
    //   354: aload 16
    //   356: invokestatic 359	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   359: aload 7
    //   361: invokestatic 364	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Reader;)V
    //   364: aload 13
    //   366: ifnull +8 -> 374
    //   369: aload 13
    //   371: invokevirtual 367	java/lang/Process:destroy	()V
    //   374: aload 5
    //   376: areturn
    //   377: astore 12
    //   379: aconst_null
    //   380: astore 7
    //   382: aconst_null
    //   383: astore 16
    //   385: aconst_null
    //   386: astore 15
    //   388: aconst_null
    //   389: astore 14
    //   391: aconst_null
    //   392: astore 13
    //   394: goto -126 -> 268
    //   397: astore 12
    //   399: aconst_null
    //   400: astore 7
    //   402: aconst_null
    //   403: astore 16
    //   405: aconst_null
    //   406: astore 15
    //   408: aconst_null
    //   409: astore 14
    //   411: goto -143 -> 268
    //   414: astore 12
    //   416: aconst_null
    //   417: astore 7
    //   419: aconst_null
    //   420: astore 16
    //   422: aconst_null
    //   423: astore 15
    //   425: goto -157 -> 268
    //   428: astore 12
    //   430: aconst_null
    //   431: astore 7
    //   433: aconst_null
    //   434: astore 16
    //   436: goto -168 -> 268
    //   439: astore 12
    //   441: aconst_null
    //   442: astore 7
    //   444: goto -176 -> 268
    //   447: astore 6
    //   449: aconst_null
    //   450: astore 7
    //   452: aconst_null
    //   453: astore 8
    //   455: aconst_null
    //   456: astore 9
    //   458: aconst_null
    //   459: astore 10
    //   461: aconst_null
    //   462: astore 11
    //   464: goto -254 -> 210
    //   467: astore 6
    //   469: aload 13
    //   471: astore 11
    //   473: aconst_null
    //   474: astore 7
    //   476: aconst_null
    //   477: astore 8
    //   479: aconst_null
    //   480: astore 9
    //   482: aconst_null
    //   483: astore 10
    //   485: goto -275 -> 210
    //   488: astore 6
    //   490: aload 14
    //   492: astore 10
    //   494: aload 13
    //   496: astore 11
    //   498: aconst_null
    //   499: astore 7
    //   501: aconst_null
    //   502: astore 8
    //   504: aconst_null
    //   505: astore 9
    //   507: goto -297 -> 210
    //   510: astore 6
    //   512: aload 15
    //   514: astore 9
    //   516: aload 14
    //   518: astore 10
    //   520: aload 13
    //   522: astore 11
    //   524: aconst_null
    //   525: astore 7
    //   527: aconst_null
    //   528: astore 8
    //   530: goto -320 -> 210
    //   533: astore 6
    //   535: aload 16
    //   537: astore 8
    //   539: aload 15
    //   541: astore 9
    //   543: aload 14
    //   545: astore 10
    //   547: aload 13
    //   549: astore 11
    //   551: aconst_null
    //   552: astore 7
    //   554: goto -344 -> 210
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	557	0	this	FileSystemUtils
    //   0	557	1	paramArrayOfString	String[]
    //   0	557	2	paramInt	int
    //   0	557	3	paramLong	long
    //   9	366	5	localArrayList	java.util.ArrayList
    //   192	53	6	localInterruptedException1	InterruptedException
    //   447	1	6	localInterruptedException2	InterruptedException
    //   467	1	6	localInterruptedException3	InterruptedException
    //   488	1	6	localInterruptedException4	InterruptedException
    //   510	1	6	localInterruptedException5	InterruptedException
    //   533	1	6	localInterruptedException6	InterruptedException
    //   77	476	7	localBufferedReader	java.io.BufferedReader
    //   196	342	8	localObject1	Object
    //   200	342	9	localObject2	Object
    //   204	342	10	localObject3	Object
    //   208	342	11	localObject4	Object
    //   250	49	12	localObject5	Object
    //   339	1	12	localObject6	Object
    //   377	1	12	localObject7	Object
    //   397	1	12	localObject8	Object
    //   414	1	12	localObject9	Object
    //   428	1	12	localObject10	Object
    //   439	1	12	localObject11	Object
    //   26	522	13	localObject12	Object
    //   37	507	14	localObject13	Object
    //   48	492	15	localObject14	Object
    //   59	477	16	localObject15	Object
    //   15	123	17	localThread	Thread
    //   22	3	18	localProcess	Process
    //   33	3	19	localInputStream1	java.io.InputStream
    //   44	3	20	localOutputStream	java.io.OutputStream
    //   55	3	21	localInputStream2	java.io.InputStream
    //   84	43	22	str	String
    // Exception table:
    //   from	to	target	type
    //   79	86	192	java/lang/InterruptedException
    //   91	128	192	java/lang/InterruptedException
    //   131	192	192	java/lang/InterruptedException
    //   301	339	192	java/lang/InterruptedException
    //   210	250	250	finally
    //   79	86	339	finally
    //   91	128	339	finally
    //   131	192	339	finally
    //   301	339	339	finally
    //   11	24	377	finally
    //   28	35	397	finally
    //   39	46	414	finally
    //   50	57	428	finally
    //   61	79	439	finally
    //   11	24	447	java/lang/InterruptedException
    //   28	35	467	java/lang/InterruptedException
    //   39	46	488	java/lang/InterruptedException
    //   50	57	510	java/lang/InterruptedException
    //   61	79	533	java/lang/InterruptedException
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/FileSystemUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */