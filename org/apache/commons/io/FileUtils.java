package org.apache.commons.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.CRC32;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class FileUtils
{
  public static final File[] EMPTY_FILE_ARRAY = new File[0];
  private static final long FILE_COPY_BUFFER_SIZE = 31457280L;
  public static final long ONE_EB = 1152921504606846976L;
  public static final BigInteger ONE_EB_BI;
  public static final long ONE_GB = 1073741824L;
  public static final BigInteger ONE_GB_BI;
  public static final long ONE_KB = 1024L;
  public static final BigInteger ONE_KB_BI;
  public static final long ONE_MB = 1048576L;
  public static final BigInteger ONE_MB_BI;
  public static final long ONE_PB = 1125899906842624L;
  public static final BigInteger ONE_PB_BI;
  public static final long ONE_TB = 1099511627776L;
  public static final BigInteger ONE_TB_BI;
  public static final BigInteger ONE_YB;
  public static final BigInteger ONE_ZB;
  private static final Charset UTF8 = Charset.forName("UTF-8");
  
  static
  {
    BigInteger localBigInteger = BigInteger.valueOf(1024L);
    ONE_KB_BI = localBigInteger;
    ONE_MB_BI = localBigInteger.multiply(localBigInteger);
    ONE_GB_BI = ONE_KB_BI.multiply(ONE_MB_BI);
    ONE_TB_BI = ONE_KB_BI.multiply(ONE_GB_BI);
    ONE_PB_BI = ONE_KB_BI.multiply(ONE_TB_BI);
    ONE_EB_BI = ONE_KB_BI.multiply(ONE_PB_BI);
    ONE_ZB = BigInteger.valueOf(1024L).multiply(BigInteger.valueOf(1152921504606846976L));
    ONE_YB = ONE_KB_BI.multiply(ONE_ZB);
  }
  
  public static String byteCountToDisplaySize(long paramLong)
  {
    return byteCountToDisplaySize(BigInteger.valueOf(paramLong));
  }
  
  public static String byteCountToDisplaySize(BigInteger paramBigInteger)
  {
    if (paramBigInteger.divide(ONE_EB_BI).compareTo(BigInteger.ZERO) > 0) {
      return String.valueOf(paramBigInteger.divide(ONE_EB_BI)) + " EB";
    }
    if (paramBigInteger.divide(ONE_PB_BI).compareTo(BigInteger.ZERO) > 0) {
      return String.valueOf(paramBigInteger.divide(ONE_PB_BI)) + " PB";
    }
    if (paramBigInteger.divide(ONE_TB_BI).compareTo(BigInteger.ZERO) > 0) {
      return String.valueOf(paramBigInteger.divide(ONE_TB_BI)) + " TB";
    }
    if (paramBigInteger.divide(ONE_GB_BI).compareTo(BigInteger.ZERO) > 0) {
      return String.valueOf(paramBigInteger.divide(ONE_GB_BI)) + " GB";
    }
    if (paramBigInteger.divide(ONE_MB_BI).compareTo(BigInteger.ZERO) > 0) {
      return String.valueOf(paramBigInteger.divide(ONE_MB_BI)) + " MB";
    }
    if (paramBigInteger.divide(ONE_KB_BI).compareTo(BigInteger.ZERO) > 0) {
      return String.valueOf(paramBigInteger.divide(ONE_KB_BI)) + " KB";
    }
    return String.valueOf(paramBigInteger) + " bytes";
  }
  
  private static void checkDirectory(File paramFile)
  {
    if (!paramFile.exists()) {
      throw new IllegalArgumentException(paramFile + " does not exist");
    }
    if (!paramFile.isDirectory()) {
      throw new IllegalArgumentException(paramFile + " is not a directory");
    }
  }
  
  /* Error */
  public static java.util.zip.Checksum checksum(File paramFile, java.util.zip.Checksum paramChecksum)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 148	java/io/File:isDirectory	()Z
    //   4: ifeq +13 -> 17
    //   7: new 137	java/lang/IllegalArgumentException
    //   10: dup
    //   11: ldc -102
    //   13: invokespecial 145	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   16: athrow
    //   17: new 156	java/util/zip/CheckedInputStream
    //   20: dup
    //   21: new 158	java/io/FileInputStream
    //   24: dup
    //   25: aload_0
    //   26: invokespecial 160	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   29: aload_1
    //   30: invokespecial 163	java/util/zip/CheckedInputStream:<init>	(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V
    //   33: astore_2
    //   34: aload_2
    //   35: new 165	org/apache/commons/io/output/NullOutputStream
    //   38: dup
    //   39: invokespecial 166	org/apache/commons/io/output/NullOutputStream:<init>	()V
    //   42: invokestatic 172	org/apache/commons/io/IOUtils:copy	(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    //   45: pop
    //   46: aload_2
    //   47: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   50: aload_1
    //   51: areturn
    //   52: astore_3
    //   53: aconst_null
    //   54: astore_2
    //   55: aload_2
    //   56: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   59: aload_3
    //   60: athrow
    //   61: astore_3
    //   62: goto -7 -> 55
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	65	0	paramFile	File
    //   0	65	1	paramChecksum	java.util.zip.Checksum
    //   33	23	2	localCheckedInputStream	java.util.zip.CheckedInputStream
    //   52	8	3	localObject1	Object
    //   61	1	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   17	34	52	finally
    //   34	46	61	finally
  }
  
  public static long checksumCRC32(File paramFile)
  {
    CRC32 localCRC32 = new CRC32();
    checksum(paramFile, localCRC32);
    return localCRC32.getValue();
  }
  
  public static void cleanDirectory(File paramFile)
  {
    if (!paramFile.exists()) {
      throw new IllegalArgumentException(paramFile + " does not exist");
    }
    if (!paramFile.isDirectory()) {
      throw new IllegalArgumentException(paramFile + " is not a directory");
    }
    File[] arrayOfFile = paramFile.listFiles();
    if (arrayOfFile == null) {
      throw new IOException("Failed to list contents of " + paramFile);
    }
    int i = arrayOfFile.length;
    Object localObject = null;
    int j = 0;
    for (;;)
    {
      File localFile;
      if (j < i) {
        localFile = arrayOfFile[j];
      }
      try
      {
        forceDelete(localFile);
        j++;
        continue;
        if (localObject != null) {
          throw ((Throwable)localObject);
        }
      }
      catch (IOException localIOException)
      {
        for (;;) {}
      }
    }
  }
  
  private static void cleanDirectoryOnExit(File paramFile)
  {
    if (!paramFile.exists()) {
      throw new IllegalArgumentException(paramFile + " does not exist");
    }
    if (!paramFile.isDirectory()) {
      throw new IllegalArgumentException(paramFile + " is not a directory");
    }
    File[] arrayOfFile = paramFile.listFiles();
    if (arrayOfFile == null) {
      throw new IOException("Failed to list contents of " + paramFile);
    }
    int i = arrayOfFile.length;
    Object localObject = null;
    int j = 0;
    for (;;)
    {
      File localFile;
      if (j < i) {
        localFile = arrayOfFile[j];
      }
      try
      {
        forceDeleteOnExit(localFile);
        j++;
        continue;
        if (localObject != null) {
          throw ((Throwable)localObject);
        }
      }
      catch (IOException localIOException)
      {
        for (;;) {}
      }
    }
  }
  
  /* Error */
  public static boolean contentEquals(File paramFile1, File paramFile2)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_0
    //   3: invokevirtual 135	java/io/File:exists	()Z
    //   6: istore_3
    //   7: iload_3
    //   8: aload_1
    //   9: invokevirtual 135	java/io/File:exists	()Z
    //   12: if_icmpeq +5 -> 17
    //   15: iconst_0
    //   16: ireturn
    //   17: iload_3
    //   18: ifne +5 -> 23
    //   21: iconst_1
    //   22: ireturn
    //   23: aload_0
    //   24: invokevirtual 148	java/io/File:isDirectory	()Z
    //   27: ifne +10 -> 37
    //   30: aload_1
    //   31: invokevirtual 148	java/io/File:isDirectory	()Z
    //   34: ifeq +13 -> 47
    //   37: new 190	java/io/IOException
    //   40: dup
    //   41: ldc -47
    //   43: invokespecial 198	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   46: athrow
    //   47: aload_0
    //   48: invokevirtual 212	java/io/File:length	()J
    //   51: aload_1
    //   52: invokevirtual 212	java/io/File:length	()J
    //   55: lcmp
    //   56: ifne -41 -> 15
    //   59: aload_0
    //   60: invokevirtual 216	java/io/File:getCanonicalFile	()Ljava/io/File;
    //   63: aload_1
    //   64: invokevirtual 216	java/io/File:getCanonicalFile	()Ljava/io/File;
    //   67: invokevirtual 220	java/io/File:equals	(Ljava/lang/Object;)Z
    //   70: ifeq +5 -> 75
    //   73: iconst_1
    //   74: ireturn
    //   75: new 158	java/io/FileInputStream
    //   78: dup
    //   79: aload_0
    //   80: invokespecial 160	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   83: astore 4
    //   85: new 158	java/io/FileInputStream
    //   88: dup
    //   89: aload_1
    //   90: invokespecial 160	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   93: astore 5
    //   95: aload 4
    //   97: aload 5
    //   99: invokestatic 223	org/apache/commons/io/IOUtils:contentEquals	(Ljava/io/InputStream;Ljava/io/InputStream;)Z
    //   102: istore 7
    //   104: aload 4
    //   106: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   109: aload 5
    //   111: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   114: iload 7
    //   116: ireturn
    //   117: astore 6
    //   119: aconst_null
    //   120: astore 5
    //   122: aload_2
    //   123: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   126: aload 5
    //   128: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   131: aload 6
    //   133: athrow
    //   134: astore 6
    //   136: aload 4
    //   138: astore_2
    //   139: aconst_null
    //   140: astore 5
    //   142: goto -20 -> 122
    //   145: astore 6
    //   147: aload 4
    //   149: astore_2
    //   150: goto -28 -> 122
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	153	0	paramFile1	File
    //   0	153	1	paramFile2	File
    //   1	149	2	localObject1	Object
    //   6	12	3	bool1	boolean
    //   83	65	4	localFileInputStream1	FileInputStream
    //   93	48	5	localFileInputStream2	FileInputStream
    //   117	15	6	localObject2	Object
    //   134	1	6	localObject3	Object
    //   145	1	6	localObject4	Object
    //   102	13	7	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   75	85	117	finally
    //   85	95	134	finally
    //   95	104	145	finally
  }
  
  public static boolean contentEqualsIgnoreEOL(File paramFile1, File paramFile2, String paramString)
  {
    Object localObject1 = null;
    boolean bool1 = true;
    boolean bool2 = paramFile1.exists();
    if (bool2 != paramFile2.exists()) {
      bool1 = false;
    }
    do
    {
      do
      {
        return bool1;
      } while (!bool2);
      if ((paramFile1.isDirectory()) || (paramFile2.isDirectory())) {
        throw new IOException("Can't compare directories, only files");
      }
    } while (paramFile1.getCanonicalFile().equals(paramFile2.getCanonicalFile()));
    if (paramString == null) {}
    InputStreamReader localInputStreamReader1;
    for (;;)
    {
      try
      {
        localInputStreamReader1 = new InputStreamReader(new FileInputStream(paramFile1));
      }
      finally
      {
        boolean bool3;
        InputStreamReader localInputStreamReader2;
        localInputStreamReader1 = null;
      }
      try
      {
        localObject1 = new InputStreamReader(new FileInputStream(paramFile2));
        bool3 = IOUtils.contentEqualsIgnoreEOL(localInputStreamReader1, (Reader)localObject1);
        IOUtils.closeQuietly(localInputStreamReader1);
        IOUtils.closeQuietly((Reader)localObject1);
        return bool3;
      }
      finally
      {
        for (;;) {}
      }
      localInputStreamReader1 = new InputStreamReader(new FileInputStream(paramFile1), paramString);
      localInputStreamReader2 = new InputStreamReader(new FileInputStream(paramFile2), paramString);
      localObject1 = localInputStreamReader2;
    }
    IOUtils.closeQuietly(localInputStreamReader1);
    IOUtils.closeQuietly((Reader)localObject1);
    throw ((Throwable)localObject2);
  }
  
  public static File[] convertFileCollectionToFileArray(Collection<File> paramCollection)
  {
    return (File[])paramCollection.toArray(new File[paramCollection.size()]);
  }
  
  public static void copyDirectory(File paramFile1, File paramFile2)
  {
    copyDirectory(paramFile1, paramFile2, true);
  }
  
  public static void copyDirectory(File paramFile1, File paramFile2, FileFilter paramFileFilter)
  {
    copyDirectory(paramFile1, paramFile2, paramFileFilter, true);
  }
  
  public static void copyDirectory(File paramFile1, File paramFile2, FileFilter paramFileFilter, boolean paramBoolean)
  {
    if (paramFile1 == null) {
      throw new NullPointerException("Source must not be null");
    }
    if (paramFile2 == null) {
      throw new NullPointerException("Destination must not be null");
    }
    if (!paramFile1.exists()) {
      throw new FileNotFoundException("Source '" + paramFile1 + "' does not exist");
    }
    if (!paramFile1.isDirectory()) {
      throw new IOException("Source '" + paramFile1 + "' exists but is not a directory");
    }
    if (paramFile1.getCanonicalPath().equals(paramFile2.getCanonicalPath())) {
      throw new IOException("Source '" + paramFile1 + "' and destination '" + paramFile2 + "' are the same");
    }
    boolean bool = paramFile2.getCanonicalPath().startsWith(paramFile1.getCanonicalPath());
    ArrayList localArrayList = null;
    if (bool)
    {
      if (paramFileFilter == null) {}
      for (File[] arrayOfFile = paramFile1.listFiles();; arrayOfFile = paramFile1.listFiles(paramFileFilter))
      {
        localArrayList = null;
        if (arrayOfFile == null) {
          break;
        }
        int i = arrayOfFile.length;
        localArrayList = null;
        if (i <= 0) {
          break;
        }
        localArrayList = new ArrayList(arrayOfFile.length);
        int j = arrayOfFile.length;
        for (int k = 0; k < j; k++) {
          localArrayList.add(new File(paramFile2, arrayOfFile[k].getName()).getCanonicalPath());
        }
      }
    }
    doCopyDirectory(paramFile1, paramFile2, paramFileFilter, paramBoolean, localArrayList);
  }
  
  public static void copyDirectory(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    copyDirectory(paramFile1, paramFile2, null, paramBoolean);
  }
  
  public static void copyDirectoryToDirectory(File paramFile1, File paramFile2)
  {
    if (paramFile1 == null) {
      throw new NullPointerException("Source must not be null");
    }
    if ((paramFile1.exists()) && (!paramFile1.isDirectory())) {
      throw new IllegalArgumentException("Source '" + paramFile2 + "' is not a directory");
    }
    if (paramFile2 == null) {
      throw new NullPointerException("Destination must not be null");
    }
    if ((paramFile2.exists()) && (!paramFile2.isDirectory())) {
      throw new IllegalArgumentException("Destination '" + paramFile2 + "' is not a directory");
    }
    copyDirectory(paramFile1, new File(paramFile2, paramFile1.getName()), true);
  }
  
  public static long copyFile(File paramFile, OutputStream paramOutputStream)
  {
    FileInputStream localFileInputStream = new FileInputStream(paramFile);
    try
    {
      long l = IOUtils.copyLarge(localFileInputStream, paramOutputStream);
      return l;
    }
    finally
    {
      localFileInputStream.close();
    }
  }
  
  public static void copyFile(File paramFile1, File paramFile2)
  {
    copyFile(paramFile1, paramFile2, true);
  }
  
  public static void copyFile(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    if (paramFile1 == null) {
      throw new NullPointerException("Source must not be null");
    }
    if (paramFile2 == null) {
      throw new NullPointerException("Destination must not be null");
    }
    if (!paramFile1.exists()) {
      throw new FileNotFoundException("Source '" + paramFile1 + "' does not exist");
    }
    if (paramFile1.isDirectory()) {
      throw new IOException("Source '" + paramFile1 + "' exists but is a directory");
    }
    if (paramFile1.getCanonicalPath().equals(paramFile2.getCanonicalPath())) {
      throw new IOException("Source '" + paramFile1 + "' and destination '" + paramFile2 + "' are the same");
    }
    File localFile = paramFile2.getParentFile();
    if ((localFile != null) && (!localFile.mkdirs()) && (!localFile.isDirectory())) {
      throw new IOException("Destination '" + localFile + "' directory cannot be created");
    }
    if ((paramFile2.exists()) && (!paramFile2.canWrite())) {
      throw new IOException("Destination '" + paramFile2 + "' exists but is read-only");
    }
    doCopyFile(paramFile1, paramFile2, paramBoolean);
  }
  
  public static void copyFileToDirectory(File paramFile1, File paramFile2)
  {
    copyFileToDirectory(paramFile1, paramFile2, true);
  }
  
  public static void copyFileToDirectory(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    if (paramFile2 == null) {
      throw new NullPointerException("Destination must not be null");
    }
    if ((paramFile2.exists()) && (!paramFile2.isDirectory())) {
      throw new IllegalArgumentException("Destination '" + paramFile2 + "' is not a directory");
    }
    copyFile(paramFile1, new File(paramFile2, paramFile1.getName()), paramBoolean);
  }
  
  /* Error */
  public static void copyInputStreamToFile(java.io.InputStream paramInputStream, File paramFile)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 354	org/apache/commons/io/FileUtils:openOutputStream	(Ljava/io/File;)Ljava/io/FileOutputStream;
    //   4: astore_3
    //   5: aload_0
    //   6: aload_3
    //   7: invokestatic 172	org/apache/commons/io/IOUtils:copy	(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    //   10: pop
    //   11: aload_3
    //   12: invokevirtual 357	java/io/FileOutputStream:close	()V
    //   15: aload_3
    //   16: invokestatic 360	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/OutputStream;)V
    //   19: aload_0
    //   20: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   23: return
    //   24: astore 4
    //   26: aload_3
    //   27: invokestatic 360	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/OutputStream;)V
    //   30: aload 4
    //   32: athrow
    //   33: astore_2
    //   34: aload_0
    //   35: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   38: aload_2
    //   39: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	40	0	paramInputStream	java.io.InputStream
    //   0	40	1	paramFile	File
    //   33	6	2	localObject1	Object
    //   4	23	3	localFileOutputStream	FileOutputStream
    //   24	7	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   5	15	24	finally
    //   0	5	33	finally
    //   15	19	33	finally
    //   26	33	33	finally
  }
  
  public static void copyURLToFile(URL paramURL, File paramFile)
  {
    copyInputStreamToFile(paramURL.openStream(), paramFile);
  }
  
  public static void copyURLToFile(URL paramURL, File paramFile, int paramInt1, int paramInt2)
  {
    URLConnection localURLConnection = paramURL.openConnection();
    localURLConnection.setConnectTimeout(paramInt1);
    localURLConnection.setReadTimeout(paramInt2);
    copyInputStreamToFile(localURLConnection.getInputStream(), paramFile);
  }
  
  static String decodeUrl(String paramString)
  {
    if ((paramString != null) && (paramString.indexOf('%') >= 0))
    {
      int i = paramString.length();
      StringBuffer localStringBuffer = new StringBuffer();
      ByteBuffer localByteBuffer = ByteBuffer.allocate(i);
      int j = 0;
      while (j < i)
      {
        label51:
        int m;
        int n;
        if (paramString.charAt(j) == '%')
        {
          m = j + 1;
          n = j + 3;
        }
        try
        {
          localByteBuffer.put((byte)Integer.parseInt(paramString.substring(m, n), 16));
          j += 3;
          if (j < i)
          {
            int i1 = paramString.charAt(j);
            if (i1 == 37) {
              break label51;
            }
          }
          if (localByteBuffer.position() > 0)
          {
            localByteBuffer.flip();
            localStringBuffer.append(UTF8.decode(localByteBuffer).toString());
            localByteBuffer.clear();
          }
        }
        catch (RuntimeException localRuntimeException)
        {
          if (localByteBuffer.position() > 0)
          {
            localByteBuffer.flip();
            localStringBuffer.append(UTF8.decode(localByteBuffer).toString());
            localByteBuffer.clear();
          }
          int k = j + 1;
          localStringBuffer.append(paramString.charAt(j));
          j = k;
        }
        finally
        {
          if (localByteBuffer.position() > 0)
          {
            localByteBuffer.flip();
            localStringBuffer.append(UTF8.decode(localByteBuffer).toString());
            localByteBuffer.clear();
          }
        }
      }
      paramString = localStringBuffer.toString();
    }
    return paramString;
  }
  
  public static void deleteDirectory(File paramFile)
  {
    if (!paramFile.exists()) {}
    do
    {
      return;
      if (!isSymlink(paramFile)) {
        cleanDirectory(paramFile);
      }
    } while (paramFile.delete());
    throw new IOException("Unable to delete directory " + paramFile + ".");
  }
  
  private static void deleteDirectoryOnExit(File paramFile)
  {
    if (!paramFile.exists()) {}
    do
    {
      return;
      paramFile.deleteOnExit();
    } while (isSymlink(paramFile));
    cleanDirectoryOnExit(paramFile);
  }
  
  public static boolean deleteQuietly(File paramFile)
  {
    if (paramFile == null) {
      return false;
    }
    try
    {
      if (paramFile.isDirectory()) {
        cleanDirectory(paramFile);
      }
      try
      {
        boolean bool = paramFile.delete();
        return bool;
      }
      catch (Exception localException2)
      {
        return false;
      }
    }
    catch (Exception localException1)
    {
      for (;;) {}
    }
  }
  
  public static boolean directoryContains(File paramFile1, File paramFile2)
  {
    if (paramFile1 == null) {
      throw new IllegalArgumentException("Directory must not be null");
    }
    if (!paramFile1.isDirectory()) {
      throw new IllegalArgumentException("Not a directory: " + paramFile1);
    }
    if (paramFile2 == null) {}
    while ((!paramFile1.exists()) || (!paramFile2.exists())) {
      return false;
    }
    return FilenameUtils.directoryContains(paramFile1.getCanonicalPath(), paramFile2.getCanonicalPath());
  }
  
  private static void doCopyDirectory(File paramFile1, File paramFile2, FileFilter paramFileFilter, boolean paramBoolean, List<String> paramList)
  {
    if (paramFileFilter == null) {}
    for (File[] arrayOfFile = paramFile1.listFiles(); arrayOfFile == null; arrayOfFile = paramFile1.listFiles(paramFileFilter)) {
      throw new IOException("Failed to list contents of " + paramFile1);
    }
    if (paramFile2.exists())
    {
      if (!paramFile2.isDirectory()) {
        throw new IOException("Destination '" + paramFile2 + "' exists but is not a directory");
      }
    }
    else if ((!paramFile2.mkdirs()) && (!paramFile2.isDirectory())) {
      throw new IOException("Destination '" + paramFile2 + "' directory cannot be created");
    }
    if (!paramFile2.canWrite()) {
      throw new IOException("Destination '" + paramFile2 + "' cannot be written to");
    }
    int i = arrayOfFile.length;
    int j = 0;
    if (j < i)
    {
      File localFile1 = arrayOfFile[j];
      File localFile2 = new File(paramFile2, localFile1.getName());
      if ((paramList == null) || (!paramList.contains(localFile1.getCanonicalPath())))
      {
        if (!localFile1.isDirectory()) {
          break label259;
        }
        doCopyDirectory(localFile1, localFile2, paramFileFilter, paramBoolean, paramList);
      }
      for (;;)
      {
        j++;
        break;
        label259:
        doCopyFile(localFile1, localFile2, paramBoolean);
      }
    }
    if (paramBoolean) {
      paramFile2.setLastModified(paramFile1.lastModified());
    }
  }
  
  /* Error */
  private static void doCopyFile(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_1
    //   3: invokevirtual 135	java/io/File:exists	()Z
    //   6: ifeq +41 -> 47
    //   9: aload_1
    //   10: invokevirtual 148	java/io/File:isDirectory	()Z
    //   13: ifeq +34 -> 47
    //   16: new 190	java/io/IOException
    //   19: dup
    //   20: new 101	java/lang/StringBuilder
    //   23: dup
    //   24: ldc_w 316
    //   27: invokespecial 197	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   30: aload_1
    //   31: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   34: ldc_w 329
    //   37: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   40: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   43: invokespecial 198	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   46: athrow
    //   47: new 158	java/io/FileInputStream
    //   50: dup
    //   51: aload_0
    //   52: invokespecial 160	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   55: astore 4
    //   57: new 356	java/io/FileOutputStream
    //   60: dup
    //   61: aload_1
    //   62: invokespecial 493	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   65: astore 5
    //   67: aload 4
    //   69: invokevirtual 497	java/io/FileInputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   72: astore 10
    //   74: aload 5
    //   76: invokevirtual 498	java/io/FileOutputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   79: astore 11
    //   81: aload 10
    //   83: invokevirtual 502	java/nio/channels/FileChannel:size	()J
    //   86: lstore 13
    //   88: lconst_0
    //   89: lstore 15
    //   91: goto +234 -> 325
    //   94: aload 11
    //   96: aload 10
    //   98: lload 15
    //   100: lload 18
    //   102: invokevirtual 506	java/nio/channels/FileChannel:transferFrom	(Ljava/nio/channels/ReadableByteChannel;JJ)J
    //   105: lstore 20
    //   107: lload 15
    //   109: lload 20
    //   111: ladd
    //   112: lstore 15
    //   114: goto +211 -> 325
    //   117: lload 13
    //   119: lload 15
    //   121: lsub
    //   122: lstore 18
    //   124: goto -30 -> 94
    //   127: aload 11
    //   129: invokestatic 509	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   132: aload 5
    //   134: invokestatic 360	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/OutputStream;)V
    //   137: aload 10
    //   139: invokestatic 509	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   142: aload 4
    //   144: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   147: aload_0
    //   148: invokevirtual 212	java/io/File:length	()J
    //   151: aload_1
    //   152: invokevirtual 212	java/io/File:length	()J
    //   155: lcmp
    //   156: ifeq +77 -> 233
    //   159: new 190	java/io/IOException
    //   162: dup
    //   163: new 101	java/lang/StringBuilder
    //   166: dup
    //   167: ldc_w 511
    //   170: invokespecial 197	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   173: aload_0
    //   174: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   177: ldc_w 513
    //   180: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: aload_1
    //   184: invokevirtual 140	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   187: ldc_w 515
    //   190: invokevirtual 111	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   193: invokevirtual 117	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   196: invokespecial 198	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   199: athrow
    //   200: astore 6
    //   202: aconst_null
    //   203: astore 9
    //   205: aconst_null
    //   206: astore 7
    //   208: aconst_null
    //   209: astore 8
    //   211: aload 9
    //   213: invokestatic 509	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   216: aload 7
    //   218: invokestatic 360	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/OutputStream;)V
    //   221: aload_3
    //   222: invokestatic 509	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/Closeable;)V
    //   225: aload 8
    //   227: invokestatic 176	org/apache/commons/io/IOUtils:closeQuietly	(Ljava/io/InputStream;)V
    //   230: aload 6
    //   232: athrow
    //   233: iload_2
    //   234: ifeq +12 -> 246
    //   237: aload_1
    //   238: aload_0
    //   239: invokevirtual 488	java/io/File:lastModified	()J
    //   242: invokevirtual 492	java/io/File:setLastModified	(J)Z
    //   245: pop
    //   246: return
    //   247: astore 6
    //   249: aload 4
    //   251: astore 8
    //   253: aconst_null
    //   254: astore 9
    //   256: aconst_null
    //   257: astore_3
    //   258: aconst_null
    //   259: astore 7
    //   261: goto -50 -> 211
    //   264: astore 6
    //   266: aload 5
    //   268: astore 7
    //   270: aload 4
    //   272: astore 8
    //   274: aconst_null
    //   275: astore 9
    //   277: aconst_null
    //   278: astore_3
    //   279: goto -68 -> 211
    //   282: astore 6
    //   284: aload 5
    //   286: astore 7
    //   288: aload 4
    //   290: astore 8
    //   292: aload 10
    //   294: astore_3
    //   295: aconst_null
    //   296: astore 9
    //   298: goto -87 -> 211
    //   301: astore 12
    //   303: aload 5
    //   305: astore 7
    //   307: aload 4
    //   309: astore 8
    //   311: aload 11
    //   313: astore 9
    //   315: aload 12
    //   317: astore 6
    //   319: aload 10
    //   321: astore_3
    //   322: goto -111 -> 211
    //   325: lload 15
    //   327: lload 13
    //   329: lcmp
    //   330: ifge -203 -> 127
    //   333: lload 13
    //   335: lload 15
    //   337: lsub
    //   338: ldc2_w 9
    //   341: lcmp
    //   342: ifle -225 -> 117
    //   345: ldc2_w 9
    //   348: lstore 18
    //   350: goto -256 -> 94
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	353	0	paramFile1	File
    //   0	353	1	paramFile2	File
    //   0	353	2	paramBoolean	boolean
    //   1	321	3	localObject1	Object
    //   55	253	4	localFileInputStream	FileInputStream
    //   65	239	5	localFileOutputStream	FileOutputStream
    //   200	31	6	localObject2	Object
    //   247	1	6	localObject3	Object
    //   264	1	6	localObject4	Object
    //   282	1	6	localObject5	Object
    //   317	1	6	localObject6	Object
    //   206	100	7	localObject7	Object
    //   209	101	8	localObject8	Object
    //   203	111	9	localObject9	Object
    //   72	248	10	localFileChannel1	java.nio.channels.FileChannel
    //   79	233	11	localFileChannel2	java.nio.channels.FileChannel
    //   301	15	12	localObject10	Object
    //   86	248	13	l1	long
    //   89	247	15	l2	long
    //   100	1	18	localObject11	Object
    //   122	227	18	l3	long
    //   105	5	20	l4	long
    // Exception table:
    //   from	to	target	type
    //   47	57	200	finally
    //   57	67	247	finally
    //   67	74	264	finally
    //   74	81	282	finally
    //   81	88	301	finally
    //   94	107	301	finally
  }
  
  public static void forceDelete(File paramFile)
  {
    if (paramFile.isDirectory()) {
      deleteDirectory(paramFile);
    }
    boolean bool;
    do
    {
      return;
      bool = paramFile.exists();
    } while (paramFile.delete());
    if (!bool) {
      throw new FileNotFoundException("File does not exist: " + paramFile);
    }
    throw new IOException("Unable to delete file: " + paramFile);
  }
  
  public static void forceDeleteOnExit(File paramFile)
  {
    if (paramFile.isDirectory())
    {
      deleteDirectoryOnExit(paramFile);
      return;
    }
    paramFile.deleteOnExit();
  }
  
  public static void forceMkdir(File paramFile)
  {
    if (paramFile.exists())
    {
      if (!paramFile.isDirectory()) {
        throw new IOException("File " + paramFile + " exists and is not a directory. Unable to create directory.");
      }
    }
    else if ((!paramFile.mkdirs()) && (!paramFile.isDirectory())) {
      throw new IOException("Unable to create directory " + paramFile);
    }
  }
  
  public static File getFile(File paramFile, String... paramVarArgs)
  {
    if (paramFile == null) {
      throw new NullPointerException("directorydirectory must not be null");
    }
    if (paramVarArgs == null) {
      throw new NullPointerException("names must not be null");
    }
    int i = paramVarArgs.length;
    int j = 0;
    while (j < i)
    {
      File localFile = new File(paramFile, paramVarArgs[j]);
      j++;
      paramFile = localFile;
    }
    return paramFile;
  }
  
  public static File getFile(String... paramVarArgs)
  {
    if (paramVarArgs == null) {
      throw new NullPointerException("names must not be null");
    }
    int i = paramVarArgs.length;
    Object localObject = null;
    int j = 0;
    if (j < i)
    {
      String str = paramVarArgs[j];
      if (localObject == null) {}
      for (File localFile = new File(str);; localFile = new File((File)localObject, str))
      {
        j++;
        localObject = localFile;
        break;
      }
    }
    return (File)localObject;
  }
  
  public static File getTempDirectory()
  {
    return new File(getTempDirectoryPath());
  }
  
  public static String getTempDirectoryPath()
  {
    return System.getProperty("java.io.tmpdir");
  }
  
  public static File getUserDirectory()
  {
    return new File(getUserDirectoryPath());
  }
  
  public static String getUserDirectoryPath()
  {
    return System.getProperty("user.home");
  }
  
  private static void innerListFiles(Collection<File> paramCollection, File paramFile, IOFileFilter paramIOFileFilter, boolean paramBoolean)
  {
    File[] arrayOfFile = paramFile.listFiles(paramIOFileFilter);
    if (arrayOfFile != null)
    {
      int i = arrayOfFile.length;
      int j = 0;
      if (j < i)
      {
        File localFile = arrayOfFile[j];
        if (localFile.isDirectory())
        {
          if (paramBoolean) {
            paramCollection.add(localFile);
          }
          innerListFiles(paramCollection, localFile, paramIOFileFilter, paramBoolean);
        }
        for (;;)
        {
          j++;
          break;
          paramCollection.add(localFile);
        }
      }
    }
  }
  
  public static boolean isFileNewer(File paramFile, long paramLong)
  {
    if (paramFile == null) {
      throw new IllegalArgumentException("No specified file");
    }
    if (!paramFile.exists()) {}
    while (paramFile.lastModified() <= paramLong) {
      return false;
    }
    return true;
  }
  
  public static boolean isFileNewer(File paramFile1, File paramFile2)
  {
    if (paramFile2 == null) {
      throw new IllegalArgumentException("No specified reference file");
    }
    if (!paramFile2.exists()) {
      throw new IllegalArgumentException("The reference file '" + paramFile2 + "' doesn't exist");
    }
    return isFileNewer(paramFile1, paramFile2.lastModified());
  }
  
  public static boolean isFileNewer(File paramFile, Date paramDate)
  {
    if (paramDate == null) {
      throw new IllegalArgumentException("No specified date");
    }
    return isFileNewer(paramFile, paramDate.getTime());
  }
  
  public static boolean isFileOlder(File paramFile, long paramLong)
  {
    if (paramFile == null) {
      throw new IllegalArgumentException("No specified file");
    }
    if (!paramFile.exists()) {}
    while (paramFile.lastModified() >= paramLong) {
      return false;
    }
    return true;
  }
  
  public static boolean isFileOlder(File paramFile1, File paramFile2)
  {
    if (paramFile2 == null) {
      throw new IllegalArgumentException("No specified reference file");
    }
    if (!paramFile2.exists()) {
      throw new IllegalArgumentException("The reference file '" + paramFile2 + "' doesn't exist");
    }
    return isFileOlder(paramFile1, paramFile2.lastModified());
  }
  
  public static boolean isFileOlder(File paramFile, Date paramDate)
  {
    if (paramDate == null) {
      throw new IllegalArgumentException("No specified date");
    }
    return isFileOlder(paramFile, paramDate.getTime());
  }
  
  public static boolean isSymlink(File paramFile)
  {
    if (paramFile == null) {
      throw new NullPointerException("File must not be null");
    }
    if (FilenameUtils.isSystemWindows()) {
      return false;
    }
    if (paramFile.getParent() != null) {
      paramFile = new File(paramFile.getParentFile().getCanonicalFile(), paramFile.getName());
    }
    return !paramFile.getCanonicalFile().equals(paramFile.getAbsoluteFile());
  }
  
  public static Iterator<File> iterateFiles(File paramFile, IOFileFilter paramIOFileFilter1, IOFileFilter paramIOFileFilter2)
  {
    return listFiles(paramFile, paramIOFileFilter1, paramIOFileFilter2).iterator();
  }
  
  public static Iterator<File> iterateFiles(File paramFile, String[] paramArrayOfString, boolean paramBoolean)
  {
    return listFiles(paramFile, paramArrayOfString, paramBoolean).iterator();
  }
  
  public static Iterator<File> iterateFilesAndDirs(File paramFile, IOFileFilter paramIOFileFilter1, IOFileFilter paramIOFileFilter2)
  {
    return listFilesAndDirs(paramFile, paramIOFileFilter1, paramIOFileFilter2).iterator();
  }
  
  public static LineIterator lineIterator(File paramFile)
  {
    return lineIterator(paramFile, null);
  }
  
  public static LineIterator lineIterator(File paramFile, String paramString)
  {
    FileInputStream localFileInputStream = null;
    try
    {
      localFileInputStream = openInputStream(paramFile);
      LineIterator localLineIterator = IOUtils.lineIterator(localFileInputStream, paramString);
      return localLineIterator;
    }
    catch (IOException localIOException)
    {
      IOUtils.closeQuietly(localFileInputStream);
      throw localIOException;
    }
    catch (RuntimeException localRuntimeException)
    {
      IOUtils.closeQuietly(localFileInputStream);
      throw localRuntimeException;
    }
  }
  
  public static Collection<File> listFiles(File paramFile, IOFileFilter paramIOFileFilter1, IOFileFilter paramIOFileFilter2)
  {
    validateListFilesParameters(paramFile, paramIOFileFilter1);
    IOFileFilter localIOFileFilter1 = setUpEffectiveFileFilter(paramIOFileFilter1);
    IOFileFilter localIOFileFilter2 = setUpEffectiveDirFilter(paramIOFileFilter2);
    LinkedList localLinkedList = new LinkedList();
    innerListFiles(localLinkedList, paramFile, FileFilterUtils.or(new IOFileFilter[] { localIOFileFilter1, localIOFileFilter2 }), false);
    return localLinkedList;
  }
  
  public static Collection<File> listFiles(File paramFile, String[] paramArrayOfString, boolean paramBoolean)
  {
    Object localObject;
    if (paramArrayOfString == null)
    {
      localObject = TrueFileFilter.INSTANCE;
      if (!paramBoolean) {
        break label40;
      }
    }
    label40:
    for (IOFileFilter localIOFileFilter = TrueFileFilter.INSTANCE;; localIOFileFilter = FalseFileFilter.INSTANCE)
    {
      return listFiles(paramFile, (IOFileFilter)localObject, localIOFileFilter);
      localObject = new SuffixFileFilter(toSuffixes(paramArrayOfString));
      break;
    }
  }
  
  public static Collection<File> listFilesAndDirs(File paramFile, IOFileFilter paramIOFileFilter1, IOFileFilter paramIOFileFilter2)
  {
    validateListFilesParameters(paramFile, paramIOFileFilter1);
    IOFileFilter localIOFileFilter1 = setUpEffectiveFileFilter(paramIOFileFilter1);
    IOFileFilter localIOFileFilter2 = setUpEffectiveDirFilter(paramIOFileFilter2);
    LinkedList localLinkedList = new LinkedList();
    if (paramFile.isDirectory()) {
      localLinkedList.add(paramFile);
    }
    innerListFiles(localLinkedList, paramFile, FileFilterUtils.or(new IOFileFilter[] { localIOFileFilter1, localIOFileFilter2 }), true);
    return localLinkedList;
  }
  
  public static void moveDirectory(File paramFile1, File paramFile2)
  {
    if (paramFile1 == null) {
      throw new NullPointerException("Source must not be null");
    }
    if (paramFile2 == null) {
      throw new NullPointerException("Destination must not be null");
    }
    if (!paramFile1.exists()) {
      throw new FileNotFoundException("Source '" + paramFile1 + "' does not exist");
    }
    if (!paramFile1.isDirectory()) {
      throw new IOException("Source '" + paramFile1 + "' is not a directory");
    }
    if (paramFile2.exists()) {
      throw new FileExistsException("Destination '" + paramFile2 + "' already exists");
    }
    if (!paramFile1.renameTo(paramFile2))
    {
      if (paramFile2.getCanonicalPath().startsWith(paramFile1.getCanonicalPath())) {
        throw new IOException("Cannot move directory: " + paramFile1 + " to a subdirectory of itself: " + paramFile2);
      }
      copyDirectory(paramFile1, paramFile2);
      deleteDirectory(paramFile1);
      if (paramFile1.exists()) {
        throw new IOException("Failed to delete original directory '" + paramFile1 + "' after copy to '" + paramFile2 + "'");
      }
    }
  }
  
  public static void moveDirectoryToDirectory(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    if (paramFile1 == null) {
      throw new NullPointerException("Source must not be null");
    }
    if (paramFile2 == null) {
      throw new NullPointerException("Destination directory must not be null");
    }
    if ((!paramFile2.exists()) && (paramBoolean)) {
      paramFile2.mkdirs();
    }
    if (!paramFile2.exists()) {
      throw new FileNotFoundException("Destination directory '" + paramFile2 + "' does not exist [createDestDir=" + paramBoolean + "]");
    }
    if (!paramFile2.isDirectory()) {
      throw new IOException("Destination '" + paramFile2 + "' is not a directory");
    }
    moveDirectory(paramFile1, new File(paramFile2, paramFile1.getName()));
  }
  
  public static void moveFile(File paramFile1, File paramFile2)
  {
    if (paramFile1 == null) {
      throw new NullPointerException("Source must not be null");
    }
    if (paramFile2 == null) {
      throw new NullPointerException("Destination must not be null");
    }
    if (!paramFile1.exists()) {
      throw new FileNotFoundException("Source '" + paramFile1 + "' does not exist");
    }
    if (paramFile1.isDirectory()) {
      throw new IOException("Source '" + paramFile1 + "' is a directory");
    }
    if (paramFile2.exists()) {
      throw new FileExistsException("Destination '" + paramFile2 + "' already exists");
    }
    if (paramFile2.isDirectory()) {
      throw new IOException("Destination '" + paramFile2 + "' is a directory");
    }
    if (!paramFile1.renameTo(paramFile2))
    {
      copyFile(paramFile1, paramFile2);
      if (!paramFile1.delete())
      {
        deleteQuietly(paramFile2);
        throw new IOException("Failed to delete original file '" + paramFile1 + "' after copy to '" + paramFile2 + "'");
      }
    }
  }
  
  public static void moveFileToDirectory(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    if (paramFile1 == null) {
      throw new NullPointerException("Source must not be null");
    }
    if (paramFile2 == null) {
      throw new NullPointerException("Destination directory must not be null");
    }
    if ((!paramFile2.exists()) && (paramBoolean)) {
      paramFile2.mkdirs();
    }
    if (!paramFile2.exists()) {
      throw new FileNotFoundException("Destination directory '" + paramFile2 + "' does not exist [createDestDir=" + paramBoolean + "]");
    }
    if (!paramFile2.isDirectory()) {
      throw new IOException("Destination '" + paramFile2 + "' is not a directory");
    }
    moveFile(paramFile1, new File(paramFile2, paramFile1.getName()));
  }
  
  public static void moveToDirectory(File paramFile1, File paramFile2, boolean paramBoolean)
  {
    if (paramFile1 == null) {
      throw new NullPointerException("Source must not be null");
    }
    if (paramFile2 == null) {
      throw new NullPointerException("Destination must not be null");
    }
    if (!paramFile1.exists()) {
      throw new FileNotFoundException("Source '" + paramFile1 + "' does not exist");
    }
    if (paramFile1.isDirectory())
    {
      moveDirectoryToDirectory(paramFile1, paramFile2, paramBoolean);
      return;
    }
    moveFileToDirectory(paramFile1, paramFile2, paramBoolean);
  }
  
  public static FileInputStream openInputStream(File paramFile)
  {
    if (paramFile.exists())
    {
      if (paramFile.isDirectory()) {
        throw new IOException("File '" + paramFile + "' exists but is a directory");
      }
      if (!paramFile.canRead()) {
        throw new IOException("File '" + paramFile + "' cannot be read");
      }
    }
    else
    {
      throw new FileNotFoundException("File '" + paramFile + "' does not exist");
    }
    return new FileInputStream(paramFile);
  }
  
  public static FileOutputStream openOutputStream(File paramFile)
  {
    return openOutputStream(paramFile, false);
  }
  
  public static FileOutputStream openOutputStream(File paramFile, boolean paramBoolean)
  {
    if (paramFile.exists())
    {
      if (paramFile.isDirectory()) {
        throw new IOException("File '" + paramFile + "' exists but is a directory");
      }
      if (!paramFile.canWrite()) {
        throw new IOException("File '" + paramFile + "' cannot be written to");
      }
    }
    else
    {
      File localFile = paramFile.getParentFile();
      if ((localFile != null) && (!localFile.mkdirs()) && (!localFile.isDirectory())) {
        throw new IOException("Directory '" + localFile + "' could not be created");
      }
    }
    return new FileOutputStream(paramFile, paramBoolean);
  }
  
  public static byte[] readFileToByteArray(File paramFile)
  {
    FileInputStream localFileInputStream = null;
    try
    {
      localFileInputStream = openInputStream(paramFile);
      byte[] arrayOfByte = IOUtils.toByteArray(localFileInputStream, paramFile.length());
      return arrayOfByte;
    }
    finally
    {
      IOUtils.closeQuietly(localFileInputStream);
    }
  }
  
  public static String readFileToString(File paramFile)
  {
    return readFileToString(paramFile, Charset.defaultCharset());
  }
  
  public static String readFileToString(File paramFile, String paramString)
  {
    return readFileToString(paramFile, Charsets.toCharset(paramString));
  }
  
  public static String readFileToString(File paramFile, Charset paramCharset)
  {
    FileInputStream localFileInputStream = null;
    try
    {
      localFileInputStream = openInputStream(paramFile);
      String str = IOUtils.toString(localFileInputStream, Charsets.toCharset(paramCharset));
      return str;
    }
    finally
    {
      IOUtils.closeQuietly(localFileInputStream);
    }
  }
  
  public static List<String> readLines(File paramFile)
  {
    return readLines(paramFile, Charset.defaultCharset());
  }
  
  public static List<String> readLines(File paramFile, String paramString)
  {
    return readLines(paramFile, Charsets.toCharset(paramString));
  }
  
  public static List<String> readLines(File paramFile, Charset paramCharset)
  {
    FileInputStream localFileInputStream = null;
    try
    {
      localFileInputStream = openInputStream(paramFile);
      List localList = IOUtils.readLines(localFileInputStream, Charsets.toCharset(paramCharset));
      return localList;
    }
    finally
    {
      IOUtils.closeQuietly(localFileInputStream);
    }
  }
  
  private static IOFileFilter setUpEffectiveDirFilter(IOFileFilter paramIOFileFilter)
  {
    if (paramIOFileFilter == null) {
      return FalseFileFilter.INSTANCE;
    }
    IOFileFilter[] arrayOfIOFileFilter = new IOFileFilter[2];
    arrayOfIOFileFilter[0] = paramIOFileFilter;
    arrayOfIOFileFilter[1] = DirectoryFileFilter.INSTANCE;
    return FileFilterUtils.and(arrayOfIOFileFilter);
  }
  
  private static IOFileFilter setUpEffectiveFileFilter(IOFileFilter paramIOFileFilter)
  {
    IOFileFilter[] arrayOfIOFileFilter = new IOFileFilter[2];
    arrayOfIOFileFilter[0] = paramIOFileFilter;
    arrayOfIOFileFilter[1] = FileFilterUtils.notFileFilter(DirectoryFileFilter.INSTANCE);
    return FileFilterUtils.and(arrayOfIOFileFilter);
  }
  
  public static long sizeOf(File paramFile)
  {
    if (!paramFile.exists()) {
      throw new IllegalArgumentException(paramFile + " does not exist");
    }
    if (paramFile.isDirectory()) {
      return sizeOfDirectory(paramFile);
    }
    return paramFile.length();
  }
  
  public static BigInteger sizeOfAsBigInteger(File paramFile)
  {
    if (!paramFile.exists()) {
      throw new IllegalArgumentException(paramFile + " does not exist");
    }
    if (paramFile.isDirectory()) {
      return sizeOfDirectoryAsBigInteger(paramFile);
    }
    return BigInteger.valueOf(paramFile.length());
  }
  
  public static long sizeOfDirectory(File paramFile)
  {
    checkDirectory(paramFile);
    File[] arrayOfFile = paramFile.listFiles();
    long l1;
    if (arrayOfFile == null) {
      l1 = 0L;
    }
    label70:
    for (;;)
    {
      return l1;
      int i = arrayOfFile.length;
      int j = 0;
      l1 = 0L;
      for (;;)
      {
        if (j >= i) {
          break label70;
        }
        File localFile = arrayOfFile[j];
        try
        {
          if (!isSymlink(localFile))
          {
            long l2 = sizeOf(localFile);
            l1 += l2;
            if (l1 < 0L) {
              break;
            }
          }
          j++;
        }
        catch (IOException localIOException)
        {
          for (;;) {}
        }
      }
    }
  }
  
  public static BigInteger sizeOfDirectoryAsBigInteger(File paramFile)
  {
    checkDirectory(paramFile);
    File[] arrayOfFile = paramFile.listFiles();
    Object localObject;
    if (arrayOfFile == null) {
      localObject = BigInteger.ZERO;
    }
    for (;;)
    {
      return (BigInteger)localObject;
      BigInteger localBigInteger1 = BigInteger.ZERO;
      int i = arrayOfFile.length;
      localObject = localBigInteger1;
      int j = 0;
      while (j < i)
      {
        File localFile = arrayOfFile[j];
        try
        {
          if (!isSymlink(localFile))
          {
            BigInteger localBigInteger2 = ((BigInteger)localObject).add(BigInteger.valueOf(sizeOf(localFile)));
            localObject = localBigInteger2;
          }
          j++;
        }
        catch (IOException localIOException)
        {
          for (;;) {}
        }
      }
    }
  }
  
  public static File toFile(URL paramURL)
  {
    if ((paramURL == null) || (!"file".equalsIgnoreCase(paramURL.getProtocol()))) {
      return null;
    }
    return new File(decodeUrl(paramURL.getFile().replace('/', File.separatorChar)));
  }
  
  public static File[] toFiles(URL[] paramArrayOfURL)
  {
    if ((paramArrayOfURL == null) || (paramArrayOfURL.length == 0)) {
      return EMPTY_FILE_ARRAY;
    }
    File[] arrayOfFile = new File[paramArrayOfURL.length];
    for (int i = 0; i < paramArrayOfURL.length; i++)
    {
      URL localURL = paramArrayOfURL[i];
      if (localURL != null)
      {
        if (!localURL.getProtocol().equals("file")) {
          throw new IllegalArgumentException("URL could not be converted to a File: " + localURL);
        }
        arrayOfFile[i] = toFile(localURL);
      }
    }
    return arrayOfFile;
  }
  
  private static String[] toSuffixes(String[] paramArrayOfString)
  {
    String[] arrayOfString = new String[paramArrayOfString.length];
    for (int i = 0; i < paramArrayOfString.length; i++) {
      arrayOfString[i] = ("." + paramArrayOfString[i]);
    }
    return arrayOfString;
  }
  
  public static URL[] toURLs(File[] paramArrayOfFile)
  {
    URL[] arrayOfURL = new URL[paramArrayOfFile.length];
    for (int i = 0; i < arrayOfURL.length; i++) {
      arrayOfURL[i] = paramArrayOfFile[i].toURI().toURL();
    }
    return arrayOfURL;
  }
  
  public static void touch(File paramFile)
  {
    if (!paramFile.exists()) {
      IOUtils.closeQuietly(openOutputStream(paramFile));
    }
    if (!paramFile.setLastModified(System.currentTimeMillis())) {
      throw new IOException("Unable to set the last modification time for " + paramFile);
    }
  }
  
  private static void validateListFilesParameters(File paramFile, IOFileFilter paramIOFileFilter)
  {
    if (!paramFile.isDirectory()) {
      throw new IllegalArgumentException("Parameter 'directory' is not a directory");
    }
    if (paramIOFileFilter == null) {
      throw new NullPointerException("Parameter 'fileFilter' is null");
    }
  }
  
  public static boolean waitFor(File paramFile, int paramInt)
  {
    int i = 0;
    int j = 0;
    int k;
    if (!paramFile.exists())
    {
      k = i + 1;
      if (i < 10) {
        break label57;
      }
      int m = j + 1;
      if (j > paramInt) {
        return false;
      }
      j = m;
    }
    label57:
    for (i = 0;; i = k)
    {
      try
      {
        Thread.sleep(100L);
      }
      catch (InterruptedException localInterruptedException) {}catch (Exception localException) {}
      return true;
    }
  }
  
  public static void write(File paramFile, CharSequence paramCharSequence)
  {
    write(paramFile, paramCharSequence, Charset.defaultCharset(), false);
  }
  
  public static void write(File paramFile, CharSequence paramCharSequence, String paramString)
  {
    write(paramFile, paramCharSequence, paramString, false);
  }
  
  public static void write(File paramFile, CharSequence paramCharSequence, String paramString, boolean paramBoolean)
  {
    write(paramFile, paramCharSequence, Charsets.toCharset(paramString), paramBoolean);
  }
  
  public static void write(File paramFile, CharSequence paramCharSequence, Charset paramCharset)
  {
    write(paramFile, paramCharSequence, paramCharset, false);
  }
  
  public static void write(File paramFile, CharSequence paramCharSequence, Charset paramCharset, boolean paramBoolean)
  {
    if (paramCharSequence == null) {}
    for (String str = null;; str = paramCharSequence.toString())
    {
      writeStringToFile(paramFile, str, paramCharset, paramBoolean);
      return;
    }
  }
  
  public static void write(File paramFile, CharSequence paramCharSequence, boolean paramBoolean)
  {
    write(paramFile, paramCharSequence, Charset.defaultCharset(), paramBoolean);
  }
  
  public static void writeByteArrayToFile(File paramFile, byte[] paramArrayOfByte)
  {
    writeByteArrayToFile(paramFile, paramArrayOfByte, false);
  }
  
  public static void writeByteArrayToFile(File paramFile, byte[] paramArrayOfByte, boolean paramBoolean)
  {
    FileOutputStream localFileOutputStream = null;
    try
    {
      localFileOutputStream = openOutputStream(paramFile, paramBoolean);
      localFileOutputStream.write(paramArrayOfByte);
      localFileOutputStream.close();
      return;
    }
    finally
    {
      IOUtils.closeQuietly(localFileOutputStream);
    }
  }
  
  public static void writeLines(File paramFile, String paramString, Collection<?> paramCollection)
  {
    writeLines(paramFile, paramString, paramCollection, null, false);
  }
  
  public static void writeLines(File paramFile, String paramString1, Collection<?> paramCollection, String paramString2)
  {
    writeLines(paramFile, paramString1, paramCollection, paramString2, false);
  }
  
  public static void writeLines(File paramFile, String paramString1, Collection<?> paramCollection, String paramString2, boolean paramBoolean)
  {
    FileOutputStream localFileOutputStream = null;
    try
    {
      localFileOutputStream = openOutputStream(paramFile, paramBoolean);
      BufferedOutputStream localBufferedOutputStream = new BufferedOutputStream(localFileOutputStream);
      IOUtils.writeLines(paramCollection, paramString2, localBufferedOutputStream, paramString1);
      localBufferedOutputStream.flush();
      localFileOutputStream.close();
      return;
    }
    finally
    {
      IOUtils.closeQuietly(localFileOutputStream);
    }
  }
  
  public static void writeLines(File paramFile, String paramString, Collection<?> paramCollection, boolean paramBoolean)
  {
    writeLines(paramFile, paramString, paramCollection, null, paramBoolean);
  }
  
  public static void writeLines(File paramFile, Collection<?> paramCollection)
  {
    writeLines(paramFile, null, paramCollection, null, false);
  }
  
  public static void writeLines(File paramFile, Collection<?> paramCollection, String paramString)
  {
    writeLines(paramFile, null, paramCollection, paramString, false);
  }
  
  public static void writeLines(File paramFile, Collection<?> paramCollection, String paramString, boolean paramBoolean)
  {
    writeLines(paramFile, null, paramCollection, paramString, paramBoolean);
  }
  
  public static void writeLines(File paramFile, Collection<?> paramCollection, boolean paramBoolean)
  {
    writeLines(paramFile, null, paramCollection, null, paramBoolean);
  }
  
  public static void writeStringToFile(File paramFile, String paramString)
  {
    writeStringToFile(paramFile, paramString, Charset.defaultCharset(), false);
  }
  
  public static void writeStringToFile(File paramFile, String paramString1, String paramString2)
  {
    writeStringToFile(paramFile, paramString1, paramString2, false);
  }
  
  public static void writeStringToFile(File paramFile, String paramString1, String paramString2, boolean paramBoolean)
  {
    writeStringToFile(paramFile, paramString1, Charsets.toCharset(paramString2), paramBoolean);
  }
  
  public static void writeStringToFile(File paramFile, String paramString, Charset paramCharset)
  {
    writeStringToFile(paramFile, paramString, paramCharset, false);
  }
  
  public static void writeStringToFile(File paramFile, String paramString, Charset paramCharset, boolean paramBoolean)
  {
    FileOutputStream localFileOutputStream = null;
    try
    {
      localFileOutputStream = openOutputStream(paramFile, paramBoolean);
      IOUtils.write(paramString, localFileOutputStream, paramCharset);
      localFileOutputStream.close();
      return;
    }
    finally
    {
      IOUtils.closeQuietly(localFileOutputStream);
    }
  }
  
  public static void writeStringToFile(File paramFile, String paramString, boolean paramBoolean)
  {
    writeStringToFile(paramFile, paramString, Charset.defaultCharset(), paramBoolean);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/org/apache/commons/io/FileUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */