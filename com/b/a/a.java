package com.b.a;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import java.lang.reflect.Method;

public final class a
{
  private static String g;
  public boolean a;
  public boolean b;
  public boolean c;
  public boolean d;
  public View e;
  public View f;
  private final a h;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 19) {}
    try
    {
      Method localMethod = Class.forName("android.os.SystemProperties").getDeclaredMethod("get", new Class[] { String.class });
      localMethod.setAccessible(true);
      g = (String)localMethod.invoke(null, new Object[] { "qemu.hw.mainkeys" });
      return;
    }
    catch (Throwable localThrowable)
    {
      g = null;
    }
  }
  
  @TargetApi(19)
  public a(Activity paramActivity)
  {
    Window localWindow = paramActivity.getWindow();
    ViewGroup localViewGroup = (ViewGroup)localWindow.getDecorView();
    TypedArray localTypedArray;
    if (Build.VERSION.SDK_INT >= 19) {
      localTypedArray = paramActivity.obtainStyledAttributes(new int[] { 16843759, 16843760 });
    }
    for (;;)
    {
      try
      {
        this.a = localTypedArray.getBoolean(0, false);
        this.b = localTypedArray.getBoolean(1, false);
        localTypedArray.recycle();
        WindowManager.LayoutParams localLayoutParams = localWindow.getAttributes();
        if ((0x4000000 & localLayoutParams.flags) != 0) {
          this.a = true;
        }
        if ((0x8000000 & localLayoutParams.flags) != 0) {
          this.b = true;
        }
        this.h = new a(paramActivity, this.a, this.b, (byte)0);
        if (!this.h.b) {
          this.b = false;
        }
        if (this.a)
        {
          this.e = new View(paramActivity);
          FrameLayout.LayoutParams localLayoutParams2 = new FrameLayout.LayoutParams(-1, this.h.a);
          localLayoutParams2.gravity = 48;
          if ((this.b) && (!this.h.a())) {
            localLayoutParams2.rightMargin = this.h.d;
          }
          this.e.setLayoutParams(localLayoutParams2);
          this.e.setBackgroundColor(-1728053248);
          this.e.setVisibility(8);
          localViewGroup.addView(this.e);
        }
        if (this.b)
        {
          this.f = new View(paramActivity);
          if (this.h.a())
          {
            localLayoutParams1 = new FrameLayout.LayoutParams(-1, this.h.c);
            localLayoutParams1.gravity = 80;
            this.f.setLayoutParams(localLayoutParams1);
            this.f.setBackgroundColor(-1728053248);
            this.f.setVisibility(8);
            localViewGroup.addView(this.f);
          }
        }
        else
        {
          return;
        }
      }
      finally
      {
        localTypedArray.recycle();
      }
      FrameLayout.LayoutParams localLayoutParams1 = new FrameLayout.LayoutParams(this.h.d, -1);
      localLayoutParams1.gravity = 5;
    }
  }
  
  public static final class a
  {
    final int a;
    final boolean b;
    final int c;
    final int d;
    private final boolean e;
    private final boolean f;
    private final int g;
    private final boolean h;
    private final float i;
    
    private a(Activity paramActivity, boolean paramBoolean1, boolean paramBoolean2)
    {
      Resources localResources1 = paramActivity.getResources();
      DisplayMetrics localDisplayMetrics;
      label67:
      TypedValue localTypedValue;
      if (localResources1.getConfiguration().orientation == j)
      {
        int m = j;
        this.h = m;
        localDisplayMetrics = new DisplayMetrics();
        if (Build.VERSION.SDK_INT < 16) {
          break label276;
        }
        paramActivity.getWindowManager().getDefaultDisplay().getRealMetrics(localDisplayMetrics);
        this.i = Math.min(localDisplayMetrics.widthPixels / localDisplayMetrics.density, localDisplayMetrics.heightPixels / localDisplayMetrics.density);
        this.a = a(localResources1, "status_bar_height");
        if (Build.VERSION.SDK_INT < 14) {
          break label318;
        }
        localTypedValue = new TypedValue();
        paramActivity.getTheme().resolveAttribute(16843499, localTypedValue, j);
      }
      label195:
      label204:
      label240:
      label276:
      label306:
      label312:
      label318:
      for (int i1 = TypedValue.complexToDimensionPixelSize(localTypedValue.data, paramActivity.getResources().getDisplayMetrics());; i1 = 0)
      {
        this.g = i1;
        Resources localResources2 = paramActivity.getResources();
        String str;
        int i2;
        int i3;
        if ((Build.VERSION.SDK_INT >= 14) && (a(paramActivity))) {
          if (this.h)
          {
            str = "navigation_bar_height";
            i2 = a(localResources2, str);
            this.c = i2;
            Resources localResources3 = paramActivity.getResources();
            if ((Build.VERSION.SDK_INT < 14) || (!a(paramActivity))) {
              break label306;
            }
            i3 = a(localResources3, "navigation_bar_width");
            this.d = i3;
            if (this.c <= 0) {
              break label312;
            }
          }
        }
        for (;;)
        {
          this.b = j;
          this.e = paramBoolean1;
          this.f = paramBoolean2;
          return;
          int n = 0;
          break;
          paramActivity.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
          break label67;
          str = "navigation_bar_height_landscape";
          break label195;
          i2 = 0;
          break label204;
          i3 = 0;
          break label240;
          int k = 0;
        }
      }
    }
    
    private static int a(Resources paramResources, String paramString)
    {
      int j = paramResources.getIdentifier(paramString, "dimen", "android");
      int k = 0;
      if (j > 0) {
        k = paramResources.getDimensionPixelSize(j);
      }
      return k;
    }
    
    @TargetApi(14)
    private static boolean a(Context paramContext)
    {
      Resources localResources = paramContext.getResources();
      int j = localResources.getIdentifier("config_showNavigationBar", "bool", "android");
      boolean bool;
      if (j != 0)
      {
        bool = localResources.getBoolean(j);
        if (!"1".equals(a.a())) {}
      }
      while (ViewConfiguration.get(paramContext).hasPermanentMenuKey())
      {
        return false;
        if (!"0".equals(a.a())) {
          break;
        }
        return true;
      }
      return true;
      return bool;
    }
    
    public final boolean a()
    {
      return (this.i >= 600.0F) || (this.h);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */