package com.cgollner.systemmonitor;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.b.a.j;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MonitorView
  extends View
{
  public List<Float> a;
  public double b;
  public boolean c;
  public boolean d;
  public boolean e;
  public boolean f;
  public Rect g;
  public boolean h;
  private Paint i;
  private Paint j;
  private Paint k;
  private Paint l;
  private Paint m;
  private Path n;
  private int o;
  private int p;
  private float q;
  private float r;
  private boolean s = true;
  private boolean t = true;
  private boolean u = true;
  private boolean v = true;
  private float w;
  private int x;
  
  public MonitorView(Context paramContext)
  {
    super(paramContext);
    a(paramContext);
  }
  
  public MonitorView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramAttributeSet, paramContext);
  }
  
  public MonitorView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramAttributeSet, paramContext);
  }
  
  public static float a(float paramFloat, Resources paramResources)
  {
    return TypedValue.applyDimension(1, paramFloat, paramResources.getDisplayMetrics());
  }
  
  private void a(Context paramContext)
  {
    if (this.a == null) {
      this.a = new ArrayList();
    }
    this.i = new Paint();
    this.i.setAntiAlias(true);
    this.i.setColor(Color.argb(200, 17, 125, 187));
    this.i.setStyle(Paint.Style.STROKE);
    this.i.setStrokeWidth(2.0F);
    this.j = new Paint();
    this.j.setAntiAlias(true);
    this.j.setColor(Color.argb(50, 17, 125, 187));
    this.k = new Paint();
    this.k.setAntiAlias(true);
    this.k.setColor(Color.rgb(217, 234, 244));
    this.l = new Paint();
    this.l.setColor(getResources().getColor(17170432));
    this.l.setTextAlign(Paint.Align.CENTER);
    this.l.setTextSize(a(10.0F, getResources()));
    this.m = new Paint();
    this.m.setAntiAlias(true);
    this.m.setColor(getResources().getColor(17170432));
    this.m.setTextAlign(Paint.Align.CENTER);
    this.m.setTextSize(a(10.0F, getResources()));
    if (PreferenceManager.getDefaultSharedPreferences(App.a).getInt("theme", 0) == 1) {
      this.m.setColor(-1);
    }
    for (;;)
    {
      this.n = new Path();
      this.q = a(20.0F, getResources());
      this.p = -1;
      this.b = -1.0D;
      this.e = true;
      this.g = new Rect();
      String str = (String)i.a(1000, paramContext);
      this.m.getTextBounds(str, 0, str.length(), this.g);
      Rect localRect = this.g;
      localRect.right = ((int)(localRect.right + a(4.0F, paramContext.getResources())));
      return;
      this.m.setColor(-16777216);
    }
  }
  
  private void a(AttributeSet paramAttributeSet, Context paramContext)
  {
    a(paramContext);
    TypedArray localTypedArray = getContext().getTheme().obtainStyledAttributes(paramAttributeSet, a.j.MonitorView, 0, 0);
    try
    {
      this.i.setStrokeWidth(a(localTypedArray.getFloat(a.j.MonitorView_lineWidth, 2.0F), getResources()));
      this.i.setColor(localTypedArray.getInt(a.j.MonitorView_lineColor, Color.argb(200, 17, 125, 187)));
      this.j.setColor(localTypedArray.getInt(a.j.MonitorView_fillColor, Color.argb(50, 17, 125, 187)));
      this.p = localTypedArray.getInt(a.j.MonitorView_backgroundColor, Color.rgb(255, 255, 255));
      this.k.setColor(localTypedArray.getInt(a.j.MonitorView_gridColor, Color.rgb(217, 234, 244)));
      this.k.setStrokeWidth(a(localTypedArray.getFloat(a.j.MonitorView_gridWidth, 1.0F), getResources()));
      this.q = a(localTypedArray.getFloat(a.j.MonitorView_valuesMargin, 20.0F), getResources());
      this.e = localTypedArray.getBoolean(a.j.MonitorView_drawBackground, true);
      return;
    }
    finally
    {
      if (localTypedArray != null) {
        localTypedArray.recycle();
      }
    }
  }
  
  public final void a(float paramFloat, boolean paramBoolean)
  {
    this.a.add(Float.valueOf(paramFloat));
    if ((this.a.size() > this.o) && (this.o > 0)) {
      this.a.remove(0);
    }
    if (paramBoolean) {
      postInvalidate();
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    float f1;
    float f2;
    label30:
    float f3;
    label71:
    int i3;
    label102:
    int i4;
    if (paramCanvas != null) {
      if (getHeight() > 0)
      {
        f1 = getHeight();
        if (getWidth() <= 0) {
          break label389;
        }
        f2 = getWidth();
        f3 = f1 / 10.0F;
        this.o = Math.max(this.o, 2 + (int)(f2 / this.q));
        if (!this.h) {
          break label398;
        }
        paramCanvas.drawColor(this.p);
        this.w = 0.0F;
        if (!this.f) {
          break label548;
        }
        if (9.0F * this.m.getTextSize() <= f1) {
          break label419;
        }
        i3 = 1;
        if (i3 == 0) {
          break label1195;
        }
        if (4.0F * this.m.getTextSize() <= f1) {
          break label425;
        }
        i4 = 3;
      }
    }
    for (;;)
    {
      label125:
      float f10 = a(8.0F, getResources());
      this.m.setTextAlign(Paint.Align.LEFT);
      int i5 = 1;
      label150:
      if (i5 <= 9)
      {
        Object localObject = (String)i.a((int)((10 - i5) / 10.0F * this.b), getContext());
        if (this.x == a.c)
        {
          boolean bool = this.b < 0.0D;
          int i7 = 0;
          if (bool)
          {
            int i8 = this.a.size();
            i7 = 0;
            if (i8 > 1)
            {
              this.b = ((Float)Collections.max(this.a)).floatValue();
              i7 = 1;
            }
          }
          String str2 = i.a((10.0F - i5) / 10.0F * this.b, 1000L);
          if (i7 != 0) {
            this.b = -1.0D;
          }
          localObject = str2;
          label291:
          if (i4 >= 3) {
            break label483;
          }
          paramCanvas.drawText((String)localObject, f10, f3 * i5 + this.m.getTextSize() / 3.0F, this.m);
        }
        for (;;)
        {
          this.m.getTextBounds((String)localObject, 0, ((String)localObject).length(), this.g);
          this.w = Math.max(this.w, this.g.right + 2.0F * f10);
          i5 += i4;
          break label150;
          f1 = paramCanvas.getHeight();
          break;
          label389:
          f2 = paramCanvas.getWidth();
          break label30;
          label398:
          if (!this.e) {
            break label71;
          }
          paramCanvas.drawColor(this.p, PorterDuff.Mode.CLEAR);
          break label71;
          label419:
          i3 = 0;
          break label102;
          label425:
          i4 = 2;
          break label125;
          if (this.x != a.a) {
            break label291;
          }
          int i6 = (int)(100.0F * ((10.0F - i5) / 10.0F));
          localObject = i6 + "%";
          break label291;
          label483:
          paramCanvas.drawText((String)localObject, f10, f3 * i5 + this.m.getTextSize() / 1.5F, this.m);
        }
      }
      paramCanvas.drawLine(0.0F, f1 - this.i.getStrokeWidth() / 2.0F, f2, f1 - this.i.getStrokeWidth() / 2.0F, this.i);
      label548:
      float f4 = f2 - this.w;
      paramCanvas.save();
      paramCanvas.translate(this.w, 0.0F);
      paramCanvas.clipRect(0.0F, 0.0F, f4, f1);
      for (int i1 = 0; i1 < 10; i1++) {
        paramCanvas.drawLine(0.0F, f3 * i1, f4, f3 * i1, this.k);
      }
      for (float f5 = this.r; f5 <= f4 + this.q; f5 += 4.0F * this.q)
      {
        Paint localPaint3 = this.k;
        paramCanvas.drawLine(f5, 0.0F, f5, f1, localPaint3);
      }
      this.r -= this.q;
      if (this.r <= -(4.0F * this.q)) {
        this.r = 0.0F;
      }
      this.n.reset();
      this.n.moveTo(f4, f1);
      int i2 = -1 + this.a.size();
      float f6 = f4;
      if ((i2 >= 0) && (f6 >= -100.0F))
      {
        if (this.b < 0.0D)
        {
          float f9 = f1 - f1 * (((Float)this.a.get(i2)).floatValue() / 100.0F);
          this.n.lineTo(f6, f9);
        }
        for (;;)
        {
          f6 -= this.q;
          i2--;
          break;
          double d1 = ((Float)this.a.get(i2)).floatValue() / this.b;
          float f8 = (float)(f1 - d1 * f1);
          this.n.lineTo(f6, f8);
        }
      }
      float f7 = f6 + this.q;
      this.n.lineTo(f7, f1);
      this.n.close();
      paramCanvas.drawPath(this.n, this.j);
      paramCanvas.drawPath(this.n, this.i);
      if ((this.s) || (this.f)) {
        paramCanvas.drawLine(0.0F, 0.0F, 0.0F, f1, this.i);
      }
      if (this.t)
      {
        Paint localPaint2 = this.i;
        paramCanvas.drawLine(f4, 0.0F, f4, f1, localPaint2);
      }
      if (this.u) {
        paramCanvas.drawLine(0.0F, 0.0F, f4, 0.0F, this.i);
      }
      if (this.v)
      {
        Paint localPaint1 = this.i;
        paramCanvas.drawLine(0.0F, f1, f4, f1, localPaint1);
      }
      try
      {
        paramCanvas.restore();
        if (this.d)
        {
          paramCanvas.drawColor(-16777216);
          str1 = App.a.getString(a.h.unlicensed_dialog_title);
          localRect = new Rect();
          this.l.setTextSize(0.2F * Math.min(f4, f1));
          do
          {
            this.l.setTextSize(this.l.getTextSize() - 0.1F);
            this.l.getTextBounds(str1, 0, str1.length(), localRect);
          } while (localRect.right >= f4);
          paramCanvas.drawText(str1, f4 / 2.0F, f1 / 2.0F + this.l.getTextSize() / 2.0F, this.l);
        }
        while (!this.c)
        {
          String str1;
          Rect localRect;
          return;
        }
        this.l.setTextSize(0.2F * Math.min(f4, f1));
        paramCanvas.drawText("OFFLINE", f4 / 2.0F, f1 / 2.0F + this.l.getTextSize() / 2.0F, this.l);
        return;
      }
      catch (Exception localException)
      {
        return;
      }
      label1195:
      i4 = 1;
    }
  }
  
  public void setBgColor(int paramInt)
  {
    this.p = paramInt;
    this.e = true;
  }
  
  public void setColors(int[] paramArrayOfInt)
  {
    int i1 = paramArrayOfInt[0];
    int i2 = paramArrayOfInt[1];
    int i3 = paramArrayOfInt[2];
    this.j.setColor(i1);
    this.k.setColor(i2);
    this.i.setColor(i3);
  }
  
  public final void setDrawBorders$1d54120b(boolean paramBoolean)
  {
    this.s = paramBoolean;
    this.t = false;
    this.u = false;
    this.v = true;
  }
  
  public void setFillColor(int paramInt)
  {
    this.j.setColor(paramInt);
  }
  
  public void setGridColor(int paramInt)
  {
    this.k.setColor(paramInt);
  }
  
  public void setGridLineWidth(float paramFloat)
  {
    this.k.setStrokeWidth(a(paramFloat, getResources()));
  }
  
  public void setGridWidth(float paramFloat)
  {
    this.k.setStrokeWidth(a(paramFloat, getResources()));
  }
  
  public void setLabelColor(int paramInt)
  {
    this.m.setColor(paramInt);
  }
  
  public void setLabelType$36c8c2f0(int paramInt)
  {
    this.x = paramInt;
    if (paramInt == 0)
    {
      this.f = false;
      return;
    }
    this.f = true;
    this.g = new Rect();
    String str = "";
    if (this.x == a.b) {
      str = (String)i.a(1000, getContext());
    }
    for (;;)
    {
      this.m.getTextBounds(str, 0, str.length(), this.g);
      Rect localRect = this.g;
      localRect.right = ((int)(localRect.right + a(4.0F, getResources())));
      return;
      if (this.x == a.c) {
        str = i.a(921600.0D, 1000L);
      } else if (this.x == a.a) {
        str = "100%";
      }
    }
  }
  
  public void setLineColor(int paramInt)
  {
    this.i.setColor(paramInt);
  }
  
  public void setLineWidth(float paramFloat)
  {
    this.i.setStrokeWidth(a(paramFloat, getResources()));
  }
  
  public void setMargin(float paramFloat)
  {
    this.q = paramFloat;
  }
  
  public void setTextSize(int paramInt)
  {
    this.m.setTextSize(a(paramInt, getResources()));
    String str = (String)i.a(1000, getContext());
    this.m.getTextBounds(str, 0, str.length(), this.g);
    Rect localRect = this.g;
    localRect.right = ((int)(localRect.right + a(16.0F, getResources())));
  }
  
  public static enum a
  {
    static
    {
      int[] arrayOfInt = new int[4];
      arrayOfInt[0] = a;
      arrayOfInt[1] = b;
      arrayOfInt[2] = c;
      arrayOfInt[3] = d;
      e = arrayOfInt;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/MonitorView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */