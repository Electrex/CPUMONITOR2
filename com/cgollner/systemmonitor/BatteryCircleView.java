package com.cgollner.systemmonitor;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter.Blur;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.c;

public class BatteryCircleView
  extends View
{
  private float a = 60.0F;
  private Paint b;
  private Paint c;
  private Paint d;
  private Paint e;
  private Paint f;
  private Paint g;
  private RectF h;
  private Paint i;
  private Paint j;
  private int k;
  private int l;
  private float m;
  private EmbossMaskFilter n;
  private BlurMaskFilter o;
  
  public BatteryCircleView(Context paramContext)
  {
    super(paramContext);
    a();
  }
  
  public BatteryCircleView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a();
  }
  
  public BatteryCircleView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a();
  }
  
  private void a()
  {
    this.k = 10;
    this.l = 10;
    this.m = 1.75F;
    this.b = new Paint(1);
    this.b.setColor(Color.rgb(210, 210, 210));
    this.b.setStyle(Paint.Style.STROKE);
    this.b.setStrokeWidth(MonitorView.a(13.0F, getResources()));
    this.n = new EmbossMaskFilter(new float[] { 1.0F, 1.0F, 1.0F }, 0.4F, 6.0F, 3.5F);
    this.o = new BlurMaskFilter(16.0F, BlurMaskFilter.Blur.NORMAL);
    this.j = new Paint(1);
    this.j.setColor(0);
    this.j.setStyle(Paint.Style.FILL);
    this.c = new Paint(1);
    this.c.setColor(getResources().getColor(a.c.holo_red_dark));
    this.c.setStyle(Paint.Style.STROKE);
    this.c.setStrokeWidth(MonitorView.a(13.0F, getResources()));
    this.e = new Paint(1);
    this.e.setColor(getResources().getColor(a.c.holo_orange_dark));
    this.e.setStyle(Paint.Style.STROKE);
    this.e.setStrokeWidth(MonitorView.a(13.0F, getResources()));
    this.f = new Paint(1);
    this.f.setColor(Color.rgb(182, 219, 73));
    this.f.setStyle(Paint.Style.STROKE);
    this.f.setStrokeWidth(MonitorView.a(13.0F, getResources()));
    this.g = new Paint(1);
    this.g.setColor(getResources().getColor(a.c.holo_green_dark));
    this.g.setStyle(Paint.Style.STROKE);
    this.g.setStrokeWidth(MonitorView.a(13.0F, getResources()));
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
    int i1 = 0;
    if (localSharedPreferences != null) {
      i1 = localSharedPreferences.getInt("theme", 0);
    }
    if (i1 == 1) {}
    for (int i2 = -1;; i2 = -16777216)
    {
      this.d = new Paint(1);
      this.d.setColor(i2);
      this.d.setTextAlign(Paint.Align.CENTER);
      this.h = new RectF();
      return;
    }
  }
  
  public int getCircleWidthMultiplier()
  {
    return this.k;
  }
  
  public int getCurrentColor()
  {
    if (this.a < 25.0F) {
      this.i = this.c;
    }
    for (;;)
    {
      return this.i.getColor();
      if (this.a < 50.0F) {
        this.i = this.e;
      } else if (this.a < 75.0F) {
        this.i = this.f;
      } else {
        this.i = this.g;
      }
    }
  }
  
  public int getInnerCircleBgColor()
  {
    return this.j.getColor();
  }
  
  public int getInnerCircleColor()
  {
    return this.b.getColor();
  }
  
  public int getOuterCircleWidth()
  {
    return this.l;
  }
  
  public int getPercentageCircleWidth()
  {
    return (int)(100.0F * this.m);
  }
  
  public int getTextColor()
  {
    return this.d.getColor();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    int i1;
    int i2;
    label24:
    float f1;
    int i3;
    int i4;
    float f2;
    int i5;
    if (getWidth() == 0)
    {
      i1 = paramCanvas.getWidth();
      if (getHeight() != 0) {
        break label372;
      }
      i2 = paramCanvas.getHeight();
      f1 = 0.01F * Math.min(i1, i2);
      this.b.setStrokeWidth(f1 * (this.k / 10.0F));
      this.j.setStrokeWidth(f1 * (this.k / 10.0F));
      i3 = i1 / 2;
      i4 = i2 / 2;
      f2 = Math.min(i1 / 2.5F, i2 / 2.5F);
      paramCanvas.drawCircle(i3, i4, f2, this.j);
      paramCanvas.drawCircle(i3, i4, f2, this.b);
      this.h.set(i3 - f2, i4 - f2, f2 + i3, f2 + i4);
      i5 = (int)(360.0F * (this.a / 100.0F));
      if (this.a >= 25.0F) {
        break label380;
      }
      this.i = this.c;
    }
    for (;;)
    {
      this.i.setStrokeWidth(f1 * (this.l / 10.0F));
      paramCanvas.drawArc(this.h, -90.0F, i5, false, this.i);
      float f3 = (float)(i3 + Math.cos(0.0174532925D * (i5 - 90)) * f2);
      float f4 = (float)(i4 + Math.sin(0.0174532925D * (i5 - 90)) * f2);
      this.i.setStyle(Paint.Style.FILL);
      paramCanvas.drawCircle(f3, f4, this.i.getStrokeWidth() * this.m, this.i);
      this.i.setStyle(Paint.Style.STROKE);
      this.d.setTextSize(0.2F * i2);
      paramCanvas.drawText(i.c(this.a), i3, i4 + this.d.getTextSize() / 2.75F, this.d);
      return;
      i1 = getWidth();
      break;
      label372:
      i2 = getHeight();
      break label24;
      label380:
      if (this.a < 50.0F) {
        this.i = this.e;
      } else if (this.a < 75.0F) {
        this.i = this.f;
      } else {
        this.i = this.g;
      }
    }
  }
  
  public void setBlurRadius(int paramInt)
  {
    this.j.setMaskFilter(new BlurMaskFilter(paramInt, BlurMaskFilter.Blur.INNER));
    postInvalidate();
  }
  
  public void setInnerCircleBgColor(int paramInt)
  {
    this.j.setColor(paramInt);
  }
  
  public void setInnerCircleColor(int paramInt)
  {
    this.b.setColor(paramInt);
  }
  
  public void setInnerCircleWidth(int paramInt)
  {
    this.k = paramInt;
  }
  
  public void setOuterCircleWidth(int paramInt)
  {
    this.l = paramInt;
  }
  
  public void setPercentageCircleWidth(int paramInt)
  {
    this.m = (paramInt / 100.0F);
  }
  
  public void setTextColor(int paramInt)
  {
    this.d.setColor(paramInt);
  }
  
  public void setValue(float paramFloat)
  {
    this.a = paramFloat;
    postInvalidate();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/BatteryCircleView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */