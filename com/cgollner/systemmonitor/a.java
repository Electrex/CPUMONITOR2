package com.cgollner.systemmonitor;

import android.content.res.Resources;
import android.util.TypedValue;

public final class a
{
  public static float a(float paramFloat, Resources paramResources)
  {
    return TypedValue.applyDimension(1, paramFloat, paramResources.getDisplayMetrics());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */