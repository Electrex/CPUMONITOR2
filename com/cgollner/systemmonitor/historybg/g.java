package com.cgollner.systemmonitor.historybg;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.TimeMonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.battery.c;
import java.util.Iterator;
import java.util.List;

public class g
  extends Fragment
  implements HistoryBgService.a
{
  private TextView A;
  private TextView B;
  private TextView C;
  private TextView D;
  private TextView E;
  private TextView F;
  private TextView G;
  private boolean H;
  private Handler I;
  private TimeMonitorView a;
  private TimeMonitorView b;
  private View c;
  private long d;
  private long e;
  private long f;
  private long g;
  private long h;
  private long i;
  private long j;
  private long k;
  private long l;
  private long m;
  private long n;
  private long o;
  private f p;
  private int q;
  private TextView r;
  private TextView s;
  private TextView t;
  private TextView u;
  private TextView v;
  private TextView w;
  private TextView x;
  private TextView y;
  private TextView z;
  
  private void a()
  {
    this.I.post(new Runnable()
    {
      public final void run()
      {
        if ((g.this.getActivity() == null) || (g.this.isDetached())) {
          return;
        }
        g.c(g.this).setText(i.a(g.a(g.this) / g.b(g.this), 1000L));
        g.e(g.this).setText(i.a(g.d(g.this), 1000L));
        g.g(g.this).setText(i.a(g.f(g.this)));
        g.i(g.this).setText(i.a(g.h(g.this).b, g.h(g.this).h));
        g.k(g.this).setText(i.a(g.j(g.this) / g.b(g.this), 1000L));
        g.m(g.this).setText(i.a(g.l(g.this), 1000L));
        g.o(g.this).setText(i.a(g.n(g.this)));
        g.p(g.this).setText(i.a(g.h(g.this).c, g.h(g.this).h));
        g.r(g.this).setText(i.a(g.q(g.this) / g.b(g.this), 1000L));
        g.t(g.this).setText(i.a(g.s(g.this), 1000L));
        g.v(g.this).setText(i.a(g.u(g.this)));
        g.w(g.this).setText(i.a(g.h(g.this).e, g.h(g.this).h));
        g.y(g.this).setText(i.a(g.x(g.this) / g.b(g.this), 1000L));
        g.A(g.this).setText(i.a(g.z(g.this), 1000L));
        g.C(g.this).setText(i.a(g.B(g.this)));
        g.D(g.this).setText(i.a(g.h(g.this).f, g.h(g.this).h));
        g.E(g.this).invalidate();
        g.F(g.this).invalidate();
      }
    });
  }
  
  private void a(f paramf)
  {
    double d1 = 1000.0D / paramf.h;
    long l1 = (d1 * paramf.b);
    this.d = (l1 + this.d);
    this.e = Math.max(this.e, l1);
    this.f += paramf.b;
    long l2 = (d1 * paramf.c);
    this.g = (l2 + this.g);
    this.h = Math.max(this.h, l2);
    this.i += paramf.c;
    long l3 = (d1 * paramf.e);
    this.j = (l3 + this.j);
    this.k = Math.max(this.k, l3);
    this.l += paramf.e;
    long l4 = (d1 * paramf.f);
    this.m = (l4 + this.m);
    this.n = Math.max(this.n, l4);
    this.o += paramf.f;
    long l5 = (d1 * paramf.b + d1 * paramf.c);
    this.a.b = Math.max(this.a.b, l5);
    this.a.a(new c(paramf.g, (float)l5));
    long l6 = (d1 * paramf.e + d1 * paramf.f);
    this.b.b = Math.max(this.b.b, l6);
    this.b.a(new c(paramf.g, (float)l6));
  }
  
  private void a(List<f> paramList)
  {
    this.q = paramList.size();
    if (this.q == 0) {
      return;
    }
    this.p = ((f)paramList.get(-1 + this.q));
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext()) {
      a((f)localIterator.next());
    }
    a();
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.d = 0L;
      this.e = 0L;
      this.f = 0L;
      this.g = 0L;
      this.h = 0L;
      this.i = 0L;
      this.j = 0L;
      this.k = 0L;
      this.l = 0L;
      this.m = 0L;
      this.n = 0L;
      this.o = 0L;
      this.a.a();
      this.b.a();
      a(HistoryBgService.d);
      return;
    }
    this.q = HistoryBgService.d.size();
    this.p = ((f)HistoryBgService.d.get(-1 + this.q));
    a(this.p);
    a();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments().getString("SEE_ONLY") != null) {}
    for (boolean bool = true;; bool = false)
    {
      this.H = bool;
      if (!this.H) {
        HistoryBgService.i.add(this);
      }
      return;
    }
  }
  
  @SuppressLint({"UseSparseArrays"})
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.I = new Handler();
    this.c = paramLayoutInflater.inflate(a.f.net_fragment_history_layout, null);
    this.a = ((TimeMonitorView)this.c.findViewById(a.e.monitorviewWifi));
    this.a.a();
    this.a.b = 100.0D;
    this.b = ((TimeMonitorView)this.c.findViewById(a.e.monitorviewMobile));
    this.b.a();
    this.b.b = 100.0D;
    this.r = ((TextView)this.c.findViewById(a.e.wifiRecvAvg));
    this.s = ((TextView)this.c.findViewById(a.e.wifiRecvMax));
    this.t = ((TextView)this.c.findViewById(a.e.wifiRecvLast));
    this.u = ((TextView)this.c.findViewById(a.e.wifiRecvTotal));
    this.v = ((TextView)this.c.findViewById(a.e.wifiSendAvg));
    this.w = ((TextView)this.c.findViewById(a.e.wifiSendMax));
    this.x = ((TextView)this.c.findViewById(a.e.wifiSendLast));
    this.y = ((TextView)this.c.findViewById(a.e.wifiSendTotal));
    this.z = ((TextView)this.c.findViewById(a.e.mobileRecvAvg));
    this.A = ((TextView)this.c.findViewById(a.e.mobileRecvMax));
    this.B = ((TextView)this.c.findViewById(a.e.mobileRecvLast));
    this.C = ((TextView)this.c.findViewById(a.e.mobileRecvTotal));
    this.D = ((TextView)this.c.findViewById(a.e.mobileSendAvg));
    this.E = ((TextView)this.c.findViewById(a.e.mobileSendMax));
    this.F = ((TextView)this.c.findViewById(a.e.mobileSendLast));
    this.G = ((TextView)this.c.findViewById(a.e.mobileSendTotal));
    this.d = 0L;
    this.e = 0L;
    this.f = 0L;
    this.g = 0L;
    this.h = 0L;
    this.i = 0L;
    this.j = 0L;
    this.k = 0L;
    this.l = 0L;
    this.m = 0L;
    this.n = 0L;
    this.o = 0L;
    if (this.H) {
      a(HistoryBgActivity.a.d);
    }
    for (;;)
    {
      return this.c;
      synchronized (HistoryBgService.d)
      {
        a(HistoryBgService.d);
      }
    }
  }
  
  public void onDestroy()
  {
    if (!this.H) {
      HistoryBgService.i.remove(this);
    }
    super.onDestroy();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */