package com.cgollner.systemmonitor.historybg;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat.Builder;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.b.a.d;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.battery.BatteryService;
import com.cgollner.systemmonitor.c.e.a;
import com.cgollner.systemmonitor.c.g;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class HistoryBgService
  extends Service
{
  public static List<b> a;
  public static List<h> b;
  public static List<d> c;
  public static List<f> d;
  public static a e;
  public static List<a> f;
  public static List<a> g;
  public static List<a> h;
  public static List<a> i;
  public static long j = 5000L;
  private com.cgollner.systemmonitor.c.a k;
  private g l;
  private com.cgollner.systemmonitor.c.d m;
  private com.cgollner.systemmonitor.c.f n;
  private Notification o;
  private NotificationCompat.Builder p;
  private e.a q = new e.a()
  {
    public final void a()
    {
      b localb1 = new b((int[])HistoryBgService.a(HistoryBgService.this).b.clone(), (int[])HistoryBgService.a(HistoryBgService.this).c.clone(), (int[])HistoryBgService.a(HistoryBgService.this).d.clone(), (float[])HistoryBgService.a(HistoryBgService.this).a.clone());
      for (;;)
      {
        boolean bool2;
        synchronized (HistoryBgService.a)
        {
          int i = HistoryBgService.a.size();
          bool1 = false;
          if (i > 0)
          {
            b localb2 = (b)HistoryBgService.a.get(-1 + HistoryBgService.a.size());
            long l1 = localb2.e;
            long l2 = (localb1.e - localb2.e) / HistoryBgService.j;
            int j = 1;
            bool2 = false;
            if (j >= l2) {
              break label254;
            }
            long l3 = l1 + HistoryBgService.j;
            HistoryBgService.a.add(new b(l3));
            j++;
            l1 = l3;
            bool2 = true;
            continue;
          }
          HistoryBgService.a.add(localb1);
          Iterator localIterator = HistoryBgService.f.iterator();
          if (localIterator.hasNext()) {
            ((HistoryBgService.a)localIterator.next()).a(bool1);
          }
        }
        return;
        label254:
        boolean bool1 = bool2;
      }
    }
  };
  private e.a r = new e.a()
  {
    public final void a()
    {
      h localh = new h(HistoryBgService.b(HistoryBgService.this).a, HistoryBgService.b(HistoryBgService.this).b, HistoryBgService.b(HistoryBgService.this).c, HistoryBgService.b(HistoryBgService.this).d);
      synchronized (HistoryBgService.b)
      {
        HistoryBgService.b.add(localh);
        Iterator localIterator = HistoryBgService.g.iterator();
        if (localIterator.hasNext()) {
          ((HistoryBgService.a)localIterator.next()).a(false);
        }
      }
    }
  };
  private e.a s = new e.a()
  {
    public final void a()
    {
      d locald1 = new d(HistoryBgService.c(HistoryBgService.this).c, HistoryBgService.c(HistoryBgService.this).d, HistoryBgService.c(HistoryBgService.this).e, HistoryBgService.j);
      for (;;)
      {
        synchronized (HistoryBgService.c)
        {
          if (HistoryBgService.c.size() <= 0) {
            break label219;
          }
          d locald2 = (d)HistoryBgService.c.get(-1 + HistoryBgService.c.size());
          long l1 = locald2.d;
          long l2 = (locald1.d - locald2.d) / HistoryBgService.j;
          bool = false;
          long l3 = l1;
          int i = 1;
          if (i < l2)
          {
            l3 += HistoryBgService.j;
            HistoryBgService.c.add(new d(l3, HistoryBgService.j));
            i++;
            bool = true;
            continue;
          }
          HistoryBgService.c.add(locald1);
          Iterator localIterator = HistoryBgService.h.iterator();
          if (localIterator.hasNext()) {
            ((HistoryBgService.a)localIterator.next()).a(bool);
          }
        }
        return;
        label219:
        boolean bool = false;
      }
    }
  };
  private e.a t = new e.a()
  {
    public final void a()
    {
      f localf1 = new f(HistoryBgService.d(HistoryBgService.this).c, HistoryBgService.d(HistoryBgService.this).a, HistoryBgService.d(HistoryBgService.this).b, HistoryBgService.d(HistoryBgService.this).j, HistoryBgService.d(HistoryBgService.this).d, HistoryBgService.d(HistoryBgService.this).e, HistoryBgService.j);
      for (;;)
      {
        boolean bool2;
        synchronized (HistoryBgService.d)
        {
          int i = HistoryBgService.d.size();
          bool1 = false;
          if (i > 0)
          {
            f localf2 = (f)HistoryBgService.d.get(-1 + HistoryBgService.d.size());
            long l1 = localf2.g;
            long l2 = (localf1.g - localf2.g) / HistoryBgService.j;
            int j = 1;
            bool2 = false;
            long l3 = l1;
            if (j >= l2) {
              break label256;
            }
            l3 += HistoryBgService.j;
            HistoryBgService.d.add(new f(l3, HistoryBgService.j));
            j++;
            bool2 = true;
            continue;
          }
          HistoryBgService.d.add(localf1);
          Iterator localIterator = HistoryBgService.i.iterator();
          if (localIterator.hasNext()) {
            ((HistoryBgService.a)localIterator.next()).a(bool1);
          }
        }
        return;
        label256:
        boolean bool1 = bool2;
      }
    }
  };
  
  public static void a(long paramLong1, long paramLong2, Context paramContext)
  {
    AlarmManager localAlarmManager = (AlarmManager)paramContext.getSystemService("alarm");
    Intent localIntent1 = new Intent(paramContext, HistoryBgService.class);
    PendingIntent localPendingIntent1 = PendingIntent.getService(paramContext, (int)paramLong1, localIntent1, 134217728);
    Intent localIntent2 = new Intent(paramContext, HistoryBgService.class);
    localIntent2.putExtra("EXTRA_FINISH", true);
    PendingIntent localPendingIntent2 = PendingIntent.getService(paramContext, (int)paramLong2, localIntent2, 134217728);
    localAlarmManager.cancel(localPendingIntent1);
    localAlarmManager.cancel(localPendingIntent2);
  }
  
  public static void a(long paramLong1, long paramLong2, String paramString, Context paramContext)
  {
    BatteryService.a(paramContext, "schedules", "\"" + paramString + "\"" + paramLong1 + "-" + paramLong2, Boolean.valueOf(true));
    Intent localIntent1 = new Intent(paramContext, HistoryBgService.class);
    PendingIntent localPendingIntent1 = PendingIntent.getService(paramContext, (int)paramLong1, localIntent1, 134217728);
    Intent localIntent2 = new Intent(paramContext, HistoryBgService.class);
    localIntent2.putExtra("EXTRA_FINISH", true);
    localIntent2.putExtra("SCHEDULE_NAME", "\"" + paramString + "\"" + paramLong1 + "-" + paramLong2);
    localIntent2.putExtra("EXTRA_FINISH_TIME", paramLong2);
    PendingIntent localPendingIntent2 = PendingIntent.getService(paramContext, (int)paramLong2, localIntent2, 134217728);
    AlarmManager localAlarmManager = (AlarmManager)paramContext.getSystemService("alarm");
    localAlarmManager.set(0, paramLong1, localPendingIntent1);
    localAlarmManager.set(0, paramLong2, localPendingIntent2);
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onCreate()
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(App.a).edit();
    localEditor.putBoolean(App.a.getString(a.h.history_bg_status_key), true);
    localEditor.commit();
    super.onCreate();
    j = 1000 * PreferenceManager.getDefaultSharedPreferences(App.a).getInt(App.a.getString(a.h.history_bg_update_interval_key), 5);
    f = new LinkedList();
    g = new LinkedList();
    h = new LinkedList();
    i = new LinkedList();
    a = new LinkedList();
    b = new LinkedList();
    c = new LinkedList();
    d = new LinkedList();
    e = new a(a, b, c, d);
    this.k = new com.cgollner.systemmonitor.c.a(j, this.q);
    this.l = new g(j, this.r, App.a);
    this.m = new com.cgollner.systemmonitor.c.d(j, this.s);
    this.n = new com.cgollner.systemmonitor.c.f(j, this.t);
    Intent localIntent = new Intent(App.a, HistoryBgActivity.class);
    PendingIntent localPendingIntent = PendingIntent.getActivity(App.a, 0, localIntent, 134217728);
    this.p = new NotificationCompat.Builder(App.a).setContentTitle(App.a.getString(a.h.history_bg_service_is_running)).setContentText(App.a.getString(a.h.history_bg_service_see_progress)).setTicker(App.a.getString(a.h.history_bg_service_is_running)).setSmallIcon(a.d.ic_stat_iconstatus).setAutoCancel(false).setWhen(0L).setOngoing(true).setContentIntent(localPendingIntent);
    this.o = this.p.build();
    startForeground(9920, this.o);
  }
  
  public void onDestroy()
  {
    stopForeground(true);
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(App.a).edit();
    localEditor.putBoolean(App.a.getString(a.h.history_bg_status_key), false);
    localEditor.commit();
    this.k.c();
    this.l.c();
    this.m.c();
    this.n.c();
    super.onDestroy();
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    String str1;
    int i2;
    if ((paramIntent != null) && (paramIntent.getBooleanExtra("EXTRA_FINISH", false)))
    {
      str1 = paramIntent.getStringExtra("SCHEDULE_NAME");
      Context localContext = App.a;
      if (a.size() > 0)
      {
        long l1 = ((b)a.get(-1 + a.size())).e;
        long l2 = GregorianCalendar.getInstance().getTimeInMillis();
        long l3 = (l2 - l1) / j;
        if (l3 > 1L) {}
        for (int i4 = 1;; i4 = 0) {
          for (int i5 = 1; i5 < l3; i5++)
          {
            l1 += j;
            a.add(new b(l1));
            c.add(new d(l1, j));
            d.add(new f(l1, j));
          }
        }
        if (i4 != 0)
        {
          h localh = (h)b.get(-1 + b.size());
          b.add(new h(localh.a, localh.c, localh.b, localh.d, l2));
        }
      }
      BatteryService.a(localContext, "history", str1, e);
      File localFile1 = App.a.getDir("schedules", 0);
      Date localDate1 = new Date();
      File[] arrayOfFile = localFile1.listFiles();
      int i1 = arrayOfFile.length;
      i2 = 0;
      if (i2 >= i1) {
        break label615;
      }
      File localFile2 = arrayOfFile[i2];
      String str3 = localFile2.getName();
      Date localDate2 = new Date(Long.valueOf(Long.parseLong(str3.substring(1 + localFile2.getName().lastIndexOf('"'), str3.indexOf('-')))).longValue());
      Date localDate3 = new Date(Long.valueOf(Long.parseLong(str3.substring(1 + str3.indexOf('-')))).longValue());
      if ((!localDate2.before(localDate1)) || (!localDate3.after(localDate1))) {
        break label609;
      }
    }
    label609:
    label615:
    for (int i3 = 0;; i3 = 1)
    {
      if (i3 != 0) {
        stopSelf();
      }
      Intent localIntent = new Intent(getApplicationContext(), HistoryBgActivity.class);
      localIntent.putExtra("SEE_ONLY", str1);
      PendingIntent localPendingIntent = PendingIntent.getActivity(getApplicationContext(), (int)paramIntent.getLongExtra("EXTRA_FINISH_TIME", 1L), localIntent, 134217728);
      String str2 = str1.substring(1, str1.lastIndexOf('"'));
      this.p.setTicker(str2 + " " + App.a.getString(a.h.history_bg_service_is_finished)).setContentTitle(str2 + " " + App.a.getString(a.h.history_bg_service_is_finished)).setContentIntent(localPendingIntent).setOngoing(false).setAutoCancel(true);
      Notification localNotification = this.p.build();
      ((NotificationManager)getSystemService("notification")).notify(str1, 12, localNotification);
      return 1;
      i2++;
      break;
    }
  }
  
  public static abstract interface a
  {
    public abstract void a(boolean paramBoolean);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/HistoryBgService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */