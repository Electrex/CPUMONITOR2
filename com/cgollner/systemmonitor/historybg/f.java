package com.cgollner.systemmonitor.historybg;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

public final class f
  implements Serializable
{
  private static final long serialVersionUID = -3687270606712856602L;
  public long a;
  public long b;
  public long c;
  public long d;
  public long e;
  public long f;
  public long g;
  public long h;
  
  public f(long paramLong1, long paramLong2)
  {
    this.g = paramLong1;
    this.h = paramLong2;
  }
  
  public f(long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6, long paramLong7)
  {
    this.a = paramLong1;
    this.b = paramLong2;
    this.c = paramLong3;
    this.d = paramLong4;
    this.e = paramLong5;
    this.f = paramLong6;
    this.g = GregorianCalendar.getInstance().getTimeInMillis();
    this.h = paramLong7;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */