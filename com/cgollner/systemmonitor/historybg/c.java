package com.cgollner.systemmonitor.historybg;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.TimeMonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class c
  extends Fragment
  implements HistoryBgService.a
{
  private TimeMonitorView a;
  private View b;
  private float c;
  private float d;
  private float e;
  private int f;
  private int g;
  private TextView h;
  private TextView i;
  private TextView j;
  private TextView k;
  private TextView l;
  private TextView m;
  private b n;
  private int o;
  private Map<Integer, Integer> p;
  private TextView q;
  private TextView r;
  private boolean s;
  private Handler t;
  
  private void a()
  {
    this.t.post(new Runnable()
    {
      private int a()
      {
        Iterator localIterator = c.q(c.this).keySet().iterator();
        int i = 0;
        int j = 0;
        int m;
        int k;
        if (localIterator.hasNext())
        {
          Integer localInteger1 = (Integer)localIterator.next();
          Integer localInteger2 = (Integer)c.q(c.this).get(localInteger1);
          if (localInteger2.intValue() <= j) {
            break label95;
          }
          m = localInteger2.intValue();
          k = localInteger1.intValue();
        }
        for (;;)
        {
          i = k;
          j = m;
          break;
          return i;
          label95:
          k = i;
          m = j;
        }
      }
      
      public final void run()
      {
        if ((c.this.getActivity() == null) || (c.this.isDetached())) {
          return;
        }
        int i = a();
        TextView localTextView1 = c.a(c.this);
        String str1;
        String str2;
        label69:
        String str3;
        label99:
        TextView localTextView4;
        if (i <= 0)
        {
          str1 = "Offline";
          localTextView1.setText(str1);
          TextView localTextView2 = c.c(c.this);
          if (c.b(c.this) > 0) {
            break label258;
          }
          str2 = "Offline";
          localTextView2.setText(str2);
          TextView localTextView3 = c.e(c.this);
          if (c.d(c.this) > 0) {
            break label274;
          }
          str3 = "Offline";
          localTextView3.setText(str3);
          localTextView4 = c.g(c.this);
          if (c.f(c.this).a[0] > 0) {
            break label290;
          }
        }
        label258:
        label274:
        label290:
        for (String str4 = "Offline";; str4 = i.a(c.f(c.this).a[0]))
        {
          localTextView4.setText(str4);
          c.j(c.this).setText(i.c(c.h(c.this) / c.i(c.this)));
          c.l(c.this).setText(i.c(c.k(c.this)));
          c.n(c.this).setText(i.c(c.m(c.this)));
          c.o(c.this).setText(i.c(c.f(c.this).d[0]));
          c.p(c.this).invalidate();
          return;
          str1 = i.a(a());
          break;
          str2 = i.a(c.b(c.this));
          break label69;
          str3 = i.a(c.d(c.this));
          break label99;
        }
      }
    });
  }
  
  private void a(b paramb)
  {
    paramb.d[0] = Math.max(0.0F, paramb.d[0]);
    this.c += paramb.d[0];
    this.e = Math.min(this.e, paramb.d[0]);
    this.d = Math.max(this.d, paramb.d[0]);
    Integer localInteger = (Integer)this.p.get(Integer.valueOf(paramb.a[0]));
    if (localInteger == null) {
      localInteger = Integer.valueOf(0);
    }
    this.p.put(Integer.valueOf(paramb.a[0]), Integer.valueOf(1 + localInteger.intValue()));
    this.g = Math.min(this.g, paramb.a[0]);
    this.f = Math.max(this.f, paramb.a[0]);
    this.a.a(new com.cgollner.systemmonitor.battery.c(paramb.e, paramb.d[0]));
  }
  
  private void a(List<b> paramList)
  {
    this.o = paramList.size();
    if (this.o == 0) {
      return;
    }
    this.n = ((b)paramList.get(-1 + this.o));
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext()) {
      a((b)localIterator.next());
    }
    a();
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.c = 0.0F;
      this.p.clear();
      this.e = 100.0F;
      this.d = 0.0F;
      this.f = 0;
      this.g = Integer.MAX_VALUE;
      this.a.a();
      a(HistoryBgService.a);
      return;
    }
    this.o = HistoryBgService.a.size();
    this.n = ((b)HistoryBgService.a.get(-1 + this.o));
    a(this.n);
    a();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Bundle localBundle = getArguments();
    boolean bool = false;
    if (localBundle != null)
    {
      String str = getArguments().getString("SEE_ONLY");
      bool = false;
      if (str != null) {
        bool = true;
      }
    }
    this.s = bool;
    if (!this.s) {
      HistoryBgService.f.add(this);
    }
  }
  
  @SuppressLint({"UseSparseArrays"})
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.t = new Handler();
    this.b = paramLayoutInflater.inflate(a.f.cpu_fragment_history_layout, null);
    this.a = ((TimeMonitorView)this.b.findViewById(a.e.monitorview));
    this.a.a();
    this.h = ((TextView)this.b.findViewById(a.e.usageAvg));
    this.i = ((TextView)this.b.findViewById(a.e.usageMin));
    this.j = ((TextView)this.b.findViewById(a.e.usageMax));
    this.q = ((TextView)this.b.findViewById(a.e.usageLast));
    this.k = ((TextView)this.b.findViewById(a.e.freqAvg));
    this.l = ((TextView)this.b.findViewById(a.e.freqMin));
    this.m = ((TextView)this.b.findViewById(a.e.freqMax));
    this.r = ((TextView)this.b.findViewById(a.e.freqLast));
    this.c = 0.0F;
    this.e = 100.0F;
    this.d = 0.0F;
    this.f = 0;
    this.g = Integer.MAX_VALUE;
    this.p = new HashMap();
    if (this.s) {
      a(HistoryBgActivity.a.a);
    }
    for (;;)
    {
      return this.b;
      synchronized (HistoryBgService.a)
      {
        a(HistoryBgService.a);
      }
    }
  }
  
  public void onDestroy()
  {
    if (!this.s) {
      HistoryBgService.f.remove(this);
    }
    super.onDestroy();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */