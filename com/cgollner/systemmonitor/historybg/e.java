package com.cgollner.systemmonitor.historybg;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.TimeMonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.battery.c;
import java.util.Iterator;
import java.util.List;

public class e
  extends Fragment
  implements HistoryBgService.a
{
  private TextView A;
  private TextView B;
  private TextView C;
  private TextView D;
  private TextView E;
  private boolean F;
  private Handler G;
  private TimeMonitorView a;
  private View b;
  private float c;
  private float d;
  private float e;
  private long f;
  private long g;
  private long h;
  private long i;
  private long j;
  private long k;
  private long l;
  private long m;
  private long n;
  private TextView o;
  private TextView p;
  private TextView q;
  private d r;
  private int s;
  private TextView t;
  private TextView u;
  private TextView v;
  private TextView w;
  private TextView x;
  private TextView y;
  private TextView z;
  
  private void a()
  {
    this.G.post(new Runnable()
    {
      public final void run()
      {
        if ((e.this.getActivity() == null) || (e.this.isDetached())) {
          return;
        }
        e.c(e.this).setText(i.c(e.a(e.this) / e.b(e.this)));
        e.e(e.this).setText(i.c(e.d(e.this)));
        e.g(e.this).setText(i.c(e.f(e.this)));
        e.i(e.this).setText(i.c(e.h(e.this).a));
        e.k(e.this).setText(i.a(e.j(e.this) / e.b(e.this), 1000L));
        e.m(e.this).setText(i.a(e.l(e.this), 1000L));
        e.o(e.this).setText(i.a(e.n(e.this), 1000L));
        e.p(e.this).setText(i.a(e.h(e.this).b, e.h(e.this).e));
        e.r(e.this).setText(i.a(e.q(e.this) / e.b(e.this), 1000L));
        e.t(e.this).setText(i.a(e.s(e.this), 1000L));
        e.v(e.this).setText(i.a(e.u(e.this), 1000L));
        e.w(e.this).setText(i.a(e.h(e.this).c, e.h(e.this).e));
        e.y(e.this).setText(i.a(e.x(e.this)));
        e.A(e.this).setText(i.a(e.z(e.this)));
        e.C(e.this).setText(i.a(e.B(e.this)));
        e.D(e.this).invalidate();
      }
    });
  }
  
  private void a(d paramd)
  {
    this.c += paramd.a;
    this.e = Math.min(this.e, paramd.a);
    this.d = Math.max(this.d, paramd.a);
    this.a.a(new c(paramd.d, paramd.a));
    double d1 = 1000.0D / paramd.e;
    long l1 = (d1 * paramd.b);
    this.f = (l1 + this.f);
    this.h = Math.min(this.h, l1);
    this.g = Math.max(this.g, l1);
    long l2 = (d1 * paramd.c);
    this.i = (l2 + this.i);
    this.k = Math.min(this.k, l2);
    this.j = Math.max(this.j, l2);
    this.l += paramd.b;
    this.m += paramd.c;
    this.n += paramd.b + paramd.c;
  }
  
  private void a(List<d> paramList)
  {
    this.s = paramList.size();
    if (this.s == 0) {
      return;
    }
    this.r = ((d)paramList.get(-1 + this.s));
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext()) {
      a((d)localIterator.next());
    }
    a();
  }
  
  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.c = 0.0F;
      this.e = 100.0F;
      this.d = 0.0F;
      this.f = 0L;
      this.h = Long.MAX_VALUE;
      this.g = 0L;
      this.i = 0L;
      this.k = Long.MAX_VALUE;
      this.j = 0L;
      this.l = 0L;
      this.l = 0L;
      this.n = 0L;
      this.a.a();
      a(HistoryBgService.c);
      return;
    }
    this.s = HistoryBgService.c.size();
    this.r = ((d)HistoryBgService.c.get(-1 + this.s));
    a(this.r);
    a();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments().getString("SEE_ONLY") != null) {}
    for (boolean bool = true;; bool = false)
    {
      this.F = bool;
      if (!this.F) {
        HistoryBgService.h.add(this);
      }
      return;
    }
  }
  
  @SuppressLint({"UseSparseArrays"})
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.G = new Handler();
    this.b = paramLayoutInflater.inflate(a.f.io_fragment_history_layout, null);
    this.a = ((TimeMonitorView)this.b.findViewById(a.e.monitorview));
    this.a.a();
    this.o = ((TextView)this.b.findViewById(a.e.usageAvg));
    this.p = ((TextView)this.b.findViewById(a.e.usageMin));
    this.q = ((TextView)this.b.findViewById(a.e.usageMax));
    this.t = ((TextView)this.b.findViewById(a.e.usageLast));
    this.u = ((TextView)this.b.findViewById(a.e.readAvg));
    this.v = ((TextView)this.b.findViewById(a.e.readMax));
    this.w = ((TextView)this.b.findViewById(a.e.readMin));
    this.x = ((TextView)this.b.findViewById(a.e.readLast));
    this.y = ((TextView)this.b.findViewById(a.e.writeAvg));
    this.z = ((TextView)this.b.findViewById(a.e.writeMax));
    this.A = ((TextView)this.b.findViewById(a.e.writeMin));
    this.B = ((TextView)this.b.findViewById(a.e.writeLast));
    this.C = ((TextView)this.b.findViewById(a.e.totalRead));
    this.D = ((TextView)this.b.findViewById(a.e.totalWrite));
    this.E = ((TextView)this.b.findViewById(a.e.totalTotal));
    this.c = 0.0F;
    this.e = 100.0F;
    this.d = 0.0F;
    this.f = 0L;
    this.h = Long.MAX_VALUE;
    this.g = 0L;
    this.i = 0L;
    this.k = Long.MAX_VALUE;
    this.j = 0L;
    this.l = 0L;
    this.l = 0L;
    this.n = 0L;
    if (this.F) {
      a(HistoryBgActivity.a.c);
    }
    for (;;)
    {
      return this.b;
      synchronized (HistoryBgService.c)
      {
        a(HistoryBgService.c);
      }
    }
  }
  
  public void onDestroy()
  {
    if (!this.F) {
      HistoryBgService.h.remove(this);
    }
    super.onDestroy();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */