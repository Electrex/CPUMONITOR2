package com.cgollner.systemmonitor.historybg;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.TimeMonitorView;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.battery.c;
import java.util.Iterator;
import java.util.List;

public class i
  extends Fragment
  implements HistoryBgService.a
{
  private TimeMonitorView a;
  private View b;
  private float c;
  private float d;
  private float e;
  private TextView f;
  private TextView g;
  private TextView h;
  private h i;
  private int j;
  private TextView k;
  private boolean l;
  private Handler m;
  
  private void a()
  {
    this.m.post(new Runnable()
    {
      public final void run()
      {
        if ((i.this.getActivity() == null) || (i.this.isDetached())) {
          return;
        }
        i.c(i.this).setText(com.cgollner.systemmonitor.a.i.c(i.a(i.this) / i.b(i.this)));
        i.e(i.this).setText(com.cgollner.systemmonitor.a.i.c(i.d(i.this)));
        i.g(i.this).setText(com.cgollner.systemmonitor.a.i.c(i.f(i.this)));
        i.i(i.this).setText(com.cgollner.systemmonitor.a.i.c(i.h(i.this).a));
        i.j(i.this).invalidate();
      }
    });
  }
  
  private void a(h paramh)
  {
    this.c += paramh.a;
    this.e = Math.min(this.e, paramh.a);
    this.d = Math.max(this.d, paramh.a);
    this.a.a(new c(paramh.e, paramh.a));
  }
  
  private void a(List<h> paramList)
  {
    this.j = paramList.size();
    if (this.j == 0) {
      return;
    }
    this.i = ((h)paramList.get(-1 + this.j));
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext()) {
      a((h)localIterator.next());
    }
    a();
  }
  
  public final void a(boolean paramBoolean)
  {
    this.j = HistoryBgService.b.size();
    this.i = ((h)HistoryBgService.b.get(-1 + this.j));
    a(this.i);
    a();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments().getString("SEE_ONLY") != null) {}
    for (boolean bool = true;; bool = false)
    {
      this.l = bool;
      if (!this.l) {
        HistoryBgService.g.add(this);
      }
      return;
    }
  }
  
  @SuppressLint({"UseSparseArrays"})
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.m = new Handler();
    this.b = paramLayoutInflater.inflate(a.f.ram_fragment_history_layout, null);
    this.a = ((TimeMonitorView)this.b.findViewById(a.e.monitorview));
    this.a.a();
    this.f = ((TextView)this.b.findViewById(a.e.usageAvg));
    this.g = ((TextView)this.b.findViewById(a.e.usageMin));
    this.h = ((TextView)this.b.findViewById(a.e.usageMax));
    this.k = ((TextView)this.b.findViewById(a.e.usageLast));
    this.c = 0.0F;
    this.e = 100.0F;
    this.d = 0.0F;
    if (this.l) {
      a(HistoryBgActivity.a.b);
    }
    for (;;)
    {
      return this.b;
      synchronized (HistoryBgService.b)
      {
        a(HistoryBgService.b);
      }
    }
  }
  
  public void onDestroy()
  {
    if (!this.l) {
      HistoryBgService.g.remove(this);
    }
    super.onDestroy();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */