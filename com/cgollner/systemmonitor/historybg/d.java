package com.cgollner.systemmonitor.historybg;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

public final class d
  implements Serializable
{
  private static final long serialVersionUID = 6672708347539882349L;
  public float a;
  public long b;
  public long c;
  public long d;
  public long e;
  
  public d(float paramFloat, long paramLong1, long paramLong2, long paramLong3)
  {
    this.a = paramFloat;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = GregorianCalendar.getInstance().getTimeInMillis();
    this.e = paramLong3;
  }
  
  public d(long paramLong1, long paramLong2)
  {
    this.d = paramLong1;
    this.e = paramLong2;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */