package com.cgollner.systemmonitor.historybg;

import android.text.format.Time;
import java.io.Serializable;

public final class b
  implements Serializable
{
  private static final long serialVersionUID = -6551566965092519901L;
  public int[] a;
  public int[] b;
  public int[] c;
  public float[] d;
  public long e;
  
  public b(long paramLong)
  {
    this.a = new int[] { 0 };
    this.b = new int[] { 0 };
    this.c = new int[] { 0 };
    this.d = new float[] { 0.0F };
    this.e = paramLong;
  }
  
  public b(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3, float[] paramArrayOfFloat)
  {
    this.a = paramArrayOfInt1;
    this.b = paramArrayOfInt2;
    this.c = paramArrayOfInt3;
    this.d = paramArrayOfFloat;
    Time localTime = new Time();
    localTime.setToNow();
    this.e = localTime.toMillis(false);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */