package com.cgollner.systemmonitor.historybg;

import android.text.format.Time;
import java.io.Serializable;

public final class h
  implements Serializable
{
  private static final long serialVersionUID = 2043896090780580498L;
  public float a;
  public long b;
  public long c;
  public long d;
  public long e;
  
  public h(float paramFloat, long paramLong1, long paramLong2, long paramLong3)
  {
    this.a = paramFloat;
    this.b = paramLong1;
    this.c = paramLong2;
    this.d = paramLong3;
    Time localTime = new Time();
    localTime.setToNow();
    this.e = localTime.toMillis(false);
  }
  
  public h(float paramFloat, long paramLong1, long paramLong2, long paramLong3, long paramLong4)
  {
    this.a = paramFloat;
    this.b = paramLong2;
    this.c = paramLong1;
    this.d = paramLong3;
    this.e = paramLong4;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */