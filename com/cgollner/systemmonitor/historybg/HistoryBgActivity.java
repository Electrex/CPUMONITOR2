package com.cgollner.systemmonitor.historybg;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.EditText;
import android.widget.Toast;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.b.a.b;
import com.cgollner.systemmonitor.b.a.c;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.b.a.i;
import com.cgollner.systemmonitor.battery.BatteryService;
import java.util.List;

public class HistoryBgActivity
  extends ActionBarActivity
{
  public static a a;
  private static final int[] b;
  private static final int[] c;
  private static final Class<? extends Fragment>[] j = { c.class, i.class, e.class, g.class };
  private ViewPager d;
  private PagerTitleStrip e;
  private a f;
  private boolean g;
  private String h;
  private boolean i;
  
  static
  {
    int[] arrayOfInt1 = new int[4];
    arrayOfInt1[0] = a.c.holo_blue_dark;
    arrayOfInt1[1] = a.c.holo_purple_dark;
    arrayOfInt1[2] = a.c.holo_green_dark;
    arrayOfInt1[3] = a.c.holo_orange_dark;
    b = arrayOfInt1;
    int[] arrayOfInt2 = new int[4];
    arrayOfInt2[0] = a.h.cpu_title;
    arrayOfInt2[1] = a.h.ram_title;
    arrayOfInt2[2] = a.h.io_title;
    arrayOfInt2[3] = a.h.network_title;
    c = arrayOfInt2;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(App.a);
    int k;
    label52:
    String str;
    if (Build.VERSION.SDK_INT < 11)
    {
      k = 1;
      switch (localSharedPreferences.getInt("theme", k))
      {
      default: 
        super.onCreate(paramBundle);
        setContentView(a.f.activity_main);
        setSupportActionBar((Toolbar)findViewById(a.e.toolbar));
        getSupportActionBar().setTitle(a.h.background_history_title);
        this.d = ((ViewPager)findViewById(a.e.viewPager));
        this.e = ((PagerTitleStrip)findViewById(a.e.pager_title_strip));
        this.h = getIntent().getStringExtra("SEE_ONLY");
        str = this.h;
        if (str != null) {
          break;
        }
      }
    }
    for (a = HistoryBgService.e;; a = (a)BatteryService.b(App.a, "history", str))
    {
      this.f = new a(getSupportFragmentManager());
      this.d.setAdapter(this.f);
      this.d.setOnPageChangeListener(this.f);
      this.g = true;
      this.i = com.cgollner.systemmonitor.a.i.a(this);
      this.d.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener()
      {
        public final boolean onPreDraw()
        {
          HistoryBgActivity.a(HistoryBgActivity.this).getViewTreeObserver().removeOnPreDrawListener(this);
          HistoryBgActivity.a(HistoryBgActivity.this, HistoryBgActivity.a()[HistoryBgActivity.a(HistoryBgActivity.this).getCurrentItem()]);
          return true;
        }
      });
      return;
      k = 0;
      break;
      setTheme(a.i.AppTheme);
      break label52;
      setTheme(a.i.AppThemeDark);
      break label52;
      setTheme(a.i.AppTheme);
      break label52;
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (this.h == null) {
      getMenuInflater().inflate(a.g.bg_activity_menu, paramMenu);
    }
    return super.onCreateOptionsMenu(paramMenu);
  }
  
  @SuppressLint({"NewApi"})
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == a.e.save) {
      if (!getPackageName().equals("com.cgollner.systemmonitor")) {
        new AlertDialog.Builder(this).setTitle(App.a.getString(a.h.full_version_feature_title)).setMessage(a.h.full_version_feature_message).setPositiveButton("Play Store", new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            Intent localIntent = new Intent("android.intent.action.VIEW");
            localIntent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.cgollner.systemmonitor"));
            HistoryBgActivity.this.startActivity(localIntent);
          }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {}
        }).create().show();
      }
    }
    for (;;)
    {
      if (Build.VERSION.SDK_INT >= 11) {
        invalidateOptionsMenu();
      }
      return super.onOptionsItemSelected(paramMenuItem);
      if (this.i)
      {
        new AlertDialog.Builder(this).setTitle(App.a.getString(a.h.unlicensed_dialog_title)).setMessage(a.h.unlicensed_dialog_body).setNeutralButton(17039370, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {}
        }).create().show();
      }
      else
      {
        final EditText localEditText = new EditText(this);
        localEditText.setHint(App.a.getString(a.h.history_bg_history_enter_name));
        new AlertDialog.Builder(this).setTitle(a.h.history_bg_save_menu).setView(localEditText).setPositiveButton(a.h.history_bg_save_menu, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            String str = localEditText.getEditableText().toString();
            long l1;
            if (HistoryBgService.e.a.size() > 0)
            {
              l1 = ((b)HistoryBgService.e.a.get(0)).e;
              if (HistoryBgService.e.a.size() <= 0) {
                break label159;
              }
            }
            label159:
            for (long l2 = ((b)HistoryBgService.e.a.get(-1 + HistoryBgService.e.a.size())).e;; l2 = System.currentTimeMillis())
            {
              BatteryService.a(App.a, "history", "\"" + str + "\"" + l1 + "-" + l2, HistoryBgService.e);
              Toast.makeText(App.a, "File saved", 0).show();
              return;
              l1 = System.currentTimeMillis();
              break;
            }
          }
        }).setNegativeButton(a.h.dialog_cancel, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {}
        }).create().show();
        continue;
        if (paramMenuItem.getItemId() == a.e.stop_start)
        {
          if (!this.g) {}
          Intent localIntent;
          for (boolean bool = true;; bool = false)
          {
            this.g = bool;
            localIntent = new Intent(App.a, HistoryBgService.class);
            if (!this.g) {
              break label321;
            }
            startService(localIntent);
            finish();
            startActivity(new Intent(App.a, HistoryBgActivity.class));
            break;
          }
          label321:
          stopService(localIntent);
        }
      }
    }
  }
  
  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    int k = 1;
    TypedArray localTypedArray;
    MenuItem localMenuItem;
    if (this.h == null)
    {
      Resources.Theme localTheme = getTheme();
      int[] arrayOfInt = new int[2];
      arrayOfInt[0] = a.b.playIcon;
      arrayOfInt[k] = a.b.stopIcon;
      localTypedArray = localTheme.obtainStyledAttributes(arrayOfInt);
      localMenuItem = paramMenu.findItem(a.e.stop_start);
      if (!this.g) {
        break label84;
      }
    }
    for (;;)
    {
      localMenuItem.setIcon(localTypedArray.getDrawable(k));
      localTypedArray.recycle();
      return super.onPrepareOptionsMenu(paramMenu);
      label84:
      k = 0;
    }
  }
  
  private final class a
    extends FragmentPagerAdapter
    implements ViewPager.OnPageChangeListener
  {
    public a(FragmentManager paramFragmentManager)
    {
      super();
    }
    
    public final int getCount()
    {
      return HistoryBgActivity.b().length;
    }
    
    public final Fragment getItem(int paramInt)
    {
      try
      {
        Fragment localFragment = (Fragment)HistoryBgActivity.b()[paramInt].newInstance();
        Bundle localBundle = new Bundle();
        localBundle.putString("SEE_ONLY", HistoryBgActivity.b(HistoryBgActivity.this));
        localFragment.setArguments(localBundle);
        return localFragment;
      }
      catch (InstantiationException localInstantiationException)
      {
        localInstantiationException.printStackTrace();
        return null;
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        for (;;)
        {
          localIllegalAccessException.printStackTrace();
        }
      }
    }
    
    public final CharSequence getPageTitle(int paramInt)
    {
      return App.a.getString(HistoryBgActivity.c()[paramInt]);
    }
    
    public final void onPageScrollStateChanged(int paramInt) {}
    
    public final void onPageScrolled(int paramInt1, float paramFloat, int paramInt2) {}
    
    public final void onPageSelected(int paramInt)
    {
      HistoryBgActivity.a(HistoryBgActivity.this, HistoryBgActivity.a()[paramInt]);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/historybg/HistoryBgActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */