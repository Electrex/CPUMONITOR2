package com.cgollner.systemmonitor;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import com.b.a.a;
import com.cgollner.systemmonitor.b.a.c;

public class App
  extends Application
  implements Application.ActivityLifecycleCallbacks
{
  public static Context a;
  
  public static void a(Activity paramActivity, int paramInt)
  {
    if ((Build.VERSION.SDK_INT >= 19) && (Build.VERSION.SDK_INT < 21))
    {
      Window localWindow = paramActivity.getWindow();
      WindowManager.LayoutParams localLayoutParams = localWindow.getAttributes();
      localLayoutParams.flags = (0xF7FFFFFF & localLayoutParams.flags);
      localLayoutParams.flags = (0xFBFFFFFF & localLayoutParams.flags);
      localWindow.setAttributes(localLayoutParams);
      localWindow.clearFlags(512);
      localWindow.setFlags(67108864, 67108864);
      int i = Color.red(paramInt);
      int j = Color.green(paramInt);
      int k = Color.blue(paramInt);
      int m = Color.rgb((int)(1.0F * i), (int)(1.0F * j), (int)(1.0F * k));
      a locala = new a(paramActivity);
      if (locala.a) {
        locala.e.setBackgroundColor(m);
      }
      locala.c = true;
      if (locala.a) {
        locala.e.setVisibility(0);
      }
      locala.d = false;
      if (locala.b) {
        locala.f.setVisibility(8);
      }
      if (locala.b) {
        locala.f.setBackgroundColor(m);
      }
    }
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle)
  {
    a(paramActivity, a.getResources().getColor(a.c.colorPrimaryDark));
  }
  
  public void onActivityDestroyed(Activity paramActivity) {}
  
  public void onActivityPaused(Activity paramActivity) {}
  
  public void onActivityResumed(Activity paramActivity) {}
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity) {}
  
  public void onActivityStopped(Activity paramActivity) {}
  
  public void onCreate()
  {
    super.onCreate();
    a = getApplicationContext();
    registerActivityLifecycleCallbacks(this);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/App.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */