package com.cgollner.systemmonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.cgollner.systemmonitor.a.b;
import com.cgollner.systemmonitor.battery.BatteryService;
import java.io.IOException;

public class BootService
  extends BroadcastReceiver
{
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    try
    {
      b.a(paramContext);
      if (PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean(App.a.getString(2131492915), true)) {
        paramContext.startService(new Intent(paramContext, BatteryService.class));
      }
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localIOException.printStackTrace();
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/BootService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */