package com.cgollner.systemmonitor.pref;

import android.annotation.TargetApi;
import android.content.Context;
import android.preference.SwitchPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;

@TargetApi(14)
public class SwitchPreferenceFix
  extends SwitchPreference
{
  public SwitchPreferenceFix(Context paramContext)
  {
    super(paramContext, null);
  }
  
  public SwitchPreferenceFix(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public SwitchPreferenceFix(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  private void a(ViewGroup paramViewGroup)
  {
    if (paramViewGroup == null) {}
    for (;;)
    {
      return;
      int i = paramViewGroup.getChildCount();
      for (int j = 0; j < i; j++)
      {
        View localView = paramViewGroup.getChildAt(j);
        if ((localView instanceof Switch))
        {
          ((Switch)localView).setOnCheckedChangeListener(null);
          return;
        }
        if ((localView instanceof ViewGroup)) {
          a((ViewGroup)localView);
        }
      }
    }
  }
  
  protected void onBindView(View paramView)
  {
    a((ViewGroup)paramView);
    super.onBindView(paramView);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/pref/SwitchPreferenceFix.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */