package com.cgollner.systemmonitor.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.cgollner.systemmonitor.b.a.e;

public class SeekbarPreference
  extends DialogPreference
  implements SeekBar.OnSeekBarChangeListener
{
  private SeekBar a;
  private TextView b;
  private int c;
  private int d;
  
  public SeekbarPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public SeekbarPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  protected void onBindDialogView(View paramView)
  {
    this.a = ((SeekBar)paramView.findViewById(a.e.seekBar));
    this.b = ((TextView)paramView.findViewById(a.e.infoTextView));
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
    if (this.c != 0) {
      this.a.setMax(this.c);
    }
    this.a.setProgress(localSharedPreferences.getInt(getKey(), this.d));
    String str = "Transparency: " + this.a.getProgress() + "%";
    this.b.setText(str);
    this.a.setOnSeekBarChangeListener(this);
    super.onBindDialogView(paramView);
  }
  
  protected void onDialogClosed(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
      int i = this.a.getProgress();
      localEditor.putInt(getKey(), i);
      localEditor.commit();
    }
  }
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean) {}
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  public void onStopTrackingTouch(SeekBar paramSeekBar) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/pref/SeekbarPreference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */