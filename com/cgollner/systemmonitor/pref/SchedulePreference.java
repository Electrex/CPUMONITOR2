package com.cgollner.systemmonitor.pref;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.historybg.HistoryBgService;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SchedulePreference
  extends DialogPreference
{
  private TimePicker a;
  private TimePicker b;
  private EditText c;
  
  public SchedulePreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a();
  }
  
  public SchedulePreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a();
  }
  
  private void a()
  {
    setDialogLayoutResource(a.f.history_bg_schedule);
    setPositiveButtonText(17039370);
    setNegativeButtonText(17039360);
    setDialogIcon(null);
  }
  
  protected void onBindDialogView(View paramView)
  {
    this.a = ((TimePicker)paramView.findViewById(a.e.timePickerStart));
    this.b = ((TimePicker)paramView.findViewById(a.e.timePickerEnd));
    this.c = ((EditText)paramView.findViewById(a.e.name));
    super.onBindDialogView(paramView);
  }
  
  protected void onDialogClosed(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      if ((this.c.getText().toString().equals("")) || (this.c.getText().toString().equals(" "))) {
        new AlertDialog.Builder(getContext()).setTitle("Name missing").setMessage("Please enter a name").setNeutralButton(17039370, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            SchedulePreference.a(SchedulePreference.this);
          }
        }).create().show();
      }
    }
    else {
      return;
    }
    int i = this.a.getCurrentHour().intValue();
    int j = this.a.getCurrentMinute().intValue();
    Calendar localCalendar1 = GregorianCalendar.getInstance();
    localCalendar1.set(11, i);
    localCalendar1.set(12, j);
    localCalendar1.set(13, 0);
    int k = this.b.getCurrentHour().intValue();
    int m = this.b.getCurrentMinute().intValue();
    Calendar localCalendar2 = GregorianCalendar.getInstance();
    localCalendar2.set(11, k);
    localCalendar2.set(12, m);
    localCalendar2.set(13, 0);
    if (localCalendar1.before(GregorianCalendar.getInstance()))
    {
      localCalendar1.add(6, 1);
      localCalendar2.add(6, 1);
    }
    if (localCalendar2.before(localCalendar1)) {
      localCalendar2.add(6, 1);
    }
    HistoryBgService.a(localCalendar1.getTimeInMillis(), localCalendar2.getTimeInMillis(), this.c.getText().toString(), getContext());
    callChangeListener(Boolean.valueOf(true));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/pref/SchedulePreference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */