package com.cgollner.systemmonitor;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.a.a.d;
import com.cgollner.systemmonitor.d.g;
import com.cgollner.systemmonitor.d.i;
import com.cgollner.systemmonitor.d.j;
import com.cgollner.systemmonitor.d.k;
import com.cgollner.systemmonitor.d.l;
import com.cgollner.systemmonitor.d.m;
import com.cgollner.systemmonitor.d.o;
import com.cgollner.systemmonitor.settings.Settings;

public class MainActivity
  extends b
{
  protected final void a()
  {
    startActivity(new Intent(this, Settings.class));
  }
  
  protected final Class<? extends Fragment> b()
  {
    return l.class;
  }
  
  protected final Class<? extends Fragment> c()
  {
    return k.class;
  }
  
  protected final Class<? extends Fragment> d()
  {
    return o.class;
  }
  
  protected final Class<? extends Fragment> e()
  {
    return g.class;
  }
  
  protected final Class<? extends Fragment> f()
  {
    return m.class;
  }
  
  protected final Class<? extends Fragment> g()
  {
    return i.class;
  }
  
  protected final Class<? extends Fragment> h()
  {
    return j.class;
  }
  
  @SuppressLint({"NewApi"})
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    d.a(this);
    d.a(this);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/MainActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */