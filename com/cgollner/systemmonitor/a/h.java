package com.cgollner.systemmonitor.a;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.io.FileUtils;

public final class h
  extends AsyncTaskLoader<List<b>>
{
  private final File a;
  private List<b> b;
  
  public h(Context paramContext, File paramFile)
  {
    super(paramContext);
    this.a = paramFile;
  }
  
  private static List<b> a(File paramFile, Context paramContext)
  {
    LinkedList localLinkedList = new LinkedList();
    File[] arrayOfFile = paramFile.listFiles();
    if (arrayOfFile == null) {
      return new LinkedList();
    }
    int i = arrayOfFile.length;
    for (int j = 0; j < i; j++)
    {
      a locala1 = new a(arrayOfFile[j], paramContext);
      locala1.start();
      localLinkedList.add(locala1);
    }
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator1 = localLinkedList.iterator();
    long l1 = 0L;
    a locala2;
    if (localIterator1.hasNext()) {
      locala2 = (a)localIterator1.next();
    }
    for (;;)
    {
      try
      {
        locala2.join();
        if ((a.a(locala2) != null) || (locala2.a == null)) {
          break label245;
        }
        localArrayList.add(locala2.a);
        long l3 = locala2.a.b;
        l2 = l1 + l3;
        l1 = l2;
      }
      catch (InterruptedException localInterruptedException)
      {
        localInterruptedException.printStackTrace();
      }
      break;
      Collections.sort(localArrayList);
      Iterator localIterator2 = localArrayList.iterator();
      while (localIterator2.hasNext())
      {
        b localb = (b)localIterator2.next();
        localb.c = ((float)(100.0D * (localb.b / l1)));
      }
      return localArrayList;
      label245:
      long l2 = l1;
    }
  }
  
  private void a(List<b> paramList)
  {
    this.b = paramList;
    super.deliverResult(paramList);
  }
  
  protected final void onStartLoading()
  {
    if (this.b == null)
    {
      forceLoad();
      return;
    }
    a(this.b);
  }
  
  private static final class a
    extends Thread
  {
    public h.b a;
    public File b;
    private Context c;
    private Throwable d;
    
    public a(File paramFile, Context paramContext)
    {
      this.b = paramFile;
      this.c = paramContext;
    }
    
    public final void run()
    {
      try
      {
        this.a = new h.b(this.b, FileUtils.sizeOf(this.b));
        return;
      }
      catch (Throwable localThrowable)
      {
        this.d = localThrowable;
      }
    }
  }
  
  public static final class b
    implements Comparable<b>
  {
    public File a;
    public long b;
    public float c;
    
    public b(File paramFile, long paramLong)
    {
      this.a = paramFile;
      this.b = paramLong;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */