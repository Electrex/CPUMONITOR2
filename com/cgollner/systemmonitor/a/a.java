package com.cgollner.systemmonitor.a;

import a.a.a.b.a;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.AsyncTaskLoader;
import java.io.File;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@SuppressLint({"SdCardPath"})
public final class a
  extends AsyncTaskLoader<List<a>>
{
  public static final a a = new a();
  private static final String c = new String("/data/data");
  private List<a> b;
  private HashMap<String, a> d;
  
  public a(Activity paramActivity)
  {
    super(paramActivity);
  }
  
  public static void a(a parama, final b paramb)
  {
    new Thread(new Runnable()
    {
      public final void run()
      {
        String[] arrayOfString = new String[2];
        arrayOfString[0] = ("rm -rf /data/data/" + this.a.c + "/cache");
        arrayOfString[1] = ("rm -rf " + Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + this.a.c + "/cache");
        b.a.a(arrayOfString);
        if (paramb != null) {
          this.c.post(new Runnable()
          {
            public final void run()
            {
              a.2.this.b.a();
            }
          });
        }
      }
    }).start();
  }
  
  private void a(List<a> paramList)
  {
    this.b = paramList;
    super.deliverResult(paramList);
  }
  
  public static void a(List<a> paramList, final b paramb)
  {
    new Thread(new Runnable()
    {
      public final void run()
      {
        LinkedList localLinkedList = new LinkedList();
        Iterator localIterator = this.a.iterator();
        while (localIterator.hasNext())
        {
          a.a locala = (a.a)localIterator.next();
          localLinkedList.add("rm -rf /data/data/" + locala.c + "/cache");
          localLinkedList.add("rm -rf " + Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + locala.c + "/cache");
        }
        b.a.a(localLinkedList);
        if (paramb != null) {
          this.c.post(new Runnable()
          {
            public final void run()
            {
              a.3.this.b.a();
            }
          });
        }
      }
    }).start();
  }
  
  protected final void onStartLoading()
  {
    if (this.b != null)
    {
      a(this.b);
      return;
    }
    forceLoad();
  }
  
  public static final class a
  {
    public String a;
    public Drawable b;
    public String c;
    public long d;
    
    public final String toString()
    {
      return this.a;
    }
  }
  
  public static abstract interface b
  {
    public abstract void a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */