package com.cgollner.systemmonitor.a;

public abstract class d
{
  private static final Class<? extends d>[] a = { e.class, f.class };
  private static d b;
  
  public static d a()
  {
    if (b == null)
    {
      Class[] arrayOfClass = a;
      int i = arrayOfClass.length;
      int j = 0;
      while (j < i)
      {
        Class localClass = arrayOfClass[j];
        try
        {
          d locald1 = (d)localClass.newInstance();
          b = locald1;
          if (locald1.b())
          {
            d locald2 = b;
            return locald2;
          }
        }
        catch (Exception localException)
        {
          j++;
        }
      }
    }
    return b;
  }
  
  public boolean b()
  {
    return false;
  }
  
  public abstract float c();
  
  public abstract int d();
  
  public abstract int e();
  
  public abstract int f();
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */