package com.cgollner.systemmonitor.a;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public final class c
{
  public static String a(String paramString)
  {
    if (!new File(paramString).exists()) {
      return "-1";
    }
    try
    {
      BufferedReader localBufferedReader = new BufferedReader(new FileReader(paramString));
      String str = localBufferedReader.readLine();
      localBufferedReader.close();
      return str;
    }
    catch (IOException localIOException) {}
    return "-1";
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */