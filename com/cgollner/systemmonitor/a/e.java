package com.cgollner.systemmonitor.a;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import org.apache.commons.io.FileUtils;

public class e
  extends d
{
  static final File[] a;
  static final File[] b;
  static final File[] c;
  static final File[] d;
  private static Integer[] e;
  private static int f = Integer.MIN_VALUE;
  private static int g = Integer.MIN_VALUE;
  
  static
  {
    File[] arrayOfFile1 = new File[1];
    arrayOfFile1[0] = new File("/sys/class/kgsl/kgsl-3d0/gpubusy");
    a = arrayOfFile1;
    File[] arrayOfFile2 = new File[1];
    arrayOfFile2[0] = new File("/sys/class/kgsl/kgsl-3d0/gpuclk");
    b = arrayOfFile2;
    File[] arrayOfFile3 = new File[1];
    arrayOfFile3[0] = new File("/sys/class/kgsl/kgsl-3d0/gpu_available_frequencies");
    c = arrayOfFile3;
    File[] arrayOfFile4 = new File[1];
    arrayOfFile4[0] = new File("/sys/class/kgsl/kgsl-3d0/max_gpuclk");
    d = arrayOfFile4;
  }
  
  private static File g()
  {
    for (File localFile : a) {
      if ((localFile.exists()) && ((localFile.canRead()) || (localFile.setReadable(true, false)))) {
        return localFile;
      }
    }
    return null;
  }
  
  private static File h()
  {
    for (File localFile : b) {
      if ((localFile.exists()) && ((localFile.canRead()) || (localFile.setReadable(true, false)))) {
        return localFile;
      }
    }
    return null;
  }
  
  private static File i()
  {
    for (File localFile : d) {
      if ((localFile.exists()) && ((localFile.canRead()) || (localFile.setReadable(true, false)))) {
        return localFile;
      }
    }
    return null;
  }
  
  private static File j()
  {
    for (File localFile : c) {
      if ((localFile.exists()) && ((localFile.canRead()) || (localFile.setReadable(true, false)))) {
        return localFile;
      }
    }
    return null;
  }
  
  private static Integer[] k()
  {
    if (e != null) {
      return e;
    }
    try
    {
      String[] arrayOfString = FileUtils.readFileToString(j()).trim().split("\\s+");
      e = new Integer[arrayOfString.length];
      for (int i = 0; i < arrayOfString.length; i++) {
        e[i] = Integer.valueOf(Integer.parseInt(arrayOfString[i]));
      }
      Integer[] arrayOfInteger = e;
      return arrayOfInteger;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public final boolean b()
  {
    return (j() != null) && (g() != null) && (h() != null) && (i() != null);
  }
  
  public final float c()
  {
    try
    {
      String[] arrayOfString = FileUtils.readFileToString(g()).trim().split("\\s+");
      int i = Integer.parseInt(arrayOfString[0]);
      int j = Integer.parseInt(arrayOfString[1]);
      if (j == 0) {
        return 0.0F;
      }
      float f1 = 100.0F * (i / j);
      float f2 = d();
      int k;
      if (g != Integer.MIN_VALUE) {
        k = g;
      }
      for (;;)
      {
        return f1 * (f2 / k);
        Integer[] arrayOfInteger = k();
        Arrays.sort(arrayOfInteger);
        k = arrayOfInteger[(-1 + arrayOfInteger.length)].intValue();
        g = k;
      }
      return -1.0F;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
  
  public final int d()
  {
    try
    {
      int i = Integer.parseInt(FileUtils.readFileToString(h()).trim());
      return i;
    }
    catch (Exception localException) {}
    return -1;
  }
  
  public final int e()
  {
    if (f != Integer.MIN_VALUE) {
      return f;
    }
    Integer[] arrayOfInteger = k();
    Arrays.sort(arrayOfInteger);
    int i = arrayOfInteger[0].intValue();
    f = i;
    return i;
  }
  
  public final int f()
  {
    try
    {
      int i = Integer.parseInt(FileUtils.readFileToString(i()).trim());
      return i;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return -1;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */