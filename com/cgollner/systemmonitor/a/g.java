package com.cgollner.systemmonitor.a;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public final class g
{
  private static int a(String paramString)
  {
    for (int i = 0; i < paramString.length(); i++)
    {
      int j = paramString.charAt(i);
      if (((j >= 97) && (j <= 122)) || ((j >= 65) && (j <= 90))) {
        return i;
      }
    }
    return -1;
  }
  
  public static long a(List<Long> paramList1, List<Long> paramList2)
  {
    long l1 = 0L;
    for (int i = 0; i < paramList1.size(); i++) {
      if ((i < paramList1.size()) && (i < paramList2.size()))
      {
        long l2 = ((Long)paramList1.get(i)).longValue();
        long l3 = ((Long)paramList2.get(i)).longValue() - l2;
        if (l3 > l1) {
          l1 = l3;
        }
      }
    }
    return l1;
  }
  
  public static List<Long> a(int paramInt)
  {
    try
    {
      ArrayList localArrayList = new ArrayList();
      BufferedReader localBufferedReader = new BufferedReader(new FileReader(new File("/proc/diskstats")));
      for (String str = localBufferedReader.readLine(); str != null; str = localBufferedReader.readLine()) {
        localArrayList.add(Long.valueOf(Long.parseLong(str.substring(a(str)).split(" ")[paramInt])));
      }
      localBufferedReader.close();
      return localArrayList;
    }
    catch (Exception localException) {}
    return null;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */