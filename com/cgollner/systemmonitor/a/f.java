package com.cgollner.systemmonitor.a;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;

public class f
  extends d
{
  private static final File a = new File("/sys/devices/platform/mali.0/clock");
  private static final File b = new File("/sys/devices/platform/mali.0/clock");
  private static final File c = new File("/sys/devices/platform/mali.0/dvfs");
  private static Integer[] d;
  private static int e = Integer.MIN_VALUE;
  private static int f = Integer.MIN_VALUE;
  
  private static File a(File paramFile)
  {
    if ((paramFile.exists()) && ((paramFile.canRead()) || (paramFile.setReadable(true, false)))) {
      return paramFile;
    }
    return null;
  }
  
  private static Integer[] g()
  {
    if (d != null) {
      return d;
    }
    try
    {
      String str = FileUtils.readFileToString(a(b)).split("\\n")[1];
      LinkedList localLinkedList = new LinkedList();
      Matcher localMatcher = Pattern.compile("\\d+").matcher(str);
      while (localMatcher.find()) {
        localLinkedList.add(Integer.valueOf(Integer.parseInt(localMatcher.group())));
      }
      Integer[] arrayOfInteger = (Integer[])localLinkedList.toArray(new Integer[localLinkedList.size()]);
      d = arrayOfInteger;
      return arrayOfInteger;
    }
    catch (Exception localException) {}
    return null;
  }
  
  private static int h()
  {
    if (f != Integer.MIN_VALUE) {
      return f;
    }
    Integer[] arrayOfInteger = g();
    Arrays.sort(arrayOfInteger);
    int i = 1000 * (1000 * arrayOfInteger[(-1 + arrayOfInteger.length)].intValue());
    f = i;
    return i;
  }
  
  public final boolean b()
  {
    return (a(b) != null) && (g() != null) && (g().length > 1) && (a(c) != null) && (a(a) != null) && (a(b) != null);
  }
  
  public final float c()
  {
    try
    {
      float f1 = Integer.parseInt(FileUtils.readFileToString(a(c)).replaceAll("\\n", "").replaceAll("\\r", "").replaceAll("\\t", "").replaceFirst("(.*utilisation:)(\\d{1,3})", "$2"));
      float f2 = d();
      int i = h();
      return f1 * (f2 / i);
    }
    catch (Exception localException) {}
    return -1.0F;
  }
  
  public final int d()
  {
    try
    {
      int i = Integer.parseInt(FileUtils.readFileToString(a(a)).split("\\n")[0].replaceFirst("(.*)(\\d{3,})(.*)", "$2"));
      return 1000 * (i * 1000);
    }
    catch (Exception localException) {}
    return -1;
  }
  
  public final int e()
  {
    if (e != Integer.MIN_VALUE) {
      return e;
    }
    Integer[] arrayOfInteger = g();
    Arrays.sort(arrayOfInteger);
    int i = 1000 * (1000 * arrayOfInteger[0].intValue());
    e = i;
    return i;
  }
  
  public final int f()
  {
    return h();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */