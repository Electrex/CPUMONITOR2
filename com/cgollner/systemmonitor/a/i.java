package com.cgollner.systemmonitor.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.b.a.a;
import com.cgollner.systemmonitor.b.a.h;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

@SuppressLint({"DefaultLocale"})
public final class i
{
  static int[] a;
  private static final String[] b = { "%.0f Bytes", "%.0f KB", "%.1f MB", "%.1f GB", "%.3f TB" };
  
  static
  {
    int[] arrayOfInt = new int[7];
    arrayOfInt[0] = a.h.sunday;
    arrayOfInt[1] = a.h.monday;
    arrayOfInt[2] = a.h.tuesday;
    arrayOfInt[3] = a.h.wednesday;
    arrayOfInt[4] = a.h.thursday;
    arrayOfInt[5] = a.h.friday;
    arrayOfInt[6] = a.h.saturday;
    a = arrayOfInt;
  }
  
  public static CharSequence a(int paramInt, Context paramContext)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    String str = localSharedPreferences.getString(App.a.getString(a.h.temperature_units_key), null);
    SharedPreferences.Editor localEditor;
    if (str == null)
    {
      localEditor = localSharedPreferences.edit();
      if (Locale.getDefault() != Locale.US) {
        break label117;
      }
    }
    label117:
    for (str = "1";; str = "0")
    {
      localEditor.putString(App.a.getString(a.h.temperature_units_key), str);
      localEditor.commit();
      if (!str.equals("1")) {
        break;
      }
      float f = 32.0F + 1.8F * (paramInt / 10.0F);
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Float.valueOf(f);
      return String.format("%.0fºF", arrayOfObject2);
    }
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Float.valueOf(paramInt / 10.0F);
    return String.format("%.0fºC", arrayOfObject1);
  }
  
  public static String a(double paramDouble)
  {
    if (paramDouble < 1.0D) {
      return "-";
    }
    double d = paramDouble / 1000.0D;
    String str = "%.0fMHz";
    if (d >= 1000.0D)
    {
      d /= 1000.0D;
      str = "%.1fGHz";
    }
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Double.valueOf(d);
    return String.format(str, arrayOfObject);
  }
  
  public static String a(double paramDouble, long paramLong)
  {
    double d = paramDouble * (1000.0D / paramLong);
    String str = "%.0fB/s";
    if (d >= 1000.0D)
    {
      d /= 1024.0D;
      str = "%.0fKB/s";
    }
    if (d >= 1000.0D)
    {
      d /= 1024.0D;
      str = "%.1fMB/s";
    }
    if (d >= 1000.0D)
    {
      d /= 1024.0D;
      str = "%.1GB/s";
    }
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Double.valueOf(d);
    return String.format(str, arrayOfObject);
  }
  
  public static String a(float paramFloat)
  {
    if (paramFloat < -0.1F) {
      return "0%";
    }
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Float.valueOf(paramFloat);
    return String.format("%.0f%%", arrayOfObject);
  }
  
  public static String a(int paramInt)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Double.valueOf(paramInt / 1000.0D / 1000.0D);
    return String.format("%.0fMHz", arrayOfObject);
  }
  
  public static String a(long paramLong)
  {
    double d = paramLong;
    String str = "%.0fB";
    if (d >= 1000.0D)
    {
      d /= 1024.0D;
      str = "%.0fKB";
    }
    if (d >= 1000.0D)
    {
      d /= 1024.0D;
      str = "%.1fMB";
    }
    if (d >= 1000.0D)
    {
      d /= 1024.0D;
      str = "%.1fGB";
    }
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Double.valueOf(d);
    return String.format(str, arrayOfObject);
  }
  
  public static String a(long paramLong, Context paramContext)
  {
    return android.text.format.DateFormat.getTimeFormat(paramContext).format(new Date(paramLong));
  }
  
  public static String a(Date paramDate)
  {
    Calendar localCalendar1 = GregorianCalendar.getInstance();
    Calendar localCalendar2 = GregorianCalendar.getInstance();
    localCalendar2.setTime(paramDate);
    int i = localCalendar1.get(6) - localCalendar2.get(6);
    int j;
    if (i == 0) {
      j = a.h.today;
    }
    for (;;)
    {
      return App.a.getString(j);
      if (i == 1) {
        j = a.h.yesterday;
      } else if (i == -1) {
        j = a.h.tomorrow;
      } else {
        j = a[(-1 + localCalendar2.get(7))];
      }
    }
  }
  
  public static boolean a(Context paramContext)
  {
    try
    {
      Signature[] arrayOfSignature = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 64).signatures;
      String[] arrayOfString = paramContext.getResources().getStringArray(a.a.chaves);
      int i = arrayOfSignature.length;
      for (int j = 0; j < i; j++)
      {
        String str = arrayOfSignature[j].toCharsString();
        int k = arrayOfString.length;
        for (int m = 0; m < k; m++)
        {
          boolean bool = str.equals(arrayOfString[m]);
          if (bool) {
            return false;
          }
        }
      }
      return true;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      localNameNotFoundException.printStackTrace();
    }
  }
  
  public static CharSequence b(int paramInt)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Double.valueOf(paramInt / 1000.0D);
    return String.format("%.0fMHz", arrayOfObject);
  }
  
  public static String b(double paramDouble)
  {
    String str = "%.0fMB";
    if (paramDouble >= 1000.0D)
    {
      paramDouble /= 1024.0D;
      str = "%.1fGB";
    }
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Double.valueOf(paramDouble);
    return String.format(str, arrayOfObject);
  }
  
  public static String b(float paramFloat)
  {
    if (paramFloat <= -0.1F) {
      return "Offline";
    }
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Float.valueOf(paramFloat);
    return String.format("%.0f%%", arrayOfObject);
  }
  
  public static String b(long paramLong)
  {
    double d = paramLong;
    int i = 0;
    for (;;)
    {
      if ((d < 1024.0D) || (i == -1 + b.length))
      {
        String str = b[i];
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Double.valueOf(d);
        return String.format(str, arrayOfObject);
      }
      i++;
      d /= 1024.0D;
    }
  }
  
  public static String c(float paramFloat)
  {
    if (paramFloat < 1.0F)
    {
      Locale localLocale3 = Locale.getDefault();
      Object[] arrayOfObject3 = new Object[1];
      arrayOfObject3[0] = Float.valueOf(paramFloat);
      return String.format(localLocale3, "%.2f%%", arrayOfObject3);
    }
    if (paramFloat < 10.0F)
    {
      Locale localLocale2 = Locale.getDefault();
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Float.valueOf(paramFloat);
      return String.format(localLocale2, "%.1f%%", arrayOfObject2);
    }
    Locale localLocale1 = Locale.getDefault();
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Float.valueOf(paramFloat);
    return String.format(localLocale1, "%.0f%%", arrayOfObject1);
  }
  
  public static String c(long paramLong)
  {
    return b(paramLong / 1048576L);
  }
  
  public static String d(long paramLong)
  {
    long l1 = paramLong / 1000L;
    long l2 = l1 / 60L % 60L;
    long l3 = l1 / 3600L % 24L;
    long l4 = l1 / 3600L / 24L;
    long l5 = l1 % 60L;
    Context localContext1 = App.a;
    int i;
    String str1;
    int j;
    label89:
    String str2;
    int k;
    label115:
    String str3;
    Context localContext4;
    if (l4 == 1L)
    {
      i = a.h.day;
      str1 = localContext1.getString(i);
      Context localContext2 = App.a;
      if (l3 != 1L) {
        break label210;
      }
      j = a.h.hour;
      str2 = localContext2.getString(j);
      Context localContext3 = App.a;
      if (l2 != 1L) {
        break label218;
      }
      k = a.h.minute;
      str3 = localContext3.getString(k);
      localContext4 = App.a;
      if (l5 != 1L) {
        break label226;
      }
    }
    String str4;
    label210:
    label218:
    label226:
    for (int m = a.h.second;; m = a.h.seconds)
    {
      str4 = localContext4.getString(m);
      if (l4 < 1L) {
        break label234;
      }
      Object[] arrayOfObject5 = new Object[4];
      arrayOfObject5[0] = Long.valueOf(l4);
      arrayOfObject5[1] = str1;
      arrayOfObject5[2] = Long.valueOf(l3);
      arrayOfObject5[3] = str2;
      return String.format("%d %s %d %s", arrayOfObject5);
      i = a.h.days;
      break;
      j = a.h.hours;
      break label89;
      k = a.h.minutes;
      break label115;
    }
    label234:
    if (l3 >= 1L)
    {
      Object[] arrayOfObject4 = new Object[4];
      arrayOfObject4[0] = Long.valueOf(l3);
      arrayOfObject4[1] = str2;
      arrayOfObject4[2] = Long.valueOf(l2);
      arrayOfObject4[3] = str3;
      return String.format("%d %s %d %s", arrayOfObject4);
    }
    if (l2 > 1L)
    {
      Object[] arrayOfObject3 = new Object[2];
      arrayOfObject3[0] = Long.valueOf(l2);
      arrayOfObject3[1] = str3;
      return String.format("%d %s", arrayOfObject3);
    }
    if (l2 >= 1L)
    {
      Object[] arrayOfObject2 = new Object[4];
      arrayOfObject2[0] = Long.valueOf(l2);
      arrayOfObject2[1] = str3;
      arrayOfObject2[2] = Long.valueOf(l5);
      arrayOfObject2[3] = str4;
      return String.format("%d %s %d %s", arrayOfObject2);
    }
    Object[] arrayOfObject1 = new Object[2];
    arrayOfObject1[0] = Long.valueOf(l5);
    arrayOfObject1[1] = str4;
    return String.format("%d %s", arrayOfObject1);
  }
  
  public static String e(long paramLong)
  {
    long l1 = paramLong / 1000L;
    long l2 = l1 / 60L % 60L;
    long l3 = l1 / 3600L % 24L;
    long l4 = l1 / 3600L / 24L;
    long l5 = l1 % 60L;
    if (l4 >= 1L)
    {
      Object[] arrayOfObject3 = new Object[4];
      arrayOfObject3[0] = Long.valueOf(l4);
      arrayOfObject3[1] = "d";
      arrayOfObject3[2] = Long.valueOf(l3);
      arrayOfObject3[3] = "h";
      return String.format("%02d%s%02d%s", arrayOfObject3);
    }
    if (l3 >= 1L)
    {
      Object[] arrayOfObject2 = new Object[4];
      arrayOfObject2[0] = Long.valueOf(l3);
      arrayOfObject2[1] = "h";
      arrayOfObject2[2] = Long.valueOf(l2);
      arrayOfObject2[3] = "m";
      return String.format("%02d%s%02d%s", arrayOfObject2);
    }
    Object[] arrayOfObject1 = new Object[4];
    arrayOfObject1[0] = Long.valueOf(l2);
    arrayOfObject1[1] = "m";
    arrayOfObject1[2] = Long.valueOf(l5);
    arrayOfObject1[3] = "s";
    return String.format("%02d%s%02d%s", arrayOfObject1);
  }
  
  public static String f(long paramLong)
  {
    return a(new Date(paramLong));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */