package com.cgollner.systemmonitor.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.apache.commons.io.FileUtils;

@SuppressLint({"DefaultLocale"})
public final class b
{
  static final File a = new File("/proc/stat");
  public static String b = "/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state";
  static HashMap<String, String> c = null;
  static File d = null;
  private static final File e = new File("/proc");
  
  public static float a(String paramString1, String paramString2)
  {
    float f;
    if ((paramString1 == null) || (paramString2 == null)) {
      f = -1.0F;
    }
    long l1;
    long l2;
    long l3;
    long l4;
    boolean bool6;
    do
    {
      boolean bool5;
      do
      {
        boolean bool4;
        do
        {
          boolean bool3;
          do
          {
            boolean bool2;
            do
            {
              boolean bool1;
              do
              {
                return f;
                String[] arrayOfString1 = paramString1.split(" ");
                l1 = c(arrayOfString1);
                l2 = b(arrayOfString1);
                String[] arrayOfString2 = paramString2.split(" ");
                l3 = c(arrayOfString2);
                l4 = b(arrayOfString2);
                bool1 = l1 < 0L;
                f = 0.0F;
              } while (bool1);
              bool2 = l2 < 0L;
              f = 0.0F;
            } while (bool2);
            bool3 = l3 < 0L;
            f = 0.0F;
          } while (bool3);
          bool4 = l4 < 0L;
          f = 0.0F;
        } while (bool4);
        bool5 = l4 + l3 < l2 + l1;
        f = 0.0F;
      } while (!bool5);
      bool6 = l4 < l2;
      f = 0.0F;
    } while (bool6);
    return 100.0F * ((float)(l4 - l2) / (float)(l3 + l4 - (l1 + l2)));
  }
  
  private static float a(String paramString1, String paramString2, long paramLong)
  {
    int i = 0;
    for (;;)
    {
      try
      {
        long l1 = d(paramString1.split(" "));
        long l2 = d(paramString2.split(" "));
        if ((l1 >= 0L) && (l2 >= l1) && (paramLong > 0.0D))
        {
          f1 = 100.0F * (float)(l2 - l1) / (float)paramLong;
          float f2 = Math.max(0.0F, f1);
          int j = b();
          int[] arrayOfInt1 = new int[j];
          int[] arrayOfInt2 = new int[j];
          int k = 0;
          if (k < j)
          {
            arrayOfInt1[0] = f(0);
            arrayOfInt2[0] = e(0);
            k++;
            continue;
          }
          int m = arrayOfInt1.length;
          int n = 0;
          int i1 = 0;
          if (n < m)
          {
            i1 += arrayOfInt1[n];
            n++;
            continue;
          }
          int i2 = arrayOfInt2.length;
          int i3 = 0;
          if (i < i2)
          {
            int i4 = arrayOfInt2[i];
            i3 += i4;
            i++;
            continue;
          }
          return f2 * (i1 / i3);
        }
      }
      catch (Exception localException)
      {
        return 0.0F;
      }
      float f1 = 0.0F;
    }
  }
  
  public static float a(String paramString1, String paramString2, long paramLong, float paramFloat)
  {
    return paramFloat / 100.0F * a(paramString1, paramString2, paramLong);
  }
  
  public static String a()
  {
    for (;;)
    {
      try
      {
        localBufferedReader = new BufferedReader(new FileReader(new File("/proc/stat")));
        localObject = localBufferedReader.readLine();
        if (localObject == null) {
          continue;
        }
        boolean bool = ((String)localObject).contains("cpu ");
        if (!bool) {}
      }
      catch (IOException localIOException1)
      {
        BufferedReader localBufferedReader;
        String str1;
        String str2;
        Object localObject = null;
        localIOException1.printStackTrace();
        continue;
        localObject = null;
        continue;
      }
      try
      {
        localBufferedReader.close();
        str1 = null;
        if (localObject != null) {
          str1 = ((String)localObject).substring(5);
        }
        return str1;
      }
      catch (IOException localIOException2)
      {
        continue;
      }
      str2 = localBufferedReader.readLine();
      localObject = str2;
    }
  }
  
  public static String a(int paramInt)
  {
    try
    {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile("/proc/" + paramInt + "/stat", "r");
      String str2 = localRandomAccessFile.readLine();
      str1 = str2;
      IOException localIOException2;
      localIOException2.printStackTrace();
    }
    catch (IOException localIOException1)
    {
      try
      {
        localRandomAccessFile.close();
        return str1;
      }
      catch (IOException localIOException3)
      {
        String str1;
        for (;;) {}
      }
      localIOException1 = localIOException1;
      str1 = null;
      localIOException2 = localIOException1;
    }
    return str1;
  }
  
  public static void a(Context paramContext)
  {
    LinkedList localLinkedList = new LinkedList();
    BufferedReader localBufferedReader = new BufferedReader(new FileReader(b));
    localLinkedList.add(new a(0, Math.max(0L, SystemClock.elapsedRealtime() - SystemClock.uptimeMillis())));
    for (;;)
    {
      String str = localBufferedReader.readLine();
      if (str == null) {
        break;
      }
      String[] arrayOfString = str.split(" ");
      localLinkedList.add(new a(Integer.parseInt(arrayOfString[0]), Math.max(0L, 10L * Long.parseLong(arrayOfString[1]))));
    }
    localBufferedReader.close();
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    Iterator localIterator = localLinkedList.iterator();
    while (localIterator.hasNext())
    {
      a locala = (a)localIterator.next();
      localEditor.putLong(locala.a, locala.b);
    }
    localEditor.commit();
  }
  
  public static void a(String[] paramArrayOfString)
  {
    for (;;)
    {
      try
      {
        BufferedReader localBufferedReader = new BufferedReader(new FileReader(a));
        String str = localBufferedReader.readLine();
        int i = 0;
        if (str != null) {
          if (str.startsWith("cpu"))
          {
            if (!Character.isDigit(str.charAt(3))) {
              break label100;
            }
            j = 1 + Character.getNumericValue(str.charAt(3));
            paramArrayOfString[j] = str.substring(5);
            i++;
            if (i >= paramArrayOfString.length) {}
          }
          else
          {
            str = localBufferedReader.readLine();
            continue;
          }
        }
        localBufferedReader.close();
        return;
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
        return;
      }
      label100:
      int j = 0;
    }
  }
  
  public static int b()
  {
    String[] arrayOfString = new File("/sys/devices/system/cpu").list();
    int i = arrayOfString.length;
    int j = 0;
    int k = 0;
    int m;
    label47:
    int n;
    if (j < i)
    {
      String str = arrayOfString[j];
      if (str.length() >= 4)
      {
        m = str.charAt(3);
        if ((m < 48) || (m > 57)) {
          break label100;
        }
        n = 1;
        label64:
        if ((!str.startsWith("cpu")) || (n == 0)) {
          break label112;
        }
      }
    }
    label100:
    label112:
    for (int i1 = k + 1;; i1 = k)
    {
      j++;
      k = i1;
      break;
      m = 107;
      break label47;
      n = 0;
      break label64;
      return Math.min(4, k);
    }
  }
  
  public static long b(String[] paramArrayOfString)
  {
    long l1 = 0L;
    int i = 0;
    while (i < paramArrayOfString.length)
    {
      if (i != 3) {}
      try
      {
        long l2 = Long.parseLong(paramArrayOfString[i]);
        l1 += l2;
        i++;
      }
      catch (NumberFormatException localNumberFormatException)
      {
        l1 = -1L;
      }
    }
    return l1;
  }
  
  public static CharSequence b(int paramInt)
  {
    String[] arrayOfString = c.a("/proc/" + paramInt + "/stat").split(" ");
    if (arrayOfString.length > 1)
    {
      String str = arrayOfString[1];
      int i = 1 + str.indexOf('(');
      int j = str.indexOf(')');
      if (i == -1) {
        i = 0;
      }
      if (j == -1) {
        j = str.length();
      }
      return str.substring(i, j);
    }
    return "";
  }
  
  public static List<a> b(Context paramContext)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    LinkedList localLinkedList = new LinkedList();
    BufferedReader localBufferedReader = new BufferedReader(new FileReader(b));
    long l1 = localSharedPreferences.getLong("0", 0L);
    long l2 = Math.max(0L, SystemClock.elapsedRealtime() - SystemClock.uptimeMillis() - l1);
    localLinkedList.add(new a(0, l2));
    a locala3;
    for (long l3 = l2;; l3 += locala3.b)
    {
      String str = localBufferedReader.readLine();
      if (str == null) {
        break;
      }
      String[] arrayOfString = str.split(" ");
      int i = Integer.parseInt(arrayOfString[0]);
      long l5 = Integer.parseInt(arrayOfString[1]);
      long l6 = localSharedPreferences.getLong(String.valueOf(i), 0L);
      locala3 = new a(i, Math.max(0L, l5 * 10L - l6));
      localLinkedList.add(locala3);
    }
    Iterator localIterator1 = localLinkedList.iterator();
    while (localIterator1.hasNext())
    {
      a locala2 = (a)localIterator1.next();
      locala2.c = ((float)(100.0D * (locala2.b / l3)));
      if (locala2.c < 1.0F) {
        localIterator1.remove();
      }
    }
    Iterator localIterator2 = localLinkedList.iterator();
    for (long l4 = 0L; localIterator2.hasNext(); l4 += ((a)localIterator2.next()).b) {}
    Iterator localIterator3 = localLinkedList.iterator();
    while (localIterator3.hasNext())
    {
      a locala1 = (a)localIterator3.next();
      locala1.c = ((float)(100.0D * (locala1.b / l4)));
    }
    Collections.sort(localLinkedList);
    localBufferedReader.close();
    return localLinkedList;
  }
  
  private static long c(String[] paramArrayOfString)
  {
    try
    {
      long l = Long.parseLong(paramArrayOfString[3]);
      return l;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      localNumberFormatException.printStackTrace();
    }
    return -1L;
  }
  
  public static List<Integer> c()
  {
    ArrayList localArrayList = new ArrayList();
    String[] arrayOfString = e.list();
    int i = arrayOfString.length;
    int j = 0;
    for (;;)
    {
      String str;
      if (j < i) {
        str = arrayOfString[j];
      }
      try
      {
        localArrayList.add(Integer.valueOf(Integer.parseInt(str)));
        j++;
        continue;
        return localArrayList;
      }
      catch (Exception localException)
      {
        for (;;) {}
      }
    }
  }
  
  public static boolean c(int paramInt)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    return Integer.parseInt(c.a(String.format("sys/devices/system/cpu/cpu%d/online", arrayOfObject))) == 0;
  }
  
  public static int d(int paramInt)
  {
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Integer.valueOf(paramInt);
    int i = Integer.parseInt(c.a(String.format("/sys/devices/system/cpu/cpu%d/cpufreq/scaling_min_freq", arrayOfObject1)));
    if (i > 0) {
      return i;
    }
    Object[] arrayOfObject2 = new Object[1];
    arrayOfObject2[0] = Integer.valueOf(paramInt);
    return Integer.parseInt(c.a(String.format("/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_min_freq", arrayOfObject2)));
  }
  
  private static long d(String[] paramArrayOfString)
  {
    return Long.parseLong(paramArrayOfString[13]) + Long.parseLong(paramArrayOfString[14]);
  }
  
  public static Set<Integer> d()
  {
    HashSet localHashSet = new HashSet();
    String[] arrayOfString = e.list();
    int i = arrayOfString.length;
    int j = 0;
    for (;;)
    {
      String str;
      if (j < i) {
        str = arrayOfString[j];
      }
      try
      {
        localHashSet.add(Integer.valueOf(Integer.parseInt(str)));
        j++;
        continue;
        return localHashSet;
      }
      catch (Exception localException)
      {
        for (;;) {}
      }
    }
  }
  
  public static int e()
  {
    File localFile = f();
    d = localFile;
    if ((localFile == null) || (!d.exists())) {
      return -1;
    }
    try
    {
      int i = Integer.parseInt(FileUtils.readFileToString(d).trim().substring(0, 2));
      return i;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return -1;
  }
  
  public static int e(int paramInt)
  {
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Integer.valueOf(paramInt);
    int i = Integer.parseInt(c.a(String.format("/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq", arrayOfObject1)));
    if (i > 0) {
      return i;
    }
    Object[] arrayOfObject2 = new Object[1];
    arrayOfObject2[0] = Integer.valueOf(paramInt);
    return Integer.parseInt(c.a(String.format("/sys/devices/system/cpu/cpu%d/cpufreq/cpuinfo_max_freq", arrayOfObject2)));
  }
  
  public static int f(int paramInt)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    return Integer.parseInt(c.a(String.format("/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq", arrayOfObject)));
  }
  
  public static File f()
  {
    if (d != null) {
      return d;
    }
    if (c == null)
    {
      HashMap localHashMap = new HashMap();
      c = localHashMap;
      localHashMap.put("tuna", "/sys/devices/platform/omap/omap_temp_sensor.0/temperature");
      c.put("maguro", "/sys/devices/platform/omap/omap_temp_sensor.0/temperature");
      c.put("toro", "/sys/devices/platform/omap/omap_temp_sensor.0/temperature");
      c.put("toroplus", "/sys/devices/platform/omap/omap_temp_sensor.0/temperature");
      c.put("mako", "/sys/devices/virtual/thermal/thermal_zone7/temp");
      c.put("grouper", "/sys/devices/platform/tegra-i2c.4/i2c-4/4-004c/temperature");
      c.put("flo", "/sys/devices/virtual/thermal/thermal_zone0/temp");
      c.put("hammerhead", "/sys/devices/virtual/thermal/thermal_zone7/temp");
      c.put("m7", "/sys/devices/virtual/thermal/thermal_zone1/temp");
    }
    try
    {
      File localFile = new File((String)c.get(Build.DEVICE.toLowerCase(Locale.US)));
      return localFile;
    }
    catch (Exception localException) {}
    return null;
  }
  
  public static final class a
    implements Comparable<a>
  {
    public int a;
    public long b;
    public float c;
    public String d;
    
    public a(int paramInt, long paramLong)
    {
      this.a = paramInt;
      this.b = paramLong;
      if (paramInt == 0) {}
      for (String str = "Deep sleep";; str = i.a(paramInt))
      {
        this.d = str;
        return;
      }
    }
    
    public final String toString()
    {
      return "[frequency=" + this.a + ", time=" + this.b + ", percentage=" + this.c + "]";
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */