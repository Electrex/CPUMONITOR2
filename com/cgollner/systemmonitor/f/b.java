package com.cgollner.systemmonitor.f;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import com.cgollner.systemmonitor.BatteryCircleView;
import com.cgollner.systemmonitor.MonitorView;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.battery.BatteryService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class b
  extends AppWidgetProvider
{
  private com.cgollner.systemmonitor.battery.a a;
  
  @SuppressLint({"WorldReadableFiles"})
  private static Uri a(Bitmap paramBitmap, String paramString, Context paramContext)
  {
    try
    {
      File localFile1 = paramContext.getFileStreamPath(paramString + ".png");
      File localFile2 = paramContext.getFileStreamPath(paramString + "_1.png");
      if (localFile1.exists()) {
        localFile1.delete();
      }
      for (;;)
      {
        FileOutputStream localFileOutputStream = paramContext.openFileOutput(localFile2.getName(), 1);
        paramBitmap.compress(Bitmap.CompressFormat.PNG, 100, localFileOutputStream);
        localFileOutputStream.close();
        return Uri.fromFile(localFile2);
        localFile2.delete();
        localFile2 = localFile1;
      }
      return null;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
  
  @TargetApi(16)
  private void a(Context paramContext, int paramInt, AppWidgetManager paramAppWidgetManager)
  {
    int i = 200;
    RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), a.f.battery_widget_layout);
    int k;
    int j;
    int m;
    if (Build.VERSION.SDK_INT >= 16)
    {
      Bundle localBundle = paramAppWidgetManager.getAppWidgetOptions(paramInt);
      k = Math.max(localBundle.getInt("appWidgetMinWidth"), localBundle.getInt("appWidgetMaxWidth"));
      j = Math.max(localBundle.getInt("appWidgetMinHeight"), localBundle.getInt("appWidgetMinHeight"));
      m = (int)MonitorView.a(Math.min(k, j), paramContext.getResources());
      if (m > 0) {
        break label446;
      }
    }
    for (;;)
    {
      Bitmap localBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
      Canvas localCanvas = new Canvas(localBitmap);
      BatteryCircleView localBatteryCircleView = new BatteryCircleView(paramContext);
      SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
      localBatteryCircleView.setTextColor(localSharedPreferences.getInt(paramInt + "-textColor", -1));
      localBatteryCircleView.setInnerCircleWidth(localSharedPreferences.getInt(paramInt + "-innerCircleWidth", 10));
      localBatteryCircleView.setInnerCircleColor(localSharedPreferences.getInt(paramInt + "-innerCircleColor", -7829368));
      localBatteryCircleView.setOuterCircleWidth(localSharedPreferences.getInt(paramInt + "-outerCircleWidth", 10));
      localBatteryCircleView.setPercentageCircleWidth(localSharedPreferences.getInt(paramInt + "-percentageCircleWidth", 100));
      localBatteryCircleView.setInnerCircleBgColor(localSharedPreferences.getInt(paramInt + "-innerCircleBG", 0));
      localBatteryCircleView.setValue(this.a.a);
      localBatteryCircleView.draw(localCanvas);
      localRemoteViews.setImageViewUri(a.e.batteryWidgetImage, a(localBitmap, paramInt + "bttrywidgetimg", paramContext));
      Intent localIntent = new Intent(paramContext, a.class);
      localIntent.putExtra("appWidgetId", paramInt);
      PendingIntent localPendingIntent = PendingIntent.getActivity(paramContext, 0, localIntent, 134217728);
      localRemoteViews.setOnClickPendingIntent(a.e.batteryWidgetImage, localPendingIntent);
      paramAppWidgetManager.updateAppWidget(paramInt, localRemoteViews);
      return;
      j = i;
      k = i;
      break;
      label446:
      i = m;
    }
  }
  
  @TargetApi(16)
  public void onAppWidgetOptionsChanged(Context paramContext, AppWidgetManager paramAppWidgetManager, int paramInt, Bundle paramBundle)
  {
    this.a = BatteryService.c(paramContext);
    a(paramContext, paramInt, paramAppWidgetManager);
  }
  
  public void onDeleted(Context paramContext, int[] paramArrayOfInt)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
    {
      int k = paramArrayOfInt[j];
      localEditor.remove(k + "-textColor");
      localEditor.remove(k + "-innerCircleWidth");
      localEditor.remove(k + "-innerCircleColor");
      localEditor.remove(k + "-outerCircleWidth");
      localEditor.remove(k + "-percentageCircleWidth");
      localEditor.remove(k + "-innerCircleBG");
      paramContext.getFileStreamPath(k + "bttrywidgetimg.png").delete();
      paramContext.getFileStreamPath(k + "bttrywidgetimg_1.png").delete();
    }
    localEditor.commit();
    super.onDeleted(paramContext, paramArrayOfInt);
  }
  
  public void onEnabled(Context paramContext)
  {
    paramContext.sendBroadcast(new Intent("com.cgollner.systemmonitor.battery.ACTION_INFO_REQUEST"));
    super.onEnabled(paramContext);
  }
  
  @TargetApi(16)
  public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
  {
    this.a = BatteryService.c(paramContext);
    if (this.a != null)
    {
      int[] arrayOfInt = paramAppWidgetManager.getAppWidgetIds(new ComponentName(paramContext, b.class));
      int i = arrayOfInt.length;
      for (int j = 0; j < i; j++) {
        a(paramContext, arrayOfInt[j], paramAppWidgetManager);
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/f/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */