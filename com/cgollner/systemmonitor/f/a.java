package com.cgollner.systemmonitor.f;

import android.app.WallpaperManager;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.cgollner.systemmonitor.BatteryCircleView;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import net.margaritov.preference.colorpicker.ColorPickerDialog;
import net.margaritov.preference.colorpicker.ColorPickerDialog.OnColorChangedListener;

public class a
  extends ActionBarActivity
  implements SeekBar.OnSeekBarChangeListener
{
  private SeekBar.OnSeekBarChangeListener A = new SeekBar.OnSeekBarChangeListener()
  {
    public final void onProgressChanged(SeekBar paramAnonymousSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
    {
      a.e(a.this).setPercentageCircleWidth(paramAnonymousInt);
      a.e(a.this).invalidate();
    }
    
    public final void onStartTrackingTouch(SeekBar paramAnonymousSeekBar) {}
    
    public final void onStopTrackingTouch(SeekBar paramAnonymousSeekBar) {}
  };
  private SeekBar.OnSeekBarChangeListener B = new SeekBar.OnSeekBarChangeListener()
  {
    public final void onProgressChanged(SeekBar paramAnonymousSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
    {
      a.e(a.this).setInnerCircleWidth(paramAnonymousInt);
      a.e(a.this).invalidate();
      a.g(a.this).setText("Inner circle width: " + paramAnonymousInt);
    }
    
    public final void onStartTrackingTouch(SeekBar paramAnonymousSeekBar) {}
    
    public final void onStopTrackingTouch(SeekBar paramAnonymousSeekBar) {}
  };
  private View.OnClickListener C = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      ColorPickerDialog localColorPickerDialog = new ColorPickerDialog(a.this, a.e(a.this).getTextColor());
      localColorPickerDialog.setOnColorChangedListener(a.h(a.this));
      localColorPickerDialog.setAlphaSliderVisible(true);
      localColorPickerDialog.show();
    }
  };
  private ColorPickerDialog.OnColorChangedListener D = new ColorPickerDialog.OnColorChangedListener()
  {
    public final void onColorChanged(int paramAnonymousInt)
    {
      a.i(a.this).setBackgroundColor(paramAnonymousInt);
      a.e(a.this).setTextColor(paramAnonymousInt);
      a.e(a.this).invalidate();
    }
  };
  private View.OnClickListener E = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      ColorPickerDialog localColorPickerDialog = new ColorPickerDialog(a.this, a.e(a.this).getInnerCircleColor());
      localColorPickerDialog.setOnColorChangedListener(a.j(a.this));
      localColorPickerDialog.setAlphaSliderVisible(true);
      localColorPickerDialog.show();
    }
  };
  private ColorPickerDialog.OnColorChangedListener F = new ColorPickerDialog.OnColorChangedListener()
  {
    public final void onColorChanged(int paramAnonymousInt)
    {
      a.k(a.this).setBackgroundColor(paramAnonymousInt);
      a.e(a.this).setInnerCircleColor(paramAnonymousInt);
      a.e(a.this).invalidate();
    }
  };
  private View.OnClickListener G = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      ColorPickerDialog localColorPickerDialog = new ColorPickerDialog(a.this, a.e(a.this).getInnerCircleColor());
      localColorPickerDialog.setOnColorChangedListener(a.l(a.this));
      localColorPickerDialog.setAlphaSliderVisible(true);
      localColorPickerDialog.show();
    }
  };
  private ColorPickerDialog.OnColorChangedListener H = new ColorPickerDialog.OnColorChangedListener()
  {
    public final void onColorChanged(int paramAnonymousInt)
    {
      a.m(a.this).setBackgroundColor(paramAnonymousInt);
      a.e(a.this).setInnerCircleBgColor(paramAnonymousInt);
      a.e(a.this).invalidate();
    }
  };
  private BatteryCircleView a;
  private SeekBar b;
  private SeekBar c;
  private SeekBar d;
  private SeekBar e;
  private TextView f;
  private TextView g;
  private TextView h;
  private View i;
  private View j;
  private View k;
  private View l;
  private View m;
  private View n;
  private View o;
  private View p;
  private View q;
  private View r;
  private View s;
  private View[] t;
  private int u;
  private View v;
  private int w;
  private View.OnClickListener x = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      a.a(a.this, (1 + a.a(a.this)) % a.b(a.this).length);
      a.c(a.this);
    }
  };
  private View.OnClickListener y = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      a.d(a.this);
      if (a.a(a.this) < 0) {
        a.a(a.this, -1 + a.b(a.this).length);
      }
      a.c(a.this);
    }
  };
  private SeekBar.OnSeekBarChangeListener z = new SeekBar.OnSeekBarChangeListener()
  {
    public final void onProgressChanged(SeekBar paramAnonymousSeekBar, int paramAnonymousInt, boolean paramAnonymousBoolean)
    {
      a.e(a.this).setOuterCircleWidth(paramAnonymousInt);
      a.e(a.this).invalidate();
      a.f(a.this).setText("Outer circle width: " + paramAnonymousInt);
    }
    
    public final void onStartTrackingTouch(SeekBar paramAnonymousSeekBar) {}
    
    public final void onStopTrackingTouch(SeekBar paramAnonymousSeekBar) {}
  };
  
  private void a()
  {
    int i1 = 0;
    if (i1 < this.t.length)
    {
      if (i1 == this.u) {
        this.t[i1].setVisibility(0);
      }
      for (;;)
      {
        i1++;
        break;
        this.t[i1].setVisibility(8);
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(a.f.battery_circle_layout);
    this.a = ((BatteryCircleView)findViewById(a.e.batteryView));
    this.d = ((SeekBar)findViewById(a.e.percentageSeekBar));
    this.d.setOnSeekBarChangeListener(this);
    Drawable localDrawable = WallpaperManager.getInstance(getApplicationContext()).getDrawable();
    this.a.setBackgroundDrawable(localDrawable);
    this.g = ((TextView)findViewById(a.e.percentageText));
    this.m = findViewById(a.e.percentageLayout);
    this.p = findViewById(a.e.nextButton);
    this.p.setOnClickListener(this.x);
    this.q = findViewById(a.e.prevButton);
    this.q.setOnClickListener(this.y);
    this.n = findViewById(a.e.textColorLayout);
    this.n.setOnClickListener(this.C);
    this.r = findViewById(a.e.colorSquare);
    this.r.setBackgroundColor(this.a.getTextColor());
    this.r.setOnClickListener(this.C);
    this.o = findViewById(a.e.circleWidthLayout);
    this.e = ((SeekBar)findViewById(a.e.circleWidthSeekBar));
    this.e.setProgress(this.a.getCircleWidthMultiplier());
    this.e.setOnSeekBarChangeListener(this.B);
    this.h = ((TextView)findViewById(a.e.circleWidthText));
    this.l = findViewById(a.e.innerCircleColorLayout);
    this.s = findViewById(a.e.colorInnerCircle);
    this.s.setBackgroundColor(this.a.getInnerCircleColor());
    this.l.setOnClickListener(this.E);
    this.s.setOnClickListener(this.E);
    this.i = findViewById(a.e.innerCircleBgColorLayout);
    this.j = findViewById(a.e.colorInnerCircleBg);
    this.j.setOnClickListener(this.G);
    this.i.setOnClickListener(this.G);
    this.k = findViewById(a.e.outerCircleWidthLayout);
    this.f = ((TextView)findViewById(a.e.outerCircleWidthText));
    this.c = ((SeekBar)findViewById(a.e.outerCircleWidthSeekBar));
    this.c.setOnSeekBarChangeListener(this.z);
    this.v = findViewById(a.e.percentageCircleWidthLayout);
    this.b = ((SeekBar)findViewById(a.e.percentageCircleWidthSeekBar));
    this.b.setProgress(175);
    this.b.setOnSeekBarChangeListener(this.A);
    this.t = new View[7];
    this.t[0] = this.m;
    this.t[1] = this.n;
    this.t[2] = this.o;
    this.t[3] = this.l;
    this.t[4] = this.i;
    this.t[5] = this.k;
    this.t[6] = this.v;
    this.u = 0;
    a();
    setResult(0);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null) {
      this.w = localBundle.getInt("appWidgetId", 0);
    }
    if (this.w == 0) {
      finish();
    }
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    this.a.setTextColor(localSharedPreferences.getInt(this.w + "-textColor", -1));
    this.a.setInnerCircleWidth(localSharedPreferences.getInt(this.w + "-innerCircleWidth", 100));
    this.a.setInnerCircleColor(localSharedPreferences.getInt(this.w + "-innerCircleColor", -16777216));
    this.a.setOuterCircleWidth(localSharedPreferences.getInt(this.w + "-outerCircleWidth", 10));
    this.a.setPercentageCircleWidth(localSharedPreferences.getInt(this.w + "-percentageCircleWidth", 100));
    this.a.setInnerCircleBgColor(localSharedPreferences.getInt(this.w + "-innerCircleBG", 0));
    this.r.setBackgroundColor(this.a.getTextColor());
    this.h.setText("Inner circle width: " + this.a.getCircleWidthMultiplier());
    this.s.setBackgroundColor(this.a.getInnerCircleColor());
    this.f.setText("Outer circle width: " + this.a.getOuterCircleWidth());
    this.j.setBackgroundColor(this.a.getInnerCircleBgColor());
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(a.g.activity_configure_battery_widget, paramMenu);
    return super.onCreateOptionsMenu(paramMenu);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
    localEditor.putInt(this.w + "-textColor", this.a.getTextColor());
    localEditor.putInt(this.w + "-innerCircleWidth", this.a.getCircleWidthMultiplier());
    localEditor.putInt(this.w + "-innerCircleColor", this.a.getInnerCircleColor());
    localEditor.putInt(this.w + "-outerCircleWidth", this.a.getOuterCircleWidth());
    localEditor.putInt(this.w + "-percentageCircleWidth", this.a.getPercentageCircleWidth());
    localEditor.putInt(this.w + "-innerCircleBG", this.a.getInnerCircleBgColor());
    localEditor.commit();
    Intent localIntent = new Intent();
    localIntent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
    localIntent.putExtra("appWidgetId", this.w);
    setResult(-1, localIntent);
    finish();
    new Thread(new Runnable()
    {
      public final void run()
      {
        Context localContext = a.this.getApplicationContext();
        Intent localIntent = new Intent(localContext, b.class);
        ComponentName localComponentName = new ComponentName(localContext, b.class);
        localIntent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        localIntent.putExtra("appWidgetIds", AppWidgetManager.getInstance(localContext).getAppWidgetIds(localComponentName));
        localContext.sendBroadcast(localIntent);
      }
    }).start();
    return true;
  }
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    this.a.setValue(paramInt);
    this.g.setText("Percentage: " + paramInt);
  }
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  public void onStopTrackingTouch(SeekBar paramSeekBar) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/f/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */