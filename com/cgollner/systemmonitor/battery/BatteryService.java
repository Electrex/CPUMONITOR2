package com.cgollner.systemmonitor.battery;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import com.cgollner.systemmonitor.App;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class BatteryService
  extends Service
{
  public static a a;
  public static a b;
  public static List<c> c;
  public static List<c> d;
  public static List<c> e;
  private static a f;
  
  public static a a(Context paramContext)
  {
    Intent localIntent = paramContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    a locala = new a();
    int i = localIntent.getIntExtra("status", -1);
    if ((i == 2) || (i == 5)) {}
    for (boolean bool = true;; bool = false)
    {
      locala.d = bool;
      int j = localIntent.getIntExtra("level", -1);
      int k = localIntent.getIntExtra("scale", -1);
      locala.a = ((int)(100.0F * (j / k)));
      locala.b = localIntent.getIntExtra("temperature", 0);
      locala.c = System.currentTimeMillis();
      return locala;
    }
  }
  
  public static Object a(Context paramContext, String paramString)
  {
    return a(paramContext.getFileStreamPath(paramString));
  }
  
  private static Object a(File paramFile)
  {
    try
    {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramFile, "rws");
      FileLock localFileLock = localRandomAccessFile.getChannel().lock();
      byte[] arrayOfByte = new byte[(int)localRandomAccessFile.length()];
      localRandomAccessFile.read(arrayOfByte);
      localFileLock.release();
      localRandomAccessFile.close();
      ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(arrayOfByte);
      ObjectInputStream localObjectInputStream = new ObjectInputStream(localByteArrayInputStream);
      Object localObject = localObjectInputStream.readObject();
      localByteArrayInputStream.close();
      localObjectInputStream.close();
      return localObject;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      return null;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
  
  public static void a(int paramInt, Context paramContext)
  {
    a(paramContext, "STRATEGY_FILE", Integer.valueOf(paramInt));
    paramContext.sendBroadcast(new Intent("com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"));
  }
  
  public static void a(Context paramContext, int paramInt)
  {
    a(paramContext, "BATT_HIST_SIZE_FILE", Integer.valueOf(paramInt));
  }
  
  public static void a(Context paramContext, String paramString, Object paramObject)
  {
    a(paramContext.getFileStreamPath(paramString), paramObject);
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, Object paramObject)
  {
    a(new File(paramContext.getDir(paramString1, 0), paramString2), paramObject);
  }
  
  public static void a(Context paramContext, String paramString1, String paramString2, byte[] paramArrayOfByte)
  {
    File localFile = new File(paramContext.getDir(paramString1, 0), paramString2);
    try
    {
      try
      {
        RandomAccessFile localRandomAccessFile = new RandomAccessFile(localFile, "rws");
        FileLock localFileLock = localRandomAccessFile.getChannel().lock();
        localRandomAccessFile.write(paramArrayOfByte);
        localFileLock.release();
        localRandomAccessFile.close();
        return;
      }
      finally {}
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
  
  public static void a(Context paramContext, boolean paramBoolean)
  {
    b.b(paramContext, paramBoolean);
    paramContext.sendBroadcast(new Intent("com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"));
  }
  
  private static void a(File paramFile, Object paramObject)
  {
    try
    {
      try
      {
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(localByteArrayOutputStream);
        localObjectOutputStream.writeObject(paramObject);
        byte[] arrayOfByte = localByteArrayOutputStream.toByteArray();
        localObjectOutputStream.close();
        localByteArrayOutputStream.close();
        RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramFile, "rws");
        FileLock localFileLock = localRandomAccessFile.getChannel().lock();
        localRandomAccessFile.write(arrayOfByte);
        localFileLock.release();
        localRandomAccessFile.close();
        return;
      }
      finally {}
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
  
  public static byte[] a(Context paramContext, String paramString1, String paramString2)
  {
    File localFile = new File(paramContext.getDir(paramString1, 0), paramString2);
    try
    {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile(localFile, "rws");
      FileLock localFileLock = localRandomAccessFile.getChannel().lock();
      byte[] arrayOfByte = new byte[(int)localRandomAccessFile.length()];
      localRandomAccessFile.read(arrayOfByte);
      localFileLock.release();
      localRandomAccessFile.close();
      return arrayOfByte;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    return null;
  }
  
  public static Object b(Context paramContext, String paramString1, String paramString2)
  {
    return a(new File(paramContext.getDir(paramString1, 0), paramString2));
  }
  
  public static void b(Context paramContext)
  {
    a(paramContext, "CURRENT_BATTERY_INFO", a);
  }
  
  public static a c(Context paramContext)
  {
    return (a)a(paramContext, "CURRENT_BATTERY_INFO");
  }
  
  public static void d(Context paramContext)
  {
    a(paramContext, "BATTERY_HISTORY_FILE", c);
  }
  
  public static List<c> e(Context paramContext)
  {
    Object localObject = a(paramContext, "BATTERY_HISTORY_FILE");
    if (localObject == null) {
      return new LinkedList();
    }
    return (List)localObject;
  }
  
  public static List<c> f(Context paramContext)
  {
    Object localObject = a(paramContext, "TEMPERATURE_HISTORY_FILE");
    if (localObject == null) {
      return new LinkedList();
    }
    return (List)localObject;
  }
  
  public static void g(Context paramContext)
  {
    try
    {
      a(paramContext, "TEMPERATURE_HISTORY_FILE", d);
      return;
    }
    catch (ConcurrentModificationException localConcurrentModificationException) {}
  }
  
  public static void h(Context paramContext)
  {
    a(paramContext, "PREDICTION_ARRAY", e);
  }
  
  public static List<c> i(Context paramContext)
  {
    return (List)a(paramContext, "PREDICTION_ARRAY");
  }
  
  public static Integer j(Context paramContext)
  {
    Object localObject = a(paramContext, "STRATEGY_FILE");
    if (localObject == null) {
      a(0, paramContext);
    }
    if (localObject == null) {}
    for (int i = 0;; i = ((Integer)localObject).intValue()) {
      return Integer.valueOf(i);
    }
  }
  
  public static void k(Context paramContext)
  {
    b.c(paramContext);
    paramContext.sendBroadcast(new Intent("com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"));
  }
  
  public static void l(Context paramContext)
  {
    Intent localIntent = new Intent("com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST");
    localIntent.putExtra("clear", true);
    paramContext.sendBroadcast(localIntent);
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    unregisterReceiver(f);
    f = null;
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    if (f == null)
    {
      f = new a();
      registerReceiver(f, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
      registerReceiver(f, new IntentFilter("com.cgollner.systemmonitor.battery.ACTION_INFO_REQUEST"));
      registerReceiver(f, new IntentFilter("com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"));
    }
    return 1;
  }
  
  public void onTaskRemoved(Intent paramIntent)
  {
    AlarmManager localAlarmManager = (AlarmManager)getSystemService("alarm");
    Intent localIntent = new Intent(App.a, BatteryService.class);
    PendingIntent localPendingIntent = PendingIntent.getService(App.a, 0, localIntent, 0);
    localAlarmManager.set(1, 5000L + System.currentTimeMillis(), localPendingIntent);
  }
  
  public final class a
    extends BroadcastReceiver
  {
    public a() {}
    
    public final void onReceive(Context paramContext, Intent paramIntent)
    {
      new BatteryService.b(BatteryService.this, paramContext, paramIntent).start();
    }
  }
  
  private final class b
    extends Thread
  {
    private Context b;
    private Intent c;
    private long d;
    private int e;
    
    public b(Context paramContext, Intent paramIntent)
    {
      this.b = paramContext;
      this.c = paramIntent;
    }
    
    public final void run()
    {
      boolean bool1 = this.c.getBooleanExtra("clear", false);
      a locala1;
      boolean bool2;
      int m;
      if ((bool1) || (this.c.getAction().equals("android.intent.action.BATTERY_CHANGED")))
      {
        if (bool1)
        {
          BatteryService.b = null;
          BatteryService.a = null;
          BatteryService.c.clear();
          BatteryService.e.clear();
          BatteryService.d.clear();
          BatteryService.d(this.b);
          BatteryService.g(this.b);
          BatteryService.h(this.b);
          this.c = BatteryService.this.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        }
        locala1 = new a();
        int i = this.c.getIntExtra("status", -1);
        if ((i == 2) || (i == 5))
        {
          bool2 = true;
          locala1.d = bool2;
          int j = this.c.getIntExtra("level", -1);
          int k = this.c.getIntExtra("scale", -1);
          locala1.a = ((int)(100.0F * (j / k)));
          locala1.b = this.c.getIntExtra("temperature", 0);
          locala1.c = System.currentTimeMillis();
          a locala2 = BatteryService.a;
          m = 0;
          if (locala2 == null) {
            break label288;
          }
          if ((locala1.a >= BatteryService.a.a) || (!locala1.d)) {
            break label276;
          }
          m = 1;
          label242:
          if ((BatteryService.a.a - locala1.a != 0) || (BatteryService.a.d != locala1.d)) {
            break label282;
          }
        }
      }
      label276:
      label282:
      label288:
      do
      {
        return;
        bool2 = false;
        break;
        m = 0;
        break label242;
        BatteryService.b = BatteryService.a;
        int n = m;
        BatteryService.a = locala1;
        if (BatteryService.c == null) {
          BatteryService.c = BatteryService.e(this.b);
        }
        if (BatteryService.d == null) {
          BatteryService.d = BatteryService.f(this.b);
        }
        int i1 = BatteryService.m(this.b).intValue();
        synchronized (BatteryService.c)
        {
          if ((BatteryService.b == null) || ((BatteryService.b != null) && (Math.abs(BatteryService.a.a - BatteryService.b.a) != 0)))
          {
            BatteryService.c.add(new c(BatteryService.a.c, BatteryService.a.a));
            try
            {
              Iterator localIterator2 = BatteryService.c.iterator();
              while (localIterator2.hasNext())
              {
                c localc2 = (c)localIterator2.next();
                if (BatteryService.a.c - localc2.a <= 1000L * (3600L * i1)) {
                  break;
                }
                localIterator2.remove();
              }
            }
            catch (ClassCastException localClassCastException)
            {
              LinkedList localLinkedList = new LinkedList();
              BatteryService.c = localLinkedList;
              localLinkedList.add(new c(BatteryService.a.c, BatteryService.a.a));
            }
          }
          synchronized (BatteryService.d)
          {
            BatteryService.d.add(new c(BatteryService.a.c, BatteryService.a.b));
            Iterator localIterator1 = BatteryService.d.iterator();
            if (localIterator1.hasNext())
            {
              c localc1 = (c)localIterator1.next();
              if (BatteryService.a.c - localc1.a > 1000L * (3600L * i1)) {
                localIterator1.remove();
              }
            }
          }
        }
        BatteryService.b(this.b);
        BatteryService.g(this.b);
        BatteryService.d(this.b);
        if ((BatteryService.b != null) && (BatteryService.a.d == BatteryService.b.d) && (n == 0)) {}
        for (int i2 = 1;; i2 = 0)
        {
          if (i2 != 0)
          {
            this.d = (System.currentTimeMillis() - BatteryService.b.c);
            this.e = (BatteryService.a.a - BatteryService.b.a);
            if (((BatteryService.a.d) && (this.e == 1)) || ((!BatteryService.a.d) && (this.e == -1)))
            {
              Context localContext = this.b;
              boolean bool3 = BatteryService.a.d;
              int i3 = BatteryService.a.a;
              long l = this.d;
              Math.abs(this.e);
              b.a(localContext, bool3, i3, l);
            }
          }
          if ((BatteryService.b == null) || (BatteryService.b.d != BatteryService.a.d))
          {
            b.c = null;
            b.a(App.a);
          }
          if (BatteryService.a == null) {
            break;
          }
          BatteryService.e = b.a(this.b, BatteryService.a.d, BatteryService.a.a);
          BatteryService.h(this.b);
          BatteryService.n(this.b);
          return;
        }
        if (this.c.getAction().equals("com.cgollner.systemmonitor.battery.ACTION_UPDATE_REQUEST"))
        {
          BatteryService.e = b.a(this.b, BatteryService.a.d, BatteryService.a.a);
          BatteryService.h(this.b);
          BatteryService.n(this.b);
          return;
        }
      } while ((BatteryService.a == null) || (!this.c.getAction().equals("com.cgollner.systemmonitor.battery.ACTION_INFO_REQUEST")));
      BatteryService.n(this.b);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/battery/BatteryService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */