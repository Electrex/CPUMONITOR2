package com.cgollner.systemmonitor.battery;

import android.content.Context;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class b
{
  public static long a = 782608L;
  public static long b = 216867L;
  public static List<Long> c;
  public static Long d = Long.valueOf(-1L);
  private static Boolean e;
  private static Integer f;
  private static Pattern g;
  private static int h;
  
  private static int a(String paramString)
  {
    int i = 0;
    char[] arrayOfChar = paramString.toCharArray();
    int j = arrayOfChar.length;
    for (int k = 0; k < j; k++) {
      if (arrayOfChar[k] == ',') {
        i++;
      }
    }
    return i;
  }
  
  private static long a(Context paramContext, int paramInt, boolean paramBoolean)
  {
    long l1;
    if (f.intValue() == 2) {
      if (d.longValue() > 0L) {
        l1 = d.longValue();
      }
    }
    for (;;)
    {
      return l1;
      if (paramBoolean) {
        return b;
      }
      return a;
      int i;
      if ((d.longValue() > 0L) && (f.intValue() == 0)) {
        i = 1;
      }
      label74:
      double d1;
      double d2;
      for (;;)
      {
        String str1;
        String str2;
        File localFile2;
        if (paramBoolean)
        {
          str1 = "charging";
          File localFile1 = paramContext.getDir(str1, 0);
          str2 = String.valueOf(paramInt);
          localFile2 = new File(localFile1, str2);
          d1 = Math.max(0.0D, 0.5D - Math.abs(paramInt - h) / 100.0D);
          d2 = 1.0D - d1;
        }
        try
        {
          if (localFile2.exists())
          {
            String str3 = new String(BatteryService.a(paramContext, str1, str2));
            long[] arrayOfLong = new long[a(str3)];
            if (g == null) {
              g = Pattern.compile("\\d+");
            }
            Matcher localMatcher = g.matcher(str3);
            int j = 0;
            for (;;)
            {
              if (localMatcher.find())
              {
                int k = j + 1;
                arrayOfLong[j] = Long.parseLong(localMatcher.group());
                j = k;
                continue;
                i = 0;
                break;
                str1 = "discharging";
                break label74;
              }
            }
            Arrays.sort(arrayOfLong);
            if (i != 0) {
              return (d2 * a(arrayOfLong) + d1 * d.longValue());
            }
            long l2 = a(arrayOfLong);
            return l2;
          }
        }
        catch (Exception localException) {}
      }
      if (paramBoolean) {}
      for (l1 = b; i != 0; l1 = a) {
        return (d2 * l1 + d1 * d.longValue());
      }
    }
  }
  
  public static long a(Context paramContext, boolean paramBoolean)
  {
    String str1;
    File localFile1;
    ArrayList localArrayList;
    if (paramBoolean)
    {
      str1 = "charging";
      localFile1 = paramContext.getDir(str1, 0);
      localArrayList = new ArrayList();
    }
    for (int i = 0;; i++)
    {
      if (i > 100) {
        break label174;
      }
      File localFile2 = new File(localFile1, String.valueOf(i));
      if (localFile2.exists())
      {
        String str2 = new String(BatteryService.a(paramContext, str1, localFile2.getName()));
        Long[] arrayOfLong = new Long[a(str2)];
        if (g == null) {
          g = Pattern.compile("\\d+");
        }
        Matcher localMatcher = g.matcher(str2);
        int j = 0;
        for (;;)
        {
          if (localMatcher.find())
          {
            int k = j + 1;
            arrayOfLong[j] = Long.valueOf(Long.parseLong(localMatcher.group()));
            j = k;
            continue;
            str1 = "discharging";
            break;
          }
        }
        localArrayList.addAll(Arrays.asList(arrayOfLong));
      }
    }
    label174:
    Collections.sort(localArrayList);
    if (paramBoolean) {}
    for (float f1 = 0.5F; localArrayList.size() == 0; f1 = 0.7F) {
      return -1L;
    }
    return ((Long)localArrayList.get((int)(f1 * localArrayList.size()))).longValue();
  }
  
  private static long a(long[] paramArrayOfLong)
  {
    if (paramArrayOfLong.length == 0) {
      return -1L;
    }
    if (paramArrayOfLong.length % 2 != 0) {
      return paramArrayOfLong[(paramArrayOfLong.length / 2)];
    }
    return (paramArrayOfLong[(-1 + paramArrayOfLong.length / 2)] + paramArrayOfLong[(paramArrayOfLong.length / 2)]) / 2L;
  }
  
  public static List<c> a(Context paramContext, boolean paramBoolean, int paramInt)
  {
    f = BatteryService.j(paramContext);
    h = paramInt;
    if (c == null) {
      c = new LinkedList();
    }
    if ((e == null) || (e.booleanValue() != paramBoolean))
    {
      e = Boolean.valueOf(paramBoolean);
      c.clear();
    }
    long l = System.currentTimeMillis();
    ArrayList localArrayList = new ArrayList(101);
    localArrayList.add(new c(l, paramInt));
    if (!paramBoolean) {
      for (int j = paramInt - 1; j >= 0; j--)
      {
        l += a(paramContext, j, paramBoolean);
        localArrayList.add(new c(l, j));
      }
    }
    for (int i = paramInt + 1; i <= 100; i++)
    {
      l += a(paramContext, i, paramBoolean);
      localArrayList.add(new c(l, i));
    }
    return localArrayList;
  }
  
  public static void a(Context paramContext)
  {
    if ((c != null) && (c.size() > 0)) {}
    for (long l = ((Long)c.get(-1 + c.size())).longValue();; l = -1L)
    {
      BatteryService.a(paramContext, "LAST_FIVE", Long.valueOf(l));
      return;
    }
  }
  
  public static void a(Context paramContext, boolean paramBoolean, int paramInt, long paramLong)
  {
    h = paramInt;
    if (c == null) {
      c = new LinkedList();
    }
    if ((e == null) || (e.booleanValue() != paramBoolean))
    {
      e = Boolean.valueOf(paramBoolean);
      c.clear();
    }
    c.add(Long.valueOf(paramLong));
    if (c.size() > 5) {
      c.remove(0);
    }
    List localList = c;
    long l;
    if ((localList == null) || (localList.size() == 0))
    {
      l = -1L;
      d = Long.valueOf(l);
      a(paramContext);
      if (!paramBoolean) {
        break label285;
      }
    }
    label285:
    for (String str1 = "charging";; str1 = "discharging")
    {
      String str2 = String.valueOf(paramInt);
      String str3 = new String(BatteryService.a(paramContext, str1, str2));
      BatteryService.a(paramContext, str1, str2, (str3 + paramLong + ",").getBytes());
      return;
      if (localList.size() % 2 != 0)
      {
        l = ((Long)localList.get(localList.size() / 2)).longValue();
        break;
      }
      l = (((Long)localList.get(-1 + localList.size() / 2)).longValue() + ((Long)localList.get(localList.size() / 2)).longValue()) / 2L;
      break;
    }
  }
  
  public static long b(Context paramContext)
  {
    Long localLong = (Long)BatteryService.a(paramContext, "LAST_FIVE");
    if ((localLong != null) && (localLong.longValue() > 0L)) {
      return localLong.longValue();
    }
    return -1L;
  }
  
  public static void b(Context paramContext, boolean paramBoolean)
  {
    int i = 0;
    if (paramBoolean)
    {
      File[] arrayOfFile2 = paramContext.getDir("charging", 0).listFiles();
      int k = arrayOfFile2.length;
      while (i < k)
      {
        arrayOfFile2[i].delete();
        i++;
      }
    }
    File[] arrayOfFile1 = paramContext.getDir("discharging", 0).listFiles();
    int j = arrayOfFile1.length;
    while (i < j)
    {
      arrayOfFile1[i].delete();
      i++;
    }
  }
  
  public static void c(Context paramContext)
  {
    int i = 0;
    File[] arrayOfFile1 = paramContext.getDir("charging", 0).listFiles();
    int j = arrayOfFile1.length;
    for (int k = 0; k < j; k++) {
      arrayOfFile1[k].delete();
    }
    File[] arrayOfFile2 = paramContext.getDir("discharging", 0).listFiles();
    int m = arrayOfFile2.length;
    while (i < m)
    {
      arrayOfFile2[i].delete();
      i++;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/battery/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */