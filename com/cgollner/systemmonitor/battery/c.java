package com.cgollner.systemmonitor.battery;

import java.io.Serializable;

public final class c
  implements Serializable
{
  private static final long serialVersionUID = -1966049907730681436L;
  public long a;
  public float b;
  
  public c(long paramLong, float paramFloat)
  {
    this.a = paramLong;
    this.b = paramFloat;
  }
  
  public final String toString()
  {
    return "TimeValue [timestamp=" + this.a + ", value=" + this.b + "]";
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/battery/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */