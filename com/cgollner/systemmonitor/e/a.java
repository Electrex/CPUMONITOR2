package com.cgollner.systemmonitor.e;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;

@TargetApi(11)
public final class a
  implements ViewPager.PageTransformer
{
  private static float a = 0.85F;
  private static float b = 0.5F;
  
  public final void transformPage(View paramView, float paramFloat)
  {
    if (Build.VERSION.SDK_INT < 11) {
      return;
    }
    int i = paramView.getWidth();
    int j = paramView.getHeight();
    if ((paramFloat >= -1.0F) && (paramFloat <= 1.0F))
    {
      float f1 = Math.max(a, 1.0F - Math.abs(paramFloat));
      float f2 = j * (1.0F - f1) / 2.0F;
      float f3 = i * (1.0F - f1) / 2.0F;
      if (paramFloat < 0.0F) {
        paramView.setTranslationX(f3 - f2 / 2.0F);
      }
      for (;;)
      {
        paramView.setScaleX(f1);
        paramView.setScaleY(f1);
        paramView.setAlpha(b + (f1 - a) / (1.0F - a) * (1.0F - b));
        return;
        paramView.setTranslationX(-f3 + f2 / 2.0F);
      }
    }
    paramView.setAlpha(0.0F);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/e/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */