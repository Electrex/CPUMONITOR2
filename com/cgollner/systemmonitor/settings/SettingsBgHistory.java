package com.cgollner.systemmonitor.settings;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.TwoStatePreference;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.historybg.HistoryBgActivity;
import com.cgollner.systemmonitor.historybg.HistoryBgService;
import java.io.File;
import java.util.Date;

public class SettingsBgHistory
  extends b
{
  protected final b.a a()
  {
    return new a();
  }
  
  public static final class a
    extends b.a
  {
    private Preference.OnPreferenceChangeListener a = new Preference.OnPreferenceChangeListener()
    {
      public final boolean onPreferenceChange(Preference paramAnonymousPreference, Object paramAnonymousObject)
      {
        SettingsBgHistory.a.a(SettingsBgHistory.a.this);
        return true;
      }
    };
    
    private void b()
    {
      java.text.DateFormat localDateFormat1 = android.text.format.DateFormat.getDateFormat(App.a);
      java.text.DateFormat localDateFormat2 = android.text.format.DateFormat.getTimeFormat(App.a);
      PreferenceCategory localPreferenceCategory1 = (PreferenceCategory)findPreference(App.a.getString(2131493014));
      final PreferenceCategory localPreferenceCategory2;
      if (localPreferenceCategory1 == null)
      {
        localPreferenceCategory2 = new PreferenceCategory(getActivity());
        localPreferenceCategory2.setTitle(2131493012);
        localPreferenceCategory2.setKey(App.a.getString(2131493014));
        localPreferenceCategory2.setOrder(2);
        getPreferenceScreen().addPreference(localPreferenceCategory2);
      }
      for (;;)
      {
        localPreferenceCategory2.removeAll();
        File localFile1 = App.a.getDir("schedules", 0);
        File[] arrayOfFile1 = localFile1.listFiles();
        if ((arrayOfFile1 == null) || (arrayOfFile1.length == 0)) {
          getPreferenceScreen().removePreference(localPreferenceCategory2);
        }
        int i = arrayOfFile1.length;
        int j = 0;
        if (j < i)
        {
          final File localFile2 = arrayOfFile1[j];
          String str1 = localFile2.getName();
          String str2 = localFile2.getName().substring(1, localFile2.getName().lastIndexOf('"'));
          final Long localLong1 = Long.valueOf(Long.parseLong(str1.substring(1 + localFile2.getName().lastIndexOf('"'), str1.indexOf('-'))));
          Date localDate1 = new Date(localLong1.longValue());
          final Long localLong2 = Long.valueOf(Long.parseLong(str1.substring(1 + str1.indexOf('-'))));
          Date localDate2 = new Date(localLong2.longValue());
          if (localDate2.before(new Date())) {
            localFile2.delete();
          }
          for (;;)
          {
            j++;
            break;
            final Preference localPreference = new Preference(getActivity());
            localPreference.setTitle(str2);
            localPreference.setSummary(localDateFormat1.format(localDate1) + ", " + localDateFormat2.format(localDate1) + " - " + localDateFormat1.format(localDate2) + ", " + localDateFormat2.format(localDate2) + "\nClick to delete");
            localPreferenceCategory2.addPreference(localPreference);
            localPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
            {
              public final boolean onPreferenceClick(Preference paramAnonymousPreference)
              {
                new AlertDialog.Builder(SettingsBgHistory.a.this.getActivity()).setTitle(2131492955).setMessage(App.a.getString(2131492894)).setPositiveButton(App.a.getString(2131493214), new DialogInterface.OnClickListener()
                {
                  public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
                  {
                    SettingsBgHistory.a.2.this.a.delete();
                    SettingsBgHistory.a.2.this.b.removePreference(SettingsBgHistory.a.2.this.c);
                    if (SettingsBgHistory.a.2.this.b.getPreferenceCount() == 0) {
                      SettingsBgHistory.a.this.getPreferenceScreen().removePreference(SettingsBgHistory.a.2.this.b);
                    }
                    HistoryBgService.a(SettingsBgHistory.a.2.this.d.longValue(), SettingsBgHistory.a.2.this.e.longValue(), App.a);
                  }
                }).setNegativeButton(App.a.getString(2131492864), new DialogInterface.OnClickListener()
                {
                  public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {}
                }).create().show();
                return true;
              }
            });
          }
        }
        File[] arrayOfFile2 = localFile1.listFiles();
        if ((arrayOfFile2 == null) || (arrayOfFile2.length == 0)) {
          getPreferenceScreen().removePreference(localPreferenceCategory2);
        }
        return;
        localPreferenceCategory2 = localPreferenceCategory1;
      }
    }
    
    protected final int a()
    {
      return 2131034114;
    }
    
    public final void onResume()
    {
      super.onResume();
      Preference localPreference = findPreference("schedule_key");
      CharSequence localCharSequence = localPreference.getTitle();
      String str1 = App.a.getString(2131493165);
      String str2 = str1.substring(-1 + str1.indexOf('['));
      localPreference.setTitle(localCharSequence + str2);
      b();
    }
    
    @SuppressLint({"NewApi"})
    public final void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
    {
      super.onSharedPreferenceChanged(paramSharedPreferences, paramString);
      boolean bool;
      if (paramString.equals(App.a.getString(2131493024)))
      {
        bool = paramSharedPreferences.getBoolean(paramString, false);
        if (Build.VERSION.SDK_INT >= 14) {
          break label60;
        }
        ((CheckBoxPreference)findPreference(paramString)).setChecked(bool);
        if (!bool) {
          break label75;
        }
        a(HistoryBgService.class);
      }
      for (;;)
      {
        return;
        label60:
        ((TwoStatePreference)findPreference(paramString)).setChecked(bool);
        break;
        label75:
        b(HistoryBgService.class);
        b();
        final PreferenceCategory localPreferenceCategory = (PreferenceCategory)findPreference(App.a.getString(2131493082));
        localPreferenceCategory.removeAll();
        for (final File localFile : App.a.getDir("history", 0).listFiles())
        {
          String str1 = localFile.getName();
          String str2 = localFile.getName().substring(1, localFile.getName().lastIndexOf('"'));
          Date localDate1 = new Date(Long.valueOf(Long.parseLong(str1.substring(1 + localFile.getName().lastIndexOf('"'), str1.indexOf('-')))).longValue());
          Date localDate2 = new Date(Long.valueOf(Long.parseLong(str1.substring(1 + str1.indexOf('-')))).longValue());
          final Preference localPreference = new Preference(getActivity());
          localPreference.setTitle(str2);
          localPreference.setKey(str2);
          java.text.DateFormat localDateFormat1 = android.text.format.DateFormat.getDateFormat(App.a);
          java.text.DateFormat localDateFormat2 = android.text.format.DateFormat.getTimeFormat(App.a);
          localPreference.setSummary(localDateFormat1.format(localDate1) + ", " + localDateFormat2.format(localDate1) + " - " + localDateFormat1.format(localDate2) + ", " + localDateFormat2.format(localDate2) + "\n" + App.a.getString(2131492991));
          localPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
          {
            public final boolean onPreferenceClick(Preference paramAnonymousPreference)
            {
              AlertDialog.Builder localBuilder = new AlertDialog.Builder(SettingsBgHistory.a.this.getActivity()).setTitle(2131492989);
              String[] arrayOfString = new String[2];
              arrayOfString[0] = App.a.getString(2131493016);
              arrayOfString[1] = App.a.getString(2131492955);
              localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener()
              {
                public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
                {
                  if (paramAnonymous2Int == 0)
                  {
                    Intent localIntent = new Intent(App.a, HistoryBgActivity.class);
                    localIntent.putExtra("SEE_ONLY", SettingsBgHistory.a.1.this.a.getName());
                    SettingsBgHistory.a.this.startActivity(localIntent);
                    return;
                  }
                  SettingsBgHistory.a.1.this.a.delete();
                  SettingsBgHistory.a.1.this.b.removePreference(SettingsBgHistory.a.1.this.c);
                }
              }).create().show();
              return true;
            }
          });
          localPreferenceCategory.addPreference(localPreference);
        }
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/settings/SettingsBgHistory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */