package com.cgollner.systemmonitor.settings;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public final class a
  extends DialogPreference
  implements SeekBar.OnSeekBarChangeListener
{
  String a;
  int b;
  private SeekBar c;
  private TextView d;
  private TextView e;
  private Context f;
  private String g;
  private int h;
  private int i;
  
  protected final View onCreateDialogView()
  {
    LinearLayout localLinearLayout = new LinearLayout(this.f);
    localLinearLayout.setOrientation(1);
    localLinearLayout.setPadding(6, 6, 6, 6);
    this.d = new TextView(this.f);
    if (this.g != null) {
      this.d.setText(this.g);
    }
    localLinearLayout.addView(this.d);
    this.e = new TextView(this.f);
    this.e.setGravity(1);
    this.e.setTextAppearance(this.f, this.f.getTheme().obtainStyledAttributes(new int[] { 16842816 }).getResourceId(0, 0));
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-1, -2);
    localLinearLayout.addView(this.e, localLayoutParams);
    this.c = new SeekBar(this.f);
    this.c.setOnSeekBarChangeListener(this);
    localLinearLayout.addView(this.c, new LinearLayout.LayoutParams(-1, -2));
    this.b = getPersistedInt(this.h);
    TextView localTextView = this.e;
    if (this.a == null) {}
    for (String str = this.b;; str = this.b.concat(this.a))
    {
      localTextView.setText(str);
      this.c.setMax(this.i);
      this.c.setProgress(this.b);
      return localLinearLayout;
    }
  }
  
  protected final void onDialogClosed(boolean paramBoolean)
  {
    super.onDialogClosed(paramBoolean);
    if (paramBoolean)
    {
      callChangeListener(Integer.valueOf(this.b));
      persistInt(this.b);
      notifyChanged();
    }
  }
  
  protected final Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
  {
    return Integer.valueOf(paramTypedArray.getInteger(paramInt, this.h));
  }
  
  public final void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    String str;
    TextView localTextView;
    if (paramBoolean)
    {
      str = String.valueOf(paramInt);
      localTextView = this.e;
      if (this.a != null) {
        break label61;
      }
    }
    for (;;)
    {
      localTextView.setText(str);
      this.b = paramInt;
      callChangeListener(Integer.valueOf(this.b));
      persistInt(this.b);
      notifyChanged();
      return;
      label61:
      str = str.concat(this.a);
    }
  }
  
  protected final void onSetInitialValue(boolean paramBoolean, Object paramObject)
  {
    if (paramBoolean) {}
    for (int j = getPersistedInt(this.h);; j = ((Integer)paramObject).intValue())
    {
      this.b = j;
      persistInt(this.b);
      return;
    }
  }
  
  public final void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  public final void onStopTrackingTouch(SeekBar paramSeekBar) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/settings/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */