package com.cgollner.systemmonitor.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.b.a.k;
import com.cgollner.systemmonitor.battery.BatteryService;

public class SettingsApp
  extends b
{
  protected final b.a a()
  {
    return new a();
  }
  
  public static final class a
    extends b.a
  {
    private Preference.OnPreferenceClickListener a = new Preference.OnPreferenceClickListener()
    {
      public final boolean onPreferenceClick(Preference paramAnonymousPreference)
      {
        new AlertDialog.Builder(SettingsApp.a.this.getActivity()).setTitle(a.h.clear_battery_history).setMessage(a.h.battery_clear_stats_message).setPositiveButton(a.h.yes, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
          {
            BatteryService.l(App.a);
          }
        }).setNegativeButton(a.h.No, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {}
        }).create().show();
        return true;
      }
    };
    private Preference.OnPreferenceClickListener b = new Preference.OnPreferenceClickListener()
    {
      public final boolean onPreferenceClick(Preference paramAnonymousPreference)
      {
        new AlertDialog.Builder(SettingsApp.a.this.getActivity()).setTitle(a.h.battery_clear_stats).setMessage(a.h.battery_clear_stats_message).setPositiveButton(a.h.yes, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
          {
            BatteryService.k(App.a);
          }
        }).setNegativeButton(a.h.No, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {}
        }).create().show();
        return true;
      }
    };
    
    protected final int a()
    {
      return a.k.settings_app_prefs;
    }
    
    public final void onCreate(Bundle paramBundle)
    {
      super.onCreate(paramBundle);
      findPreference(App.a.getString(a.h.battery_clear_stats_key)).setOnPreferenceClickListener(this.b);
      findPreference(App.a.getString(a.h.batteryhistoryclearkey)).setOnPreferenceClickListener(this.a);
    }
    
    public final void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
    {
      super.onSharedPreferenceChanged(paramSharedPreferences, paramString);
      if (paramString.equals(App.a.getString(a.h.settings_theme_choose_key)))
      {
        int j = ((ListPreference)findPreference(paramString)).findIndexOfValue(paramSharedPreferences.getString(paramString, ""));
        SharedPreferences.Editor localEditor = paramSharedPreferences.edit();
        localEditor.putInt("theme", j);
        localEditor.commit();
        Intent localIntent = getActivity().getPackageManager().getLaunchIntentForPackage(getActivity().getPackageName());
        localIntent.setFlags(67108864);
        localIntent.putExtra("settings", true);
        startActivity(localIntent);
        getActivity().finish();
      }
      do
      {
        return;
        if (paramString.equals(App.a.getString(a.h.settings_app_cpu_updatefreq_key)))
        {
          com.cgollner.systemmonitor.d.g.a = 1000 * paramSharedPreferences.getInt(paramString, 1);
          return;
        }
        if (paramString.equals(App.a.getString(a.h.settings_app_ram_updatefreq_key)))
        {
          com.cgollner.systemmonitor.d.o.a = 1000 * paramSharedPreferences.getInt(paramString, 1);
          return;
        }
        if (paramString.equals(App.a.getString(a.h.settings_app_io_updatefreq_key)))
        {
          com.cgollner.systemmonitor.d.k.a = 1000 * paramSharedPreferences.getInt(paramString, 1);
          return;
        }
        if (paramString.equals(App.a.getString(a.h.settings_app_net_updatefreq_key)))
        {
          com.cgollner.systemmonitor.d.l.a = 1000 * paramSharedPreferences.getInt(paramString, 1);
          return;
        }
        if (paramString.equals(App.a.getString(a.h.settings_app_topapps_updatefreq_key)))
        {
          com.cgollner.systemmonitor.d.n.a = 1000 * paramSharedPreferences.getInt(paramString, 1);
          return;
        }
        if (paramString.equals(App.a.getString(a.h.battery_strategy_key)))
        {
          ListPreference localListPreference = (ListPreference)findPreference(paramString);
          BatteryService.a(localListPreference.findIndexOfValue(localListPreference.getValue()), App.a);
          return;
        }
        if (paramString.equals(App.a.getString(a.h.battery_history_key)))
        {
          if (paramSharedPreferences.getBoolean(paramString, true))
          {
            a(BatteryService.class);
            return;
          }
          b(BatteryService.class);
          return;
        }
      } while (!paramString.equals(App.a.getString(a.h.battery_history_size_key)));
      int i = paramSharedPreferences.getInt(paramString, 48);
      BatteryService.a(App.a, i);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/settings/SettingsApp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */