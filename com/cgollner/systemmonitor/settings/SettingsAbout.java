package com.cgollner.systemmonitor.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.b.a.k;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SettingsAbout
  extends b
{
  protected final b.a a()
  {
    return new a();
  }
  
  public static final class a
    extends b.a
  {
    private Preference.OnPreferenceClickListener a = new Preference.OnPreferenceClickListener()
    {
      public final boolean onPreferenceClick(Preference paramAnonymousPreference)
      {
        Intent localIntent = new Intent("android.intent.action.SEND");
        localIntent.setType("message/rfc822");
        localIntent.putExtra("android.intent.extra.EMAIL", new String[] { "christian.goellner88@gmail.com" });
        localIntent.putExtra("android.intent.extra.SUBJECT", App.a.getString(a.h.app_name));
        SettingsAbout.a.this.startActivity(localIntent);
        return true;
      }
    };
    
    protected final int a()
    {
      return a.k.settings_about;
    }
    
    public final void onCreate(Bundle paramBundle)
    {
      super.onCreate(paramBundle);
      findPreference(App.a.getString(a.h.email_key)).setOnPreferenceClickListener(this.a);
      try
      {
        PackageInfo localPackageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        Preference localPreference2 = findPreference("versionKey");
        localPreference2.setTitle(localPackageInfo.applicationInfo.loadLabel(getActivity().getPackageManager()));
        localPreference2.setSummary(localPackageInfo.versionName);
        if (Build.VERSION.SDK_INT >= 11) {
          localPreference2.setIcon(localPackageInfo.applicationInfo.loadIcon(getActivity().getPackageManager()));
        }
        Preference localPreference1 = findPreference("facebookKey");
        if (localPreference1 != null)
        {
          Calendar localCalendar1 = GregorianCalendar.getInstance();
          Calendar localCalendar2 = GregorianCalendar.getInstance();
          localCalendar2.set(2013, 4, 5, 20, 0);
          if (localCalendar1.before(localCalendar2)) {
            localPreference1.setSummary("Open contest for you to win 50$ with System Monitor! Visit the Facebook page for more details.");
          }
        }
        return;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        for (;;)
        {
          localNameNotFoundException.printStackTrace();
        }
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/settings/SettingsAbout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */