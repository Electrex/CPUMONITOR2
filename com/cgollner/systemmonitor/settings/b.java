package com.cgollner.systemmonitor.settings;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.i;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class b
  extends ActionBarActivity
{
  protected abstract a a();
  
  protected void onCreate(Bundle paramBundle)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    int i;
    if (Build.VERSION.SDK_INT < 11)
    {
      i = 1;
      switch (localSharedPreferences.getInt("theme", i))
      {
      }
    }
    for (;;)
    {
      super.onCreate(paramBundle);
      setContentView(a.f.activity_frame);
      setSupportActionBar((Toolbar)findViewById(a.e.toolbar));
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      if (paramBundle == null) {
        getFragmentManager().beginTransaction().add(a.e.contentFrame, a()).commit();
      }
      return;
      i = 0;
      break;
      setTheme(a.i.AppThemeSettings);
      continue;
      setTheme(a.i.AppThemeDarkSettings);
      continue;
      setTheme(a.i.AppThemeSettings);
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    finish();
    return true;
  }
  
  public static abstract class a
    extends PreferenceFragment
    implements SharedPreferences.OnSharedPreferenceChangeListener
  {
    protected static void a(Class<?> paramClass)
    {
      Intent localIntent = new Intent(App.a, paramClass);
      App.a.startService(localIntent);
    }
    
    private void b()
    {
      Map localMap = getPreferenceScreen().getSharedPreferences().getAll();
      Iterator localIterator = localMap.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str1 = (String)localIterator.next();
        String str2 = localMap.get(str1).toString();
        Preference localPreference = findPreference(str1);
        if ((localPreference instanceof ListPreference))
        {
          ListPreference localListPreference = (ListPreference)localPreference;
          int i = localListPreference.findIndexOfValue(str2);
          if (i >= 0) {}
          for (CharSequence localCharSequence = localListPreference.getEntries()[i];; localCharSequence = null)
          {
            localPreference.setSummary(localCharSequence);
            break;
          }
        }
        if ((localPreference instanceof a))
        {
          a locala = (a)localPreference;
          localPreference.setSummary(locala.b + locala.a);
        }
      }
    }
    
    protected static void b(Class<?> paramClass)
    {
      App.a.stopService(new Intent(App.a, paramClass));
    }
    
    protected abstract int a();
    
    public void onCreate(Bundle paramBundle)
    {
      super.onCreate(paramBundle);
      if (Build.VERSION.SDK_INT >= 11)
      {
        PreferenceManager localPreferenceManager = getPreferenceManager();
        localPreferenceManager.setSharedPreferencesName(getActivity().getPackageName() + "_preferences");
        localPreferenceManager.setSharedPreferencesMode(4);
      }
      addPreferencesFromResource(a());
      b();
    }
    
    public void onDestroyView()
    {
      super.onDestroyView();
      getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
    
    public void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
    {
      b();
    }
    
    public void onViewCreated(View paramView, Bundle paramBundle)
    {
      super.onViewCreated(paramView, paramBundle);
      getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/settings/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */