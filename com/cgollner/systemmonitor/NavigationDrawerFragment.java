package com.cgollner.systemmonitor;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.b.a.b;
import com.cgollner.systemmonitor.b.a.f;
import java.util.List;

public class NavigationDrawerFragment
  extends Fragment
  implements View.OnClickListener
{
  private boolean a;
  private int b;
  private int c;
  private int d;
  private int e;
  private int f;
  private Context g;
  private ViewGroup h;
  private a i;
  private TextView j;
  
  public final void a(Integer paramInteger, int paramInt)
  {
    try
    {
      TextView localTextView = (TextView)this.h.findViewById(paramInteger.intValue());
      if (localTextView == null) {
        return;
      }
      if (!this.a)
      {
        this.b = localTextView.getPaddingLeft();
        this.f = localTextView.getPaddingRight();
        this.c = localTextView.getPaddingTop();
        Resources.Theme localTheme = this.g.getTheme();
        int[] arrayOfInt = new int[1];
        arrayOfInt[0] = a.b.selectableItemBackground;
        TypedArray localTypedArray = localTheme.obtainStyledAttributes(arrayOfInt);
        this.d = localTypedArray.getResourceId(0, 0);
        this.e = localTextView.getTextColors().getDefaultColor();
        localTypedArray.recycle();
        this.a = true;
      }
      if (this.j != null)
      {
        this.j.setBackgroundResource(this.d);
        this.j.setPadding(this.b, this.c, this.f, this.c);
        this.j.setTextColor(this.e);
      }
      this.j = localTextView;
      this.j.setBackgroundColor(getResources().getColor(paramInt));
      this.j.setTextColor(-1);
      return;
    }
    catch (Exception localException) {}
  }
  
  public final void a(List<b.a> paramList)
  {
    for (int k = 0; k < this.h.getChildCount(); k++)
    {
      View localView = this.h.getChildAt(k);
      if ((localView.isClickable()) && (!paramList.contains(new b.a(localView.getId()))))
      {
        this.h.removeView(localView);
        k--;
      }
    }
  }
  
  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    try
    {
      this.i = ((a)paramActivity);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      throw new ClassCastException("Activity Must implement MenuItemSelectedListener");
    }
  }
  
  public void onClick(View paramView)
  {
    this.i.a(Integer.valueOf(paramView.getId()).intValue());
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.g = getActivity();
    ViewGroup localViewGroup = (ViewGroup)paramLayoutInflater.inflate(a.f.drawer_layout, null);
    this.h = ((ViewGroup)localViewGroup.getChildAt(0));
    for (int k = 0; k < this.h.getChildCount(); k++)
    {
      View localView = this.h.getChildAt(k);
      if (localView.isClickable()) {
        localView.setOnClickListener(this);
      }
    }
    return localViewGroup;
  }
  
  public static abstract interface a
  {
    public abstract void a(int paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/NavigationDrawerFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */