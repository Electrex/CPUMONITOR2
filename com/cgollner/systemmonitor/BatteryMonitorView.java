package com.cgollner.systemmonitor;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.c;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.battery.c;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class BatteryMonitorView
  extends View
{
  private long A;
  private long B;
  private boolean C;
  public List<c> a;
  public boolean b;
  public boolean c;
  public float d;
  public Rect e;
  public float f;
  private Paint g;
  private Paint h;
  private Paint i;
  private Paint j;
  private Paint k;
  private Paint l;
  private Path m;
  private int n;
  private float o;
  private float p;
  private Canvas q;
  private float r;
  private float s;
  private int t;
  private int u;
  private long v;
  private float w;
  private List<c> x;
  private float y;
  private float z;
  
  public BatteryMonitorView(Context paramContext)
  {
    super(paramContext);
    a(paramContext);
  }
  
  public BatteryMonitorView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext);
  }
  
  public BatteryMonitorView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext);
  }
  
  private static float a(float paramFloat, Resources paramResources)
  {
    return TypedValue.applyDimension(1, paramFloat, paramResources.getDisplayMetrics());
  }
  
  private void a(int paramInt1, int paramInt2, int paramInt3)
  {
    this.h.setColor(paramInt1);
    this.i.setColor(paramInt2);
    this.g.setColor(paramInt3);
    this.C = true;
  }
  
  private void a(Context paramContext)
  {
    if (this.a == null) {
      this.a = new ArrayList();
    }
    this.g = new Paint();
    this.g.setAntiAlias(true);
    this.g.setColor(paramContext.getResources().getColor(a.c.lineColor));
    this.g.setStyle(Paint.Style.STROKE);
    this.g.setStrokeWidth(a(2.0F, paramContext.getResources()));
    this.h = new Paint();
    this.h.setAntiAlias(true);
    this.h.setColor(paramContext.getResources().getColor(a.c.fillColor));
    this.h.setStyle(Paint.Style.FILL);
    this.l = new Paint();
    this.l.setAntiAlias(true);
    this.l.setColor(Color.argb(255, 120, 120, 120));
    this.l.setStyle(Paint.Style.STROKE);
    this.l.setStrokeWidth(a(1.0F, paramContext.getResources()));
    this.k = new Paint();
    this.k.setAntiAlias(true);
    this.k.setColor(Color.argb(120, 120, 120, 120));
    this.k.setStyle(Paint.Style.FILL);
    this.i = new Paint();
    this.i.setAntiAlias(true);
    this.i.setColor(paramContext.getResources().getColor(a.c.gridColor));
    this.j = new Paint();
    this.j.setAntiAlias(true);
    this.j.setColor(getResources().getColor(17170432));
    this.j.setTextAlign(Paint.Align.CENTER);
    this.j.setTextSize(a(10.0F, getResources()));
    if (PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("theme", 0) == 1) {
      this.j.setColor(-1);
    }
    for (;;)
    {
      this.m = new Path();
      this.o = a(50.0F, getResources());
      this.n = 0;
      this.e = new Rect();
      String str = (String)i.a(1000, paramContext);
      this.j.getTextBounds(str, 0, str.length(), this.e);
      Rect localRect = this.e;
      localRect.right = ((int)(localRect.right + a(4.0F, paramContext.getResources())));
      this.v = 0L;
      return;
      this.j.setColor(-16777216);
    }
  }
  
  private void a(boolean paramBoolean, Paint paramPaint)
  {
    int i1 = 0;
    this.m.reset();
    this.y = 0.0F;
    long l1 = ((float)((c)this.a.get(-1 + this.a.size())).a - 0.01F * this.w * this.r);
    Iterator localIterator1 = this.a.iterator();
    Object localObject = null;
    int i2 = 0;
    c localc2;
    label180:
    float f2;
    float f3;
    label235:
    label273:
    float f1;
    for (;;)
    {
      if (localIterator1.hasNext())
      {
        localc2 = (c)localIterator1.next();
        if ((localc2.b >= 1.0F) && ((this.c) || (localObject == null) || ((int)((c)localObject).b != (int)localc2.b)))
        {
          this.y = (this.z + (float)(localc2.a - l1) / this.r * this.o);
          if ((this.y >= 0.0F) && (i2 == 0))
          {
            if (localObject != null)
            {
              f2 = this.z + (float)(((c)localObject).a - l1) / this.r * this.o;
              if (!this.c) {
                break label364;
              }
              f3 = this.s - ((c)localObject).b / this.d * this.s;
              if (f2 < 0.0F) {
                f2 = 0.0F;
              }
              if (!paramBoolean) {
                break label388;
              }
              this.m.moveTo(f2, this.s);
              this.m.lineTo(f2, f3);
              i2 = 1;
            }
          }
          else
          {
            if (this.y < 0.0F) {
              break label768;
            }
            if (!this.c) {
              break label402;
            }
            f1 = this.s - localc2.b / this.d * this.s;
            label314:
            this.m.lineTo(this.y, f1);
            if (i1 != 0) {
              break label426;
            }
            if (this.y <= this.t) {
              break label768;
            }
          }
        }
      }
    }
    label364:
    label388:
    label402:
    label426:
    label768:
    for (int i3 = 1;; i3 = i1)
    {
      i1 = i3;
      localObject = localc2;
      break;
      localObject = localc2;
      break label180;
      f3 = this.s - ((c)localObject).b / 100.0F * this.s;
      break label235;
      this.m.moveTo(f2, f3);
      break label273;
      f1 = this.s - localc2.b / 100.0F * this.s;
      break label314;
      if (paramBoolean)
      {
        this.m.lineTo(this.y, this.s);
        this.m.close();
      }
      this.q.drawPath(this.m, paramPaint);
      if (this.x == null) {
        return;
      }
      this.m.reset();
      if (paramBoolean)
      {
        this.m.moveTo(this.y, this.s);
        this.m.lineTo(this.y, this.s - ((c)this.a.get(-1 + this.a.size())).b / 100.0F * this.s);
      }
      for (;;)
      {
        Iterator localIterator2 = this.x.iterator();
        while (localIterator2.hasNext())
        {
          c localc1 = (c)localIterator2.next();
          this.y = (this.z + (float)(localc1.a - l1) / this.r * this.o);
          this.m.lineTo(this.y, this.s - localc1.b / 100.0F * this.s);
          if (i1 != 0) {
            break;
          }
          if (this.y > this.t) {
            i1 = 1;
          }
        }
        this.m.moveTo(this.y, this.s - ((c)this.a.get(-1 + this.a.size())).b / 100.0F * this.s);
      }
      if (paramBoolean)
      {
        this.m.lineTo(this.y, this.s);
        this.m.close();
      }
      paramPaint.setAlpha(50);
      this.q.drawPath(this.m, paramPaint);
      paramPaint.setAlpha(255);
      return;
    }
  }
  
  private long getDefaultVisible()
  {
    return PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(getContext().getString(a.h.battery_visible_range_key), 12);
  }
  
  public final void a()
  {
    this.v += (0.1D * Math.min(5.0F * this.r, (float)this.B));
    postInvalidate();
  }
  
  public final void b()
  {
    long l1 = (0.1D * Math.min(5.0F * this.r, (float)this.B));
    this.v -= l1;
    if (this.B - l1 < l1) {
      this.v = (l1 + this.v);
    }
    postInvalidate();
  }
  
  public final void c()
  {
    this.w = (50.0F + this.w);
    postInvalidate();
  }
  
  public final void d()
  {
    this.w -= 50.0F;
    postInvalidate();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    int i6;
    label77:
    label91:
    int i1;
    if ((!this.C) && (!this.c))
    {
      if (this.u != 0) {
        break label455;
      }
      if (this.a.size() == 0)
      {
        i6 = 0;
        if (i6 >= 25) {
          break label330;
        }
        this.h.setColor(getResources().getColor(a.c.batteryQ1Fill));
        this.g.setColor(getResources().getColor(a.c.batteryQ1Line));
        this.i.setColor(this.h.getColor());
      }
    }
    else
    {
      this.q = paramCanvas;
      paramCanvas.drawColor(this.n);
      if (getHeight() <= 0) {
        break label538;
      }
      i1 = getHeight();
      label116:
      this.s = (i1 - 2.5F * this.j.getTextSize());
      if (getWidth() <= 0) {
        break label546;
      }
    }
    float f1;
    label330:
    label455:
    label538:
    label546:
    for (int i2 = getWidth();; i2 = paramCanvas.getWidth())
    {
      this.t = i2;
      f1 = this.s / 10.0F;
      this.f = 0.0F;
      if (!this.c) {
        break label554;
      }
      float f6 = a(8.0F, getResources());
      this.j.setTextAlign(Paint.Align.LEFT);
      for (int i5 = 0; i5 <= 10; i5++)
      {
        String str2 = (String)i.a((int)((10 - i5) / 10.0F * this.d), getContext());
        this.j.getTextBounds(str2, 0, str2.length(), this.e);
        this.f = Math.max(this.f, this.e.right + f6 * 2.0F);
        paramCanvas.drawText(str2, f6, f1 * i5, this.j);
      }
      i6 = (int)((c)this.a.get(-1 + this.a.size())).b;
      break;
      if (i6 < 50)
      {
        this.h.setColor(getResources().getColor(a.c.batteryQ2Fill));
        this.g.setColor(getResources().getColor(a.c.batteryQ2Line));
        break label77;
      }
      if (i6 < 75)
      {
        this.h.setColor(getResources().getColor(a.c.batteryQ3Fill));
        this.g.setColor(getResources().getColor(a.c.batteryQ3Line));
        break label77;
      }
      this.h.setColor(getResources().getColor(a.c.batteryQ4Fill));
      this.g.setColor(getResources().getColor(a.c.batteryQ4Line));
      break label77;
      if (this.u == 1)
      {
        a(getResources().getColor(a.c.fillColorDark), getResources().getColor(a.c.gridColorDark), getResources().getColor(a.c.lineColor));
        break label91;
      }
      a(Color.argb(190, 255, 255, 255), Color.argb(128, 255, 255, 255), -1);
      break label91;
      i1 = paramCanvas.getHeight();
      break label116;
    }
    label554:
    this.t = ((int)(this.t - this.f));
    if ((this.a == null) || (this.a.size() == 0)) {}
    label971:
    do
    {
      return;
      paramCanvas.save();
      paramCanvas.translate(this.f, 0.0F);
      long l1;
      if ((this.x == null) || (this.x.size() == 0))
      {
        this.B = (((c)this.a.get(-1 + this.a.size())).a - ((c)this.a.get(0)).a);
        this.A = (3600000L * getDefaultVisible());
        this.o = (this.t / 5);
        this.r = ((float)((this.v + Math.min(this.B, this.A)) / 5L));
        l1 = ((c)this.a.get(-1 + this.a.size())).a - ((c)this.a.get(0)).a;
        if (!this.c) {
          break label971;
        }
      }
      for (this.z = this.t;; this.z = Math.min((float)l1 / this.r * this.o, this.t / 2.0F))
      {
        if ((this.x != null) && (this.x.size() > 0))
        {
          float f5 = (float)(((c)this.x.get(-1 + this.x.size())).a - ((c)this.x.get(0)).a) / this.r * this.o;
          if (f5 < this.z) {
            this.z += this.z - f5;
          }
        }
        for (int i3 = 0; i3 <= 10; i3++) {
          paramCanvas.drawLine(0.0F, f1 * i3, this.t, f1 * i3, this.i);
        }
        this.B = (((c)this.x.get(-1 + this.x.size())).a - ((c)this.a.get(0)).a);
        break;
      }
      for (float f2 = this.p; f2 <= this.t + this.o; f2 += this.o)
      {
        float f4 = this.s;
        Paint localPaint = this.i;
        paramCanvas.drawLine(f2, 0.0F, f2, f4, localPaint);
      }
      a(true, this.h);
      a(false, this.g);
      java.text.DateFormat localDateFormat = android.text.format.DateFormat.getTimeFormat(getContext());
      this.j.setTextAlign(Paint.Align.CENTER);
      float f3 = this.z - this.o;
      long l2 = ((float)((c)this.a.get(-1 + this.a.size())).a - 0.01F * this.w * this.r - f3 / this.o * this.r);
      for (int i4 = 1; i4 < 5; i4++)
      {
        Date localDate = new Date(l2);
        paramCanvas.drawText(localDateFormat.format(localDate), i4 * this.o, this.s + this.j.getTextSize(), this.j);
        getContext();
        paramCanvas.drawText(i.a(localDate), i4 * this.o, this.s + 2.3F * this.j.getTextSize(), this.j);
        l2 = ((float)l2 + this.r);
      }
      paramCanvas.restore();
    } while (!this.b);
    paramCanvas.drawColor(-16777216);
    String str1 = getContext().getString(a.h.unlicensed_dialog_title);
    Rect localRect = new Rect();
    this.j.setTextSize(0.2F * Math.min(this.t, this.s));
    do
    {
      this.j.setTextSize(this.j.getTextSize() - 0.1F);
      this.j.getTextBounds(str1, 0, str1.length(), localRect);
    } while (localRect.right >= this.t);
    paramCanvas.drawText(str1, this.t / 2, this.s / 2.0F + this.j.getTextSize() / 2.0F, this.j);
  }
  
  public void setBgColor(int paramInt)
  {
    this.n = paramInt;
  }
  
  public void setColorScheme(int paramInt)
  {
    this.u = paramInt;
  }
  
  public void setColors(int[] paramArrayOfInt)
  {
    a(paramArrayOfInt[0], paramArrayOfInt[1], paramArrayOfInt[2]);
  }
  
  public void setGridWidth(float paramFloat)
  {
    this.i.setStrokeWidth(a(paramFloat, getResources()));
  }
  
  public void setLabelColor(int paramInt)
  {
    this.j.setColor(paramInt);
  }
  
  public void setLineWidth(float paramFloat)
  {
    this.g.setStrokeWidth(a(paramFloat, getResources()));
  }
  
  public void setMargin(float paramFloat)
  {
    this.o = paramFloat;
  }
  
  public void setPredictionArray(List<c> paramList)
  {
    if (this.x == null)
    {
      this.x = paramList;
      return;
    }
    this.x.clear();
    this.x.addAll(paramList);
  }
  
  public void setTextColor(int paramInt)
  {
    this.j.setColor(paramInt);
  }
  
  public void setTextSize(int paramInt)
  {
    this.j.setTextSize(a(paramInt, getResources()));
    String str = (String)i.a(1000, getContext());
    this.j.getTextBounds(str, 0, str.length(), this.e);
    Rect localRect = this.e;
    localRect.right = ((int)(localRect.right + a(16.0F, getResources())));
  }
  
  public void setValues(List<c> paramList)
  {
    this.a.clear();
    this.a.addAll(paramList);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/BatteryMonitorView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */