package com.cgollner.systemmonitor.d;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.MonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.c.c;
import com.cgollner.systemmonitor.c.e.a;
import java.util.List;

public class j
  extends Fragment
  implements e.a
{
  public static int a;
  private static List<Float> i;
  private MonitorView b;
  private c c;
  private TextView d;
  private TextView e;
  private TextView f;
  private TextView g;
  private Handler h;
  
  private void b()
  {
    if (this.c != null)
    {
      this.c.c();
      this.c = null;
    }
  }
  
  public final void a()
  {
    this.b.a(this.c.d, b.a);
    if (!b.a) {
      return;
    }
    this.h.post(new Runnable()
    {
      public final void run()
      {
        if ((j.this.getActivity() == null) || (j.this.isDetached())) {
          return;
        }
        try
        {
          j.b(j.this).setText(i.a(j.a(j.this).d));
          j.c(j.this).setText(i.a(j.a(j.this).a));
          j.d(j.this).setText(i.a(j.a(j.this).b));
          j.e(j.this).setText(i.a(j.a(j.this).c));
          return;
        }
        catch (NullPointerException localNullPointerException) {}
      }
    });
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.h = new Handler();
    a = 1000;
    View localView = paramLayoutInflater.inflate(a.f.gpu_fragment_layout, null);
    this.b = ((MonitorView)localView.findViewById(a.e.monitorview));
    if (i != null) {
      this.b.a.addAll(i);
    }
    this.b.setDrawBorders$1d54120b(false);
    this.d = ((TextView)localView.findViewById(a.e.statValue0));
    this.g = ((TextView)localView.findViewById(a.e.statValue1));
    this.f = ((TextView)localView.findViewById(a.e.statValue2));
    this.e = ((TextView)localView.findViewById(a.e.statValue3));
    setHasOptionsMenu(true);
    return localView;
  }
  
  public void onDestroyView()
  {
    i = this.b.a;
    super.onDestroyView();
  }
  
  public void onPause()
  {
    super.onPause();
    b();
  }
  
  public void onResume()
  {
    super.onResume();
    if (this.c != null) {
      b();
    }
    this.c = new c(a, this);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */