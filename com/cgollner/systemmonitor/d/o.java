package com.cgollner.systemmonitor.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.MonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.c.e.a;
import com.cgollner.systemmonitor.c.g;
import java.util.List;
import java.util.Locale;

public class o
  extends Fragment
  implements e.a
{
  public static int a;
  private static List<Float> j;
  private MonitorView b;
  private g c;
  private TextView d;
  private TextView e;
  private TextView f;
  private TextView g;
  private Handler h;
  private Context i;
  
  private void b()
  {
    if (this.c != null)
    {
      this.c.c();
      this.c = null;
    }
  }
  
  public final void a()
  {
    this.b.a(this.c.a, b.a);
    if (!b.a) {
      return;
    }
    this.h.post(new Runnable()
    {
      public final void run()
      {
        if ((o.this.getActivity() == null) || (o.this.isDetached())) {
          return;
        }
        try
        {
          TextView localTextView = o.b(o.this);
          g localg = o.a(o.this);
          Locale localLocale = Locale.getDefault();
          Object[] arrayOfObject = new Object[1];
          arrayOfObject[0] = Float.valueOf(localg.a);
          localTextView.setText(String.format(localLocale, "%.0f%%", arrayOfObject));
          o.c(o.this).setText(i.b(o.a(o.this).c));
          o.d(o.this).setText(i.b(o.a(o.this).d));
          o.e(o.this).setText(i.b(o.a(o.this).b));
          return;
        }
        catch (NullPointerException localNullPointerException) {}
      }
    });
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.h = new Handler();
    this.i = getActivity();
    a = 1000 * PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(App.a.getString(a.h.settings_app_ram_updatefreq_key), 1);
    View localView = paramLayoutInflater.inflate(a.f.ram_fragment_layout, null);
    this.b = ((MonitorView)localView.findViewById(a.e.monitorview));
    if (j != null) {
      this.b.a.addAll(j);
    }
    this.b.setDrawBorders$1d54120b(false);
    this.d = ((TextView)localView.findViewById(a.e.usageAvg));
    this.e = ((TextView)localView.findViewById(a.e.freeRamValue));
    this.f = ((TextView)localView.findViewById(a.e.totalRamValue));
    this.g = ((TextView)localView.findViewById(a.e.usedRamValue));
    setHasOptionsMenu(true);
    return localView;
  }
  
  public void onDestroyView()
  {
    j = this.b.a;
    super.onDestroyView();
  }
  
  public void onPause()
  {
    super.onPause();
    b();
  }
  
  public void onResume()
  {
    super.onResume();
    if (this.c != null) {
      b();
    }
    this.c = new g(a, this, this.i);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */