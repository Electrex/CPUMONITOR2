package com.cgollner.systemmonitor.d;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.FrequenciesPieChart;
import com.cgollner.systemmonitor.FrequenciesPieChart.a;
import com.cgollner.systemmonitor.a.b;
import com.cgollner.systemmonitor.a.b.a;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import com.cgollner.systemmonitor.b.a.h;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class h
  extends Fragment
  implements View.OnClickListener
{
  private int a;
  private FrequenciesPieChart b;
  private int c;
  private View d;
  private Context e;
  private View.OnClickListener f = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      h.a(h.this, (1 + h.c(h.this)) % h.d(h.this));
      h.b(h.this);
    }
  };
  
  private void a()
  {
    a(LayoutInflater.from(getActivity()));
  }
  
  private void a(LayoutInflater paramLayoutInflater)
  {
    LinkedList localLinkedList;
    for (;;)
    {
      try
      {
        ViewGroup localViewGroup = (ViewGroup)this.d.findViewById(getResources().getIdentifier("cpu0", "id", getActivity().getPackageName()));
        localViewGroup.removeAllViews();
        List localList = b.b(this.e);
        Iterator localIterator1 = localList.iterator();
        int i = 0;
        if (localIterator1.hasNext())
        {
          b.a locala2 = (b.a)localIterator1.next();
          View localView = paramLayoutInflater.inflate(a.f.cpu_freq_state_item, null);
          TextView localTextView1 = (TextView)localView.findViewById(a.e.tvFreq);
          TextView localTextView2 = (TextView)localView.findViewById(a.e.tvFreqTime);
          TextView localTextView3 = (TextView)localView.findViewById(a.e.tvFreqPercentage);
          localTextView1.setText(locala2.d);
          long l = locala2.b;
          getActivity();
          localTextView2.setText(i.e(l));
          localTextView3.setText(i.c(locala2.c));
          int[][] arrayOfInt = FrequenciesPieChart.a;
          if (i == this.c)
          {
            this.c = i;
            localView.setBackgroundColor(arrayOfInt[(i % arrayOfInt.length)][0]);
          }
          int j;
          if (i == this.c)
          {
            j = -1;
            localTextView1.setTextColor(j);
            localTextView2.setTextColor(j);
            localTextView3.setTextColor(j);
            localView.setOnClickListener(this);
            localView.setId(i);
            int k = i + 1;
            localViewGroup.addView(localView);
            i = k;
          }
          else
          {
            j = arrayOfInt[(i % arrayOfInt.length)][1];
          }
        }
        else
        {
          this.a = i;
          this.b = ((FrequenciesPieChart)this.d.findViewById(a.e.pie));
          localLinkedList = new LinkedList();
          Iterator localIterator2 = localList.iterator();
          if (!localIterator2.hasNext()) {
            break;
          }
          b.a locala1 = (b.a)localIterator2.next();
          FrequenciesPieChart.a locala = new FrequenciesPieChart.a();
          if (locala1.a <= 0)
          {
            locala.b = "Deep sleep";
            locala.a = i.e(locala1.b);
            locala.d = locala1.c;
            locala.c = locala1.b;
            localLinkedList.add(locala);
          }
          else
          {
            locala.b = ((String)i.b(locala1.a));
          }
        }
      }
      catch (Exception localException)
      {
        this.d.findViewById(a.e.freqsFileNotFound).setVisibility(0);
        return;
      }
    }
    this.b.a(localLinkedList, this.c, true);
    this.b.setClickable(true);
    this.b.setOnClickListener(this.f);
  }
  
  public void onClick(View paramView)
  {
    this.c = paramView.getId();
    a();
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(a.g.cpu_freqs_menu, paramMenu);
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    this.d = paramLayoutInflater.inflate(a.f.cpu_freq_state_layout, paramViewGroup, false);
    this.c = 0;
    this.e = getActivity();
    a(paramLayoutInflater);
    return this.d;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == a.e.replay)
    {
      a();
      return true;
    }
    if (paramMenuItem.getItemId() == a.e.resetFrequencies)
    {
      new AlertDialog.Builder(this.e).setTitle(a.h.reset_timers).setMessage(a.h.are_you_sure_message).setPositiveButton(a.h.yes, new DialogInterface.OnClickListener()
      {
        public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          try
          {
            b.a(h.a(h.this));
            h.b(h.this);
            return;
          }
          catch (IOException localIOException)
          {
            localIOException.printStackTrace();
          }
        }
      }).setNegativeButton(a.h.No, new DialogInterface.OnClickListener()
      {
        public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {}
      }).create().show();
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public void onResume()
  {
    super.onResume();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */