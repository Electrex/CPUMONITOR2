package com.cgollner.systemmonitor.d;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.FrequenciesPieChart;
import com.cgollner.systemmonitor.FrequenciesPieChart.a;
import com.cgollner.systemmonitor.a.h;
import com.cgollner.systemmonitor.a.h.b;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.d;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import com.cgollner.systemmonitor.b.a.h;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.io.FileUtils;

public class p
  extends Fragment
  implements LoaderManager.LoaderCallbacks<List<h.b>>, View.OnClickListener, f.b
{
  private int a;
  private FrequenciesPieChart b;
  private int c;
  private View d;
  private List<h.b> e;
  private File f;
  private long g;
  private float h;
  private long i;
  private ViewGroup j;
  private Context k;
  private Handler l;
  private boolean m;
  private View.OnClickListener n = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      p.a(p.this, (1 + p.c(p.this)) % p.d(p.this));
      p.e(p.this);
    }
  };
  private File o;
  
  private void a(ViewGroup paramViewGroup, final h.b paramb, int paramInt)
  {
    View localView = LayoutInflater.from(this.k).inflate(a.f.storage_item, paramViewGroup, false);
    TextView localTextView1 = (TextView)localView.findViewById(a.e.tvFreq);
    TextView localTextView2 = (TextView)localView.findViewById(a.e.tvFreqTime);
    TextView localTextView3 = (TextView)localView.findViewById(a.e.tvFreqPercentage);
    ImageView localImageView = (ImageView)localView.findViewById(a.e.delete);
    int[][] arrayOfInt;
    if (paramb.a.equals(this.f.getParentFile()))
    {
      localTextView1.setText("../");
      localView.findViewById(a.e.delete).setVisibility(8);
      if (paramb.b != -1L)
      {
        localTextView2.setText(i.b(paramb.b));
        localTextView3.setText(i.c(paramb.c));
      }
      arrayOfInt = FrequenciesPieChart.a;
      if (paramInt == this.c)
      {
        localView.setBackgroundColor(arrayOfInt[(paramInt % arrayOfInt.length)][0]);
        localImageView.setImageResource(a.d.ic_ac_content_discard_holo_dark);
      }
      if (paramInt != this.c) {
        break label306;
      }
    }
    label306:
    for (int i1 = -1;; i1 = arrayOfInt[(paramInt % arrayOfInt.length)][1])
    {
      localTextView1.setTextColor(i1);
      localTextView2.setTextColor(i1);
      localTextView3.setTextColor(i1);
      localView.setOnClickListener(this);
      localView.setId(paramInt);
      localView.setTag(paramb);
      localImageView.setOnClickListener(new View.OnClickListener()
      {
        public final void onClick(View paramAnonymousView)
        {
          if (!p.a(p.this).getPackageName().equals("com.cgollner.systemmonitor"))
          {
            new AlertDialog.Builder(p.a(p.this)).setTitle(App.a.getString(a.h.full_version_feature_title)).setMessage(a.h.full_version_feature_message).setPositiveButton("Play Store", new DialogInterface.OnClickListener()
            {
              public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
              {
                Intent localIntent = new Intent("android.intent.action.VIEW");
                localIntent.setData(Uri.parse("market://details?id=com.cgollner.systemmonitor"));
                p.this.startActivity(localIntent);
              }
            }).setNegativeButton(17039360, new DialogInterface.OnClickListener()
            {
              public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {}
            }).create().show();
            return;
          }
          new AlertDialog.Builder(p.a(p.this)).setTitle("Delete").setMessage("Delete \"" + paramb.a.getName() + "\"?").setPositiveButton("Yes", new DialogInterface.OnClickListener()
          {
            public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
            {
              FileUtils.deleteQuietly(p.1.this.a.a);
              p.a(p.this, p.b(p.this));
            }
          }).setNegativeButton("No", new DialogInterface.OnClickListener()
          {
            public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {}
          }).create().show();
        }
      });
      paramViewGroup.addView(localView);
      return;
      StringBuilder localStringBuilder = new StringBuilder().append(paramb.a.getName());
      if (paramb.a.isDirectory()) {}
      for (String str = "/";; str = "")
      {
        localTextView1.setText(str);
        break;
      }
    }
  }
  
  private void a(File paramFile, boolean paramBoolean)
  {
    this.f = paramFile;
    this.d.findViewById(a.e.progressBar).setVisibility(0);
    this.d.findViewById(a.e.stats).setVisibility(8);
    if (paramBoolean)
    {
      getLoaderManager().restartLoader(1, null, this);
      return;
    }
    getLoaderManager().initLoader(1, null, this);
  }
  
  private void b()
  {
    int i1 = 0;
    LayoutInflater.from(this.k);
    this.d.findViewById(a.e.progressBar).setVisibility(8);
    this.d.findViewById(a.e.stats).setVisibility(0);
    TextView localTextView = (TextView)this.d.findViewById(a.e.freeStorage);
    StringBuilder localStringBuilder1 = new StringBuilder().append(App.a.getString(a.h.usedRam)).append(": ");
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Float.valueOf(100.0F - this.h);
    localTextView.setText(String.format("%.1f%%", arrayOfObject) + " - " + i.c(this.i - this.g) + "/" + i.c(this.i));
    this.j = ((ViewGroup)this.d.findViewById(getResources().getIdentifier("cpu0", "id", this.k.getPackageName())));
    this.j.removeAllViews();
    if (!this.f.equals(this.o)) {
      a(this.j, new h.b(this.f.getParentFile(), -1L), -1 + FrequenciesPieChart.a.length);
    }
    Iterator localIterator1 = this.e.iterator();
    while (localIterator1.hasNext())
    {
      h.b localb2 = (h.b)localIterator1.next();
      ViewGroup localViewGroup = this.j;
      localb2.a.getName();
      a(localViewGroup, localb2, i1);
      i1++;
    }
    this.a = i1;
    this.b = ((FrequenciesPieChart)this.d.findViewById(a.e.pie));
    LinkedList localLinkedList = new LinkedList();
    Iterator localIterator2 = this.e.iterator();
    if (localIterator2.hasNext())
    {
      h.b localb1 = (h.b)localIterator2.next();
      FrequenciesPieChart.a locala = new FrequenciesPieChart.a();
      StringBuilder localStringBuilder2 = new StringBuilder().append(localb1.a.getName());
      if (localb1.a.isDirectory()) {}
      for (String str = "/";; str = "")
      {
        locala.b = str;
        locala.a = i.c(localb1.b);
        locala.d = localb1.c;
        locala.c = localb1.b;
        localLinkedList.add(locala);
        break;
      }
    }
    this.b.a(localLinkedList, this.c, true);
    this.b.setClickable(true);
    this.b.setOnClickListener(this.n);
  }
  
  public final void a()
  {
    if (!this.m)
    {
      this.m = true;
      a(this.o, false);
    }
  }
  
  public final void a(File paramFile)
  {
    this.c = 0;
    this.o = paramFile;
    PreferenceManager.getDefaultSharedPreferences(this.k).edit().putString("ROOT_FOLDER", this.o.getAbsolutePath()).commit();
    a(paramFile, true);
  }
  
  public void onClick(View paramView)
  {
    this.c = paramView.getId();
    h.b localb = (h.b)paramView.getTag();
    if (localb.a.getName().equals("../"))
    {
      this.c = 0;
      a(this.f.getParentFile(), true);
      return;
    }
    if (localb.a.isDirectory())
    {
      this.c = 0;
      a(localb.a, true);
      return;
    }
    b();
  }
  
  public Loader<List<h.b>> onCreateLoader(int paramInt, Bundle paramBundle)
  {
    return new h(getActivity(), this.f);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(a.g.menu_storage, paramMenu);
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    setHasOptionsMenu(true);
    this.k = getActivity();
    this.l = new Handler();
    this.d = paramLayoutInflater.inflate(a.f.storage_stats_layout, paramViewGroup, false);
    this.c = 0;
    this.o = new File(PreferenceManager.getDefaultSharedPreferences(this.k).getString("ROOT_FOLDER", Environment.getExternalStorageDirectory().getAbsolutePath()));
    if (this.o.getAbsolutePath().equals("/")) {
      this.o = Environment.getExternalStorageDirectory();
    }
    this.m = false;
    setHasOptionsMenu(true);
    return this.d;
  }
  
  public void onLoaderReset(Loader<List<h.b>> paramLoader) {}
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == a.e.menu_storage_folder)
    {
      FragmentManager localFragmentManager = getChildFragmentManager();
      new f().show(localFragmentManager, "tag");
    }
    for (;;)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      if (paramMenuItem.getItemId() == a.e.replay) {
        a(this.f, true);
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */