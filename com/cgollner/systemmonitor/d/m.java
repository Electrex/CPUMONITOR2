package com.cgollner.systemmonitor.d;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

@SuppressLint({"UseSparseArrays"})
public class m
  extends ListFragment
  implements View.OnClickListener
{
  private Handler a;
  private Context b;
  private n c;
  private View d;
  private View e;
  private HashMap<Integer, Comparator<a>> f;
  private q g;
  private TextView h;
  
  public void onClick(View paramView)
  {
    if ((paramView instanceof TextView))
    {
      this.h.setTypeface(Typeface.SANS_SERIF, 0);
      String str = this.h.getText().toString().replaceAll("<u>", "").replaceAll("</u>", "");
      this.h.setText(str);
      TextView localTextView = (TextView)paramView;
      localTextView.setTypeface(Typeface.SANS_SERIF, 1);
      this.h = localTextView;
      this.h.setText(Html.fromHtml("<u>" + this.h.getText() + "</u>"));
    }
    this.c.l = ((Comparator)this.f.get(Integer.valueOf(paramView.getId())));
    try
    {
      synchronized (this.g.a)
      {
        Collections.sort(this.g.a, this.c.l);
        this.g.notifyDataSetChanged();
        return;
      }
    }
    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
    {
      for (;;) {}
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.a = new Handler();
    this.g = new q(getActivity(), a.f.apps_top_entry);
    setListAdapter(this.g);
    setHasOptionsMenu(true);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(a.g.topapps_fragment_menu, paramMenu);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.b = getActivity();
    this.d = paramLayoutInflater.inflate(a.f.top_apps_layout, null);
    this.e = this.d.findViewById(a.e.progressBar);
    this.d.findViewById(a.e.buttonCPU).setOnClickListener(this);
    this.d.findViewById(a.e.buttonRAM).setOnClickListener(this);
    this.d.findViewById(a.e.buttonCPUTIME).setOnClickListener(this);
    this.d.findViewById(a.e.buttonName).setOnClickListener(this);
    this.f = new HashMap();
    this.f.put(Integer.valueOf(a.e.buttonName), new a.c());
    this.f.put(Integer.valueOf(a.e.buttonCPU), new a.a());
    this.f.put(Integer.valueOf(a.e.buttonRAM), new a.d());
    this.f.put(Integer.valueOf(a.e.buttonCPUTIME), new a.b());
    return this.d;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == a.e.menu_show_all_processes)
    {
      n localn = this.c;
      if (!this.c.k) {}
      for (boolean bool = true;; bool = false)
      {
        localn.k = bool;
        paramMenuItem.setChecked(this.c.k);
        PreferenceManager.getDefaultSharedPreferences(this.b).edit().putBoolean("showAllProcesses", this.c.k).commit();
        return true;
      }
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public void onPause()
  {
    if (this.c != null) {
      this.c.g = false;
    }
    super.onPause();
  }
  
  public void onPrepareOptionsMenu(Menu paramMenu)
  {
    paramMenu.findItem(a.e.menu_show_all_processes).setChecked(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("showAllProcesses", false));
    super.onPrepareOptionsMenu(paramMenu);
  }
  
  public void onResume()
  {
    if (this.c != null) {
      this.c.g = false;
    }
    this.c = new n(this.g, getActivity(), this.a, this.e);
    this.c.l = ((Comparator)this.f.get(Integer.valueOf(a.e.buttonCPU)));
    this.h = ((TextView)this.d.findViewById(a.e.buttonCPU));
    this.h.setText(Html.fromHtml("<u>" + this.h.getText() + "</u>"));
    this.c.k = PreferenceManager.getDefaultSharedPreferences(this.b).getBoolean("showAllProcesses", false);
    this.c.start();
    super.onResume();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */