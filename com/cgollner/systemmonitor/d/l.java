package com.cgollner.systemmonitor.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.MonitorView;
import com.cgollner.systemmonitor.MonitorView.a;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.c.e.a;
import com.cgollner.systemmonitor.c.f;
import java.util.List;

public class l
  extends Fragment
  implements e.a
{
  public static int a;
  private static List<Float> h;
  private static double i = 102400.0D;
  private MonitorView b;
  private f c;
  private TextView d;
  private TextView e;
  private TextView f;
  private Handler g;
  
  private void b()
  {
    if (this.c != null)
    {
      this.c.c();
      this.c = null;
    }
  }
  
  public final void a()
  {
    this.b.b = Math.max(this.b.b, this.c.k);
    this.b.a((float)this.c.k, b.a);
    if (!b.a) {
      return;
    }
    this.g.post(new Runnable()
    {
      public final void run()
      {
        if ((l.this.getActivity() == null) || (l.this.isDetached())) {
          return;
        }
        try
        {
          TextView localTextView1 = l.b(l.this);
          f localf1 = l.a(l.this);
          localTextView1.setText(i.a(localf1.k, localf1.f));
          TextView localTextView2 = l.c(l.this);
          f localf2 = l.a(l.this);
          localTextView2.setText(i.a(localf2.l, localf2.f));
          TextView localTextView3 = l.d(l.this);
          f localf3 = l.a(l.this);
          localTextView3.setText(i.a(localf3.m, localf3.f));
          return;
        }
        catch (NullPointerException localNullPointerException) {}
      }
    });
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.g = new Handler();
    a = 1000 * PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(App.a.getString(a.h.settings_app_net_updatefreq_key), 1);
    View localView = paramLayoutInflater.inflate(a.f.network_fragment_layout, null);
    this.b = ((MonitorView)localView.findViewById(a.e.monitorview));
    this.b.setLabelType$36c8c2f0(MonitorView.a.c);
    if (h != null) {
      this.b.a.addAll(h);
    }
    this.b.setDrawBorders$1d54120b(false);
    if (i != -1.0D) {
      this.b.b = i;
    }
    this.d = ((TextView)localView.findViewById(a.e.throughputValue));
    this.e = ((TextView)localView.findViewById(a.e.readSpeedValue));
    this.f = ((TextView)localView.findViewById(a.e.writeSpeedValue));
    setHasOptionsMenu(true);
    return localView;
  }
  
  public void onDestroyView()
  {
    h = this.b.a;
    i = this.b.b;
    super.onDestroyView();
  }
  
  public void onPause()
  {
    super.onPause();
    b();
  }
  
  public void onResume()
  {
    super.onResume();
    if (this.c != null) {
      b();
    }
    this.c = new f(a, this);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */