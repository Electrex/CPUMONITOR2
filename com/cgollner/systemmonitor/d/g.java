package com.cgollner.systemmonitor.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.MonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.c.a;
import com.cgollner.systemmonitor.c.e.a;
import java.util.List;

public class g
  extends Fragment
  implements e.a
{
  public static int a;
  private static int j;
  private static List<Float> n;
  private static List<Float> o;
  private static List<Float> p;
  private static List<Float> q;
  private static boolean v;
  private Context A;
  private MonitorView b;
  private MonitorView c;
  private MonitorView d;
  private MonitorView e;
  private TextView f;
  private TextView g;
  private TextView h;
  private TextView i;
  private View k;
  private View l;
  private View m;
  private TextView[] r;
  private TextView[] s;
  private TextView[] t;
  private View u;
  private Handler w;
  private a x;
  private MonitorView[] y;
  private int z;
  
  private void b()
  {
    this.z = com.cgollner.systemmonitor.a.b.b();
    boolean bool = PreferenceManager.getDefaultSharedPreferences(this.A).getBoolean("displayAllCores", true);
    v = bool;
    if (!bool) {}
    for (j = 1;; j = this.z) {
      for (int i1 = 3; i1 >= j; i1--)
      {
        this.y[i1].setVisibility(8);
        this.s[i1].setVisibility(8);
        this.t[i1].setVisibility(8);
        this.r[i1].setVisibility(8);
      }
    }
    for (int i2 = 0; i2 < j; i2++)
    {
      this.y[i2].setVisibility(0);
      this.s[i2].setVisibility(0);
      this.t[i2].setVisibility(0);
      this.r[i2].setVisibility(0);
    }
    if (getResources().getConfiguration().orientation == 2) {
      if (j < 4) {
        this.k.setVisibility(8);
      }
    }
    while (j > 1)
    {
      this.l.setVisibility(8);
      this.m.setVisibility(0);
      return;
      this.k.setVisibility(0);
      continue;
      if (j < 2) {
        this.k.setVisibility(8);
      } else {
        this.k.setVisibility(0);
      }
    }
    this.l.setVisibility(0);
    this.m.setVisibility(8);
  }
  
  private void c()
  {
    if (this.x != null)
    {
      this.x.c();
      this.x = null;
    }
  }
  
  public final void a()
  {
    int i1;
    final int i2;
    if (j == 1)
    {
      i1 = 0;
      i2 = 0;
      label11:
      if (i2 >= j) {
        return;
      }
      this.y[i2].c = com.cgollner.systemmonitor.a.b.c(i2);
      this.y[i2].a(this.x.a[(i2 + i1)], com.cgollner.systemmonitor.b.a);
      if (getResources().getConfiguration().orientation != 2) {
        break label168;
      }
      if ((i2 != 1) && (i2 != 3)) {
        break label155;
      }
      this.y[i2].setDrawBorders$1d54120b(true);
      label88:
      if (com.cgollner.systemmonitor.b.a)
      {
        if (j <= 1) {
          break label204;
        }
        final String str5 = this.x.a(i2 + i1);
        final String str6 = this.x.b(i2);
        this.w.post(new Runnable()
        {
          public final void run()
          {
            try
            {
              g.e(g.this)[i2].setText(str5);
              g.f(g.this)[i2].setText(str6);
              return;
            }
            catch (NullPointerException localNullPointerException) {}
          }
        });
      }
    }
    for (;;)
    {
      i2++;
      break label11;
      i1 = 1;
      break;
      label155:
      this.y[i2].setDrawBorders$1d54120b(false);
      break label88;
      label168:
      if ((i2 == 2) || (i2 == 3))
      {
        this.y[i2].setDrawBorders$1d54120b(true);
        break label88;
      }
      this.y[i2].setDrawBorders$1d54120b(false);
      break label88;
      label204:
      final String str1 = this.x.a(i2 + i1);
      final String str2 = this.x.b(0);
      final String str3 = i.a(this.x.c[0]);
      final String str4 = i.a(this.x.d[0]);
      this.w.post(new Runnable()
      {
        public final void run()
        {
          try
          {
            g.a(g.this).setText(str1);
            g.b(g.this).setText(str2);
            g.c(g.this).setText(str3);
            g.d(g.this).setText(str4);
            return;
          }
          catch (NullPointerException localNullPointerException) {}
        }
      });
    }
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (com.cgollner.systemmonitor.a.b.b() > 1) {
      paramMenuInflater.inflate(a.g.cpu_fragment_menu, paramMenu);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i1 = 0;
    this.A = getActivity();
    a = 1000 * PreferenceManager.getDefaultSharedPreferences(this.A).getInt(App.a.getString(a.h.settings_app_cpu_updatefreq_key), 1);
    this.w = new Handler();
    this.u = paramLayoutInflater.inflate(a.f.cpu_fragment_layout, null);
    this.b = ((MonitorView)this.u.findViewById(a.e.monitorview));
    if (n != null) {
      this.b.a.addAll(n);
    }
    this.c = ((MonitorView)this.u.findViewById(a.e.monitorview2));
    if (o != null) {
      this.c.a.addAll(o);
    }
    this.d = ((MonitorView)this.u.findViewById(a.e.monitorview3));
    if (p != null) {
      this.d.a.addAll(p);
    }
    this.e = ((MonitorView)this.u.findViewById(a.e.monitorview4));
    if (q != null) {
      this.c.a.addAll(q);
    }
    MonitorView[] arrayOfMonitorView = new MonitorView[4];
    arrayOfMonitorView[0] = this.b;
    arrayOfMonitorView[1] = this.c;
    arrayOfMonitorView[2] = this.d;
    arrayOfMonitorView[3] = this.e;
    this.y = arrayOfMonitorView;
    this.f = ((TextView)this.u.findViewById(a.e.usageAvg));
    this.g = ((TextView)this.u.findViewById(a.e.speedValue));
    this.h = ((TextView)this.u.findViewById(a.e.minSpeedValue));
    this.i = ((TextView)this.u.findViewById(a.e.maxSpeedValue));
    this.k = this.u.findViewById(a.e.quadCoreLayout);
    this.l = this.u.findViewById(a.e.stats_layout);
    this.m = this.u.findViewById(a.e.stats_layout_cpu);
    this.s = new TextView[4];
    this.t = new TextView[4];
    this.r = new TextView[4];
    while (i1 < 4)
    {
      int i2 = getResources().getIdentifier("cpuStatTitle" + i1, "id", this.A.getPackageName());
      int i3 = getResources().getIdentifier("cpuStatVal" + i1, "id", this.A.getPackageName());
      int i4 = getResources().getIdentifier("cpuTitle" + i1, "id", this.A.getPackageName());
      this.s[i1] = ((TextView)this.u.findViewById(i2));
      this.t[i1] = ((TextView)this.u.findViewById(i3));
      this.r[i1] = ((TextView)this.u.findViewById(i4));
      i1++;
    }
    b();
    setHasOptionsMenu(true);
    return this.u;
  }
  
  public void onDestroyView()
  {
    n = this.b.a;
    o = this.c.a;
    p = this.d.a;
    q = this.e.a;
    super.onDestroyView();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == a.e.display_all_cores)
    {
      SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.A);
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      boolean bool1 = localSharedPreferences.getBoolean("displayAllCores", true);
      boolean bool2 = false;
      if (!bool1) {
        bool2 = true;
      }
      localEditor.putBoolean("displayAllCores", bool2);
      localEditor.commit();
      b();
      return true;
    }
    return false;
  }
  
  public void onPause()
  {
    super.onPause();
    c();
  }
  
  public void onResume()
  {
    super.onResume();
    if (this.x != null) {
      c();
    }
    this.x = new a(a, this);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */