package com.cgollner.systemmonitor.d;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Comparator;

public final class f
  extends DialogFragment
{
  private b a;
  private ListView b;
  private View c;
  private View d;
  private File e;
  private File[] f;
  private Context g;
  private View.OnClickListener h = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      f.this.dismiss();
      f.b(f.this).a(f.a(f.this));
    }
  };
  
  private void a(File paramFile)
  {
    if ((getDialog() == null) || (paramFile == null)) {
      return;
    }
    getDialog().setTitle(paramFile.getAbsolutePath());
    this.e = paramFile;
    if ("/".equals(this.e.getAbsolutePath())) {
      this.d.setVisibility(8);
    }
    for (;;)
    {
      FileFilter local1 = new FileFilter()
      {
        public final boolean accept(File paramAnonymousFile)
        {
          return paramAnonymousFile.isDirectory();
        }
      };
      this.f = this.e.listFiles(local1);
      if (this.f != null) {
        break;
      }
      this.f = new File[0];
      return;
      this.d.setVisibility(0);
    }
    Arrays.sort(this.f, new Comparator() {});
  }
  
  public final void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    if (getParentFragment() != null)
    {
      this.a = ((b)getParentFragment());
      return;
    }
    this.a = ((b)getActivity());
  }
  
  public final View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.g = getActivity();
    this.c = paramLayoutInflater.inflate(a.f.list_layout, paramViewGroup, false);
    this.b = ((ListView)this.c.findViewById(16908298));
    this.d = this.c.findViewById(a.e.choose);
    this.d.setOnClickListener(this.h);
    a(new File("/"));
    this.b.setAdapter(new a());
    return this.c;
  }
  
  final class a
    extends BaseAdapter
  {
    public a() {}
    
    public final int getCount()
    {
      int i = f.c(f.this).length;
      if (f.a(f.this).getParentFile() != null) {}
      for (int j = 1;; j = 0) {
        return j + i;
      }
    }
    
    public final Object getItem(int paramInt)
    {
      if (f.a(f.this).getParentFile() != null)
      {
        if (paramInt == 0) {
          return f.a(f.this).getParentFile();
        }
        return f.c(f.this)[(paramInt - 1)];
      }
      return f.c(f.this)[paramInt];
    }
    
    public final long getItemId(int paramInt)
    {
      return paramInt;
    }
    
    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      TextView localTextView;
      final File localFile;
      if (paramView == null)
      {
        localTextView = (TextView)LayoutInflater.from(f.d(f.this)).inflate(a.f.simple_list_item, paramViewGroup, false);
        localFile = (File)getItem(paramInt);
        if ((paramInt != 0) || (f.a(f.this).getParentFile() == null)) {
          break label92;
        }
      }
      label92:
      for (String str = "../";; str = localFile.getName() + "/")
      {
        localTextView.setText(str);
        localTextView.setOnClickListener(new View.OnClickListener()
        {
          public final void onClick(View paramAnonymousView)
          {
            if (localFile.isDirectory())
            {
              f.a(f.this, localFile);
              f.a.this.notifyDataSetChanged();
            }
          }
        });
        return localTextView;
        localTextView = (TextView)paramView;
        break;
      }
    }
  }
  
  public static abstract interface b
  {
    public abstract void a(File paramFile);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */