package com.cgollner.systemmonitor.d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.BatteryMonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.c;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.battery.BatteryService;
import com.cgollner.systemmonitor.battery.a;
import java.util.List;

public class c
  extends Fragment
{
  public a a;
  private View b;
  private View c;
  private TextView d;
  private TextView e;
  private TextView f;
  private TextView g;
  private TextView h;
  private TextView i;
  private TextView j;
  private TextView k;
  private BatteryMonitorView l;
  private Context m;
  private float n;
  private View.OnClickListener o = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      c.a(c.this).a();
    }
  };
  private View.OnClickListener p = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      c.a(c.this).b();
    }
  };
  private View.OnClickListener q = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      c.a(c.this).c();
    }
  };
  private View.OnClickListener r = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      c.a(c.this).d();
    }
  };
  private BroadcastReceiver s = new BroadcastReceiver()
  {
    public final void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      try
      {
        c.b(c.this);
        if (c.c(c.this) != null) {
          c.c(c.this).a(c.this.a());
        }
        return;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
  };
  
  private void b()
  {
    boolean bool1 = PreferenceManager.getDefaultSharedPreferences(this.m).getBoolean(App.a.getString(a.h.battery_history_key), true);
    a locala;
    if (bool1)
    {
      locala = BatteryService.c(this.m);
      if (locala != null) {
        break label51;
      }
    }
    label51:
    boolean bool2;
    label174:
    label182:
    List localList1;
    do
    {
      return;
      locala = BatteryService.a(this.m);
      break;
      this.n = locala.a;
      bool2 = locala.d;
      this.h.setText(i.c(this.n));
      TextView localTextView1 = this.e;
      Context localContext1 = App.a;
      int i1;
      TextView localTextView2;
      Context localContext2;
      if (bool2)
      {
        i1 = a.h.battery_charged_at;
        localTextView1.setText(localContext1.getString(i1));
        localTextView2 = this.g;
        localContext2 = App.a;
        if (!bool2) {
          break label174;
        }
      }
      for (int i2 = a.h.battery_charged_in;; i2 = a.h.battery_empty_in)
      {
        localTextView2.setText(localContext2.getString(i2));
        if (bool1) {
          break label182;
        }
        this.i.setText("-");
        this.j.setText("-");
        return;
        i1 = a.h.battery_empty_at;
        break;
      }
      localList1 = BatteryService.i(this.m);
    } while ((localList1 == null) || (localList1.size() == 0));
    if (((!bool2) && (this.n == 0.0F)) || ((bool2) && (this.n == 100.0F))) {}
    for (Long localLong = Long.valueOf(0L);; localLong = Long.valueOf(((com.cgollner.systemmonitor.battery.c)localList1.get(-1 + localList1.size())).a - System.currentTimeMillis()))
    {
      long l1 = localLong.longValue() + System.currentTimeMillis();
      this.i.setText(i.a(l1, this.m) + ", " + i.f(l1));
      this.j.setText(i.d(localLong.longValue()));
      List localList2 = BatteryService.e(this.m);
      if (localList2 == null) {
        break;
      }
      if (localList1 != null) {
        this.l.setPredictionArray(localList1);
      }
      this.l.setValues(localList2);
      this.l.postInvalidate();
      return;
    }
  }
  
  public final int a()
  {
    if (this.n < 25.0F) {
      return a.c.batteryQ1Line;
    }
    if (this.n < 50.0F) {
      return a.c.batteryQ2Line;
    }
    if (this.n < 75.0F) {
      return a.c.batteryQ3Line;
    }
    return a.c.batteryQ4Line;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.m = getActivity();
    this.b = paramLayoutInflater.inflate(a.f.battery_fragment, null);
    this.b.findViewById(a.e.minus).setOnClickListener(this.o);
    this.b.findViewById(a.e.plus).setOnClickListener(this.p);
    this.b.findViewById(a.e.back).setOnClickListener(this.q);
    this.b.findViewById(a.e.forward).setOnClickListener(this.r);
    this.c = this.b.findViewById(a.e.batteryStats);
    this.l = ((BatteryMonitorView)this.b.findViewById(a.e.batteryHistoryView));
    this.l.setTextSize(12);
    this.d = ((TextView)this.c.findViewById(a.e.utilizationTitle));
    this.e = ((TextView)this.c.findViewById(a.e.speedTitle));
    this.g = ((TextView)this.c.findViewById(a.e.minSpeedTitle));
    this.f = ((TextView)this.c.findViewById(a.e.maxSpeedTitle));
    this.h = ((TextView)this.c.findViewById(a.e.usageAvg));
    this.i = ((TextView)this.c.findViewById(a.e.speedValue));
    this.j = ((TextView)this.c.findViewById(a.e.minSpeedValue));
    this.k = ((TextView)this.c.findViewById(a.e.maxSpeedValue));
    this.d.setText(a.h.battery_percentage);
    this.e.setText("Complete at");
    this.g.setText(a.h.battery_time_to_empty);
    this.f.setText("");
    this.h.setText("");
    this.i.setText("");
    this.j.setText("");
    this.k.setText("");
    return this.b;
  }
  
  public void onPause()
  {
    super.onPause();
    this.m.unregisterReceiver(this.s);
  }
  
  public void onResume()
  {
    super.onResume();
    IntentFilter localIntentFilter = new IntentFilter("com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE");
    this.m.registerReceiver(this.s, localIntentFilter);
    b();
    if (this.a != null) {
      this.a.a(a());
    }
  }
  
  public static abstract interface a
  {
    public abstract void a(int paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */