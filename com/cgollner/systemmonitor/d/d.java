package com.cgollner.systemmonitor.d;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.battery.BatteryService;
import com.cgollner.systemmonitor.battery.a;
import com.cgollner.systemmonitor.battery.b;

public class d
  extends Fragment
{
  private View a;
  private TextView b;
  private TextView c;
  private TextView d;
  private TextView e;
  private TextView f;
  private TextView g;
  private TextView h;
  private TextView i;
  private TextView j;
  private TextView k;
  private Context l;
  private BroadcastReceiver m = new BroadcastReceiver()
  {
    public final void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      try
      {
        d.b(d.this);
        return;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
  };
  
  private void a()
  {
    boolean bool = BatteryService.a(this.l).d;
    long l1 = b.a(this.l, true);
    long l2 = b.a(this.l, false);
    long l3 = b.b(this.l);
    TextView localTextView1 = this.j;
    Context localContext1 = App.a;
    int n;
    String str1;
    label90:
    int i1;
    label117:
    String str2;
    label148:
    String str3;
    label173:
    String str4;
    label198:
    String str5;
    label223:
    String str6;
    label249:
    String str7;
    label275:
    TextView localTextView10;
    if (bool)
    {
      n = a.h.charge_speed_1_;
      localTextView1.setText(localContext1.getString(n));
      TextView localTextView2 = this.h;
      if (l3 != -1L) {
        break label317;
      }
      str1 = "N/A";
      localTextView2.setText(str1);
      TextView localTextView3 = this.k;
      Context localContext2 = App.a;
      if (!bool) {
        break label327;
      }
      i1 = a.h.charge_rate_per_hour;
      localTextView3.setText(localContext2.getString(i1));
      TextView localTextView4 = this.i;
      if (l3 != -1L) {
        break label335;
      }
      str2 = "N/A";
      localTextView4.setText(str2);
      TextView localTextView5 = this.b;
      if (l1 != -1L) {
        break label367;
      }
      str3 = "N/A";
      localTextView5.setText(str3);
      TextView localTextView6 = this.c;
      if (l1 != -1L) {
        break label376;
      }
      str4 = "N/A";
      localTextView6.setText(str4);
      TextView localTextView7 = this.d;
      if (l1 != -1L) {
        break label407;
      }
      str5 = "N/A";
      localTextView7.setText(str5);
      TextView localTextView8 = this.e;
      if (l2 != -1L) {
        break label420;
      }
      str6 = "N/A";
      localTextView8.setText(str6);
      TextView localTextView9 = this.f;
      if (l2 != -1L) {
        break label430;
      }
      str7 = "N/A";
      localTextView9.setText(str7);
      localTextView10 = this.g;
      if (l2 != -1L) {
        break label462;
      }
    }
    label317:
    label327:
    label335:
    label367:
    label376:
    label407:
    label420:
    label430:
    label462:
    for (String str8 = "N/A";; str8 = i.d(100L * l2))
    {
      localTextView10.setText(str8);
      return;
      n = a.h.discharge_speed_1_;
      break;
      str1 = i.d(l3);
      break label90;
      i1 = a.h.discharge_rate_per_hour;
      break label117;
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = Double.valueOf(3600000.0D / l3);
      str2 = String.format("%.1f%%", arrayOfObject1);
      break label148;
      str3 = i.d(l1);
      break label173;
      Object[] arrayOfObject2 = new Object[1];
      arrayOfObject2[0] = Double.valueOf(3600000.0D / l1);
      str4 = String.format("%.1f%%", arrayOfObject2);
      break label198;
      str5 = i.d(l1 * 100L);
      break label223;
      str6 = i.d(l2);
      break label249;
      Object[] arrayOfObject3 = new Object[1];
      arrayOfObject3[0] = Double.valueOf(3600000.0D / l2);
      str7 = String.format("%.1f%%", arrayOfObject3);
      break label275;
    }
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(a.g.battery_stats_menu, paramMenu);
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.l = getActivity();
    this.a = paramLayoutInflater.inflate(a.f.battery_stats_layout, paramViewGroup, false);
    this.h = ((TextView)this.a.findViewById(a.e.textViewCycleSpeed));
    this.i = ((TextView)this.a.findViewById(a.e.textViewCycleRateHour));
    this.j = ((TextView)this.a.findViewById(a.e.textViewCycleSpeedTitle));
    this.k = ((TextView)this.a.findViewById(a.e.textViewCycleRateTitle));
    this.b = ((TextView)this.a.findViewById(a.e.textViewStatsChargeSpeed));
    this.c = ((TextView)this.a.findViewById(a.e.textViewStatsChargeRateHour));
    this.d = ((TextView)this.a.findViewById(a.e.textViewStatsTimeToCharge));
    this.e = ((TextView)this.a.findViewById(a.e.textViewStatsDisChargeSpeed));
    this.f = ((TextView)this.a.findViewById(a.e.textViewStatsDisChargeRateHour));
    this.g = ((TextView)this.a.findViewById(a.e.textViewStatsTimeToDisCharge));
    setHasOptionsMenu(true);
    a();
    return this.a;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == a.e.resetStats)
    {
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity()).setTitle(a.h.reset_battery_statistics);
      String[] arrayOfString = new String[3];
      arrayOfString[0] = App.a.getString(a.h.reset_charging_statistics);
      arrayOfString[1] = App.a.getString(a.h.reset_discharging_statistics);
      arrayOfString[2] = App.a.getString(a.h.reset_all_statistics);
      localBuilder.setItems(arrayOfString, new DialogInterface.OnClickListener()
      {
        public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          switch (paramAnonymousInt)
          {
          default: 
            return;
          case 0: 
            BatteryService.a(d.a(d.this), true);
            return;
          case 1: 
            BatteryService.a(d.a(d.this), false);
            return;
          }
          BatteryService.k(d.a(d.this));
        }
      }).create().show();
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public void onPause()
  {
    super.onPause();
    this.l.unregisterReceiver(this.m);
  }
  
  public void onResume()
  {
    super.onResume();
    IntentFilter localIntentFilter = new IntentFilter("com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE");
    this.l.registerReceiver(this.m, localIntentFilter);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */