package com.cgollner.systemmonitor.d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.BatteryMonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.battery.BatteryService;
import com.cgollner.systemmonitor.battery.a;
import com.cgollner.systemmonitor.battery.c;
import java.util.Iterator;
import java.util.List;

public class e
  extends Fragment
{
  private View a;
  private View b;
  private TextView c;
  private TextView d;
  private TextView e;
  private TextView f;
  private TextView g;
  private TextView h;
  private TextView i;
  private TextView j;
  private BatteryMonitorView k;
  private Context l;
  private View.OnClickListener m = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      e.a(e.this).a();
    }
  };
  private View.OnClickListener n = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      e.a(e.this).b();
    }
  };
  private View.OnClickListener o = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      e.a(e.this).c();
    }
  };
  private View.OnClickListener p = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      e.a(e.this).d();
    }
  };
  private BroadcastReceiver q = new BroadcastReceiver()
  {
    public final void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      try
      {
        e.b(e.this);
        return;
      }
      finally
      {
        localObject = finally;
        throw ((Throwable)localObject);
      }
    }
  };
  
  private void a()
  {
    boolean bool = PreferenceManager.getDefaultSharedPreferences(this.l).getBoolean(App.a.getString(a.h.battery_history_key), true);
    a locala;
    if (bool)
    {
      locala = BatteryService.c(this.l);
      if (locala != null) {
        break label51;
      }
    }
    label51:
    List localList;
    do
    {
      return;
      locala = BatteryService.a(this.l);
      break;
      this.g.setText(i.a(Math.round(locala.b), this.l));
      if (!bool)
      {
        this.h.setText("-");
        this.i.setText("-");
        this.j.setText("-");
        return;
      }
      localList = BatteryService.f(this.l);
    } while (localList == null);
    TextView localTextView1 = this.h;
    Iterator localIterator1 = localList.iterator();
    for (float f1 = 0.0F; localIterator1.hasNext(); f1 += ((c)localIterator1.next()).b) {}
    localTextView1.setText(i.a(Math.round((int)(f1 / localList.size())), this.l));
    TextView localTextView2 = this.i;
    Iterator localIterator2 = localList.iterator();
    float f2 = -2.14748365E9F;
    c localc2;
    if (localIterator2.hasNext())
    {
      localc2 = (c)localIterator2.next();
      if (localc2.b <= f2) {
        break label383;
      }
    }
    label376:
    label383:
    for (float f5 = localc2.b;; f5 = f2)
    {
      f2 = f5;
      break;
      localTextView2.setText(i.a(Math.round((int)f2), this.l));
      TextView localTextView3 = this.j;
      Iterator localIterator3 = localList.iterator();
      float f3 = 2.14748365E9F;
      c localc1;
      if (localIterator3.hasNext())
      {
        localc1 = (c)localIterator3.next();
        if (localc1.b >= f3) {
          break label376;
        }
      }
      for (float f4 = localc1.b;; f4 = f3)
      {
        f3 = f4;
        break;
        localTextView3.setText(i.a(Math.round((int)f3), this.l));
        this.k.setValues(localList);
        this.k.postInvalidate();
        return;
      }
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.l = getActivity();
    this.a = paramLayoutInflater.inflate(a.f.battery_fragment, null);
    this.a.findViewById(a.e.minus).setOnClickListener(this.m);
    this.a.findViewById(a.e.plus).setOnClickListener(this.n);
    this.a.findViewById(a.e.back).setOnClickListener(this.o);
    this.a.findViewById(a.e.forward).setOnClickListener(this.p);
    this.b = this.a.findViewById(a.e.batteryStats);
    this.k = ((BatteryMonitorView)this.a.findViewById(a.e.batteryHistoryView));
    this.k.setTextSize(12);
    this.k.c = true;
    this.k.d = 1000.0F;
    this.k.invalidate();
    this.c = ((TextView)this.b.findViewById(a.e.utilizationTitle));
    this.d = ((TextView)this.b.findViewById(a.e.speedTitle));
    this.f = ((TextView)this.b.findViewById(a.e.minSpeedTitle));
    this.e = ((TextView)this.b.findViewById(a.e.maxSpeedTitle));
    this.g = ((TextView)this.b.findViewById(a.e.usageAvg));
    this.h = ((TextView)this.b.findViewById(a.e.speedValue));
    this.i = ((TextView)this.b.findViewById(a.e.minSpeedValue));
    this.j = ((TextView)this.b.findViewById(a.e.maxSpeedValue));
    this.c.setText(a.h.battery_temperature);
    this.d.setText(a.h.statistics_average);
    this.f.setText(a.h.max);
    this.e.setText(a.h.min);
    this.g.setText("");
    this.h.setText("");
    this.i.setText("");
    this.j.setText("");
    View localView1 = this.a.findViewById(a.e.plus);
    RelativeLayout.LayoutParams localLayoutParams1 = (RelativeLayout.LayoutParams)localView1.getLayoutParams();
    localLayoutParams1.leftMargin = this.k.e.right;
    localView1.setLayoutParams(localLayoutParams1);
    View localView2 = this.a.findViewById(a.e.minus);
    RelativeLayout.LayoutParams localLayoutParams2 = (RelativeLayout.LayoutParams)localView2.getLayoutParams();
    localLayoutParams2.leftMargin = this.k.e.right;
    localView2.setLayoutParams(localLayoutParams2);
    return this.a;
  }
  
  public void onPause()
  {
    super.onPause();
    this.l.unregisterReceiver(this.q);
  }
  
  public void onResume()
  {
    super.onResume();
    IntentFilter localIntentFilter = new IntentFilter("com.cgollner.systemmonitor.battery.ACTION_BATTERY_INFO_UPDATE");
    this.l.registerReceiver(this.q, localIntentFilter);
    a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */