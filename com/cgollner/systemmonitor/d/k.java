package com.cgollner.systemmonitor.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.MonitorView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.c.d;
import com.cgollner.systemmonitor.c.e.a;
import java.util.List;
import java.util.Locale;

public class k
  extends Fragment
  implements e.a
{
  public static int a;
  private static List<Float> i;
  private MonitorView b;
  private d c;
  private TextView d;
  private TextView e;
  private TextView f;
  private Context g;
  private Handler h;
  
  private void b()
  {
    if (this.c != null)
    {
      this.c.c();
      this.c = null;
    }
  }
  
  public final void a()
  {
    this.h.post(new Runnable()
    {
      public final void run()
      {
        if ((k.this.getActivity() == null) || (k.this.isDetached())) {}
        for (;;)
        {
          return;
          try
          {
            k.b(k.this).a(k.a(k.this).c, b.a);
            if (b.a)
            {
              TextView localTextView1 = k.c(k.this);
              d locald1 = k.a(k.this);
              Locale localLocale = Locale.getDefault();
              Object[] arrayOfObject = new Object[1];
              arrayOfObject[0] = Float.valueOf(locald1.c);
              localTextView1.setText(String.format(localLocale, "%.0f%%", arrayOfObject));
              TextView localTextView2 = k.d(k.this);
              d locald2 = k.a(k.this);
              localTextView2.setText(i.a(locald2.d, locald2.f));
              TextView localTextView3 = k.e(k.this);
              d locald3 = k.a(k.this);
              localTextView3.setText(i.a(locald3.e, locald3.f));
              return;
            }
          }
          catch (NullPointerException localNullPointerException) {}
        }
      }
    });
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.g = getActivity();
    this.h = new Handler();
    a = 1000 * PreferenceManager.getDefaultSharedPreferences(this.g).getInt(App.a.getString(a.h.settings_app_io_updatefreq_key), 1);
    View localView = paramLayoutInflater.inflate(a.f.io_fragment_layout, null);
    this.b = ((MonitorView)localView.findViewById(a.e.monitorview));
    if (i != null) {
      this.b.a.addAll(i);
    }
    this.b.setDrawBorders$1d54120b(false);
    this.d = ((TextView)localView.findViewById(a.e.usageAvg));
    this.e = ((TextView)localView.findViewById(a.e.readSpeedValue));
    this.f = ((TextView)localView.findViewById(a.e.writeSpeedValue));
    setHasOptionsMenu(true);
    return localView;
  }
  
  public void onDestroyView()
  {
    i = this.b.a;
    super.onDestroyView();
  }
  
  public void onPause()
  {
    super.onPause();
    b();
  }
  
  public void onResume()
  {
    super.onResume();
    if (this.c != null) {
      b();
    }
    this.c = new d(a, this);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */