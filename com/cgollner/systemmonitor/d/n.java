package com.cgollner.systemmonitor.d;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.TrafficStats;
import android.os.Debug.MemoryInfo;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.a.b;
import com.cgollner.systemmonitor.a.c;
import com.cgollner.systemmonitor.b.a.h;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressLint({"UseSparseArrays"})
public final class n
  extends Thread
{
  public static int a;
  public q b;
  public Map<Integer, a> c = new HashMap();
  public Map<Integer, a> d = new HashMap();
  public Map<String, a> e = new HashMap();
  public List<a> f = new ArrayList(200);
  public boolean g;
  public boolean h;
  public String i;
  public String j;
  public boolean k;
  public Comparator<a> l;
  private ActivityManager m;
  private PackageManager n;
  private Handler o;
  private View p;
  private float q;
  private String[] r = new String[2];
  private int[] s;
  
  public n(q paramq, Context paramContext, Handler paramHandler, View paramView)
  {
    this.b = paramq;
    a = 1000 * PreferenceManager.getDefaultSharedPreferences(paramContext).getInt(App.a.getString(a.h.settings_app_topapps_updatefreq_key), 1);
    this.m = ((ActivityManager)paramContext.getSystemService("activity"));
    this.n = paramContext.getPackageManager();
    this.o = paramHandler;
    this.p = paramView;
    if (paramView.getVisibility() == 8)
    {
      paramView.setVisibility(0);
      return;
    }
    paramView.setVisibility(8);
  }
  
  private void a(Map<Integer, a> paramMap)
  {
    if (this.k)
    {
      Iterator localIterator2 = b.c().iterator();
      while (localIterator2.hasNext())
      {
        int i1 = ((Integer)localIterator2.next()).intValue();
        a locala2 = (a)paramMap.get(Integer.valueOf(i1));
        if (locala2 == null)
        {
          a locala3 = new a(0L, b.a(i1));
          paramMap.put(Integer.valueOf(i1), locala3);
        }
        else
        {
          locala2.b = b.a(i1);
        }
      }
    }
    Iterator localIterator1 = this.m.getRunningAppProcesses().iterator();
    while (localIterator1.hasNext())
    {
      ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)localIterator1.next();
      a locala1 = (a)paramMap.get(Integer.valueOf(localRunningAppProcessInfo.pid));
      if (locala1 == null)
      {
        long l1 = TrafficStats.getUidRxBytes(localRunningAppProcessInfo.uid) + TrafficStats.getUidTxBytes(localRunningAppProcessInfo.uid);
        String str = b.a(localRunningAppProcessInfo.pid);
        paramMap.put(Integer.valueOf(localRunningAppProcessInfo.pid), new a(l1, str));
      }
      else
      {
        locala1.a = (TrafficStats.getUidRxBytes(localRunningAppProcessInfo.uid) + TrafficStats.getUidTxBytes(localRunningAppProcessInfo.uid));
        locala1.b = b.a(localRunningAppProcessInfo.pid);
      }
    }
  }
  
  public final void run()
  {
    this.g = true;
    this.h = true;
    if (this.g)
    {
      a(this.c);
      this.r[0] = b.a();
      this.i = b.a();
      for (;;)
      {
        try
        {
          if (!this.h) {
            continue;
          }
          l7 = 100L;
          Thread.sleep(l7);
          this.h = false;
        }
        catch (InterruptedException localInterruptedException)
        {
          long l7;
          long l1;
          long l2;
          Set localSet;
          Iterator localIterator1;
          ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo;
          a locala4;
          a locala5;
          String str4;
          String str5;
          long l5;
          float f2;
          Debug.MemoryInfo[] arrayOfMemoryInfo;
          long l6;
          String[] arrayOfString2;
          int i2;
          int i3;
          String str6;
          Iterator localIterator2;
          continue;
        }
        this.r[1] = b.a();
        this.q = Math.round(b.a(this.r[0], this.r[1]));
        a(this.d);
        this.j = b.a();
        l1 = b.b(this.i.split(" "));
        l2 = b.b(this.j.split(" ")) - l1;
        this.f.clear();
        localSet = b.d();
        localIterator1 = this.m.getRunningAppProcesses().iterator();
        if (localIterator1.hasNext())
        {
          localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)localIterator1.next();
          locala4 = (a)this.c.get(Integer.valueOf(localRunningAppProcessInfo.pid));
          locala5 = (a)this.d.get(Integer.valueOf(localRunningAppProcessInfo.pid));
          if ((locala4 == null) || (locala5 == null) || (localRunningAppProcessInfo.importance == 400)) {
            continue;
          }
          str4 = locala4.b;
          str5 = locala5.b;
          l5 = locala5.a - locala4.a;
          f2 = b.a(str4, str5, l2, this.q);
          if (this.s == null) {
            this.s = new int[1];
          }
          this.s[0] = localRunningAppProcessInfo.pid;
          arrayOfMemoryInfo = this.m.getProcessMemoryInfo(this.s);
          l6 = 1024L * (0L + arrayOfMemoryInfo[0].dalvikPss + arrayOfMemoryInfo[0].nativePss + arrayOfMemoryInfo[0].otherPss);
          arrayOfString2 = localRunningAppProcessInfo.pkgList;
          i2 = arrayOfString2.length;
          i3 = 0;
          if (i3 >= i2) {
            continue;
          }
          str6 = arrayOfString2[i3];
          try
          {
            str7 = (String)this.n.getApplicationInfo(str6, 129).loadLabel(this.n);
            ??? = (a)this.e.get(str7 + "-" + localRunningAppProcessInfo.pid);
            if (??? != null) {}
          }
          catch (PackageManager.NameNotFoundException localNameNotFoundException)
          {
            String str7;
            int i4;
            continue;
          }
          synchronized (new a())
          {
            ???.a = str7;
            ???.b = str6;
            if ((localRunningAppProcessInfo.processName != null) && (localRunningAppProcessInfo.processName.contains(":"))) {
              ???.g = localRunningAppProcessInfo.processName.replaceFirst("(.*:)(.*)", "$2");
            }
            ???.c = f2;
            ???.e = l6;
            ???.f = l5;
            this.e.put(str7 + "-" + localRunningAppProcessInfo.pid, ???);
            this.f.add(???);
            localSet.remove(Integer.valueOf(localRunningAppProcessInfo.pid));
            i3++;
            continue;
            i4 = a;
            l7 = i4;
          }
        }
      }
      if (this.k)
      {
        localIterator2 = localSet.iterator();
        while (localIterator2.hasNext())
        {
          int i1 = ((Integer)localIterator2.next()).intValue();
          a locala1 = (a)this.c.get(Integer.valueOf(i1));
          a locala2 = (a)this.d.get(Integer.valueOf(i1));
          if ((locala1 != null) && (locala2 != null))
          {
            String str1 = locala1.b;
            String str2 = locala2.b;
            long l3 = locala2.a - locala1.a;
            float f1 = b.a(str1, str2, l2, this.q);
            String str3 = (String)b.b(i1);
            ??? = (a)this.e.get(str3);
            if (??? == null) {}
            synchronized (new a())
            {
              ???.a = str3;
              ???.b = "systemprocess";
              ???.d = null;
              ???.c = f1;
              String[] arrayOfString1 = c.a("/proc/" + i1 + "/stat").split(" ");
              long l4;
              if (arrayOfString1.length <= 1)
              {
                l4 = 0L;
                ???.e = l4;
                ???.f = l3;
                this.e.put(???.a, ???);
                this.f.add(???);
              }
              else
              {
                l4 = 4096L * Long.parseLong(arrayOfString1[23]);
              }
            }
          }
        }
      }
    }
    try
    {
      synchronized (this.f)
      {
        Collections.sort(this.f, this.l);
        this.o.post(new Runnable()
        {
          public final void run()
          {
            synchronized (n.this.b)
            {
              n.this.b.a.clear();
              n.this.b.a.addAll(n.this.f);
              n.this.b.notifyDataSetChanged();
              n.a(n.this).setVisibility(8);
              return;
            }
          }
        });
      }
      return;
    }
    catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
    {
      for (;;) {}
    }
  }
  
  final class a
  {
    long a;
    String b;
    
    public a(long paramLong, String paramString)
    {
      this.a = paramLong;
      this.b = paramString;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */