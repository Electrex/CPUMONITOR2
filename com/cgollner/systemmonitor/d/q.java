package com.cgollner.systemmonitor.d;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import java.util.ArrayList;
import java.util.List;

public final class q
  extends BaseAdapter
{
  public List<a> a;
  private final Context b;
  private LayoutInflater c;
  private int d;
  
  public q(Context paramContext, int paramInt)
  {
    this.c = LayoutInflater.from(paramContext);
    this.a = new ArrayList(200);
    this.d = paramInt;
    this.b = paramContext;
  }
  
  public final int getCount()
  {
    return this.a.size();
  }
  
  public final Object getItem(int paramInt)
  {
    return this.a.get(paramInt);
  }
  
  public final long getItemId(int paramInt)
  {
    return 0L;
  }
  
  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null) {
      paramView = this.c.inflate(this.d, null);
    }
    ImageView localImageView = (ImageView)paramView.findViewById(a.e.topAppIcon);
    TextView localTextView1 = (TextView)paramView.findViewById(a.e.topAppName);
    TextView localTextView2 = (TextView)paramView.findViewById(a.e.topAppUsageCpu);
    TextView localTextView3 = (TextView)paramView.findViewById(a.e.topAppUsageRam);
    TextView localTextView4 = (TextView)paramView.findViewById(a.e.topAppUsageNet);
    TextView localTextView5 = (TextView)paramView.findViewById(a.e.topAppProcessName);
    final a locala = (a)this.a.get(paramInt);
    List localList = this.a;
    if (localImageView != null) {}
    for (;;)
    {
      try
      {
        if (locala.d != null)
        {
          localImageView.setImageDrawable(locala.d);
          if (localTextView1 != null) {
            localTextView1.setText(locala.a);
          }
          if (localTextView2 != null)
          {
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Float.valueOf(locala.c);
            localTextView2.setText(String.format("%.1f%%", arrayOfObject));
          }
          if (localTextView3 != null) {
            localTextView3.setText(i.c(locala.e));
          }
          if (localTextView4 != null) {
            localTextView4.setText(i.a(locala.f, n.a));
          }
          if ((localTextView5 != null) && (locala.g != null))
          {
            localTextView5.setVisibility(0);
            localTextView5.setText(locala.g);
            paramView.setOnClickListener(new View.OnClickListener()
            {
              public final void onClick(View paramAnonymousView)
              {
                if (locala.b != null)
                {
                  Intent localIntent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
                  localIntent.setData(Uri.fromParts("package", locala.b, null));
                  localIntent.setFlags(268435456);
                  q.a(q.this).startActivity(localIntent);
                }
              }
            });
            return paramView;
          }
        }
        else
        {
          PackageManager localPackageManager = this.b.getPackageManager();
          try
          {
            locala.d = localPackageManager.getApplicationInfo(locala.b, 0).loadIcon(localPackageManager);
            localImageView.setImageDrawable(locala.d);
          }
          catch (Exception localException)
          {
            localImageView.setImageResource(17301651);
          }
          continue;
        }
        if (localTextView5 == null) {
          continue;
        }
      }
      finally {}
      localTextView5.setVisibility(8);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */