package com.cgollner.systemmonitor.d;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cgollner.systemmonitor.App;
import com.cgollner.systemmonitor.a.a;
import com.cgollner.systemmonitor.a.a.a;
import com.cgollner.systemmonitor.a.a.b;
import com.cgollner.systemmonitor.a.i;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import com.cgollner.systemmonitor.b.a.h;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class b
  extends Fragment
  implements LoaderManager.LoaderCallbacks<List<a.a>>, SwipeRefreshLayout.OnRefreshListener, AbsListView.MultiChoiceModeListener
{
  protected static ProgressDialog a;
  private List<a.a> b;
  private TextView c;
  private TextView d;
  private View e;
  private View f;
  private View g;
  private TextView h;
  private View i;
  private View j;
  private ListView k;
  private boolean l;
  private View.OnClickListener m = new View.OnClickListener()
  {
    public final void onClick(View paramAnonymousView)
    {
      if (b.a(b.this.getActivity())) {
        return;
      }
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(b.this.getActivity()).setTitle(a.h.clear_cache);
      Locale localLocale = Locale.US;
      String str = App.a.getString(a.h.are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = App.a.getString(a.h.all_apps);
      localBuilder.setMessage(Html.fromHtml(String.format(localLocale, str, arrayOfObject))).setPositiveButton(17039379, new DialogInterface.OnClickListener()
      {
        public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
        {
          b.a = ProgressDialog.show(b.this.getActivity(), null, App.a.getString(a.h.clearing_cache_));
          a.a(b.a(b.this), b.b(b.this));
        }
      }).setNegativeButton(17039369, new DialogInterface.OnClickListener()
      {
        public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {}
      }).show();
    }
  };
  private a.b n = new a.b()
  {
    public final void a()
    {
      b.a.dismiss();
      b.c(b.this).setVisibility(8);
      b.d(b.this).setVisibility(8);
      b.e(b.this).setVisibility(0);
      b.this.getLoaderManager().restartLoader(0, null, b.this);
    }
  };
  
  private static long a(List<a.a> paramList)
  {
    Iterator localIterator = paramList.iterator();
    for (long l1 = 0L; localIterator.hasNext(); l1 += ((a.a)localIterator.next()).d) {}
    return l1;
  }
  
  protected static boolean a(Context paramContext)
  {
    if (!paramContext.getPackageName().equals("com.cgollner.systemmonitor")) {}
    for (int i1 = 1; i1 == 0; i1 = 0) {
      return false;
    }
    new AlertDialog.Builder(paramContext).setTitle(App.a.getString(a.h.full_version_feature_title)).setMessage(a.h.full_version_feature_message).setPositiveButton("Play Store", new DialogInterface.OnClickListener()
    {
      public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        if (paramAnonymousDialogInterface != null) {
          paramAnonymousDialogInterface.dismiss();
        }
        Intent localIntent = new Intent("android.intent.action.VIEW");
        localIntent.setData(Uri.parse("market://details?id=com.cgollner.systemmonitor"));
        this.a.startActivity(localIntent);
      }
    }).setNegativeButton(17039360, new DialogInterface.OnClickListener()
    {
      public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        if (paramAnonymousDialogInterface != null) {
          paramAnonymousDialogInterface.dismiss();
        }
      }
    }).create().show();
    return true;
  }
  
  private void b()
  {
    PreferenceManager.getDefaultSharedPreferences(App.a).edit().putBoolean("save_start_root", true).apply();
    this.j.setVisibility(8);
    this.h.setVisibility(8);
    this.g.setVisibility(8);
    this.f.setVisibility(0);
    getLoaderManager().initLoader(0, null, this);
  }
  
  public final void a()
  {
    if (!this.l)
    {
      this.l = true;
      if (PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("save_start_root", false)) {
        b();
      }
    }
    else
    {
      return;
    }
    this.j.setVisibility(0);
  }
  
  public boolean onActionItemClicked(ActionMode paramActionMode, MenuItem paramMenuItem)
  {
    String str1;
    final ArrayList localArrayList;
    int i1;
    int i2;
    int i3;
    String str2;
    if (paramMenuItem.getItemId() == a.e.delete)
    {
      if (a(getActivity())) {
        return true;
      }
      str1 = "";
      localArrayList = new ArrayList();
      SparseBooleanArray localSparseBooleanArray = this.k.getCheckedItemPositions();
      i1 = localSparseBooleanArray.size();
      i2 = 0;
      if (i2 < i1)
      {
        i3 = localSparseBooleanArray.keyAt(i2);
        if (localSparseBooleanArray.get(i3))
        {
          if ((i2 <= 0) || (i2 >= i1 - 1)) {
            break label176;
          }
          str2 = str1 + ", ";
        }
      }
    }
    for (;;)
    {
      a.a locala = (a.a)this.k.getAdapter().getItem(i3);
      localArrayList.add(locala);
      str1 = str2 + locala.a;
      i2++;
      break;
      label176:
      if ((i2 > 0) && (i2 == i1 - 1))
      {
        StringBuilder localStringBuilder = new StringBuilder().append(str1);
        if (i1 > 2) {}
        for (String str3 = ",";; str3 = "")
        {
          str2 = str3 + " and ";
          break;
        }
        new AlertDialog.Builder(getActivity()).setTitle(a.h.clear_cache).setMessage(Html.fromHtml(String.format(Locale.US, App.a.getString(a.h.are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_), new Object[] { str1 }))).setPositiveButton(17039379, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
          {
            b.a = ProgressDialog.show(b.this.getActivity(), null, App.a.getString(a.h.clearing_cache_));
            a.a(localArrayList, b.b(b.this));
          }
        }).setNegativeButton(17039369, new DialogInterface.OnClickListener()
        {
          public final void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {}
        }).create().show();
        return true;
        return false;
      }
      str2 = str1;
    }
  }
  
  public boolean onCreateActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    paramActionMode.getMenuInflater().inflate(a.g.apps_cache_contextual_menu, paramMenu);
    this.e.setVisibility(8);
    this.i.setVisibility(8);
    paramActionMode.setTitle(this.k.getCheckedItemCount());
    return true;
  }
  
  public Loader<List<a.a>> onCreateLoader(int paramInt, Bundle paramBundle)
  {
    return new a(getActivity());
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    getActivity().getMenuInflater().inflate(a.g.apps_cache_menu, paramMenu);
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(a.f.cache_fragment, paramViewGroup, false);
    this.c = ((TextView)localView.findViewById(a.e.cacheNumApps));
    this.d = ((TextView)localView.findViewById(a.e.cacheTotalSize));
    this.e = localView.findViewById(a.e.deleteAll);
    this.i = localView.findViewById(a.e.deleteAllSeparator);
    this.e.setOnClickListener(this.m);
    this.h = ((TextView)localView.findViewById(a.e.empty));
    this.f = localView.findViewById(16908301);
    this.g = localView.findViewById(16908290);
    this.j = localView.findViewById(a.e.askRootPermissions);
    this.j.findViewById(a.e.askRootStart).setOnClickListener(new View.OnClickListener()
    {
      public final void onClick(View paramAnonymousView)
      {
        b.f(b.this);
      }
    });
    this.k = ((ListView)localView.findViewById(16908298));
    this.k.setChoiceMode(3);
    this.k.setMultiChoiceModeListener(this);
    this.k.setAdapter(new b(getActivity(), this.k, this.n));
    setHasOptionsMenu(true);
    this.l = false;
    return localView;
  }
  
  public void onDestroyActionMode(ActionMode paramActionMode)
  {
    this.e.setVisibility(0);
    this.i.setVisibility(0);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    a = null;
  }
  
  public void onItemCheckedStateChanged(ActionMode paramActionMode, int paramInt, long paramLong, boolean paramBoolean)
  {
    paramActionMode.setTitle(this.k.getCheckedItemCount());
  }
  
  public void onLoaderReset(Loader<List<a.a>> paramLoader) {}
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == a.e.replay) {
      b();
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }
  
  public boolean onPrepareActionMode(ActionMode paramActionMode, Menu paramMenu)
  {
    return false;
  }
  
  public void onRefresh()
  {
    b();
  }
  
  static final class a
  {
    ImageView a;
    TextView b;
    TextView c;
    ImageView d;
    ImageView e;
    View f;
    View g;
  }
  
  static final class b
    extends BaseAdapter
  {
    private final ListView a;
    private final Context b;
    private final a.b c;
    private List<a.a> d;
    
    b(Context paramContext, ListView paramListView, a.b paramb)
    {
      this.b = paramContext;
      this.a = paramListView;
      this.c = paramb;
    }
    
    public final int getCount()
    {
      if (this.d != null) {
        return this.d.size();
      }
      return 0;
    }
    
    public final Object getItem(int paramInt)
    {
      return this.d.get(paramInt);
    }
    
    public final long getItemId(int paramInt)
    {
      return paramInt;
    }
    
    public final View getView(final int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
      {
        paramView = LayoutInflater.from(this.b).inflate(a.f.cache_item, paramViewGroup, false);
        b.a locala2 = new b.a();
        locala2.b = ((TextView)paramView.findViewById(16908308));
        locala2.c = ((TextView)paramView.findViewById(16908309));
        locala2.a = ((ImageView)paramView.findViewById(16908294));
        locala2.d = ((ImageView)paramView.findViewById(a.e.delete));
        locala2.e = ((ImageView)paramView.findViewById(a.e.checked));
        locala2.f = paramView.findViewById(a.e.separator);
        locala2.g = paramView.findViewById(a.e.cacheItemLayout);
        paramView.setTag(locala2);
      }
      b.a locala1 = (b.a)paramView.getTag();
      final a.a locala = (a.a)getItem(paramInt);
      PackageManager localPackageManager = App.a.getPackageManager();
      if (locala.a == null) {}
      for (;;)
      {
        try
        {
          ApplicationInfo localApplicationInfo = localPackageManager.getApplicationInfo(locala.c, 0);
          locala.a = localApplicationInfo.loadLabel(localPackageManager).toString();
          locala.b = localApplicationInfo.loadIcon(localPackageManager);
          locala1.b.setText(locala.a);
          if (locala.b == null) {
            break label416;
          }
          locala1.a.setImageDrawable(locala.b);
          locala1.c.setText(i.a(locala.d));
          locala1.d.setOnClickListener(new View.OnClickListener()
          {
            public final void onClick(View paramAnonymousView)
            {
              if (b.a(b.b.a(b.b.this))) {
                return;
              }
              AlertDialog.Builder localBuilder = new AlertDialog.Builder(b.b.a(b.b.this)).setTitle(a.h.clear_cache);
              Locale localLocale = Locale.US;
              String str = App.a.getString(a.h.are_you_sure_that_you_want_to_clear_the_cache_of_b_s_b_);
              Object[] arrayOfObject = new Object[1];
              arrayOfObject[0] = locala.a;
              localBuilder.setMessage(Html.fromHtml(String.format(localLocale, str, arrayOfObject))).setPositiveButton(17039379, new DialogInterface.OnClickListener()
              {
                public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int)
                {
                  b.a = ProgressDialog.show(b.b.a(b.b.this), b.b.1.this.a.a, App.a.getString(a.h.clearing_cache_));
                  a.a(b.b.1.this.a, b.b.b(b.b.this));
                }
              }).setNegativeButton(17039369, new DialogInterface.OnClickListener()
              {
                public final void onClick(DialogInterface paramAnonymous2DialogInterface, int paramAnonymous2Int) {}
              }).show();
            }
          });
          if (this.a.getCheckedItemCount() <= 0) {
            break label428;
          }
          i = 1;
          boolean bool = this.a.isItemChecked(paramInt);
          if (i == 0) {
            break label456;
          }
          locala1.d.setVisibility(8);
          locala1.f.setVisibility(8);
          if (!bool) {
            break label434;
          }
          locala1.e.setVisibility(0);
          locala1.a.setVisibility(8);
          locala1.g.setSelected(bool);
          locala1.g.setOnClickListener(new View.OnClickListener()
          {
            public final void onClick(View paramAnonymousView)
            {
              ListView localListView = b.b.c(b.b.this);
              int i = paramInt;
              if (!b.b.c(b.b.this).isItemChecked(paramInt)) {}
              for (boolean bool = true;; bool = false)
              {
                localListView.setItemChecked(i, bool);
                return;
              }
            }
          });
          return paramView;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
          localNameNotFoundException.printStackTrace();
          locala1.b.setText(locala.c);
          continue;
        }
        locala1.b.setText(locala.a);
        continue;
        label416:
        locala1.a.setImageBitmap(null);
        continue;
        label428:
        int i = 0;
        continue;
        label434:
        locala1.a.setVisibility(0);
        locala1.e.setVisibility(8);
        continue;
        label456:
        locala1.a.setVisibility(0);
        locala1.d.setVisibility(0);
        locala1.f.setVisibility(0);
        locala1.e.setVisibility(8);
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */