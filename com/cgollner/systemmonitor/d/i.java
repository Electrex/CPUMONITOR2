package com.cgollner.systemmonitor.d;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cgollner.systemmonitor.MonitorView;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.c.e.a;
import java.util.List;

public class i
  extends Fragment
  implements e.a
{
  public static int a;
  private static List<Float> i;
  private MonitorView b;
  private com.cgollner.systemmonitor.c.b c;
  private TextView d;
  private TextView e;
  private TextView f;
  private TextView g;
  private Handler h;
  
  private void b()
  {
    if (this.c != null)
    {
      this.c.c();
      this.c = null;
    }
  }
  
  public final void a()
  {
    this.b.a(this.c.a, com.cgollner.systemmonitor.b.a);
    if (!com.cgollner.systemmonitor.b.a) {
      return;
    }
    this.h.post(new Runnable()
    {
      public final void run()
      {
        if ((i.this.getActivity() == null) || (i.this.isDetached())) {
          return;
        }
        try
        {
          TextView localTextView1 = i.b(i.this);
          com.cgollner.systemmonitor.c.b localb1 = i.a(i.this);
          localTextView1.setText(com.cgollner.systemmonitor.a.i.a(localb1.a, localb1.e));
          TextView localTextView2 = i.c(i.this);
          com.cgollner.systemmonitor.c.b localb2 = i.a(i.this);
          localTextView2.setText(com.cgollner.systemmonitor.a.i.a(localb2.c, localb2.e));
          TextView localTextView3 = i.d(i.this);
          com.cgollner.systemmonitor.c.b localb3 = i.a(i.this);
          localTextView3.setText(com.cgollner.systemmonitor.a.i.a(localb3.b, localb3.e));
          TextView localTextView4 = i.e(i.this);
          com.cgollner.systemmonitor.c.b localb4 = i.a(i.this);
          localTextView4.setText(com.cgollner.systemmonitor.a.i.a(localb4.d, localb4.e));
          return;
        }
        catch (NullPointerException localNullPointerException) {}
      }
    });
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.h = new Handler();
    a = 1000;
    View localView = paramLayoutInflater.inflate(a.f.cpu_temp_fragment_layout, null);
    this.b = ((MonitorView)localView.findViewById(a.e.monitorview));
    this.b.b = 1000.0D;
    this.b.f = true;
    if (i != null) {
      this.b.a.addAll(i);
    }
    this.b.setDrawBorders$1d54120b(true);
    this.d = ((TextView)localView.findViewById(a.e.cpuTempCurrentValue));
    this.e = ((TextView)localView.findViewById(a.e.cpuTempMinValue));
    this.f = ((TextView)localView.findViewById(a.e.cpuTempMaxValue));
    this.g = ((TextView)localView.findViewById(a.e.cpuTempAvgValue));
    setHasOptionsMenu(true);
    return localView;
  }
  
  public void onDestroyView()
  {
    i = this.b.a;
    super.onDestroyView();
  }
  
  public void onPause()
  {
    super.onPause();
    b();
  }
  
  public void onResume()
  {
    super.onResume();
    if (this.c != null) {
      b();
    }
    this.c = new com.cgollner.systemmonitor.c.b(a, this, getActivity());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/d/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */