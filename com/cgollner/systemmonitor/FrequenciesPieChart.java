package com.cgollner.systemmonitor;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import com.cgollner.systemmonitor.a.i;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class FrequenciesPieChart
  extends View
{
  public static int[][] a;
  private float b;
  private float c;
  private Paint d;
  private Paint e;
  private Paint f;
  private RectF g;
  private Paint h;
  private RectF i;
  private float j;
  private int k;
  private boolean l;
  private Paint m;
  private String n;
  private int o;
  private boolean p;
  private boolean q;
  private int r;
  private List<a> s;
  
  static
  {
    int[][] arrayOfInt = new int[6][];
    int[] arrayOfInt1 = new int[2];
    arrayOfInt1[0] = Color.rgb(51, 181, 229);
    arrayOfInt1[1] = Color.rgb(0, 153, 204);
    arrayOfInt[0] = arrayOfInt1;
    int[] arrayOfInt2 = new int[2];
    arrayOfInt2[0] = Color.rgb(153, 204, 0);
    arrayOfInt2[1] = Color.rgb(102, 153, 0);
    arrayOfInt[1] = arrayOfInt2;
    int[] arrayOfInt3 = new int[2];
    arrayOfInt3[0] = Color.rgb(255, 187, 51);
    arrayOfInt3[1] = Color.rgb(255, 136, 0);
    arrayOfInt[2] = arrayOfInt3;
    int[] arrayOfInt4 = new int[2];
    arrayOfInt4[0] = Color.rgb(255, 68, 68);
    arrayOfInt4[1] = Color.rgb(204, 0, 0);
    arrayOfInt[3] = arrayOfInt4;
    int[] arrayOfInt5 = new int[2];
    arrayOfInt5[0] = Color.rgb(239, 103, 241);
    arrayOfInt5[1] = Color.rgb(255, 0, 255);
    arrayOfInt[4] = arrayOfInt5;
    int[] arrayOfInt6 = new int[2];
    arrayOfInt6[0] = Color.rgb(170, 102, 204);
    arrayOfInt6[1] = Color.rgb(153, 51, 204);
    arrayOfInt[5] = arrayOfInt6;
    a = arrayOfInt;
  }
  
  public FrequenciesPieChart(Context paramContext)
  {
    super(paramContext);
    a(paramContext);
  }
  
  public FrequenciesPieChart(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext);
  }
  
  public FrequenciesPieChart(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext);
  }
  
  private void a(Context paramContext)
  {
    this.q = true;
    this.r = 0;
    this.s = new LinkedList();
    Collections.sort(this.s);
    this.d = new Paint(1);
    this.d.setStyle(Paint.Style.STROKE);
    this.d.setStrokeWidth(a.a(18.0F, paramContext.getResources()));
    this.e = new Paint(1);
    this.e.setStyle(Paint.Style.STROKE);
    this.e.setStrokeWidth(a.a(20.0F, paramContext.getResources()));
    this.f = new Paint(1);
    this.f.setStyle(Paint.Style.STROKE);
    this.f.setStrokeWidth(a.a(5.0F, paramContext.getResources()));
    this.h = new Paint(1);
    this.h.setColor(-1);
    this.h.setTextSize(a.a(20.0F, paramContext.getResources()));
    this.h.setTextAlign(Paint.Align.CENTER);
    this.h.setTypeface(Typeface.create("sans-serif-thin", 0));
    TypedArray localTypedArray = paramContext.getTheme().obtainStyledAttributes(new int[] { 16842806 });
    this.h.setColor(localTypedArray.getColor(0, -65536));
    this.m = new Paint(1);
    this.m.setStyle(Paint.Style.FILL);
    this.m.setColor(Color.argb(255, 255, 255, 255));
    this.n = "sans-serif-thin";
  }
  
  private long getValuesSum()
  {
    Iterator localIterator = this.s.iterator();
    for (long l1 = 0L; localIterator.hasNext(); l1 += ((a)localIterator.next()).c) {}
    return l1;
  }
  
  public final void a(List<a> paramList, int paramInt, boolean paramBoolean)
  {
    this.s = paramList;
    if (this.s.size() == 0) {
      this.j = 0.0F;
    }
    for (;;)
    {
      if (paramBoolean) {
        postInvalidate();
      }
      return;
      this.j = Math.max(4.0F, Math.min(1.0F, 3.6F * ((a)Collections.min(this.s)).d));
      this.k = (paramInt % paramList.size());
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    if (this.s.size() == 0) {}
    label46:
    label314:
    label378:
    label629:
    label638:
    label671:
    label804:
    label826:
    for (;;)
    {
      return;
      float f1;
      float f2;
      float f3;
      float f4;
      float f5;
      Iterator localIterator;
      int i1;
      if (getWidth() > 1)
      {
        f1 = getWidth();
        this.b = f1;
        if (getHeight() <= 1) {
          break label629;
        }
        f2 = getHeight();
        this.c = f2;
        f3 = 0.48F * Math.min(this.b, this.c);
        f4 = this.b / 2.0F;
        f5 = this.c / 2.0F;
        float f6 = this.f.getStrokeWidth() / 2.0F;
        if (this.g == null) {
          this.g = new RectF();
        }
        this.g.top = (f5 - (f3 - f6));
        this.g.bottom = (f5 + (f3 - f6));
        this.g.left = (f4 - (f3 - f6));
        this.g.right = (f4 + (f3 - f6));
        float f7 = f6 + this.d.getStrokeWidth() / 2.0F + a.a(12.0F, getResources());
        if (this.i == null) {
          this.i = new RectF();
        }
        this.i.top = (f7 + (f5 - f3));
        this.i.bottom = (f5 + f3 - f7);
        this.i.left = (f7 + (f4 - f3));
        this.i.right = (f4 + f3 - f7);
        if (this.r != 0)
        {
          this.m.setColor(this.r);
          if (!this.q) {
            break label638;
          }
          paramCanvas.drawCircle(f4, f5, f3, this.m);
        }
        localIterator = this.s.iterator();
        i1 = 0;
      }
      float f11;
      for (float f8 = -90.0F;; f8 = f11)
      {
        if (!localIterator.hasNext()) {
          break label826;
        }
        a locala = (a)localIterator.next();
        float f9 = 3.6F * locala.d;
        int i2;
        if (this.p)
        {
          i2 = this.o;
          if (!this.p) {
            break label671;
          }
        }
        String str1;
        String str2;
        float f12;
        float f13;
        float f14;
        for (int i3 = this.o;; i3 = a[(i1 % a.length)][1])
        {
          this.d.setColor(i2);
          this.e.setColor(i2);
          this.f.setColor(i3);
          float f10 = 0.5F * this.j;
          if (f9 > 1.0F) {
            paramCanvas.drawArc(this.i, f8 + f10, f9 - f10, false, this.d);
          }
          if (this.k != i1) {
            break label804;
          }
          str1 = i.c(locala.d);
          str2 = locala.b;
          f12 = 0.25F * this.c;
          f13 = 0.1F * this.c;
          f14 = f5 + (f12 + 2.0F * f13) / 2.3F;
          if (f9 > 1.0F) {
            paramCanvas.drawArc(this.g, f8 + f10, f9 - f10, false, this.f);
          }
          this.h.setColor(i3);
          this.h.setTypeface(Typeface.create(this.n, 0));
          this.h.setTextSize(f13);
          while (this.h.measureText(str2) > f3) {
            this.h.setTextSize(this.h.getTextSize() - 0.1F);
          }
          f1 = paramCanvas.getWidth();
          break;
          f2 = paramCanvas.getHeight();
          break label46;
          paramCanvas.drawRect(this.g, this.m);
          break label314;
          i2 = a[(i1 % a.length)][0];
          break label378;
        }
        paramCanvas.drawText(str2, f4, f14, this.h);
        float f15 = f14 - 1.2F * f13;
        this.h.setTypeface(Typeface.create("sans-serif-thin", 0));
        this.h.setTextSize(f12);
        paramCanvas.drawText(str1, f4, f15, this.h);
        float f16 = f15 - 0.9F * f12;
        this.h.setTypeface(Typeface.create(this.n, 0));
        this.h.setTextSize(f13);
        paramCanvas.drawText(locala.a, f4, f16, this.h);
        int i4 = i1 + 1;
        f11 = f8 + f9;
        i1 = i4;
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    View.MeasureSpec.getMode(paramInt1);
    View.MeasureSpec.getMode(paramInt2);
    int i1 = Math.min(View.MeasureSpec.getSize(paramInt1), View.MeasureSpec.getSize(paramInt2));
    setMeasuredDimension(i1, i1);
  }
  
  public void setPaintBackground(boolean paramBoolean)
  {
    this.l = paramBoolean;
  }
  
  public void setSelected(int paramInt)
  {
    if ((this.s != null) && (this.s.size() > 0)) {}
    for (int i1 = paramInt % this.s.size();; i1 = 0)
    {
      this.k = i1;
      postInvalidate();
      return;
    }
  }
  
  public void setSmallFont(String paramString)
  {
    this.n = paramString;
  }
  
  public void setValues(List<a> paramList)
  {
    a(paramList, 0, false);
  }
  
  public static final class a
    implements Comparable<a>
  {
    public String a;
    public String b;
    public long c;
    public float d;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/FrequenciesPieChart.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */