package com.cgollner.systemmonitor;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import com.cgollner.systemmonitor.b.a.j;
import com.cgollner.systemmonitor.battery.c;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class TimeMonitorView
  extends View
{
  public List<c> a;
  public double b;
  private Paint c;
  private Paint d;
  private Paint e;
  private Paint f;
  private Paint g;
  private Paint h;
  private Path i;
  private int j;
  private float k;
  private float l;
  private String m;
  private Canvas n;
  private long o;
  private float p;
  private int q;
  private long r;
  
  public TimeMonitorView(Context paramContext)
  {
    super(paramContext);
    a(paramContext);
  }
  
  public TimeMonitorView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext, paramAttributeSet);
  }
  
  public TimeMonitorView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext, paramAttributeSet);
  }
  
  private static float a(float paramFloat, Resources paramResources)
  {
    return TypedValue.applyDimension(1, paramFloat, paramResources.getDisplayMetrics());
  }
  
  private void a(Context paramContext)
  {
    if (this.a == null)
    {
      Calendar localCalendar = GregorianCalendar.getInstance();
      Random localRandom = new Random();
      this.a = new ArrayList();
      for (int i1 = 1; i1 <= 60; i1++)
      {
        this.a.add(new c(localCalendar.getTimeInMillis(), localRandom.nextInt(100)));
        localCalendar.add(13, 5);
      }
    }
    this.c = new Paint();
    this.c.setAntiAlias(true);
    this.c.setColor(Color.argb(255, 17, 125, 187));
    this.c.setStyle(Paint.Style.STROKE);
    this.c.setStrokeWidth(a(2.0F, paramContext.getResources()));
    this.d = new Paint();
    this.d.setAntiAlias(true);
    this.d.setColor(Color.argb(180, 17, 125, 187));
    this.h = new Paint();
    this.h.setAntiAlias(true);
    this.h.setColor(Color.argb(255, 120, 120, 120));
    this.h.setStyle(Paint.Style.STROKE);
    this.h.setStrokeWidth(a(1.0F, paramContext.getResources()));
    this.g = new Paint();
    this.g.setAntiAlias(true);
    this.g.setColor(Color.argb(120, 120, 120, 120));
    this.e = new Paint();
    this.e.setAntiAlias(true);
    this.e.setColor(Color.rgb(210, 210, 210));
    this.f = new Paint();
    this.f.setAntiAlias(true);
    this.f.setColor(getResources().getColor(17170432));
    this.f.setTextAlign(Paint.Align.CENTER);
    this.f.setTextSize(a(12.0F, getResources()));
    this.f.setTextAlign(Paint.Align.CENTER);
    int[] arrayOfInt = { 16842806 };
    TypedArray localTypedArray = paramContext.getTheme().obtainStyledAttributes(arrayOfInt);
    this.f.setColor(localTypedArray.getColor(0, 0));
    localTypedArray.recycle();
    this.i = new Path();
    this.k = a(50.0F, getResources());
    this.j = 0;
    this.b = -1.0D;
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    a(paramContext);
    TypedArray localTypedArray = getContext().getTheme().obtainStyledAttributes(paramAttributeSet, a.j.MonitorView, 0, 0);
    try
    {
      this.c.setStrokeWidth(a(localTypedArray.getFloat(a.j.MonitorView_lineWidth, 1.0F), getResources()));
      this.c.setColor(localTypedArray.getInt(a.j.MonitorView_lineColor, Color.argb(200, 17, 125, 187)));
      this.d.setColor(localTypedArray.getInt(a.j.MonitorView_fillColor, Color.argb(50, 17, 125, 187)));
      this.j = localTypedArray.getInt(a.j.MonitorView_backgroundColor, Color.rgb(255, 255, 255));
      this.e.setColor(localTypedArray.getInt(a.j.MonitorView_gridColor, Color.rgb(217, 234, 244)));
      this.e.setStrokeWidth(a(localTypedArray.getFloat(a.j.MonitorView_gridWidth, 1.0F), getResources()));
      this.k = a(localTypedArray.getFloat(a.j.MonitorView_valuesMargin, 20.0F), getResources());
      this.m = localTypedArray.getString(a.j.MonitorView_titleMonitor);
      return;
    }
    finally
    {
      localTypedArray.recycle();
    }
  }
  
  private void a(boolean paramBoolean, Paint paramPaint)
  {
    this.i.reset();
    c localc1 = (c)this.a.get(0);
    this.i.moveTo(-paramPaint.getStrokeWidth(), this.p);
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      c localc2 = (c)localIterator.next();
      float f1 = (float)((localc2.a - localc1.a) / this.r * this.q);
      if (this.b < 0.0D)
      {
        float f3 = this.p - localc2.b / 100.0F * this.p;
        this.i.lineTo(f1, f3);
      }
      else
      {
        float f2 = (float)(this.p - localc2.b / this.b * this.p);
        this.i.lineTo(f1, f2);
      }
    }
    if (paramBoolean)
    {
      this.i.lineTo(this.q, this.p);
      this.i.close();
    }
    this.n.drawPath(this.i, paramPaint);
  }
  
  public final void a()
  {
    this.a.clear();
  }
  
  public final void a(c paramc)
  {
    this.a.add(paramc);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    this.n = paramCanvas;
    int i1;
    if (getHeight() > 0)
    {
      i1 = getHeight();
      this.p = (i1 - this.f.getTextSize());
      if (getWidth() <= 0) {
        break label127;
      }
    }
    label127:
    for (int i2 = getWidth();; i2 = paramCanvas.getWidth())
    {
      this.q = i2;
      float f1 = this.p / 10.0F;
      paramCanvas.drawColor(this.j);
      this.k = (this.q / 5);
      for (int i3 = 0; i3 <= 10; i3++) {
        paramCanvas.drawLine(0.0F, f1 * i3, this.q, f1 * i3, this.e);
      }
      i1 = paramCanvas.getHeight();
      break;
    }
    for (float f2 = this.l; f2 <= this.q + this.k; f2 += this.k)
    {
      float f3 = this.p;
      Paint localPaint = this.e;
      paramCanvas.drawLine(f2, 0.0F, f2, f3, localPaint);
    }
    if (this.a.size() != 0)
    {
      this.r = (((c)this.a.get(-1 + this.a.size())).a - ((c)this.a.get(0)).a);
      this.o = (this.r / 5L);
      a(true, this.d);
      a(false, this.c);
      if (this.m != null)
      {
        this.f.setTextAlign(Paint.Align.LEFT);
        paramCanvas.drawText(this.m, a(10.0F, getResources()), 10.0F + this.f.getTextSize(), this.f);
        this.f.setTextAlign(Paint.Align.CENTER);
      }
      java.text.DateFormat localDateFormat = android.text.format.DateFormat.getTimeFormat(App.a);
      Calendar localCalendar = GregorianCalendar.getInstance();
      localCalendar.setTimeInMillis(((c)this.a.get(0)).a);
      for (int i4 = 1; i4 < 5; i4++)
      {
        localCalendar.add(14, (int)this.o);
        paramCanvas.drawText(localDateFormat.format(new Date(localCalendar.getTimeInMillis())), i4 * this.k, this.p + this.f.getTextSize(), this.f);
      }
    }
  }
  
  public void setBgColor(int paramInt)
  {
    this.j = paramInt;
  }
  
  public void setColors(int[] paramArrayOfInt)
  {
    int i1 = paramArrayOfInt[0];
    int i2 = paramArrayOfInt[1];
    int i3 = paramArrayOfInt[2];
    this.d.setColor(i1);
    this.e.setColor(i2);
    this.c.setColor(i3);
  }
  
  public void setLineWidth(int paramInt)
  {
    this.c.setStrokeWidth(a(paramInt, getResources()));
  }
  
  public void setMargin(float paramFloat)
  {
    this.k = paramFloat;
  }
  
  public void setTextColor(int paramInt)
  {
    this.f.setColor(paramInt);
  }
  
  public void setTextSize(int paramInt)
  {
    this.f.setTextSize(a(paramInt, getResources()));
  }
  
  public void setValues(List<c> paramList)
  {
    a();
    this.a.addAll(paramList);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/TimeMonitorView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */