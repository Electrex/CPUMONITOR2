package com.cgollner.systemmonitor;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.WindowManager;
import com.cgollner.systemmonitor.b.a.c;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.g;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.b.a.i;
import com.cgollner.systemmonitor.battery.BatteryService;
import com.cgollner.systemmonitor.d.c;
import com.cgollner.systemmonitor.d.c.a;
import com.cgollner.systemmonitor.d.e;
import com.cgollner.systemmonitor.d.h;
import com.cgollner.systemmonitor.d.p;
import com.cgollner.systemmonitor.e.a;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public abstract class b
  extends ActionBarActivity
  implements NavigationDrawerFragment.a
{
  public static boolean a = true;
  protected List<a> b;
  private ViewPager c;
  private PagerTitleStrip d;
  private int e;
  private DrawerLayout f;
  private ActionBarDrawerToggle g;
  private NavigationDrawerFragment h;
  
  private int b(int paramInt)
  {
    for (int i = 0; i < this.b.size(); i++) {
      if (((a)this.b.get(i)).c == paramInt) {
        return i;
      }
    }
    return 0;
  }
  
  protected abstract void a();
  
  public final void a(int paramInt)
  {
    int i = b(paramInt);
    a locala = (a)this.b.get(i);
    if (i != this.c.getCurrentItem()) {
      this.c.setCurrentItem(i, true);
    }
    if (this.f != null) {
      this.f.closeDrawer(3);
    }
    int j = locala.a;
    if (locala.d.equals(c.class)) {}
    for (int k = ((c)getSupportFragmentManager().findFragmentByTag("android:switcher:" + a.e.viewPager + ":" + i)).a();; k = j)
    {
      this.h.a(Integer.valueOf(paramInt), k);
      return;
    }
  }
  
  protected abstract Class<? extends Fragment> b();
  
  protected abstract Class<? extends Fragment> c();
  
  protected abstract Class<? extends Fragment> d();
  
  protected abstract Class<? extends Fragment> e();
  
  protected abstract Class<? extends Fragment> f();
  
  protected abstract Class<? extends Fragment> g();
  
  protected abstract Class<? extends Fragment> h();
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.g != null) {
      this.g.onConfigurationChanged(paramConfiguration);
    }
  }
  
  @SuppressLint({"NewApi"})
  protected void onCreate(Bundle paramBundle)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    this.e = localSharedPreferences.getInt("theme", -1);
    int i;
    switch (this.e)
    {
    default: 
      boolean bool = getPackageName().equals("com.franco.kernel");
      if ((Build.VERSION.SDK_INT < 11) || (bool))
      {
        i = 1;
        this.e = i;
        if (this.e != 1) {
          break label847;
        }
      }
      break;
    }
    label847:
    for (int j = a.i.AppThemeDark;; j = a.i.AppTheme)
    {
      setTheme(j);
      localSharedPreferences.edit().putInt("theme", this.e).apply();
      for (;;)
      {
        super.onCreate(paramBundle);
        setContentView(a.f.nav_drawer);
        setSupportActionBar((Toolbar)findViewById(a.e.toolbar));
        this.b = new LinkedList();
        this.b.add(new a(a.e.tab_cpu, a.c.holo_blue_dark, a.h.cpu_title, e()));
        if ((com.cgollner.systemmonitor.a.b.f() != null) && (com.cgollner.systemmonitor.a.b.f().exists())) {
          this.b.add(new a(a.e.tab_cpu_temperature, a.c.holo_blue_dark, a.h.cpu_temperature, g()));
        }
        if (com.cgollner.systemmonitor.a.d.a().b()) {
          this.b.add(new a(a.e.tab_gpu, a.c.holo_blue_dark, a.h.gpu, h()));
        }
        this.b.add(new a(a.e.tab_ram, a.c.holo_purple_dark, a.h.ram_title, d()));
        this.b.add(new a(a.e.tab_io, a.c.holo_green_dark, a.h.io_title, c()));
        this.b.add(new a(a.e.tab_net, a.c.holo_orange_dark, a.h.network_title, b()));
        this.b.add(new a(a.e.tab_top_apps, a.c.holo_red_dark, a.h.top_apps_title, f()));
        this.b.add(new a(a.e.tab_cpu_freqs, a.c.holo_blue_light, a.h.cpu_frequencies, h.class));
        this.b.add(new a(a.e.tab_storage_stats, a.c.holo_blue_dark, a.h.storage_stats, p.class));
        this.b.add(new a(a.e.tab_apps_cache, a.c.holo_blue_dark, a.h.cache_stats, com.cgollner.systemmonitor.d.b.class));
        this.b.add(new a(a.e.tab_battery_history, a.c.holo_red_light, a.h.battery_title, c.class));
        this.b.add(new a(a.e.tab_battery_stats, a.c.holo_blue_dark, a.h.battery_stats, com.cgollner.systemmonitor.d.d.class));
        this.b.add(new a(a.e.tab_battery_temperature, a.c.holo_blue_light, a.h.battery_temperature, e.class));
        this.h = ((NavigationDrawerFragment)getSupportFragmentManager().findFragmentById(a.e.drawerFragment));
        this.h.a(this.b);
        this.c = ((ViewPager)findViewById(a.e.viewPager));
        this.c.setPageTransformer(true, new a());
        this.d = ((PagerTitleStrip)findViewById(a.e.pager_title_strip));
        this.c.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener()
        {
          public final boolean onPreDraw()
          {
            b.a(b.this).getViewTreeObserver().removeOnPreDrawListener(this);
            int i = b.a(b.this, b.this.getIntent().getIntExtra("pos", 0));
            if ((i < 0) || (i >= b.this.b.size())) {
              i = 0;
            }
            int k;
            b.a locala;
            int m;
            c localc;
            if ((i != 0) && (b.a(b.this).getCurrentItem() != i))
            {
              b.a(b.this).setCurrentItem(i);
              k = i;
              locala = (b.a)b.this.b.get(k);
              m = locala.a;
              if (!locala.d.equals(c.class)) {
                break label249;
              }
              localc = (c)b.this.getSupportFragmentManager().findFragmentByTag("android:switcher:" + a.e.viewPager + ":" + k);
              if (localc == null) {
                break label367;
              }
            }
            label249:
            label367:
            for (int n = localc.a();; n = m)
            {
              m = n;
              for (;;)
              {
                b.a(b.this, locala.c, m);
                return true;
                int j = PreferenceManager.getDefaultSharedPreferences(App.a).getInt("pos", 0);
                if ((j != 0) && (b.a(b.this).getCurrentItem() != j)) {
                  b.a(b.this).setCurrentItem(j);
                }
                k = j;
                break;
                if (locala.d.equals(com.cgollner.systemmonitor.d.b.class)) {
                  ((com.cgollner.systemmonitor.d.b)b.this.getSupportFragmentManager().findFragmentByTag("android:switcher:" + a.e.viewPager + ":" + k)).a();
                } else if (locala.d.equals(p.class)) {
                  ((p)b.this.getSupportFragmentManager().findFragmentByTag("android:switcher:" + a.e.viewPager + ":" + k)).a();
                }
              }
            }
          }
        });
        b localb = new b(getSupportFragmentManager());
        this.c.setAdapter(localb);
        this.c.setOnPageChangeListener(localb);
        this.c.setOffscreenPageLimit(1);
        if (getIntent().getBooleanExtra("settings", false)) {
          a();
        }
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(App.a.getString(a.h.battery_history_key), true)) {
          startService(new Intent(App.a, BatteryService.class));
        }
        this.f = ((DrawerLayout)findViewById(a.e.drawer_layout));
        if (this.f != null)
        {
          this.g = new ActionBarDrawerToggle(this, this.f, a.h.drawer_open, a.h.drawer_close)
          {
            @TargetApi(11)
            public final void onDrawerClosed(View paramAnonymousView)
            {
              b.this.invalidateOptionsMenu();
            }
            
            public final void onDrawerOpened(View paramAnonymousView)
            {
              b.this.invalidateOptionsMenu();
            }
          };
          this.f.setDrawerListener(this.g);
          getSupportActionBar().setDisplayHomeAsUpEnabled(true);
          getSupportActionBar().setHomeButtonEnabled(true);
        }
        return;
        setTheme(a.i.AppTheme);
        continue;
        setTheme(a.i.AppThemeDark);
      }
      i = 0;
      break;
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(a.g.activity_main, paramMenu);
    return super.onCreateOptionsMenu(paramMenu);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if ((paramMenuItem.getItemId() == 16908332) && (this.f != null))
    {
      if (this.f.isDrawerOpen(3))
      {
        this.f.closeDrawer(3);
        return true;
      }
      this.f.openDrawer(3);
      return true;
    }
    if (paramMenuItem.getItemId() == a.e.menu_settings)
    {
      a();
      return true;
    }
    return false;
  }
  
  protected void onPause()
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
    localEditor.putInt("pos", this.c.getCurrentItem());
    localEditor.commit();
    super.onPause();
  }
  
  protected void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    if (this.f != null) {
      this.g.syncState();
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    if (this.f == null)
    {
      int i = this.h.getView().getWidth();
      int j = getWindowManager().getDefaultDisplay().getWidth() - i;
      ViewGroup.LayoutParams localLayoutParams = this.c.getLayoutParams();
      localLayoutParams.width = j;
      this.c.setLayoutParams(localLayoutParams);
    }
  }
  
  public static final class a
  {
    public int a;
    public int b;
    public int c;
    public Class<? extends Fragment> d;
    
    public a(int paramInt)
    {
      this.c = paramInt;
    }
    
    public a(int paramInt1, int paramInt2, int paramInt3, Class<? extends Fragment> paramClass)
    {
      this.c = paramInt1;
      this.a = paramInt2;
      this.b = paramInt3;
      this.d = paramClass;
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      a locala;
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        locala = (a)paramObject;
      } while (this.c == locala.c);
      return false;
    }
    
    public final int hashCode()
    {
      return 31 + this.c;
    }
  }
  
  private final class b
    extends FragmentPagerAdapter
    implements ViewPager.OnPageChangeListener, c.a
  {
    public b(FragmentManager paramFragmentManager)
    {
      super();
    }
    
    public final void a(int paramInt)
    {
      if (((b.a)b.this.b.get(b.a(b.this).getCurrentItem())).d.equals(c.class))
      {
        b.b(b.this).setBackgroundResource(paramInt);
        b.this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(b.this.getResources().getColor(paramInt)));
        b.c(b.this).a(Integer.valueOf(((b.a)b.this.b.get(b.a(b.this).getCurrentItem())).c), paramInt);
      }
    }
    
    public final int getCount()
    {
      return b.this.b.size();
    }
    
    public final Fragment getItem(int paramInt)
    {
      try
      {
        Fragment localFragment = (Fragment)((b.a)b.this.b.get(paramInt)).d.newInstance();
        if (((b.a)b.this.b.get(paramInt)).d.equals(c.class)) {
          ((c)localFragment).a = this;
        }
        return localFragment;
      }
      catch (InstantiationException localInstantiationException)
      {
        localInstantiationException.printStackTrace();
        return null;
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        for (;;) {}
      }
    }
    
    public final CharSequence getPageTitle(int paramInt)
    {
      return App.a.getString(((b.a)b.this.b.get(paramInt)).b).toUpperCase(Locale.US);
    }
    
    public final void onPageScrollStateChanged(int paramInt)
    {
      b.a = true;
    }
    
    public final void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
    {
      b.a = true;
    }
    
    public final void onPageSelected(int paramInt)
    {
      b.a locala = (b.a)b.this.b.get(paramInt);
      int i = locala.a;
      c localc;
      if (locala.d.equals(c.class))
      {
        localc = (c)b.this.getSupportFragmentManager().findFragmentByTag("android:switcher:" + a.e.viewPager + ":" + paramInt);
        if (localc == null) {
          break label220;
        }
      }
      label220:
      for (int j = localc.a();; j = i)
      {
        i = j;
        for (;;)
        {
          b.a(b.this, locala.c, i);
          return;
          if (locala.d.equals(com.cgollner.systemmonitor.d.b.class)) {
            ((com.cgollner.systemmonitor.d.b)b.this.getSupportFragmentManager().findFragmentByTag("android:switcher:" + a.e.viewPager + ":" + paramInt)).a();
          } else if (locala.d.equals(p.class)) {
            ((p)b.this.getSupportFragmentManager().findFragmentByTag("android:switcher:" + a.e.viewPager + ":" + paramInt)).a();
          }
        }
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */