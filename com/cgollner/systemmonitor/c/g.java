package com.cgollner.systemmonitor.c;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Debug.MemoryInfo;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import com.cgollner.systemmonitor.a.a.a;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class g
  extends e
{
  public float a;
  public long b;
  public long c;
  public long d;
  private Context e;
  private a j;
  private long k;
  private ActivityManager l;
  
  public g(long paramLong, e.a parama, Context paramContext)
  {
    super(paramLong, parama);
    this.e = paramContext;
    this.l = ((ActivityManager)paramContext.getSystemService("activity"));
    this.j = new a();
    ActivityManager.MemoryInfo localMemoryInfo = new ActivityManager.MemoryInfo();
    this.l.getMemoryInfo(localMemoryInfo);
    this.k = localMemoryInfo.threshold;
    start();
  }
  
  protected final void a()
  {
    int i = 0;
    a locala = this.j;
    StrictMode.ThreadPolicy localThreadPolicy = StrictMode.allowThreadDiskReads();
    try
    {
      locala.b = 0L;
      locala.c = 0L;
      locala.d = 0L;
      FileInputStream localFileInputStream = new FileInputStream("/proc/meminfo");
      int i2 = localFileInputStream.read(locala.a);
      localFileInputStream.close();
      int i3 = locala.a.length;
      int i4 = 0;
      int i5 = 0;
      while ((i4 < i2) && (i5 < 3))
      {
        if (a.a(locala.a, i4, "MemTotal"))
        {
          i4 += 8;
          locala.b = a.a(locala.a, i4);
          i5++;
        }
        while ((i4 < i3) && (locala.a[i4] != 10))
        {
          i4++;
          continue;
          if (a.a(locala.a, i4, "MemFree"))
          {
            i4 += 7;
            locala.c = a.a(locala.a, i4);
            i5++;
          }
          else if (a.a(locala.a, i4, "Cached"))
          {
            i4 += 6;
            locala.d = a.a(locala.a, i4);
            i5++;
          }
        }
        i4++;
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      for (;;)
      {
        List localList;
        Iterator localIterator1;
        StrictMode.setThreadPolicy(localThreadPolicy);
      }
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        StrictMode.setThreadPolicy(localThreadPolicy);
      }
    }
    finally
    {
      StrictMode.setThreadPolicy(localThreadPolicy);
    }
    long l1 = this.j.c + this.j.d - this.k;
    if (l1 < 0L) {
      l1 = 0L;
    }
    localList = this.l.getRunningAppProcesses();
    HashSet localHashSet = new HashSet();
    localIterator1 = localList.iterator();
    while (localIterator1.hasNext())
    {
      ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)localIterator1.next();
      if (localRunningAppProcessInfo.importance == 400) {
        localHashSet.add(Integer.valueOf(localRunningAppProcessInfo.pid));
      }
    }
    int[] arrayOfInt = new int[localHashSet.size()];
    Iterator localIterator2 = localHashSet.iterator();
    int i1;
    for (int m = 0; localIterator2.hasNext(); m = i1)
    {
      Integer localInteger = (Integer)localIterator2.next();
      i1 = m + 1;
      arrayOfInt[m] = localInteger.intValue();
    }
    Debug.MemoryInfo[] arrayOfMemoryInfo = this.l.getProcessMemoryInfo(arrayOfInt);
    int n = arrayOfMemoryInfo.length;
    while (i < n)
    {
      Debug.MemoryInfo localMemoryInfo = arrayOfMemoryInfo[i];
      l1 += 1024 * (localMemoryInfo.dalvikPss + localMemoryInfo.nativePss + localMemoryInfo.otherPss);
      i++;
    }
    this.c = (l1 / 1024L / 1024L);
    this.d = (this.j.b / 1024L / 1024L);
    this.b = (this.d - this.c);
    this.a = ((float)(100.0D * (this.b / this.d)));
  }
  
  protected final void b() {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/c/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */