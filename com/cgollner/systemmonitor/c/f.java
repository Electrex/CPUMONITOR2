package com.cgollner.systemmonitor.c;

import android.net.TrafficStats;

public final class f
  extends e
{
  public long a;
  public long b;
  public long c;
  public long d;
  public long e;
  public long j;
  public long k;
  public long l;
  public long m;
  private a n = new a();
  
  public f(long paramLong, e.a parama)
  {
    super(paramLong, parama);
    start();
  }
  
  protected final void a()
  {
    this.n.b = TrafficStats.getMobileRxBytes();
    this.n.a = TrafficStats.getTotalRxBytes();
    this.n.d = TrafficStats.getMobileTxBytes();
    this.n.c = TrafficStats.getTotalTxBytes();
  }
  
  protected final void b()
  {
    this.l = (TrafficStats.getTotalRxBytes() - this.n.a);
    this.m = (TrafficStats.getTotalTxBytes() - this.n.c);
    this.k = (this.l + this.m);
    this.d = (TrafficStats.getMobileRxBytes() - this.n.b);
    this.e = (TrafficStats.getMobileTxBytes() - this.n.d);
    this.j = (this.d + this.e);
    this.a = (this.l - this.d);
    this.b = (this.m - this.e);
    this.c = (this.a + this.b);
  }
  
  final class a
  {
    long a;
    long b;
    long c;
    long d;
    
    a() {}
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/c/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */