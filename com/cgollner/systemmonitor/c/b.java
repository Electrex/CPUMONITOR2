package com.cgollner.systemmonitor.c;

import android.content.Context;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class b
  extends e
{
  public int a;
  public int b;
  public int c;
  public int d;
  public Context e;
  private List<Integer> j;
  
  public b(long paramLong, e.a parama, Context paramContext)
  {
    super(paramLong, parama);
    this.e = paramContext.getApplicationContext();
    this.j = new LinkedList();
    start();
  }
  
  protected final void a()
  {
    this.a = (10 * com.cgollner.systemmonitor.a.b.e());
    this.j.add(Integer.valueOf(this.a));
    this.c = ((Integer)Collections.min(this.j)).intValue();
    this.b = ((Integer)Collections.max(this.j)).intValue();
    List localList = this.j;
    Iterator localIterator = localList.iterator();
    int i = 0;
    while (localIterator.hasNext()) {
      i += ((Integer)localIterator.next()).intValue();
    }
    this.d = (i / localList.size());
  }
  
  protected final void b() {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/cgollner/systemmonitor/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */