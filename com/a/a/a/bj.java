package com.a.a.a;

import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

final class bj
  implements ThreadFactory
{
  bj(String paramString, AtomicLong paramAtomicLong) {}
  
  public final Thread newThread(Runnable paramRunnable)
  {
    Thread localThread = Executors.defaultThreadFactory().newThread(new bk(paramRunnable));
    Locale localLocale = Locale.US;
    String str = this.a;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Long.valueOf(this.b.getAndIncrement());
    localThread.setName(String.format(localLocale, str, arrayOfObject));
    return localThread;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/bj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */