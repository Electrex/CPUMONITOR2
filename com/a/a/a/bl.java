package com.a.a.a;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

final class bl
  extends az
{
  bl(String paramString, ExecutorService paramExecutorService, TimeUnit paramTimeUnit) {}
  
  public final void a()
  {
    try
    {
      ci localci2 = co.a().a();
      new StringBuilder("Executing shutdown hook for ").append(this.a);
      localci2.b();
      this.b.shutdown();
      if (!this.b.awaitTermination(this.c, this.d))
      {
        ci localci3 = co.a().a();
        new StringBuilder().append(this.a).append(" did not shut down in the allocated time. Requesting immediate shutdown.");
        localci3.b();
        this.b.shutdownNow();
      }
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      ci localci1 = co.a().a();
      Locale localLocale = Locale.US;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = this.a;
      String.format(localLocale, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", arrayOfObject);
      localci1.b();
      this.b.shutdownNow();
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/bl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */