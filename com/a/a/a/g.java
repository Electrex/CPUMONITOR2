package com.a.a.a;

import java.io.File;
import java.util.Iterator;
import java.util.List;

final class g
  extends y
  implements m
{
  public g(String paramString1, String paramString2, bt parambt)
  {
    super(paramString1, paramString2, parambt, bv.b);
  }
  
  public final boolean a(String paramString, List<File> paramList)
  {
    bw localbw1 = a().a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    co.a();
    bw localbw2 = localbw1.a("X-CRASHLYTICS-API-CLIENT-VERSION", "1.1.13.29").a("X-CRASHLYTICS-API-KEY", paramString);
    Iterator localIterator = paramList.iterator();
    for (int i = 0; localIterator.hasNext(); i++)
    {
      File localFile = (File)localIterator.next();
      new StringBuilder("Adding analytics session file ").append(localFile.getName()).append(" to multipart POST");
      ba.d();
      localbw2.a("session_analytics_file_" + i, localFile.getName(), "application/vnd.crashlytics.android.events", localFile);
    }
    new StringBuilder("Sending ").append(paramList.size()).append(" analytics files to ").append(this.a);
    ba.d();
    int j = localbw2.b();
    ba.d();
    int k = cj.a(j);
    boolean bool = false;
    if (k == 0) {
      bool = true;
    }
    return bool;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */