package com.a.a.a;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

public class bo
  implements Closeable
{
  private static final Logger a = Logger.getLogger(bo.class.getName());
  private final RandomAccessFile b;
  private int c;
  private int d;
  private bq e;
  private bq f;
  private final byte[] g = new byte[16];
  
  public bo(File paramFile)
  {
    if (!paramFile.exists())
    {
      File localFile = new File(paramFile.getPath() + ".tmp");
      RandomAccessFile localRandomAccessFile = a(localFile);
      try
      {
        localRandomAccessFile.setLength(4096L);
        localRandomAccessFile.seek(0L);
        byte[] arrayOfByte = new byte[16];
        a(arrayOfByte, new int[] { 4096, 0, 0, 0 });
        localRandomAccessFile.write(arrayOfByte);
        localRandomAccessFile.close();
        if (!localFile.renameTo(paramFile)) {
          throw new IOException("Rename failed!");
        }
      }
      finally
      {
        localRandomAccessFile.close();
      }
    }
    this.b = a(paramFile);
    this.b.seek(0L);
    this.b.readFully(this.g);
    this.c = b(this.g, 0);
    if (this.c > this.b.length()) {
      throw new IOException("File is truncated. Expected length: " + this.c + ", Actual length: " + this.b.length());
    }
    this.d = b(this.g, 4);
    int i = b(this.g, 8);
    int j = b(this.g, 12);
    this.e = a(i);
    this.f = a(j);
  }
  
  private bq a(int paramInt)
  {
    if (paramInt == 0) {
      return bq.a;
    }
    this.b.seek(paramInt);
    return new bq(paramInt, this.b.readInt());
  }
  
  private static RandomAccessFile a(File paramFile)
  {
    return new RandomAccessFile(paramFile, "rwd");
  }
  
  private void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a(this.g, new int[] { paramInt1, paramInt2, paramInt3, paramInt4 });
    this.b.seek(0L);
    this.b.write(this.g);
  }
  
  private void a(int paramInt1, byte[] paramArrayOfByte, int paramInt2)
  {
    int i = b(paramInt1);
    if (i + paramInt2 <= this.c)
    {
      this.b.seek(i);
      this.b.write(paramArrayOfByte, 0, paramInt2);
      return;
    }
    int j = this.c - i;
    this.b.seek(i);
    this.b.write(paramArrayOfByte, 0, j);
    this.b.seek(16L);
    this.b.write(paramArrayOfByte, j + 0, paramInt2 - j);
  }
  
  private static void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    paramArrayOfByte[paramInt1] = ((byte)(paramInt2 >> 24));
    paramArrayOfByte[(paramInt1 + 1)] = ((byte)(paramInt2 >> 16));
    paramArrayOfByte[(paramInt1 + 2)] = ((byte)(paramInt2 >> 8));
    paramArrayOfByte[(paramInt1 + 3)] = ((byte)paramInt2);
  }
  
  private static void a(byte[] paramArrayOfByte, int... paramVarArgs)
  {
    int i = 0;
    int j = 0;
    while (i < 4)
    {
      a(paramArrayOfByte, j, paramVarArgs[i]);
      j += 4;
      i++;
    }
  }
  
  private int b(int paramInt)
  {
    if (paramInt < this.c) {
      return paramInt;
    }
    return paramInt + 16 - this.c;
  }
  
  private static int b(byte[] paramArrayOfByte, int paramInt)
  {
    return ((0xFF & paramArrayOfByte[paramInt]) << 24) + ((0xFF & paramArrayOfByte[(paramInt + 1)]) << 16) + ((0xFF & paramArrayOfByte[(paramInt + 2)]) << 8) + (0xFF & paramArrayOfByte[(paramInt + 3)]);
  }
  
  private static <T> T b(T paramT, String paramString)
  {
    if (paramT == null) {
      throw new NullPointerException(paramString);
    }
    return paramT;
  }
  
  private void c(int paramInt)
  {
    int i = paramInt + 4;
    int j = this.c - a();
    if (j >= i) {
      return;
    }
    int k = this.c;
    do
    {
      j += k;
      k <<= 1;
    } while (j < i);
    this.b.setLength(k);
    this.b.getChannel().force(true);
    int m = b(4 + this.f.b + this.f.c);
    if (m < this.e.b)
    {
      FileChannel localFileChannel = this.b.getChannel();
      localFileChannel.position(this.c);
      int i1 = m - 4;
      if (localFileChannel.transferTo(16L, i1, localFileChannel) != i1) {
        throw new AssertionError("Copied insufficient number of bytes!");
      }
    }
    if (this.f.b < this.e.b)
    {
      int n = -16 + (this.c + this.f.b);
      a(k, this.d, this.e.b, n);
      this.f = new bq(n, this.f.c);
    }
    for (;;)
    {
      this.c = k;
      return;
      a(k, this.d, this.e.b, this.f.b);
    }
  }
  
  public final int a()
  {
    if (this.d == 0) {
      return 16;
    }
    if (this.f.b >= this.e.b) {
      return 16 + (4 + (this.f.b - this.e.b) + this.f.c);
    }
    return 4 + this.f.b + this.f.c + this.c - this.e.b;
  }
  
  public final void a(bs parambs)
  {
    int i = 0;
    try
    {
      int j = this.e.b;
      while (i < this.d)
      {
        bq localbq = a(j);
        parambs.a(new br(this, localbq, (byte)0), localbq.c);
        int k = b(4 + localbq.b + localbq.c);
        j = k;
        i++;
      }
      return;
    }
    finally {}
  }
  
  final void a(byte[] paramArrayOfByte, int paramInt)
  {
    try
    {
      b(paramArrayOfByte, "buffer");
      if (((paramInt | 0x0) < 0) || (paramInt > paramArrayOfByte.length)) {
        throw new IndexOutOfBoundsException();
      }
    }
    finally {}
    c(paramInt);
    boolean bool = b();
    int i;
    bq localbq;
    if (bool)
    {
      i = 16;
      localbq = new bq(i, paramInt);
      a(this.g, 0, paramInt);
      a(localbq.b, this.g, 4);
      a(4 + localbq.b, paramArrayOfByte, paramInt);
      if (!bool) {
        break label193;
      }
    }
    label193:
    for (int j = localbq.b;; j = this.e.b)
    {
      a(this.c, 1 + this.d, j, localbq.b);
      this.f = localbq;
      this.d = (1 + this.d);
      if (bool) {
        this.e = this.f;
      }
      return;
      i = b(4 + this.f.b + this.f.c);
      break;
    }
  }
  
  /* Error */
  public final boolean b()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 124	com/a/a/a/bo:d	I
    //   6: istore_2
    //   7: iload_2
    //   8: ifne +9 -> 17
    //   11: iconst_1
    //   12: istore_3
    //   13: aload_0
    //   14: monitorexit
    //   15: iload_3
    //   16: ireturn
    //   17: iconst_0
    //   18: istore_3
    //   19: goto -6 -> 13
    //   22: astore_1
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_1
    //   26: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	27	0	this	bo
    //   22	4	1	localObject	Object
    //   6	2	2	i	int
    //   12	7	3	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   2	7	22	finally
  }
  
  public void close()
  {
    try
    {
      this.b.close();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(getClass().getSimpleName()).append('[');
    localStringBuilder.append("fileLength=").append(this.c);
    localStringBuilder.append(", size=").append(this.d);
    localStringBuilder.append(", first=").append(this.e);
    localStringBuilder.append(", last=").append(this.f);
    localStringBuilder.append(", element lengths=[");
    try
    {
      a(new bp(localStringBuilder));
      localStringBuilder.append("]]");
      return localStringBuilder.toString();
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        a.log(Level.WARNING, "read error", localIOException);
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/bo.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */