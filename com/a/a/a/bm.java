package com.a.a.a;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class bm
{
  private static final Pattern c = Pattern.compile("[^\\p{Alnum}]");
  private static final String d = Pattern.quote("/");
  public final boolean a;
  public final boolean b;
  private final ReentrantLock e = new ReentrantLock();
  private final Context f;
  
  public bm(Context paramContext)
  {
    if (paramContext == null) {
      throw new IllegalArgumentException("appContext must not be null");
    }
    this.f = paramContext;
    this.a = ba.a(paramContext, "com.crashlytics.CollectDeviceIdentifiers", true);
    if (!this.a)
    {
      ci localci2 = co.a().a();
      new StringBuilder("Device ID collection disabled for ").append(paramContext.getPackageName());
      localci2.b();
    }
    this.b = ba.a(paramContext, "com.crashlytics.CollectUserIdentifiers", true);
    if (!this.b)
    {
      ci localci1 = co.a().a();
      new StringBuilder("User information collection disabled for ").append(paramContext.getPackageName());
      localci1.b();
    }
  }
  
  private static void a(Map<bn, String> paramMap, bn parambn, String paramString)
  {
    if (paramString != null) {
      paramMap.put(parambn, paramString);
    }
  }
  
  private boolean a(String paramString)
  {
    return this.f.checkCallingPermission(paramString) == 0;
  }
  
  public static String b()
  {
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = c(Build.VERSION.RELEASE);
    arrayOfObject[1] = c(Build.VERSION.INCREMENTAL);
    return String.format(localLocale, "%s/%s", arrayOfObject);
  }
  
  private static String b(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return c.matcher(paramString).replaceAll("").toLowerCase(Locale.US);
  }
  
  public static String c()
  {
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = c(Build.MANUFACTURER);
    arrayOfObject[1] = c(Build.MODEL);
    return String.format(localLocale, "%s/%s", arrayOfObject);
  }
  
  private static String c(String paramString)
  {
    return paramString.replaceAll(d, "");
  }
  
  private String g()
  {
    if ((this.a) && (Build.VERSION.SDK_INT >= 9)) {
      try
      {
        String str = b((String)Build.class.getField("SERIAL").get(null));
        return str;
      }
      catch (Exception localException)
      {
        co.a().a().a();
      }
    }
    return null;
  }
  
  public final String a()
  {
    String str = co.a().f;
    if (str == null)
    {
      SharedPreferences localSharedPreferences = ba.a();
      str = localSharedPreferences.getString("crashlytics.installation.id", null);
      if (str == null) {
        str = a(localSharedPreferences);
      }
    }
    return str;
  }
  
  public final String a(SharedPreferences paramSharedPreferences)
  {
    this.e.lock();
    try
    {
      String str = paramSharedPreferences.getString("crashlytics.installation.id", null);
      if (str == null)
      {
        str = b(UUID.randomUUID().toString());
        paramSharedPreferences.edit().putString("crashlytics.installation.id", str).commit();
      }
      return str;
    }
    finally
    {
      this.e.unlock();
    }
  }
  
  public final Map<bn, String> d()
  {
    HashMap localHashMap = new HashMap();
    a(localHashMap, bn.c, e());
    bn localbn1 = bn.d;
    TelephonyManager localTelephonyManager;
    if ((this.a) && (a("android.permission.READ_PHONE_STATE")))
    {
      localTelephonyManager = (TelephonyManager)this.f.getSystemService("phone");
      if (localTelephonyManager == null) {}
    }
    for (String str1 = b(localTelephonyManager.getDeviceId());; str1 = null)
    {
      a(localHashMap, localbn1, str1);
      a(localHashMap, bn.e, g());
      bn localbn2 = bn.a;
      boolean bool1 = this.a;
      String str2 = null;
      if (bool1)
      {
        boolean bool2 = a("android.permission.ACCESS_WIFI_STATE");
        str2 = null;
        if (bool2)
        {
          WifiManager localWifiManager = (WifiManager)this.f.getSystemService("wifi");
          str2 = null;
          if (localWifiManager != null)
          {
            WifiInfo localWifiInfo = localWifiManager.getConnectionInfo();
            str2 = null;
            if (localWifiInfo != null) {
              str2 = b(localWifiInfo.getMacAddress());
            }
          }
        }
      }
      a(localHashMap, localbn2, str2);
      a(localHashMap, bn.b, f());
      return Collections.unmodifiableMap(localHashMap);
    }
  }
  
  public final String e()
  {
    boolean bool1 = this.a;
    String str1 = null;
    if (bool1)
    {
      String str2 = Settings.Secure.getString(this.f.getContentResolver(), "android_id");
      boolean bool2 = "9774d56d682e549c".equals(str2);
      str1 = null;
      if (!bool2) {
        str1 = b(str2);
      }
    }
    return str1;
  }
  
  public final String f()
  {
    if ((this.a) && (a("android.permission.BLUETOOTH"))) {}
    try
    {
      BluetoothAdapter localBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      if (localBluetoothAdapter != null) {
        b(localBluetoothAdapter.getAddress());
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        co.a().a().a();
      }
    }
    return null;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/bm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */