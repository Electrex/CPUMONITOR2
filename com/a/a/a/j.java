package com.a.a.a;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

final class j
{
  aj a;
  final bh b;
  List<bi> c = new CopyOnWriteArrayList();
  private final w d;
  private final bf e;
  private final int f;
  
  private j(w paramw, bf parambf, bh parambh)
  {
    this.d = paramw;
    this.b = parambh;
    this.e = parambf;
    System.currentTimeMillis();
    this.f = 100;
  }
  
  j(w paramw, bf parambf, bh parambh, byte paramByte)
  {
    this(paramw, parambf, parambh);
  }
  
  private static long a(String paramString)
  {
    String[] arrayOfString = paramString.split("_");
    if (arrayOfString.length != 3) {
      return 0L;
    }
    try
    {
      long l = Long.valueOf(arrayOfString[2]).longValue();
      return l;
    }
    catch (NumberFormatException localNumberFormatException) {}
    return 0L;
  }
  
  private void e()
  {
    Iterator localIterator = this.c.iterator();
    while (localIterator.hasNext())
    {
      bi localbi = (bi)localIterator.next();
      try
      {
        localbi.b();
      }
      catch (Exception localException)
      {
        co.a().a().a();
      }
    }
  }
  
  /* Error */
  final boolean a()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: iconst_1
    //   3: istore_2
    //   4: aload_0
    //   5: getfield 31	com/a/a/a/j:b	Lcom/a/a/a/bh;
    //   8: getfield 105	com/a/a/a/bh:a	Lcom/a/a/a/bo;
    //   11: invokevirtual 109	com/a/a/a/bo:b	()Z
    //   14: ifne +270 -> 284
    //   17: invokestatic 115	java/util/UUID:randomUUID	()Ljava/util/UUID;
    //   20: astore_3
    //   21: new 117	java/lang/StringBuilder
    //   24: dup
    //   25: invokespecial 118	java/lang/StringBuilder:<init>	()V
    //   28: astore 4
    //   30: aload 4
    //   32: ldc 120
    //   34: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   37: pop
    //   38: aload 4
    //   40: ldc 49
    //   42: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload 4
    //   48: aload_3
    //   49: invokevirtual 128	java/util/UUID:toString	()Ljava/lang/String;
    //   52: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload 4
    //   58: ldc 49
    //   60: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   63: pop
    //   64: aload 4
    //   66: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   69: invokevirtual 131	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload 4
    //   75: ldc -123
    //   77: invokevirtual 124	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   80: pop
    //   81: aload 4
    //   83: invokevirtual 134	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   86: astore 11
    //   88: aload_0
    //   89: getfield 31	com/a/a/a/j:b	Lcom/a/a/a/bh;
    //   92: astore 12
    //   94: aload 12
    //   96: getfield 105	com/a/a/a/bh:a	Lcom/a/a/a/bo;
    //   99: invokevirtual 137	com/a/a/a/bo:close	()V
    //   102: aload 12
    //   104: getfield 140	com/a/a/a/bh:b	Ljava/io/File;
    //   107: astore 13
    //   109: new 142	java/io/File
    //   112: dup
    //   113: aload 12
    //   115: getfield 144	com/a/a/a/bh:c	Ljava/io/File;
    //   118: aload 11
    //   120: invokespecial 147	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   123: astore 14
    //   125: new 149	java/io/FileInputStream
    //   128: dup
    //   129: aload 13
    //   131: invokespecial 152	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   134: astore 15
    //   136: new 154	java/util/zip/GZIPOutputStream
    //   139: dup
    //   140: new 156	java/io/FileOutputStream
    //   143: dup
    //   144: aload 14
    //   146: invokespecial 157	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   149: invokespecial 160	java/util/zip/GZIPOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   152: astore 16
    //   154: aload 15
    //   156: aload 16
    //   158: sipush 1024
    //   161: newarray <illegal type>
    //   163: invokestatic 165	com/a/a/a/ba:a	(Ljava/io/InputStream;Ljava/io/OutputStream;[B)V
    //   166: aload 15
    //   168: invokestatic 168	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   171: aload 16
    //   173: invokestatic 168	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   176: aload 13
    //   178: invokevirtual 171	java/io/File:delete	()Z
    //   181: pop
    //   182: aload 12
    //   184: new 107	com/a/a/a/bo
    //   187: dup
    //   188: aload 12
    //   190: getfield 140	com/a/a/a/bh:b	Ljava/io/File;
    //   193: invokespecial 172	com/a/a/a/bo:<init>	(Ljava/io/File;)V
    //   196: putfield 105	com/a/a/a/bh:a	Lcom/a/a/a/bo;
    //   199: getstatic 178	java/util/Locale:US	Ljava/util/Locale;
    //   202: astore 21
    //   204: iload_2
    //   205: anewarray 4	java/lang/Object
    //   208: astore 22
    //   210: aload 22
    //   212: iconst_0
    //   213: aload 11
    //   215: aastore
    //   216: aload 21
    //   218: ldc -76
    //   220: aload 22
    //   222: invokestatic 184	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   225: invokestatic 187	com/a/a/a/ba:c	(Ljava/lang/String;)V
    //   228: invokestatic 39	java/lang/System:currentTimeMillis	()J
    //   231: pop2
    //   232: aload_0
    //   233: invokespecial 189	com/a/a/a/j:e	()V
    //   236: iload_2
    //   237: ireturn
    //   238: astore 17
    //   240: aconst_null
    //   241: astore 18
    //   243: aload 18
    //   245: invokestatic 168	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   248: aload_1
    //   249: invokestatic 168	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   252: aload 13
    //   254: invokevirtual 171	java/io/File:delete	()Z
    //   257: pop
    //   258: aload 17
    //   260: athrow
    //   261: astore 17
    //   263: aload 15
    //   265: astore 18
    //   267: aconst_null
    //   268: astore_1
    //   269: goto -26 -> 243
    //   272: astore 17
    //   274: aload 15
    //   276: astore 18
    //   278: aload 16
    //   280: astore_1
    //   281: goto -38 -> 243
    //   284: iconst_0
    //   285: istore_2
    //   286: goto -54 -> 232
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	289	0	this	j
    //   1	280	1	localObject1	Object
    //   3	283	2	bool	boolean
    //   20	29	3	localUUID	java.util.UUID
    //   28	54	4	localStringBuilder	StringBuilder
    //   86	128	11	str	String
    //   92	97	12	localbh	bh
    //   107	146	13	localFile1	File
    //   123	22	14	localFile2	File
    //   134	141	15	localFileInputStream	java.io.FileInputStream
    //   152	127	16	localGZIPOutputStream	java.util.zip.GZIPOutputStream
    //   238	21	17	localObject2	Object
    //   261	1	17	localObject3	Object
    //   272	1	17	localObject4	Object
    //   241	36	18	localObject5	Object
    //   202	15	21	localLocale	Locale
    //   208	13	22	arrayOfObject	Object[]
    // Exception table:
    //   from	to	target	type
    //   125	136	238	finally
    //   136	154	261	finally
    //   154	166	272	finally
  }
  
  final int b()
  {
    if (this.a == null) {
      return 8000;
    }
    return this.a.c;
  }
  
  final List<File> c()
  {
    bh localbh = this.b;
    ArrayList localArrayList = new ArrayList();
    File[] arrayOfFile = localbh.c.listFiles();
    int i = arrayOfFile.length;
    for (int j = 0; j < i; j++)
    {
      localArrayList.add(arrayOfFile[j]);
      if (localArrayList.size() > 0) {
        break;
      }
    }
    return localArrayList;
  }
  
  final void d()
  {
    List localList = this.b.a();
    if (localList.size() <= this.f) {
      return;
    }
    int i = localList.size() - this.f;
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = Integer.valueOf(localList.size());
    arrayOfObject[1] = Integer.valueOf(this.f);
    arrayOfObject[2] = Integer.valueOf(i);
    String.format(localLocale, "Found %d files in session analytics roll over directory, this is greater than %d, deleting %d oldest files", arrayOfObject);
    ba.d();
    TreeSet localTreeSet = new TreeSet(new k());
    Iterator localIterator1 = localList.iterator();
    while (localIterator1.hasNext())
    {
      File localFile = (File)localIterator1.next();
      localTreeSet.add(new l(localFile, a(localFile.getName())));
    }
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator2 = localTreeSet.iterator();
    do
    {
      if (!localIterator2.hasNext()) {
        break;
      }
      localArrayList.add(((l)localIterator2.next()).a);
    } while (localArrayList.size() != i);
    bh.a(localArrayList);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */