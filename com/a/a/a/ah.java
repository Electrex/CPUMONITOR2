package com.a.a.a;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;
import javax.security.auth.x500.X500Principal;

final class ah
{
  final KeyStore a;
  private final HashMap<Principal, X509Certificate> b;
  
  public ah(InputStream paramInputStream, String paramString)
  {
    KeyStore localKeyStore = a(paramInputStream, paramString);
    this.b = a(localKeyStore);
    this.a = localKeyStore;
  }
  
  /* Error */
  private static KeyStore a(InputStream paramInputStream, String paramString)
  {
    // Byte code:
    //   0: ldc 34
    //   2: invokestatic 40	java/security/KeyStore:getInstance	(Ljava/lang/String;)Ljava/security/KeyStore;
    //   5: astore 6
    //   7: new 42	java/io/BufferedInputStream
    //   10: dup
    //   11: aload_0
    //   12: invokespecial 45	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   15: astore 7
    //   17: aload 6
    //   19: aload 7
    //   21: aload_1
    //   22: invokevirtual 51	java/lang/String:toCharArray	()[C
    //   25: invokevirtual 55	java/security/KeyStore:load	(Ljava/io/InputStream;[C)V
    //   28: aload 7
    //   30: invokevirtual 58	java/io/BufferedInputStream:close	()V
    //   33: aload 6
    //   35: areturn
    //   36: astore 8
    //   38: aload 7
    //   40: invokevirtual 58	java/io/BufferedInputStream:close	()V
    //   43: aload 8
    //   45: athrow
    //   46: astore 5
    //   48: new 60	java/lang/AssertionError
    //   51: dup
    //   52: aload 5
    //   54: invokespecial 63	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   57: athrow
    //   58: astore 4
    //   60: new 60	java/lang/AssertionError
    //   63: dup
    //   64: aload 4
    //   66: invokespecial 63	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   69: athrow
    //   70: astore_3
    //   71: new 60	java/lang/AssertionError
    //   74: dup
    //   75: aload_3
    //   76: invokespecial 63	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   79: athrow
    //   80: astore_2
    //   81: new 60	java/lang/AssertionError
    //   84: dup
    //   85: aload_2
    //   86: invokespecial 63	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   89: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	90	0	paramInputStream	InputStream
    //   0	90	1	paramString	String
    //   80	6	2	localIOException	java.io.IOException
    //   70	6	3	localCertificateException	java.security.cert.CertificateException
    //   58	7	4	localNoSuchAlgorithmException	java.security.NoSuchAlgorithmException
    //   46	7	5	localKeyStoreException	KeyStoreException
    //   5	29	6	localKeyStore	KeyStore
    //   15	24	7	localBufferedInputStream	java.io.BufferedInputStream
    //   36	8	8	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   17	28	36	finally
    //   0	17	46	java/security/KeyStoreException
    //   28	33	46	java/security/KeyStoreException
    //   38	46	46	java/security/KeyStoreException
    //   0	17	58	java/security/NoSuchAlgorithmException
    //   28	33	58	java/security/NoSuchAlgorithmException
    //   38	46	58	java/security/NoSuchAlgorithmException
    //   0	17	70	java/security/cert/CertificateException
    //   28	33	70	java/security/cert/CertificateException
    //   38	46	70	java/security/cert/CertificateException
    //   0	17	80	java/io/IOException
    //   28	33	80	java/io/IOException
    //   38	46	80	java/io/IOException
  }
  
  private static HashMap<Principal, X509Certificate> a(KeyStore paramKeyStore)
  {
    try
    {
      HashMap localHashMap = new HashMap();
      Enumeration localEnumeration = paramKeyStore.aliases();
      while (localEnumeration.hasMoreElements())
      {
        X509Certificate localX509Certificate = (X509Certificate)paramKeyStore.getCertificate((String)localEnumeration.nextElement());
        if (localX509Certificate != null) {
          localHashMap.put(localX509Certificate.getSubjectX500Principal(), localX509Certificate);
        }
      }
      return localHashMap;
    }
    catch (KeyStoreException localKeyStoreException)
    {
      throw new AssertionError(localKeyStoreException);
    }
  }
  
  public final boolean a(X509Certificate paramX509Certificate)
  {
    X509Certificate localX509Certificate = (X509Certificate)this.b.get(paramX509Certificate.getSubjectX500Principal());
    return (localX509Certificate != null) && (localX509Certificate.getPublicKey().equals(paramX509Certificate.getPublicKey()));
  }
  
  public final X509Certificate b(X509Certificate paramX509Certificate)
  {
    X509Certificate localX509Certificate = (X509Certificate)this.b.get(paramX509Certificate.getIssuerX500Principal());
    if (localX509Certificate == null) {
      return null;
    }
    if (localX509Certificate.getSubjectX500Principal().equals(paramX509Certificate.getSubjectX500Principal())) {
      return null;
    }
    try
    {
      paramX509Certificate.verify(localX509Certificate.getPublicKey());
      return localX509Certificate;
    }
    catch (GeneralSecurityException localGeneralSecurityException) {}
    return null;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/ah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */