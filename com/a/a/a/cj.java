package com.a.a.a;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;

public final class cj
  implements ci
{
  public static int a(int paramInt)
  {
    if ((paramInt >= 200) && (paramInt <= 299)) {}
    do
    {
      return 0;
      if ((paramInt >= 300) && (paramInt <= 399)) {
        return 1;
      }
    } while ((paramInt >= 400) && (paramInt <= 499));
    if (paramInt >= 500) {
      return 1;
    }
    return 1;
  }
  
  public static String a(Context paramContext, boolean paramBoolean)
  {
    Object localObject;
    try
    {
      Context localContext = paramContext.getApplicationContext();
      String str1 = localContext.getPackageName();
      Bundle localBundle = localContext.getPackageManager().getApplicationInfo(str1, 128).metaData;
      localObject = null;
      if (localBundle != null)
      {
        String str2 = localBundle.getString("com.crashlytics.ApiKey");
        localObject = str2;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        int i;
        ci localci = co.a().a();
        new StringBuilder("Caught non-fatal exception while retrieving apiKey: ").append(localException);
        localci.b();
        localObject = null;
      }
      co.a().a().a();
    }
    if (ba.e((String)localObject))
    {
      i = ba.a(paramContext, "com.crashlytics.ApiKey", "string");
      if (i != 0) {
        localObject = paramContext.getResources().getString(i);
      }
    }
    if (ba.e((String)localObject)) {
      if ((paramBoolean) || (ba.f(paramContext))) {
        throw new IllegalArgumentException("Crashlytics could not be initialized, API key missing from AndroidManifest.xml. Add the following tag to your Application element \n\t<meta-data android:name=\"com.crashlytics.ApiKey\" android:value=\"YOUR_API_KEY\"/>");
      }
    }
    return (String)localObject;
  }
  
  private static boolean b(int paramInt)
  {
    return co.a().h <= paramInt;
  }
  
  public final void a()
  {
    b(6);
  }
  
  public final void a(String paramString1, String paramString2)
  {
    if (b(4)) {
      Log.println(4, paramString1, paramString2);
    }
  }
  
  public final void b()
  {
    b(3);
  }
  
  public final void c()
  {
    b(4);
  }
  
  public final void d()
  {
    b(5);
  }
  
  public final void e()
  {
    b(6);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/cj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */