package com.a.a.a;

import java.io.InputStream;
import java.io.RandomAccessFile;

final class br
  extends InputStream
{
  private int a;
  private int b;
  
  private br(bo parambo, bq parambq)
  {
    this.a = bo.a(parambo, 4 + parambq.b);
    this.b = parambq.c;
  }
  
  public final int read()
  {
    if (this.b == 0) {
      return -1;
    }
    bo.a(this.c).seek(this.a);
    int i = bo.a(this.c).read();
    this.a = bo.a(this.c, 1 + this.a);
    this.b = (-1 + this.b);
    return i;
  }
  
  public final int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    bo.a(paramArrayOfByte, "buffer");
    if (((paramInt1 | paramInt2) < 0) || (paramInt2 > paramArrayOfByte.length - paramInt1)) {
      throw new ArrayIndexOutOfBoundsException();
    }
    if (this.b > 0)
    {
      if (paramInt2 > this.b) {
        paramInt2 = this.b;
      }
      bo.a(this.c, this.a, paramArrayOfByte, paramInt1, paramInt2);
      this.a = bo.a(this.c, paramInt2 + this.a);
      this.b -= paramInt2;
      return paramInt2;
    }
    return -1;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/br.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */