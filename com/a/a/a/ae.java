package com.a.a.a;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

public final class ae
  extends BufferedOutputStream
{
  private final CharsetEncoder a;
  
  public ae(OutputStream paramOutputStream, String paramString, int paramInt)
  {
    super(paramOutputStream, paramInt);
    this.a = Charset.forName(bw.c(paramString)).newEncoder();
  }
  
  public final ae a(String paramString)
  {
    ByteBuffer localByteBuffer = this.a.encode(CharBuffer.wrap(paramString));
    super.write(localByteBuffer.array(), 0, localByteBuffer.limit());
    return this;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/ae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */