package com.a.a.a;

import java.util.Map;

final class u
{
  public final String a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final String f;
  public final String g;
  public final String h;
  public final long i;
  public final v j;
  public final Map<String, String> k;
  private String l;
  
  private u(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, long paramLong, v paramv, Map<String, String> paramMap)
  {
    this.a = paramString1;
    this.b = paramString2;
    this.c = paramString3;
    this.d = paramString4;
    this.e = paramString5;
    this.f = paramString6;
    this.g = paramString7;
    this.h = paramString8;
    this.i = paramLong;
    this.j = paramv;
    this.k = paramMap;
  }
  
  public static final u a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, v paramv, Map<String, String> paramMap)
  {
    return new u(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, System.currentTimeMillis(), paramv, paramMap);
  }
  
  public final String toString()
  {
    if (this.l == null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append(getClass().getSimpleName());
      localStringBuilder.append(": appBundleId=");
      localStringBuilder.append(this.a);
      localStringBuilder.append(", executionId=");
      localStringBuilder.append(this.b);
      localStringBuilder.append(", installationId=");
      localStringBuilder.append(this.c);
      localStringBuilder.append(", androidId=");
      localStringBuilder.append(this.d);
      localStringBuilder.append(", osVersion=");
      localStringBuilder.append(this.e);
      localStringBuilder.append(", deviceModel=");
      localStringBuilder.append(this.f);
      localStringBuilder.append(", appVersionCode=");
      localStringBuilder.append(this.g);
      localStringBuilder.append(", appVersionName=");
      localStringBuilder.append(this.h);
      localStringBuilder.append(", timestamp=");
      localStringBuilder.append(this.i);
      localStringBuilder.append(", type=");
      localStringBuilder.append(this.j);
      localStringBuilder.append(", details=");
      localStringBuilder.append(this.k.toString());
      localStringBuilder.append("]");
      this.l = localStringBuilder.toString();
    }
    return this.l;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/u.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */