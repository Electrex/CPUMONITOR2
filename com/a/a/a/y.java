package com.a.a.a;

import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public abstract class y
{
  private static String b = "1.1.13.29";
  private static final Pattern c = Pattern.compile("http(s?)://[^\\/]+", 2);
  public final String a;
  private final bt d;
  private final int e;
  private final String f;
  
  static
  {
    StringBuilder localStringBuilder = new StringBuilder("Crashlytics Android SDK/");
    co.a();
  }
  
  public y(String paramString1, String paramString2, bt parambt, int paramInt)
  {
    if (paramString2 == null) {
      throw new IllegalArgumentException("url must not be null.");
    }
    if (parambt == null) {
      throw new IllegalArgumentException("requestFactory must not be null.");
    }
    this.f = paramString1;
    if (!ba.e(this.f)) {
      paramString2 = c.matcher(paramString2).replaceFirst(this.f);
    }
    this.a = paramString2;
    this.d = parambt;
    this.e = paramInt;
  }
  
  public final bw a()
  {
    return a(Collections.emptyMap());
  }
  
  protected final bw a(Map<String, String> paramMap)
  {
    bt localbt = this.d;
    int i = this.e;
    String str = this.a;
    bw localbw;
    switch (bu.a[(i - 1)])
    {
    default: 
      throw new IllegalArgumentException("Unsupported HTTP method!");
    case 1: 
      localbw = bw.a(str, paramMap);
      if (str != null) {
        break;
      }
    }
    for (boolean bool = false;; bool = str.toLowerCase().startsWith("https"))
    {
      if ((bool) && (localbt.a != null))
      {
        SSLSocketFactory localSSLSocketFactory = localbt.a();
        if (localSSLSocketFactory != null) {
          ((HttpsURLConnection)localbw.a()).setSSLSocketFactory(localSSLSocketFactory);
        }
      }
      localbw.a().setUseCaches(false);
      localbw.a().setConnectTimeout(10000);
      return localbw.a("User-Agent", b).a("X-CRASHLYTICS-DEVELOPER-TOKEN", "bca6990fc3c15a8105800c0673517a4b579634a1");
      localbw = bw.b(str, paramMap);
      break;
      localbw = bw.a(str);
      break;
      localbw = bw.b(str);
      break;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/y.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */