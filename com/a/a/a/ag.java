package com.a.a.a;

import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

final class ag
  implements X509TrustManager
{
  private final TrustManager[] a;
  private final ah b;
  private final long c;
  private final List<byte[]> d = new LinkedList();
  private final Set<X509Certificate> e = Collections.synchronizedSet(new HashSet());
  
  public ag(ah paramah, af paramaf)
  {
    this.a = a(paramah);
    this.b = paramah;
    this.c = -1L;
    for (String str : paramaf.c()) {
      this.d.add(a(str));
    }
  }
  
  private boolean a(X509Certificate paramX509Certificate)
  {
    try
    {
      byte[] arrayOfByte = MessageDigest.getInstance("SHA1").digest(paramX509Certificate.getPublicKey().getEncoded());
      Iterator localIterator = this.d.iterator();
      while (localIterator.hasNext())
      {
        boolean bool = Arrays.equals((byte[])localIterator.next(), arrayOfByte);
        if (bool) {
          return true;
        }
      }
      return false;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      throw new CertificateException(localNoSuchAlgorithmException);
    }
  }
  
  private static byte[] a(String paramString)
  {
    int i = paramString.length();
    byte[] arrayOfByte = new byte[i / 2];
    for (int j = 0; j < i; j += 2) {
      arrayOfByte[(j / 2)] = ((byte)((Character.digit(paramString.charAt(j), 16) << 4) + Character.digit(paramString.charAt(j + 1), 16)));
    }
    return arrayOfByte;
  }
  
  private static TrustManager[] a(ah paramah)
  {
    try
    {
      TrustManagerFactory localTrustManagerFactory = TrustManagerFactory.getInstance("X509");
      localTrustManagerFactory.init(paramah.a);
      TrustManager[] arrayOfTrustManager = localTrustManagerFactory.getTrustManagers();
      return arrayOfTrustManager;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      throw new AssertionError(localNoSuchAlgorithmException);
    }
    catch (KeyStoreException localKeyStoreException)
    {
      throw new AssertionError(localKeyStoreException);
    }
  }
  
  public final void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
  {
    throw new CertificateException("Client certificates not supported!");
  }
  
  public final void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
  {
    if (this.e.contains(paramArrayOfX509Certificate[0])) {
      return;
    }
    TrustManager[] arrayOfTrustManager = this.a;
    int i = arrayOfTrustManager.length;
    for (int j = 0; j < i; j++) {
      ((X509TrustManager)arrayOfTrustManager[j]).checkServerTrusted(paramArrayOfX509Certificate, paramString);
    }
    if ((this.c != -1L) && (System.currentTimeMillis() - this.c > 15552000000L))
    {
      ci localci = co.a().a();
      new StringBuilder("Certificate pins are stale, (").append(System.currentTimeMillis() - this.c).append(" millis vs 15552000000 millis) falling back to system trust.");
      localci.d();
      this.e.add(paramArrayOfX509Certificate[0]);
      return;
    }
    X509Certificate[] arrayOfX509Certificate = bt.a(paramArrayOfX509Certificate, this.b);
    int k = arrayOfX509Certificate.length;
    for (int m = 0;; m++)
    {
      if (m >= k) {
        break label179;
      }
      if (a(arrayOfX509Certificate[m])) {
        break;
      }
    }
    label179:
    throw new CertificateException("No valid pins found in chain!");
  }
  
  public final X509Certificate[] getAcceptedIssuers()
  {
    return null;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/ag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */