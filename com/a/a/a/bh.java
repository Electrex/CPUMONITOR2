package com.a.a.a;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public final class bh
{
  bo a;
  File b;
  File c;
  private final File d;
  private final String e;
  
  public bh(File paramFile, String paramString1, String paramString2)
  {
    this.d = paramFile;
    this.e = paramString2;
    this.b = new File(paramFile, paramString1);
    this.a = new bo(this.b);
    this.c = new File(this.d, this.e);
    if (!this.c.exists()) {
      this.c.mkdirs();
    }
  }
  
  public static void a(List<File> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      File localFile = (File)localIterator.next();
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = localFile.getName();
      String.format("deleting sent analytics file %s", arrayOfObject);
      ba.d();
      localFile.delete();
    }
  }
  
  public final List<File> a()
  {
    return Arrays.asList(this.c.listFiles());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/bh.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */