package com.a.a.a;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class i
  implements t
{
  private final ScheduledExecutorService a;
  private final j b;
  private final bt c;
  private ScheduledFuture<?> d;
  private int e = -1;
  private m f;
  
  public i(ScheduledExecutorService paramScheduledExecutorService, j paramj, bt parambt)
  {
    this.a = paramScheduledExecutorService;
    this.b = paramj;
    this.c = parambt;
  }
  
  private void a(int paramInt1, int paramInt2)
  {
    try
    {
      x localx = new x(this.b, this);
      new StringBuilder("Scheduling time based file roll over every ").append(paramInt2).append(" seconds");
      ba.d();
      this.d = this.a.scheduleAtFixedRate(localx, paramInt1, paramInt2, TimeUnit.SECONDS);
      return;
    }
    catch (RejectedExecutionException localRejectedExecutionException)
    {
      ba.e();
    }
  }
  
  /* Error */
  public final void a()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 80	com/a/a/a/i:f	Lcom/a/a/a/m;
    //   4: ifnonnull +7 -> 11
    //   7: invokestatic 60	com/a/a/a/ba:d	()V
    //   10: return
    //   11: invokestatic 60	com/a/a/a/ba:d	()V
    //   14: aload_0
    //   15: getfield 30	com/a/a/a/i:b	Lcom/a/a/a/j;
    //   18: invokevirtual 85	com/a/a/a/j:c	()Ljava/util/List;
    //   21: astore_1
    //   22: iconst_0
    //   23: istore_2
    //   24: aload_1
    //   25: invokeinterface 91 1 0
    //   30: ifle +157 -> 187
    //   33: aload_0
    //   34: getfield 80	com/a/a/a/i:f	Lcom/a/a/a/m;
    //   37: invokestatic 96	com/a/a/a/c:a	()Lcom/a/a/a/c;
    //   40: getfield 102	com/a/a/a/ch:g	Landroid/content/Context;
    //   43: iconst_0
    //   44: invokestatic 107	com/a/a/a/cj:a	(Landroid/content/Context;Z)Ljava/lang/String;
    //   47: aload_1
    //   48: invokeinterface 112 3 0
    //   53: istore 7
    //   55: iload 7
    //   57: ifeq +24 -> 81
    //   60: aload_1
    //   61: invokeinterface 91 1 0
    //   66: istore 8
    //   68: iload 8
    //   70: iload_2
    //   71: iadd
    //   72: istore 4
    //   74: aload_1
    //   75: invokestatic 117	com/a/a/a/bh:a	(Ljava/util/List;)V
    //   78: iload 4
    //   80: istore_2
    //   81: getstatic 123	java/util/Locale:US	Ljava/util/Locale;
    //   84: astore 9
    //   86: iconst_2
    //   87: anewarray 4	java/lang/Object
    //   90: astore 10
    //   92: aload 10
    //   94: iconst_0
    //   95: aload_1
    //   96: invokeinterface 91 1 0
    //   101: invokestatic 129	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   104: aastore
    //   105: iload 7
    //   107: ifeq +42 -> 149
    //   110: ldc -125
    //   112: astore 11
    //   114: aload 10
    //   116: iconst_1
    //   117: aload 11
    //   119: aastore
    //   120: aload 9
    //   122: ldc -123
    //   124: aload 10
    //   126: invokestatic 139	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   129: pop
    //   130: invokestatic 60	com/a/a/a/ba:d	()V
    //   133: iload 7
    //   135: ifeq +52 -> 187
    //   138: aload_0
    //   139: getfield 30	com/a/a/a/i:b	Lcom/a/a/a/j;
    //   142: invokevirtual 85	com/a/a/a/j:c	()Ljava/util/List;
    //   145: astore_1
    //   146: goto -122 -> 24
    //   149: ldc -115
    //   151: astore 11
    //   153: goto -39 -> 114
    //   156: astore_3
    //   157: iload_2
    //   158: istore 4
    //   160: aload_3
    //   161: astore 5
    //   163: new 42	java/lang/StringBuilder
    //   166: dup
    //   167: ldc -113
    //   169: invokespecial 47	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   172: aload 5
    //   174: invokevirtual 147	java/lang/Exception:getMessage	()Ljava/lang/String;
    //   177: invokevirtual 56	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: iload 4
    //   183: istore_2
    //   184: invokestatic 76	com/a/a/a/ba:e	()V
    //   187: iload_2
    //   188: ifne -178 -> 10
    //   191: aload_0
    //   192: getfield 30	com/a/a/a/i:b	Lcom/a/a/a/j;
    //   195: invokevirtual 148	com/a/a/a/j:d	()V
    //   198: return
    //   199: astore 5
    //   201: goto -38 -> 163
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	204	0	this	i
    //   21	125	1	localList	java.util.List
    //   23	165	2	i	int
    //   156	5	3	localException1	Exception
    //   72	110	4	j	int
    //   161	12	5	localException2	Exception
    //   199	1	5	localException3	Exception
    //   53	81	7	bool	boolean
    //   66	6	8	k	int
    //   84	37	9	localLocale	Locale
    //   90	35	10	arrayOfObject	Object[]
    //   112	40	11	str	String
    // Exception table:
    //   from	to	target	type
    //   24	55	156	java/lang/Exception
    //   60	68	156	java/lang/Exception
    //   81	105	156	java/lang/Exception
    //   114	133	156	java/lang/Exception
    //   138	146	156	java/lang/Exception
    //   74	78	199	java/lang/Exception
  }
  
  public final void a(aj paramaj, String paramString)
  {
    this.f = new g(paramString, paramaj.a, this.c);
    this.b.a = paramaj;
    this.e = paramaj.b;
    a(0, this.e);
  }
  
  public final void a(u paramu)
  {
    i = 1;
    paramu.toString();
    ba.d();
    for (;;)
    {
      try
      {
        j localj = this.b;
        byte[] arrayOfByte = w.a(paramu);
        int k = arrayOfByte.length;
        bh localbh = localj.b;
        int m = localj.b();
        if (k + (4 + localbh.a.a()) > m) {
          continue;
        }
        n = i;
        if (n == 0)
        {
          Locale localLocale = Locale.US;
          Object[] arrayOfObject = new Object[3];
          arrayOfObject[0] = Integer.valueOf(localj.b.a.a());
          arrayOfObject[1] = Integer.valueOf(k);
          arrayOfObject[2] = Integer.valueOf(localj.b());
          ba.c(String.format(localLocale, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", arrayOfObject));
          localj.a();
        }
        localj.b.a.a(arrayOfByte, arrayOfByte.length);
      }
      catch (IOException localIOException)
      {
        int n;
        ba.e();
        continue;
        int j = 0;
        continue;
        i = 0;
        continue;
      }
      if (this.e == -1) {
        continue;
      }
      j = i;
      if (this.d != null) {
        continue;
      }
      if ((j != 0) && (i != 0)) {
        a(this.e, this.e);
      }
      return;
      n = 0;
    }
  }
  
  public final void b()
  {
    j localj = this.b;
    bh.a(localj.b.a());
    bh localbh = localj.b;
    try
    {
      localbh.a.close();
      localbh.b.delete();
      return;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
  }
  
  public final void c()
  {
    if (this.d != null)
    {
      ba.d();
      this.d.cancel(false);
      this.d = null;
    }
  }
  
  public final void d()
  {
    try
    {
      this.b.a();
      return;
    }
    catch (IOException localIOException)
    {
      ba.e();
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */