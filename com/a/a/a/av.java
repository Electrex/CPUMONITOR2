package com.a.a.a;

import org.json.JSONObject;

public final class av
{
  private ay a;
  private final ax b;
  private final bf c;
  private final am d;
  private final bz e;
  
  public av(ay paramay, bf parambf, ax paramax, am paramam, bz parambz)
  {
    this.a = paramay;
    this.c = parambf;
    this.b = paramax;
    this.d = paramam;
    this.e = parambz;
  }
  
  private static void a(JSONObject paramJSONObject, String paramString)
  {
    if (!ba.e(co.a().g))
    {
      JSONObject localJSONObject = new JSONObject(paramJSONObject.toString());
      localJSONObject.getJSONObject("features").remove("collect_analytics");
      localJSONObject.remove("analytics");
      paramJSONObject = localJSONObject;
    }
    ci localci = co.a().a();
    new StringBuilder().append(paramString).append(paramJSONObject.toString());
    localci.b();
  }
  
  private static aw b(au paramau)
  {
    localObject = null;
    for (;;)
    {
      try
      {
        boolean bool = au.b.equals(paramau);
        localObject = null;
        if (!bool)
        {
          JSONObject localJSONObject = am.a();
          if (localJSONObject == null) {
            break label127;
          }
          localaw = ax.a(localJSONObject);
          a(localJSONObject, "Loaded cached settings: ");
          long l1 = System.currentTimeMillis();
          if (!au.c.equals(paramau))
          {
            long l2 = localaw.f;
            if (l2 >= l1) {
              continue;
            }
            i = 1;
            if (i != 0) {
              continue;
            }
          }
        }
      }
      catch (Exception localException1) {}
      try
      {
        co.a().a().b();
        localObject = localaw;
        return (aw)localObject;
      }
      catch (Exception localException2)
      {
        for (;;)
        {
          localObject = localaw;
        }
      }
      int i = 0;
    }
    co.a().a().b();
    return null;
    co.a().a().a();
    return (aw)localObject;
    label127:
    co.a().a().b();
    return null;
  }
  
  /* Error */
  public final aw a(au paramau)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: invokestatic 35	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   5: getfield 130	com/a/a/a/cl:b	Z
    //   8: ifne +235 -> 243
    //   11: aload_1
    //   12: invokestatic 132	com/a/a/a/av:b	(Lcom/a/a/a/au;)Lcom/a/a/a/aw;
    //   15: astore 18
    //   17: aload 18
    //   19: astore 4
    //   21: aload 4
    //   23: ifnonnull +118 -> 141
    //   26: aload_0
    //   27: getfield 29	com/a/a/a/av:e	Lcom/a/a/a/bz;
    //   30: aload_0
    //   31: getfield 21	com/a/a/a/av:a	Lcom/a/a/a/ay;
    //   34: invokeinterface 137 2 0
    //   39: astore 7
    //   41: aload 7
    //   43: ifnull +98 -> 141
    //   46: aload 7
    //   48: invokestatic 107	com/a/a/a/ax:a	(Lorg/json/JSONObject;)Lcom/a/a/a/aw;
    //   51: astore 8
    //   53: aload 8
    //   55: getfield 125	com/a/a/a/aw:f	J
    //   58: lstore 10
    //   60: invokestatic 35	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   63: invokevirtual 74	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   66: invokeinterface 85 1 0
    //   71: aload 7
    //   73: ifnull +57 -> 130
    //   76: aload 7
    //   78: ldc -117
    //   80: lload 10
    //   82: invokevirtual 143	org/json/JSONObject:put	(Ljava/lang/String;J)Lorg/json/JSONObject;
    //   85: pop
    //   86: new 145	java/io/FileWriter
    //   89: dup
    //   90: new 147	java/io/File
    //   93: dup
    //   94: invokestatic 35	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   97: getfield 150	com/a/a/a/cl:c	Ljava/io/File;
    //   100: ldc -104
    //   102: invokespecial 155	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   105: invokespecial 158	java/io/FileWriter:<init>	(Ljava/io/File;)V
    //   108: astore 14
    //   110: aload 14
    //   112: aload 7
    //   114: invokevirtual 52	org/json/JSONObject:toString	()Ljava/lang/String;
    //   117: invokevirtual 161	java/io/FileWriter:write	(Ljava/lang/String;)V
    //   120: aload 14
    //   122: invokevirtual 164	java/io/FileWriter:flush	()V
    //   125: aload 14
    //   127: invokestatic 167	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   130: aload 7
    //   132: ldc -87
    //   134: invokestatic 111	com/a/a/a/av:a	(Lorg/json/JSONObject;Ljava/lang/String;)V
    //   137: aload 8
    //   139: astore 4
    //   141: aload 4
    //   143: ifnonnull +15 -> 158
    //   146: getstatic 119	com/a/a/a/au:c	Lcom/a/a/a/au;
    //   149: invokestatic 132	com/a/a/a/av:b	(Lcom/a/a/a/au;)Lcom/a/a/a/aw;
    //   152: astore 6
    //   154: aload 6
    //   156: astore 4
    //   158: aload 4
    //   160: areturn
    //   161: astore 13
    //   163: aconst_null
    //   164: astore 14
    //   166: invokestatic 35	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   169: invokevirtual 74	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   172: invokeinterface 127 1 0
    //   177: aload 14
    //   179: invokestatic 167	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   182: goto -52 -> 130
    //   185: astore 9
    //   187: aload 8
    //   189: astore 4
    //   191: invokestatic 35	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   194: invokevirtual 74	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   197: invokeinterface 127 1 0
    //   202: aload 4
    //   204: areturn
    //   205: astore 12
    //   207: aload_2
    //   208: invokestatic 167	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   211: aload 12
    //   213: athrow
    //   214: astore_3
    //   215: aconst_null
    //   216: astore 4
    //   218: goto -27 -> 191
    //   221: astore 5
    //   223: goto -32 -> 191
    //   226: astore 15
    //   228: aload 14
    //   230: astore_2
    //   231: aload 15
    //   233: astore 12
    //   235: goto -28 -> 207
    //   238: astore 17
    //   240: goto -74 -> 166
    //   243: aconst_null
    //   244: astore 4
    //   246: goto -225 -> 21
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	249	0	this	av
    //   0	249	1	paramau	au
    //   1	230	2	localObject1	Object
    //   214	1	3	localException1	Exception
    //   19	226	4	localObject2	Object
    //   221	1	5	localException2	Exception
    //   152	3	6	localaw1	aw
    //   39	92	7	localJSONObject	JSONObject
    //   51	137	8	localaw2	aw
    //   185	1	9	localException3	Exception
    //   58	23	10	l	long
    //   205	7	12	localObject3	Object
    //   233	1	12	localObject4	Object
    //   161	1	13	localException4	Exception
    //   108	121	14	localFileWriter	java.io.FileWriter
    //   226	6	15	localObject5	Object
    //   238	1	17	localException5	Exception
    //   15	3	18	localaw3	aw
    // Exception table:
    //   from	to	target	type
    //   76	110	161	java/lang/Exception
    //   53	71	185	java/lang/Exception
    //   125	130	185	java/lang/Exception
    //   130	137	185	java/lang/Exception
    //   177	182	185	java/lang/Exception
    //   207	214	185	java/lang/Exception
    //   76	110	205	finally
    //   2	17	214	java/lang/Exception
    //   26	41	221	java/lang/Exception
    //   46	53	221	java/lang/Exception
    //   146	154	221	java/lang/Exception
    //   110	125	226	finally
    //   166	177	226	finally
    //   110	125	238	java/lang/Exception
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/av.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */