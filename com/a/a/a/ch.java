package com.a.a.a;

import android.content.Context;

public abstract class ch
{
  private boolean a;
  public Context g;
  
  public abstract void b();
  
  protected final void b(Context paramContext)
  {
    for (;;)
    {
      try
      {
        boolean bool = this.a;
        if (bool) {
          return;
        }
        if (paramContext == null) {
          throw new IllegalArgumentException("context cannot be null.");
        }
      }
      finally {}
      this.g = new cp(paramContext.getApplicationContext(), getClass().getSimpleName().toLowerCase());
      this.a = true;
      b();
    }
  }
  
  public final boolean o()
  {
    try
    {
      boolean bool = this.a;
      return bool;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/ch.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */