package com.a.a.a;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.os.Build.VERSION;
import android.util.Log;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

public final class cl
  extends ch
{
  public AtomicReference<ci> a = new AtomicReference();
  public boolean b;
  public File c;
  Application d;
  public WeakReference<Activity> e;
  String f;
  int h = 4;
  private by i = new b(ce.a);
  private ConcurrentHashMap<Class<? extends ck>, ck> j = new ConcurrentHashMap();
  
  public static void a(Context paramContext, ck... paramVarArgs)
  {
    for (;;)
    {
      try
      {
        boolean bool = co.a().o();
        if (bool) {
          return;
        }
        cl localcl1 = co.a();
        cl localcl2;
        int k;
        if ((paramContext instanceof Application))
        {
          localApplication = (Application)paramContext;
          localcl1.d = localApplication;
          if (!(paramContext instanceof Activity)) {
            break label198;
          }
          localActivity = (Activity)paramContext;
          localcl2 = localcl1.a(localActivity);
          k = 0;
          if (k < 2)
          {
            ck localck = paramVarArgs[k];
            if (localcl2.j.containsKey(paramVarArgs)) {
              break label186;
            }
            localcl2.j.putIfAbsent(localck.getClass(), localck);
            break label186;
          }
        }
        else
        {
          if ((paramContext instanceof Activity))
          {
            localApplication = ((Activity)paramContext).getApplication();
            continue;
          }
          if ((paramContext instanceof Service))
          {
            localApplication = ((Service)paramContext).getApplication();
            continue;
          }
          if (!(paramContext.getApplicationContext() instanceof Application)) {
            break label192;
          }
          localApplication = (Application)paramContext.getApplicationContext();
          continue;
        }
        localcl2.b(paramContext);
        continue;
        k++;
      }
      finally {}
      label186:
      continue;
      label192:
      Application localApplication = null;
      continue;
      label198:
      Activity localActivity = null;
    }
  }
  
  public final ci a()
  {
    Object localObject = (ci)this.a.get();
    if (localObject == null)
    {
      localObject = new cj();
      if (!this.a.compareAndSet(null, localObject)) {
        localObject = (ci)this.a.get();
      }
    }
    return (ci)localObject;
  }
  
  public final <T extends ck> T a(Class<T> paramClass)
  {
    return (ck)this.j.get(paramClass);
  }
  
  final cl a(Activity paramActivity)
  {
    this.e = new WeakReference(paramActivity);
    return this;
  }
  
  protected final void b()
  {
    Context localContext = this.g;
    this.c = new File(localContext.getFilesDir(), "com.crashlytics.sdk.android");
    if (!this.c.exists()) {
      this.c.mkdirs();
    }
    if (Build.VERSION.SDK_INT >= 14)
    {
      cm localcm = new cm(this, (byte)0);
      Application localApplication = this.d;
      if (localApplication != null) {
        localApplication.registerActivityLifecycleCallbacks(new cn(localcm));
      }
    }
    StringBuilder localStringBuilder;
    Iterator localIterator2;
    if ((this.b) && (Log.isLoggable("CrashlyticsInternal", 3)))
    {
      localStringBuilder = new StringBuilder();
      localIterator2 = this.j.values().iterator();
    }
    while (localIterator2.hasNext())
    {
      ch localch = (ch)localIterator2.next();
      long l = System.nanoTime();
      localch.b(localContext);
      localStringBuilder.append("sdkPerfStart.").append(localch.getClass().getName()).append('=').append(System.nanoTime() - l).append('\n');
      continue;
      Iterator localIterator1 = this.j.values().iterator();
      while (localIterator1.hasNext()) {
        ((ch)localIterator1.next()).b(localContext);
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/cl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */