package com.a.a.a;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import java.io.File;

public class c
  extends ck
{
  public n a;
  private String b;
  private String c;
  private String d;
  private bm e;
  private ai f;
  private long h;
  private bt i;
  
  public static c a()
  {
    return (c)co.a().a(c.class);
  }
  
  private String c()
  {
    return ba.a(this.g, "com.crashlytics.ApiEndpoint");
  }
  
  protected final void b()
  {
    for (;;)
    {
      try
      {
        this.i = new bt(co.a().a());
        this.f = new ai(co.a().a(c.class));
        localContext = this.g;
        PackageManager localPackageManager = localContext.getPackageManager();
        this.e = new bm(localContext);
        this.b = localContext.getPackageName();
        localPackageInfo = localPackageManager.getPackageInfo(this.b, 0);
        this.c = Integer.toString(localPackageInfo.versionCode);
        if (localPackageInfo.versionName != null) {
          continue;
        }
        str = "0.0";
        this.d = str;
        if (Build.VERSION.SDK_INT < 9) {
          continue;
        }
        this.h = localPackageInfo.firstInstallTime;
      }
      catch (Exception localException)
      {
        Context localContext;
        PackageInfo localPackageInfo;
        String str;
        co.a().a().a();
        continue;
      }
      new Thread(new d(this), "Crashlytics Initializer").start();
      return;
      str = localPackageInfo.versionName;
      continue;
      this.h = new File(localContext.getPackageManager().getApplicationInfo(localContext.getPackageName(), 0).sourceDir).lastModified();
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */