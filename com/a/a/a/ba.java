package com.a.a.a;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public final class ba
{
  public static final Comparator<File> a = new bb();
  private static Boolean b = null;
  private static final char[] c = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
  private static long d = -1L;
  private static Boolean e = null;
  
  public static int a(Context paramContext, String paramString1, String paramString2)
  {
    Resources localResources = paramContext.getResources();
    int i = paramContext.getApplicationContext().getApplicationInfo().icon;
    if (i > 0) {}
    for (String str = paramContext.getResources().getResourcePackageName(i);; str = paramContext.getPackageName()) {
      return localResources.getIdentifier(paramString1, paramString2, str);
    }
  }
  
  public static int a(boolean paramBoolean)
  {
    float f = b(co.a().g);
    if (!paramBoolean) {
      return 1;
    }
    if ((paramBoolean) && (f >= 99.0D)) {
      return 3;
    }
    if ((paramBoolean) && (f < 99.0D)) {
      return 2;
    }
    return 0;
  }
  
  public static long a(Context paramContext)
  {
    ActivityManager.MemoryInfo localMemoryInfo = new ActivityManager.MemoryInfo();
    ((ActivityManager)paramContext.getSystemService("activity")).getMemoryInfo(localMemoryInfo);
    return localMemoryInfo.availMem;
  }
  
  private static long a(String paramString1, String paramString2, int paramInt)
  {
    return Long.parseLong(paramString1.split(paramString2)[0].trim()) * paramInt;
  }
  
  public static ActivityManager.RunningAppProcessInfo a(String paramString, Context paramContext)
  {
    List localList = ((ActivityManager)paramContext.getSystemService("activity")).getRunningAppProcesses();
    if (localList != null)
    {
      Iterator localIterator = localList.iterator();
      while (localIterator.hasNext())
      {
        ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = (ActivityManager.RunningAppProcessInfo)localIterator.next();
        if (localRunningAppProcessInfo.processName.equals(paramString)) {
          return localRunningAppProcessInfo;
        }
      }
    }
    return null;
  }
  
  public static SharedPreferences a()
  {
    return co.a().g.getSharedPreferences("com.crashlytics.prefs", 0);
  }
  
  public static String a(Context paramContext, String paramString)
  {
    int i = a(paramContext, paramString, "string");
    if (i > 0) {
      return paramContext.getString(i);
    }
    return "";
  }
  
  /* Error */
  private static String a(File paramFile, String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 192	java/io/File:exists	()Z
    //   4: istore_2
    //   5: aconst_null
    //   6: astore_3
    //   7: iload_2
    //   8: ifeq +78 -> 86
    //   11: new 194	java/io/BufferedReader
    //   14: dup
    //   15: new 196	java/io/FileReader
    //   18: dup
    //   19: aload_0
    //   20: invokespecial 199	java/io/FileReader:<init>	(Ljava/io/File;)V
    //   23: sipush 1024
    //   26: invokespecial 202	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
    //   29: astore 4
    //   31: aload 4
    //   33: invokevirtual 205	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   36: astore 9
    //   38: aconst_null
    //   39: astore_3
    //   40: aload 9
    //   42: ifnull +39 -> 81
    //   45: ldc -49
    //   47: invokestatic 213	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
    //   50: aload 9
    //   52: iconst_2
    //   53: invokevirtual 216	java/util/regex/Pattern:split	(Ljava/lang/CharSequence;I)[Ljava/lang/String;
    //   56: astore 10
    //   58: aload 10
    //   60: arraylength
    //   61: iconst_1
    //   62: if_icmple -31 -> 31
    //   65: aload 10
    //   67: iconst_0
    //   68: aaload
    //   69: aload_1
    //   70: invokevirtual 167	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   73: ifeq -42 -> 31
    //   76: aload 10
    //   78: iconst_1
    //   79: aaload
    //   80: astore_3
    //   81: aload 4
    //   83: invokestatic 219	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   86: aload_3
    //   87: areturn
    //   88: astore 12
    //   90: aconst_null
    //   91: astore 4
    //   93: invokestatic 90	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   96: invokevirtual 224	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   99: astore 7
    //   101: new 226	java/lang/StringBuilder
    //   104: dup
    //   105: ldc -28
    //   107: invokespecial 231	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   110: aload_0
    //   111: invokevirtual 235	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: aload 7
    //   117: invokeinterface 239 1 0
    //   122: aload 4
    //   124: invokestatic 219	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   127: aconst_null
    //   128: areturn
    //   129: astore 11
    //   131: aconst_null
    //   132: astore 4
    //   134: aload 11
    //   136: astore 6
    //   138: aload 4
    //   140: invokestatic 219	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   143: aload 6
    //   145: athrow
    //   146: astore 6
    //   148: goto -10 -> 138
    //   151: astore 5
    //   153: goto -60 -> 93
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	156	0	paramFile	File
    //   0	156	1	paramString	String
    //   4	4	2	bool	boolean
    //   6	81	3	str1	String
    //   29	110	4	localBufferedReader	java.io.BufferedReader
    //   151	1	5	localException1	Exception
    //   136	8	6	localObject1	Object
    //   146	1	6	localObject2	Object
    //   99	17	7	localci	ci
    //   36	15	9	str2	String
    //   56	21	10	arrayOfString	String[]
    //   129	6	11	localObject3	Object
    //   88	1	12	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   11	31	88	java/lang/Exception
    //   11	31	129	finally
    //   31	38	146	finally
    //   45	81	146	finally
    //   93	122	146	finally
    //   31	38	151	java/lang/Exception
    //   45	81	151	java/lang/Exception
  }
  
  public static String a(InputStream paramInputStream)
  {
    Scanner localScanner = new Scanner(paramInputStream).useDelimiter("\\A");
    if (localScanner.hasNext()) {
      return localScanner.next();
    }
    return "";
  }
  
  public static String a(String paramString)
  {
    return a(paramString.getBytes(), "SHA-1");
  }
  
  public static String a(byte[] paramArrayOfByte)
  {
    char[] arrayOfChar = new char[paramArrayOfByte.length << 1];
    for (int i = 0; i < paramArrayOfByte.length; i++)
    {
      int j = 0xFF & paramArrayOfByte[i];
      arrayOfChar[(i << 1)] = c[(j >>> 4)];
      arrayOfChar[(1 + (i << 1))] = c[(j & 0xF)];
    }
    return new String(arrayOfChar);
  }
  
  private static String a(byte[] paramArrayOfByte, String paramString)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance(paramString);
      localMessageDigest.update(paramArrayOfByte);
      return a(localMessageDigest.digest());
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      ci localci = co.a().a();
      new StringBuilder("Could not create hashing algorithm: ").append(paramString).append(", returning empty string.");
      localci.a();
    }
    return "";
  }
  
  public static String a(String... paramVarArgs)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i <= 0; i++)
    {
      String str2 = paramVarArgs[0];
      if (str2 != null) {
        localArrayList.add(str2.replace("-", "").toLowerCase(Locale.US));
      }
    }
    Collections.sort(localArrayList);
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = localArrayList.iterator();
    while (localIterator.hasNext()) {
      localStringBuilder.append((String)localIterator.next());
    }
    String str1 = localStringBuilder.toString();
    if (str1.length() > 0) {
      return a(str1);
    }
    return null;
  }
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException localIOException)
    {
      co.a().a().a();
    }
  }
  
  public static void a(Flushable paramFlushable)
  {
    if (paramFlushable != null) {}
    try
    {
      paramFlushable.flush();
      return;
    }
    catch (IOException localIOException)
    {
      co.a().a().a();
    }
  }
  
  public static void a(InputStream paramInputStream, OutputStream paramOutputStream, byte[] paramArrayOfByte)
  {
    for (;;)
    {
      int i = paramInputStream.read(paramArrayOfByte);
      if (i == -1) {
        break;
      }
      paramOutputStream.write(paramArrayOfByte, 0, i);
    }
  }
  
  public static boolean a(Context paramContext, String paramString, boolean paramBoolean)
  {
    if (paramContext != null)
    {
      Resources localResources = paramContext.getResources();
      if (localResources != null)
      {
        int i = a(paramContext, paramString, "bool");
        if (i <= 0) {
          break label37;
        }
        paramBoolean = localResources.getBoolean(i);
      }
    }
    label37:
    int j;
    do
    {
      return paramBoolean;
      j = a(paramContext, paramString, "string");
    } while (j <= 0);
    return Boolean.parseBoolean(paramContext.getString(j));
  }
  
  public static float b(Context paramContext)
  {
    Intent localIntent = paramContext.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    int i = localIntent.getIntExtra("level", -1);
    int j = localIntent.getIntExtra("scale", -1);
    return i / j;
  }
  
  public static int b()
  {
    return bc.a().ordinal();
  }
  
  public static long b(String paramString)
  {
    StatFs localStatFs = new StatFs(paramString);
    long l = localStatFs.getBlockSize();
    return l * localStatFs.getBlockCount() - l * localStatFs.getAvailableBlocks();
  }
  
  private static String b(InputStream paramInputStream)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
      byte[] arrayOfByte = new byte['Ѐ'];
      for (;;)
      {
        int i = paramInputStream.read(arrayOfByte);
        if (i == -1) {
          break;
        }
        localMessageDigest.update(arrayOfByte, 0, i);
      }
      str = a(localMessageDigest.digest());
    }
    catch (Exception localException)
    {
      co.a().a().a();
      return "";
    }
    String str;
    return str;
  }
  
  public static long c()
  {
    for (;;)
    {
      long l1;
      String str2;
      try
      {
        if (d == -1L)
        {
          l1 = 0L;
          String str1 = a(new File("/proc/meminfo"), "MemTotal");
          if (!TextUtils.isEmpty(str1)) {
            str2 = str1.toUpperCase(Locale.US);
          }
        }
        try
        {
          if (!str2.endsWith("KB")) {
            continue;
          }
          long l3 = a(str2, "KB", 1024);
          l1 = l3;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          long l2;
          co.a().a().a();
          continue;
        }
        d = l1;
        l2 = d;
        return l2;
      }
      finally {}
      if (str2.endsWith("MB")) {
        l1 = a(str2, "MB", 1048576);
      } else if (str2.endsWith("GB")) {
        l1 = a(str2, "GB", 1073741824);
      } else {
        co.a().a().b();
      }
    }
  }
  
  public static void c(String paramString)
  {
    if (e(co.a().g)) {
      co.a().a().a("Crashlytics", paramString);
    }
  }
  
  public static boolean c(Context paramContext)
  {
    if (f()) {
      return false;
    }
    return ((SensorManager)paramContext.getSystemService("sensor")).getDefaultSensor(8) != null;
  }
  
  public static Cipher d(String paramString)
  {
    if (paramString.length() < 32) {
      throw new InvalidKeyException("Key must be at least 32 bytes.");
    }
    SecretKeySpec localSecretKeySpec = new SecretKeySpec(paramString.getBytes(), 0, 32, "AES/ECB/PKCS7Padding");
    try
    {
      Cipher localCipher = Cipher.getInstance("AES/ECB/PKCS7Padding");
      localCipher.init(1, localSecretKeySpec);
      return localCipher;
    }
    catch (GeneralSecurityException localGeneralSecurityException)
    {
      co.a().a().a();
      throw new RuntimeException(localGeneralSecurityException);
    }
  }
  
  public static void d()
  {
    if (e(co.a().g)) {
      co.a().a().b();
    }
  }
  
  public static boolean d(Context paramContext)
  {
    if (e == null)
    {
      boolean bool1 = a(paramContext, "com.crashlytics.SilenceCrashlyticsLogCat", false);
      boolean bool2 = false;
      if (!bool1) {
        bool2 = true;
      }
      e = Boolean.valueOf(bool2);
    }
    return e.booleanValue();
  }
  
  public static void e()
  {
    if (e(co.a().g)) {
      co.a().a().e();
    }
  }
  
  public static boolean e(Context paramContext)
  {
    if (b == null) {
      b = Boolean.valueOf(a(paramContext, "com.crashlytics.Trace", false));
    }
    return b.booleanValue();
  }
  
  public static boolean e(String paramString)
  {
    return (paramString == null) || (paramString.length() == 0);
  }
  
  public static boolean f()
  {
    String str = Settings.Secure.getString(co.a().g.getContentResolver(), "android_id");
    return ("sdk".equals(Build.PRODUCT)) || ("google_sdk".equals(Build.PRODUCT)) || (str == null);
  }
  
  public static boolean f(Context paramContext)
  {
    return (0x2 & paramContext.getApplicationInfo().flags) != 0;
  }
  
  /* Error */
  public static String g(Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 56	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   4: aload_0
    //   5: invokestatic 535	com/a/a/a/ba:h	(Landroid/content/Context;)I
    //   8: invokevirtual 539	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
    //   11: astore 5
    //   13: aload 5
    //   15: invokestatic 541	com/a/a/a/ba:b	(Ljava/io/InputStream;)Ljava/lang/String;
    //   18: astore 7
    //   20: aload 7
    //   22: invokestatic 543	com/a/a/a/ba:e	(Ljava/lang/String;)Z
    //   25: istore 8
    //   27: aconst_null
    //   28: astore 9
    //   30: iload 8
    //   32: ifeq +11 -> 43
    //   35: aload 5
    //   37: invokestatic 219	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   40: aload 9
    //   42: areturn
    //   43: aload 7
    //   45: astore 9
    //   47: goto -12 -> 35
    //   50: astore 4
    //   52: aconst_null
    //   53: astore_2
    //   54: invokestatic 90	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   57: invokevirtual 224	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   60: invokeinterface 239 1 0
    //   65: aload_2
    //   66: invokestatic 219	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   69: aconst_null
    //   70: areturn
    //   71: astore_1
    //   72: aconst_null
    //   73: astore_2
    //   74: aload_1
    //   75: astore_3
    //   76: aload_2
    //   77: invokestatic 219	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   80: aload_3
    //   81: athrow
    //   82: astore_3
    //   83: aload 5
    //   85: astore_2
    //   86: goto -10 -> 76
    //   89: astore_3
    //   90: goto -14 -> 76
    //   93: astore 6
    //   95: aload 5
    //   97: astore_2
    //   98: goto -44 -> 54
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	101	0	paramContext	Context
    //   71	4	1	localObject1	Object
    //   53	45	2	localObject2	Object
    //   75	6	3	localObject3	Object
    //   82	1	3	localObject4	Object
    //   89	1	3	localObject5	Object
    //   50	1	4	localException1	Exception
    //   11	85	5	localInputStream	InputStream
    //   93	1	6	localException2	Exception
    //   18	26	7	str1	String
    //   25	6	8	bool	boolean
    //   28	18	9	str2	String
    // Exception table:
    //   from	to	target	type
    //   0	13	50	java/lang/Exception
    //   0	13	71	finally
    //   13	27	82	finally
    //   54	65	89	finally
    //   13	27	93	java/lang/Exception
  }
  
  public static boolean g()
  {
    boolean bool = f();
    String str = Build.TAGS;
    if ((!bool) && (str != null) && (str.contains("test-keys"))) {}
    File localFile;
    do
    {
      do
      {
        return true;
      } while (new File("/system/app/Superuser.apk").exists());
      localFile = new File("/system/xbin/su");
    } while ((!bool) && (localFile.exists()));
    return false;
  }
  
  public static int h()
  {
    if (f()) {}
    for (int i = 1;; i = 0)
    {
      if (g()) {
        i |= 0x2;
      }
      int j;
      if (!Debug.isDebuggerConnected())
      {
        boolean bool = Debug.waitingForDebugger();
        j = 0;
        if (!bool) {}
      }
      else
      {
        j = 1;
      }
      if (j != 0) {
        i |= 0x4;
      }
      return i;
    }
  }
  
  public static int h(Context paramContext)
  {
    return paramContext.getApplicationContext().getApplicationInfo().icon;
  }
  
  public static String i(Context paramContext)
  {
    int i = a(paramContext, "com.crashlytics.android.build_id", "string");
    String str = null;
    if (i != 0)
    {
      str = paramContext.getResources().getString(i);
      co.a().a().b();
    }
    return str;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/ba.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */