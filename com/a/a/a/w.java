package com.a.a.a;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class w
{
  private static JSONObject a(Map<String, String> paramMap)
  {
    JSONObject localJSONObject = new JSONObject();
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      localJSONObject.put((String)localEntry.getKey(), localEntry.getValue());
    }
    return localJSONObject;
  }
  
  public static byte[] a(u paramu)
  {
    try
    {
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("appBundleId", paramu.a);
      localJSONObject.put("executionId", paramu.b);
      localJSONObject.put("installationId", paramu.c);
      localJSONObject.put("androidId", paramu.d);
      localJSONObject.put("osVersion", paramu.e);
      localJSONObject.put("deviceModel", paramu.f);
      localJSONObject.put("appVersionCode", paramu.g);
      localJSONObject.put("appVersionName", paramu.h);
      localJSONObject.put("timestamp", paramu.i);
      localJSONObject.put("type", paramu.j.toString());
      localJSONObject.put("details", a(paramu.k));
      byte[] arrayOfByte = localJSONObject.toString().getBytes("UTF-8");
      return arrayOfByte;
    }
    catch (JSONException localJSONException)
    {
      throw new IOException(localJSONException.getMessage());
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/w.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */