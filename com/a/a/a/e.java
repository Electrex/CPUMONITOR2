package com.a.a.a;

import android.annotation.TargetApi;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import java.util.concurrent.ScheduledExecutorService;

@TargetApi(14)
final class e
  extends n
{
  private final Application k;
  private final Application.ActivityLifecycleCallbacks l = new f(this);
  
  public e(Application paramApplication, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, j paramj, bt parambt)
  {
    this(paramApplication, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramj, bf.a("Crashlytics Trace Manager"), parambt);
  }
  
  private e(Application paramApplication, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, j paramj, ScheduledExecutorService paramScheduledExecutorService, bt parambt)
  {
    super(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramj, paramScheduledExecutorService, parambt);
    this.k = paramApplication;
    ba.d();
    paramApplication.registerActivityLifecycleCallbacks(this.l);
  }
  
  final void a()
  {
    ba.d();
    this.k.unregisterActivityLifecycleCallbacks(this.l);
    super.a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */