package com.a.a.a;

import android.content.Context;
import android.content.SharedPreferences;

public final class ai
{
  private final String a;
  private final Context b;
  
  public ai(ck paramck)
  {
    if (paramck.g == null) {
      throw new IllegalStateException("Cannot get directory before context has been set. Call Sdk.start() first");
    }
    this.b = paramck.g;
    this.a = paramck.getClass().getName();
  }
  
  public final SharedPreferences a()
  {
    return this.b.getSharedPreferences(this.a, 0);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/ai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */