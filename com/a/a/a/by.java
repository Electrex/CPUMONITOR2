package com.a.a.a;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

public class by
{
  private final ConcurrentMap<Class<?>, Set<Object>> a = new ConcurrentHashMap();
  private final ConcurrentMap<Class<?>, Object> b = new ConcurrentHashMap();
  private final String c;
  private final ce d;
  private final cc e;
  private final ThreadLocal<ConcurrentLinkedQueue<Object>> f = new ca();
  private final ThreadLocal<Boolean> g = new cb();
  private final Map<Class<?>, Set<Class<?>>> h = new HashMap();
  
  public by(ce paramce)
  {
    this(paramce, "default");
  }
  
  private by(ce paramce, String paramString)
  {
    this(paramce, paramString, cc.a);
  }
  
  private by(ce paramce, String paramString, cc paramcc)
  {
    this.d = paramce;
    this.c = paramString;
    this.e = paramcc;
  }
  
  public String toString()
  {
    return "[Bus \"" + this.c + "\"]";
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/by.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */