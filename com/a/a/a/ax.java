package com.a.a.a;

import org.json.JSONObject;

public final class ax
{
  public static aw a(JSONObject paramJSONObject)
  {
    paramJSONObject.optInt("settings_version", 0);
    int i = paramJSONObject.optInt("cache_duration", 3600);
    JSONObject localJSONObject1 = paramJSONObject.getJSONObject("app");
    localJSONObject1.getString("identifier");
    String str1 = localJSONObject1.getString("status");
    String str2 = localJSONObject1.getString("url");
    String str3 = localJSONObject1.getString("reports_url");
    boolean bool1 = localJSONObject1.optBoolean("update_required", false);
    if ((localJSONObject1.has("icon")) && (localJSONObject1.getJSONObject("icon").has("hash")))
    {
      JSONObject localJSONObject6 = localJSONObject1.getJSONObject("icon");
      localJSONObject6.getString("hash");
      localJSONObject6.getInt("width");
      localJSONObject6.getInt("height");
      new ak();
    }
    al localal = new al(str1, str2, str3, bool1);
    JSONObject localJSONObject2 = paramJSONObject.getJSONObject("session");
    localJSONObject2.optInt("log_buffer_size", 64000);
    localJSONObject2.optInt("max_chained_exception_depth", 8);
    int j = localJSONObject2.optInt("max_custom_exception_events", 64);
    localJSONObject2.optInt("max_custom_key_value_pairs", 64);
    localJSONObject2.optInt("identifier_mask", 255);
    localJSONObject2.optBoolean("send_session_without_crash", false);
    aq localaq = new aq(j);
    JSONObject localJSONObject3 = paramJSONObject.getJSONObject("prompt");
    ap localap = new ap(localJSONObject3.optString("title", "Send Crash Report?"), localJSONObject3.optString("message", "Looks like we crashed! Please help us fix the problem by sending a crash report."), localJSONObject3.optString("send_button_title", "Send"), localJSONObject3.optBoolean("show_cancel_button", true), localJSONObject3.optString("cancel_button_title", "Don't Send"), localJSONObject3.optBoolean("show_always_send_button", true), localJSONObject3.optString("always_send_button_title", "Always Send"));
    JSONObject localJSONObject4 = paramJSONObject.getJSONObject("features");
    boolean bool2 = localJSONObject4.optBoolean("prompt_enabled", false);
    localJSONObject4.optBoolean("collect_logged_exceptions", true);
    ao localao = new ao(bool2, localJSONObject4.optBoolean("collect_reports", true), localJSONObject4.optBoolean("collect_analytics", false));
    JSONObject localJSONObject5 = paramJSONObject.getJSONObject("analytics");
    String str4 = localJSONObject5.optString("url", "https://e.crashlytics.com/spi/v2/events");
    int k = localJSONObject5.optInt("flush_interval_secs", 600);
    int m = localJSONObject5.optInt("max_byte_size_per_file", 8000);
    localJSONObject5.optInt("max_file_count_per_send", 1);
    localJSONObject5.optInt("max_pending_send_file_count", 100);
    aj localaj = new aj(str4, k, m);
    long l1 = i;
    if (paramJSONObject.has("expires_at")) {}
    for (long l2 = paramJSONObject.getLong("expires_at");; l2 = System.currentTimeMillis() + l1 * 1000L) {
      return new aw(l2, localal, localaq, localap, localao, localaj);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/ax.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */