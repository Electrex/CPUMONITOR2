package com.a.a.a;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

final class f
  implements Application.ActivityLifecycleCallbacks
{
  f(e parame) {}
  
  public final void onActivityCreated(Activity paramActivity, Bundle paramBundle)
  {
    this.a.a(v.a, paramActivity);
  }
  
  public final void onActivityDestroyed(Activity paramActivity)
  {
    this.a.a(v.g, paramActivity);
  }
  
  public final void onActivityPaused(Activity paramActivity)
  {
    this.a.a(v.e, paramActivity);
  }
  
  public final void onActivityResumed(Activity paramActivity)
  {
    this.a.a(v.c, paramActivity);
  }
  
  public final void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle)
  {
    this.a.a(v.d, paramActivity);
  }
  
  public final void onActivityStarted(Activity paramActivity)
  {
    this.a.a(v.b, paramActivity);
  }
  
  public final void onActivityStopped(Activity paramActivity)
  {
    this.a.a(v.f, paramActivity);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */