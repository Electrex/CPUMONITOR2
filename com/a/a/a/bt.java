package com.a.a.a;

import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import javax.net.ssl.SSLSocketFactory;
import javax.security.auth.x500.X500Principal;

public final class bt
{
  af a;
  private final ci b;
  private SSLSocketFactory c;
  private boolean d;
  
  public bt()
  {
    this(new cj());
  }
  
  public bt(ci paramci)
  {
    this.b = paramci;
  }
  
  private static boolean a(X509Certificate paramX509Certificate1, X509Certificate paramX509Certificate2)
  {
    if (!paramX509Certificate1.getSubjectX500Principal().equals(paramX509Certificate2.getIssuerX500Principal())) {
      return false;
    }
    try
    {
      paramX509Certificate2.verify(paramX509Certificate1.getPublicKey());
      return true;
    }
    catch (GeneralSecurityException localGeneralSecurityException) {}
    return false;
  }
  
  public static X509Certificate[] a(X509Certificate[] paramArrayOfX509Certificate, ah paramah)
  {
    int i = 1;
    LinkedList localLinkedList = new LinkedList();
    if (paramah.a(paramArrayOfX509Certificate[0])) {}
    for (int j = i;; j = 0)
    {
      localLinkedList.add(paramArrayOfX509Certificate[0]);
      int k = j;
      for (int m = i; m < paramArrayOfX509Certificate.length; m++)
      {
        if (paramah.a(paramArrayOfX509Certificate[m])) {
          k = i;
        }
        if (!a(paramArrayOfX509Certificate[m], paramArrayOfX509Certificate[(m - 1)])) {
          break;
        }
        localLinkedList.add(paramArrayOfX509Certificate[m]);
      }
      X509Certificate localX509Certificate = paramah.b(paramArrayOfX509Certificate[(m - 1)]);
      if (localX509Certificate != null) {
        localLinkedList.add(localX509Certificate);
      }
      for (;;)
      {
        if (i != 0) {
          return (X509Certificate[])localLinkedList.toArray(new X509Certificate[localLinkedList.size()]);
        }
        throw new CertificateException("Didn't find a trust anchor in chain cleanup!");
        i = k;
      }
    }
  }
  
  /* Error */
  private SSLSocketFactory b()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: iconst_1
    //   4: putfield 89	com/a/a/a/bt:d	Z
    //   7: aload_0
    //   8: getfield 91	com/a/a/a/bt:a	Lcom/a/a/a/af;
    //   11: astore 4
    //   13: ldc 93
    //   15: invokestatic 99	javax/net/ssl/SSLContext:getInstance	(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    //   18: astore 5
    //   20: aload 5
    //   22: aconst_null
    //   23: iconst_1
    //   24: anewarray 101	javax/net/ssl/TrustManager
    //   27: dup
    //   28: iconst_0
    //   29: new 103	com/a/a/a/ag
    //   32: dup
    //   33: new 56	com/a/a/a/ah
    //   36: dup
    //   37: aload 4
    //   39: invokeinterface 108 1 0
    //   44: aload 4
    //   46: invokeinterface 111 1 0
    //   51: invokespecial 114	com/a/a/a/ah:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   54: aload 4
    //   56: invokespecial 117	com/a/a/a/ag:<init>	(Lcom/a/a/a/ah;Lcom/a/a/a/af;)V
    //   59: aastore
    //   60: aconst_null
    //   61: invokevirtual 121	javax/net/ssl/SSLContext:init	([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    //   64: aload 5
    //   66: invokevirtual 124	javax/net/ssl/SSLContext:getSocketFactory	()Ljavax/net/ssl/SSLSocketFactory;
    //   69: astore_3
    //   70: aload_0
    //   71: getfield 24	com/a/a/a/bt:b	Lcom/a/a/a/ci;
    //   74: invokeinterface 128 1 0
    //   79: aload_0
    //   80: monitorexit
    //   81: aload_3
    //   82: areturn
    //   83: astore_2
    //   84: aload_0
    //   85: getfield 24	com/a/a/a/bt:b	Lcom/a/a/a/ci;
    //   88: invokeinterface 130 1 0
    //   93: aconst_null
    //   94: astore_3
    //   95: goto -16 -> 79
    //   98: astore_1
    //   99: aload_0
    //   100: monitorexit
    //   101: aload_1
    //   102: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	103	0	this	bt
    //   98	4	1	localObject	Object
    //   83	1	2	localException	Exception
    //   69	26	3	localSSLSocketFactory	SSLSocketFactory
    //   11	44	4	localaf	af
    //   18	47	5	localSSLContext	javax.net.ssl.SSLContext
    // Exception table:
    //   from	to	target	type
    //   7	79	83	java/lang/Exception
    //   2	7	98	finally
    //   7	79	98	finally
    //   84	93	98	finally
  }
  
  final SSLSocketFactory a()
  {
    try
    {
      if ((this.c == null) && (!this.d)) {
        this.c = b();
      }
      SSLSocketFactory localSSLSocketFactory = this.c;
      return localSSLSocketFactory;
    }
    finally {}
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/bt.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */