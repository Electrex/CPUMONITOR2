package com.a.a.a;

import android.app.Activity;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;

public class n
  implements bi
{
  final String a;
  final String b;
  final String c;
  final String d;
  final String e;
  final String f;
  final String g;
  final String h;
  public final ScheduledExecutorService i;
  t j;
  
  public n(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, j paramj, bt parambt)
  {
    this(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramj, bf.a("Crashlytics SAM"), parambt);
  }
  
  n(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, j paramj, ScheduledExecutorService paramScheduledExecutorService, bt parambt)
  {
    this.a = paramString1;
    this.b = paramString2;
    this.c = paramString3;
    this.d = paramString4;
    this.e = paramString5;
    this.f = paramString6;
    this.g = paramString7;
    this.h = UUID.randomUUID().toString();
    this.i = paramScheduledExecutorService;
    this.j = new i(paramScheduledExecutorService, paramj, parambt);
    if (this != null) {
      paramj.c.add(this);
    }
  }
  
  void a()
  {
    a(new s(this));
  }
  
  final void a(u paramu, boolean paramBoolean)
  {
    a(new p(this, paramu, paramBoolean));
  }
  
  final void a(v paramv, Activity paramActivity)
  {
    a(u.a(this.a, this.h, this.b, this.c, this.d, this.e, this.f, this.g, paramv, Collections.singletonMap("activity", paramActivity.getClass().getName())), false);
  }
  
  final void a(Runnable paramRunnable)
  {
    try
    {
      this.i.submit(paramRunnable);
      return;
    }
    catch (Exception localException)
    {
      ba.e();
    }
  }
  
  public final void b()
  {
    a(new r(this));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */