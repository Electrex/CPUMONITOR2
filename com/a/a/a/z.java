package com.a.a.a;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

public abstract class z<V>
  extends ad<V>
{
  private final Closeable a;
  private final boolean b;
  
  protected z(Closeable paramCloseable, boolean paramBoolean)
  {
    this.a = paramCloseable;
    this.b = paramBoolean;
  }
  
  protected final void a()
  {
    if ((this.a instanceof Flushable)) {
      ((Flushable)this.a).flush();
    }
    if (this.b) {}
    try
    {
      this.a.close();
      return;
    }
    catch (IOException localIOException) {}
    this.a.close();
    return;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/z.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */