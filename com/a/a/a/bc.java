package com.a.a.a;

import android.os.Build;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

 enum bc
{
  private static final Map<String, bc> k;
  
  static
  {
    bc[] arrayOfbc = new bc[10];
    arrayOfbc[0] = a;
    arrayOfbc[1] = b;
    arrayOfbc[2] = c;
    arrayOfbc[3] = d;
    arrayOfbc[4] = e;
    arrayOfbc[5] = f;
    arrayOfbc[6] = g;
    arrayOfbc[7] = h;
    arrayOfbc[8] = i;
    arrayOfbc[9] = j;
    l = arrayOfbc;
    HashMap localHashMap = new HashMap(4);
    k = localHashMap;
    localHashMap.put("armeabi-v7a", g);
    k.put("armeabi", f);
    k.put("x86", a);
  }
  
  private bc() {}
  
  static bc a()
  {
    String str1 = Build.CPU_ABI;
    bc localbc;
    if (TextUtils.isEmpty(str1))
    {
      co.a().a().b();
      localbc = h;
    }
    do
    {
      return localbc;
      String str2 = str1.toLowerCase(Locale.US);
      localbc = (bc)k.get(str2);
    } while (localbc != null);
    return h;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/bc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */