package com.a.a;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import com.a.a.a.ba;
import com.a.a.a.ci;
import com.a.a.a.cl;
import com.a.a.a.co;

final class aa
{
  public final String a;
  public final int b;
  public final int c;
  public final int d;
  
  private aa(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    this.a = paramString;
    this.b = paramInt1;
    this.c = paramInt2;
    this.d = paramInt3;
  }
  
  public static aa a(Context paramContext, String paramString)
  {
    if (paramString != null) {
      try
      {
        int i = ba.h(paramContext);
        co.a().a().b();
        BitmapFactory.Options localOptions = new BitmapFactory.Options();
        localOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(paramContext.getResources(), i, localOptions);
        aa localaa = new aa(paramString, i, localOptions.outWidth, localOptions.outHeight);
        return localaa;
      }
      catch (Exception localException)
      {
        co.a().a().a();
      }
    }
    return null;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/aa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */