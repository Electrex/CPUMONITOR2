package com.a.a;

import com.a.a.a.ci;
import com.a.a.a.cl;
import com.a.a.a.co;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public final class e
  extends RuntimeException
{
  private static final long serialVersionUID = -1151536370019872859L;
  
  e(String paramString1, String paramString2)
  {
    super(a(paramString1, paramString2));
  }
  
  private static String a(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder1 = new StringBuilder();
    try
    {
      localStringBuilder1.append("\nThis app relies on Crashlytics. Configure your build environment here: \n");
      StringBuilder localStringBuilder2 = new StringBuilder();
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = URLEncoder.encode(paramString1, "UTF-8");
      arrayOfObject[1] = URLEncoder.encode(paramString2, "UTF-8");
      localStringBuilder1.append(String.format("https://crashlytics.com/register/%s/android/%s", arrayOfObject) + "\n");
      return localStringBuilder1.toString();
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;)
      {
        co.a().a().a();
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */