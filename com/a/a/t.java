package com.a.a;

import java.io.File;
import java.io.FilenameFilter;

final class t
  implements FilenameFilter
{
  private final String a;
  
  public t(String paramString)
  {
    this.a = paramString;
  }
  
  public final boolean accept(File paramFile, String paramString)
  {
    return (paramString.contains(this.a)) && (!paramString.endsWith(".cls_temp"));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/t.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */