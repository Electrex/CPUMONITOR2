package com.a.a;

import com.a.a.a.ci;
import com.a.a.a.cl;
import com.a.a.a.co;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

final class ab
{
  final File a;
  final Map<String, String> b;
  
  public ab(File paramFile)
  {
    this(paramFile, Collections.emptyMap());
  }
  
  public ab(File paramFile, Map<String, String> paramMap)
  {
    this.a = paramFile;
    this.b = new HashMap(paramMap);
    if (this.a.length() == 0L) {
      this.b.putAll(ad.a);
    }
  }
  
  public final boolean a()
  {
    ci localci = co.a().a();
    new StringBuilder("Removing report at ").append(this.a.getPath());
    localci.b();
    return this.a.delete();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/ab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */