package com.a.a;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class s
  implements FilenameFilter
{
  public final boolean accept(File paramFile, String paramString)
  {
    return (!az.a.accept(paramFile, paramString)) && (az.b().matcher(paramString).matches());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/s.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */