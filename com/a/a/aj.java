package com.a.a;

import java.io.UnsupportedEncodingException;

final class aj
{
  final byte[] a;
  private volatile int b = 0;
  
  static
  {
    new aj(new byte[0]);
  }
  
  private aj(byte[] paramArrayOfByte)
  {
    this.a = paramArrayOfByte;
  }
  
  public static aj a(String paramString)
  {
    try
    {
      aj localaj = new aj(paramString.getBytes("UTF-8"));
      return localaj;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new RuntimeException("UTF-8 not supported.", localUnsupportedEncodingException);
    }
  }
  
  public static aj a(byte[] paramArrayOfByte, int paramInt)
  {
    byte[] arrayOfByte = new byte[paramInt];
    System.arraycopy(paramArrayOfByte, 0, arrayOfByte, 0, paramInt);
    return new aj(arrayOfByte);
  }
  
  public final void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    System.arraycopy(this.a, paramInt1, paramArrayOfByte, paramInt2, paramInt3);
  }
  
  public final boolean equals(Object paramObject)
  {
    if (paramObject == this) {}
    for (;;)
    {
      return true;
      if (!(paramObject instanceof aj)) {
        return false;
      }
      aj localaj = (aj)paramObject;
      int i = this.a.length;
      if (i != localaj.a.length) {
        return false;
      }
      byte[] arrayOfByte1 = this.a;
      byte[] arrayOfByte2 = localaj.a;
      for (int j = 0; j < i; j++) {
        if (arrayOfByte1[j] != arrayOfByte2[j]) {
          return false;
        }
      }
    }
  }
  
  public final int hashCode()
  {
    int i = this.b;
    if (i == 0)
    {
      byte[] arrayOfByte = this.a;
      int j = this.a.length;
      int k = 0;
      int m;
      for (i = j; k < j; i = m)
      {
        m = i * 31 + arrayOfByte[k];
        k++;
      }
      if (i == 0) {
        i = 1;
      }
      this.b = i;
    }
    return i;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/aj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */