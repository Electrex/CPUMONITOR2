package com.a.a;

import com.a.a.a.bt;
import com.a.a.a.bv;
import com.a.a.a.bw;
import com.a.a.a.ci;
import com.a.a.a.cj;
import com.a.a.a.cl;
import com.a.a.a.co;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class y
  extends com.a.a.a.y
  implements x
{
  public y(String paramString1, String paramString2, bt parambt)
  {
    super(paramString1, paramString2, parambt, bv.b);
  }
  
  public final boolean a(w paramw)
  {
    bw localbw1 = a().a("X-CRASHLYTICS-API-KEY", paramw.a).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    d.a();
    bw localbw2 = localbw1.a("X-CRASHLYTICS-API-CLIENT-VERSION", d.c());
    Iterator localIterator = Collections.unmodifiableMap(paramw.b.b).entrySet().iterator();
    Map.Entry localEntry;
    for (bw localbw3 = localbw2; localIterator.hasNext(); localbw3 = localbw3.a((String)localEntry.getKey(), (String)localEntry.getValue())) {
      localEntry = (Map.Entry)localIterator.next();
    }
    ab localab = paramw.b;
    bw localbw4 = localbw3.a("report[file]", localab.a.getName(), "application/octet-stream", localab.a);
    String str = localab.a.getName();
    bw localbw5 = localbw4.c("report[identifier]", str.substring(0, str.lastIndexOf('.')));
    ci localci1 = co.a().a();
    new StringBuilder("Sending report to: ").append(this.a);
    localci1.b();
    int i = localbw5.b();
    ci localci2 = co.a().a();
    new StringBuilder("Create report request ID: ").append(localbw5.b("X-REQUEST-ID"));
    localci2.b();
    co.a().a().b();
    return cj.a(i) == 0;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/y.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */