package com.a.a;

import java.io.ByteArrayInputStream;
import java.io.Flushable;
import java.io.InputStream;
import java.io.OutputStream;

final class an
  implements Flushable
{
  private final byte[] a;
  private final int b;
  private int c;
  private final OutputStream d;
  
  private an(OutputStream paramOutputStream, byte[] paramArrayOfByte)
  {
    this.d = paramOutputStream;
    this.a = paramArrayOfByte;
    this.c = 0;
    this.b = 4096;
  }
  
  public static int a()
  {
    return 4 + e(1);
  }
  
  public static an a(OutputStream paramOutputStream)
  {
    return new an(paramOutputStream, new byte['က']);
  }
  
  private void a(long paramLong)
  {
    for (;;)
    {
      if ((0xFFFFFFFFFFFFFF80 & paramLong) == 0L)
      {
        d((int)paramLong);
        return;
      }
      d(0x80 | 0x7F & (int)paramLong);
      paramLong >>>= 7;
    }
  }
  
  public static int b(int paramInt)
  {
    return 1 + e(paramInt);
  }
  
  public static int b(int paramInt, long paramLong)
  {
    int i = e(paramInt);
    int j;
    if ((0xFFFFFFFFFFFFFF80 & paramLong) == 0L) {
      j = 1;
    }
    for (;;)
    {
      return j + i;
      if ((0xFFFFFFFFFFFFC000 & paramLong) == 0L) {
        j = 2;
      } else if ((0xFFFFFFFFFFE00000 & paramLong) == 0L) {
        j = 3;
      } else if ((0xFFFFFFFFF0000000 & paramLong) == 0L) {
        j = 4;
      } else if ((0xFFFFFFF800000000 & paramLong) == 0L) {
        j = 5;
      } else if ((0xFFFFFC0000000000 & paramLong) == 0L) {
        j = 6;
      } else if ((0xFFFE000000000000 & paramLong) == 0L) {
        j = 7;
      } else if ((0xFF00000000000000 & paramLong) == 0L) {
        j = 8;
      } else if ((0x8000000000000000 & paramLong) == 0L) {
        j = 9;
      } else {
        j = 10;
      }
    }
  }
  
  public static int b(int paramInt, aj paramaj)
  {
    return e(paramInt) + (g(paramaj.a.length) + paramaj.a.length);
  }
  
  private void b()
  {
    if (this.d == null) {
      throw new ao();
    }
    this.d.write(this.a, 0, this.c);
    this.c = 0;
  }
  
  public static int c(int paramInt)
  {
    return e(2) + g(h(paramInt));
  }
  
  public static int c(int paramInt1, int paramInt2)
  {
    return e(paramInt1) + g(paramInt2);
  }
  
  public static int d(int paramInt1, int paramInt2)
  {
    int i = e(paramInt1);
    if (paramInt2 >= 0) {}
    for (int j = g(paramInt2);; j = 10) {
      return j + i;
    }
  }
  
  public static int e(int paramInt)
  {
    return g(0x0 | paramInt << 3);
  }
  
  public static int g(int paramInt)
  {
    if ((paramInt & 0xFFFFFF80) == 0) {
      return 1;
    }
    if ((paramInt & 0xC000) == 0) {
      return 2;
    }
    if ((0xFFE00000 & paramInt) == 0) {
      return 3;
    }
    if ((0xF0000000 & paramInt) == 0) {
      return 4;
    }
    return 5;
  }
  
  private static int h(int paramInt)
  {
    return paramInt << 1 ^ paramInt >> 31;
  }
  
  public final void a(int paramInt)
  {
    e(2, 0);
    f(h(paramInt));
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    e(paramInt1, 0);
    f(paramInt2);
  }
  
  public final void a(int paramInt, long paramLong)
  {
    e(paramInt, 0);
    a(paramLong);
  }
  
  public final void a(int paramInt, aj paramaj)
  {
    e(paramInt, 2);
    f(paramaj.a.length);
    int i = paramaj.a.length;
    if (this.b - this.c >= i)
    {
      paramaj.a(this.a, 0, this.c, i);
      this.c = (i + this.c);
      return;
    }
    int j = this.b - this.c;
    paramaj.a(this.a, 0, this.c, j);
    int k = j + 0;
    int m = i - j;
    this.c = this.b;
    b();
    if (m <= this.b)
    {
      paramaj.a(this.a, k, 0, m);
      this.c = m;
      return;
    }
    ByteArrayInputStream localByteArrayInputStream = new ByteArrayInputStream(paramaj.a);
    if (k != localByteArrayInputStream.skip(k)) {
      throw new IllegalStateException("Skip failed.");
    }
    int i1;
    int n;
    do
    {
      this.d.write(this.a, 0, i1);
      m -= i1;
      if (m <= 0) {
        break;
      }
      n = Math.min(m, this.b);
      i1 = localByteArrayInputStream.read(this.a, 0, n);
    } while (i1 == n);
    throw new IllegalStateException("Read failed.");
  }
  
  public final void a(int paramInt, boolean paramBoolean)
  {
    e(paramInt, 0);
    int i = 0;
    if (paramBoolean) {
      i = 1;
    }
    d(i);
  }
  
  public final void a(String paramString)
  {
    e(1, 2);
    byte[] arrayOfByte = paramString.getBytes("UTF-8");
    f(arrayOfByte.length);
    a(arrayOfByte);
  }
  
  public final void a(byte[] paramArrayOfByte)
  {
    int i = paramArrayOfByte.length;
    if (this.b - this.c >= i)
    {
      System.arraycopy(paramArrayOfByte, 0, this.a, this.c, i);
      this.c = (i + this.c);
      return;
    }
    int j = this.b - this.c;
    System.arraycopy(paramArrayOfByte, 0, this.a, this.c, j);
    int k = j + 0;
    int m = i - j;
    this.c = this.b;
    b();
    if (m <= this.b)
    {
      System.arraycopy(paramArrayOfByte, k, this.a, 0, m);
      this.c = m;
      return;
    }
    this.d.write(paramArrayOfByte, k, m);
  }
  
  public final void b(int paramInt1, int paramInt2)
  {
    e(paramInt1, 0);
    if (paramInt2 >= 0)
    {
      f(paramInt2);
      return;
    }
    a(paramInt2);
  }
  
  final void d(int paramInt)
  {
    int i = (byte)paramInt;
    if (this.c == this.b) {
      b();
    }
    byte[] arrayOfByte = this.a;
    int j = this.c;
    this.c = (j + 1);
    arrayOfByte[j] = i;
  }
  
  public final void e(int paramInt1, int paramInt2)
  {
    f(paramInt2 | paramInt1 << 3);
  }
  
  public final void f(int paramInt)
  {
    for (;;)
    {
      if ((paramInt & 0xFFFFFF80) == 0)
      {
        d(paramInt);
        return;
      }
      d(0x80 | paramInt & 0x7F);
      paramInt >>>= 7;
    }
  }
  
  public final void flush()
  {
    if (this.d != null) {
      b();
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/an.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */