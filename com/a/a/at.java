package com.a.a;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.widget.ScrollView;
import android.widget.TextView;
import com.a.a.a.ap;

final class at
  implements Runnable
{
  at(d paramd, Activity paramActivity, ay paramay, z paramz, ap paramap) {}
  
  public final void run()
  {
    AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(this.c);
    au localau = new au(this);
    float f = this.c.getResources().getDisplayMetrics().density;
    int i = d.a(f, 5);
    TextView localTextView = new TextView(this.c);
    localTextView.setAutoLinkMask(15);
    z localz1 = this.d;
    localTextView.setText(localz1.a("com.crashlytics.CrashSubmissionPromptMessage", localz1.a.b));
    localTextView.setTextAppearance(this.c, 16973892);
    localTextView.setPadding(i, i, i, i);
    localTextView.setFocusable(false);
    ScrollView localScrollView = new ScrollView(this.c);
    localScrollView.setPadding(d.a(f, 14), d.a(f, 2), d.a(f, 10), d.a(f, 12));
    localScrollView.addView(localTextView);
    AlertDialog.Builder localBuilder2 = localBuilder1.setView(localScrollView);
    z localz2 = this.d;
    AlertDialog.Builder localBuilder3 = localBuilder2.setTitle(localz2.a("com.crashlytics.CrashSubmissionPromptTitle", localz2.a.a)).setCancelable(false);
    z localz3 = this.d;
    localBuilder3.setNeutralButton(localz3.a("com.crashlytics.CrashSubmissionSendTitle", localz3.a.c), localau);
    if (this.e.d)
    {
      av localav = new av(this);
      z localz4 = this.d;
      localBuilder1.setNegativeButton(localz4.a("com.crashlytics.CrashSubmissionCancelTitle", localz4.a.e), localav);
    }
    if (this.e.f)
    {
      aw localaw = new aw(this);
      z localz5 = this.d;
      localBuilder1.setPositiveButton(localz5.a("com.crashlytics.CrashSubmissionAlwaysSendTitle", localz5.a.g), localaw);
    }
    localBuilder1.show();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/at.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */