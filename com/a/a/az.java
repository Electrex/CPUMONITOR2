package com.a.a;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Environment;
import com.a.a.a.bn;
import com.a.a.a.bo;
import com.a.a.a.ch;
import com.a.a.a.ci;
import com.a.a.a.cj;
import com.a.a.a.cl;
import com.a.a.a.co;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

final class az
  implements Thread.UncaughtExceptionHandler
{
  static final FilenameFilter a = new ba();
  private static Comparator<File> c = new j();
  private static Comparator<File> d = new l();
  private static final Pattern e = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
  private static final Map<String, String> f = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
  private static final aj g = aj.a("0");
  final AtomicBoolean b;
  private final AtomicInteger h = new AtomicInteger(0);
  private final AtomicBoolean i = new AtomicBoolean(false);
  private final int j;
  private final Thread.UncaughtExceptionHandler k;
  private final File l;
  private final File m;
  private final String n;
  private final BroadcastReceiver o;
  private final BroadcastReceiver p;
  private final aj q;
  private final aj r;
  private final ExecutorService s;
  private ActivityManager.RunningAppProcessInfo t;
  private bo u;
  private boolean v;
  private Thread[] w;
  private List<StackTraceElement[]> x;
  private StackTraceElement[] y;
  
  static
  {
    new m();
  }
  
  public az(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler, String paramString)
  {
    this(paramUncaughtExceptionHandler, localExecutorService, paramString);
  }
  
  private az(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler, ExecutorService paramExecutorService, String paramString)
  {
    this.k = paramUncaughtExceptionHandler;
    this.s = paramExecutorService;
    this.b = new AtomicBoolean(false);
    this.l = co.a().c;
    this.m = new File(this.l, "initialization_marker");
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[1];
    d.a();
    arrayOfObject[0] = d.c();
    this.n = String.format(localLocale, "Crashlytics Android SDK/%s", arrayOfObject);
    this.j = 8;
    co.a().a().b();
    File localFile = new File(co.a().c, "crash_marker");
    if (localFile.exists()) {
      localFile.delete();
    }
    this.q = aj.a(d.d());
    if (paramString == null) {}
    for (aj localaj = null;; localaj = aj.a(paramString.replace("-", "")))
    {
      this.r = localaj;
      this.p = new n(this);
      IntentFilter localIntentFilter1 = new IntentFilter("android.intent.action.ACTION_POWER_CONNECTED");
      this.o = new o(this);
      IntentFilter localIntentFilter2 = new IntentFilter("android.intent.action.ACTION_POWER_DISCONNECTED");
      d.a().g.registerReceiver(this.p, localIntentFilter1);
      d.a().g.registerReceiver(this.o, localIntentFilter2);
      this.i.set(true);
      return;
    }
  }
  
  private static int a(int paramInt1, int paramInt2, long paramLong1, long paramLong2)
  {
    return 0 + an.a() + an.c(paramInt1) + an.b(3) + an.c(4, paramInt2) + an.b(5, paramLong1) + an.b(6, paramLong2);
  }
  
  private static int a(int paramInt1, aj paramaj1, aj paramaj2, int paramInt2, long paramLong1, long paramLong2, Map<bn, String> paramMap, int paramInt3, aj paramaj3, aj paramaj4)
  {
    int i1 = 0 + an.b(1, paramaj1) + an.d(3, paramInt1);
    if (paramaj2 == null) {}
    int i3;
    for (int i2 = 0;; i2 = an.b(4, paramaj2))
    {
      i3 = i2 + i1 + an.c(5, paramInt2) + an.b(6, paramLong1) + an.b(7, paramLong2) + an.b(10);
      if (paramMap == null) {
        break;
      }
      Iterator localIterator = paramMap.entrySet().iterator();
      i4 = i3;
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        int i9 = a((bn)localEntry.getKey(), (String)localEntry.getValue());
        i4 += i9 + (an.e(11) + an.g(i9));
      }
    }
    int i4 = i3;
    int i5 = i4 + an.c(12, paramInt3);
    int i6;
    int i7;
    if (paramaj3 == null)
    {
      i6 = 0;
      i7 = i5 + i6;
      if (paramaj4 != null) {
        break label216;
      }
    }
    label216:
    for (int i8 = 0;; i8 = an.b(14, paramaj4))
    {
      return i8 + i7;
      i6 = an.b(13, paramaj3);
      break;
    }
  }
  
  private static int a(bn parambn, String paramString)
  {
    return an.d(1, parambn.f) + an.b(2, aj.a(paramString));
  }
  
  private static int a(StackTraceElement paramStackTraceElement, boolean paramBoolean)
  {
    int i1;
    int i2;
    if (paramStackTraceElement.isNativeMethod())
    {
      i1 = 0 + an.b(1, Math.max(paramStackTraceElement.getLineNumber(), 0));
      i2 = i1 + an.b(2, aj.a(paramStackTraceElement.getClassName() + "." + paramStackTraceElement.getMethodName()));
      if (paramStackTraceElement.getFileName() != null) {
        i2 += an.b(3, aj.a(paramStackTraceElement.getFileName()));
      }
      if ((paramStackTraceElement.isNativeMethod()) || (paramStackTraceElement.getLineNumber() <= 0)) {
        break label145;
      }
    }
    label145:
    for (int i3 = i2 + an.b(4, paramStackTraceElement.getLineNumber());; i3 = i2)
    {
      if (paramBoolean) {}
      for (int i4 = 2;; i4 = 0)
      {
        return i3 + an.c(5, i4);
        i1 = 0 + an.b(1, 0L);
        break;
      }
    }
  }
  
  private static int a(String paramString1, String paramString2)
  {
    int i1 = an.b(1, aj.a(paramString1));
    if (paramString2 == null) {
      paramString2 = "";
    }
    return i1 + an.b(2, aj.a(paramString2));
  }
  
  private int a(Thread paramThread, Throwable paramThrowable)
  {
    int i1 = a(paramThread, this.y, 4, true);
    int i2 = 0 + (i1 + (an.e(1) + an.g(i1)));
    int i3 = this.w.length;
    int i4 = 0;
    int i5 = i2;
    while (i4 < i3)
    {
      int i11 = a(this.w[i4], (StackTraceElement[])this.x.get(i4), 0, false);
      i5 += i11 + (an.e(1) + an.g(i11));
      i4++;
    }
    int i6 = a(paramThrowable, 1);
    int i7 = i5 + (i6 + (an.e(2) + an.g(i6)));
    int i8 = j();
    int i9 = i7 + (i8 + (an.e(3) + an.g(i8)));
    int i10 = i();
    return i9 + (i10 + (an.e(3) + an.g(i10)));
  }
  
  private int a(Thread paramThread, Throwable paramThrowable, Map<String, String> paramMap)
  {
    int i1 = a(paramThread, paramThrowable);
    int i2 = 0 + (i1 + (an.e(1) + an.g(i1)));
    if (paramMap != null)
    {
      Iterator localIterator = paramMap.entrySet().iterator();
      i3 = i2;
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        int i4 = a((String)localEntry.getKey(), (String)localEntry.getValue());
        i3 += i4 + (an.e(2) + an.g(i4));
      }
    }
    int i3 = i2;
    if (this.t != null) {
      i3 += an.b(3);
    }
    return i3 + an.c(4, d.a().g.getResources().getConfiguration().orientation);
  }
  
  private static int a(Thread paramThread, StackTraceElement[] paramArrayOfStackTraceElement, int paramInt, boolean paramBoolean)
  {
    int i1 = an.b(1, aj.a(paramThread.getName())) + an.c(2, paramInt);
    int i2 = paramArrayOfStackTraceElement.length;
    for (int i3 = 0; i3 < i2; i3++)
    {
      int i4 = a(paramArrayOfStackTraceElement[i3], paramBoolean);
      i1 += i4 + (an.e(3) + an.g(i4));
    }
    return i1;
  }
  
  private int a(Throwable paramThrowable, int paramInt)
  {
    int i1 = 0 + an.b(1, aj.a(paramThrowable.getClass().getName()));
    String str = paramThrowable.getLocalizedMessage();
    if (str != null) {
      i1 += an.b(3, aj.a(str));
    }
    StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
    int i2 = arrayOfStackTraceElement.length;
    int i3 = 0;
    while (i3 < i2)
    {
      int i6 = a(arrayOfStackTraceElement[i3], true);
      int i7 = i1 + (i6 + (an.e(4) + an.g(i6)));
      i3++;
      i1 = i7;
    }
    Throwable localThrowable = paramThrowable.getCause();
    int i4;
    if (localThrowable != null)
    {
      i4 = 0;
      if (paramInt < 8)
      {
        int i5 = a(localThrowable, paramInt + 1);
        i1 += i5 + (an.e(6) + an.g(i5));
      }
    }
    else
    {
      return i1;
    }
    while (localThrowable != null)
    {
      localThrowable = localThrowable.getCause();
      i4++;
    }
    return i1 + an.c(7, i4);
  }
  
  private static aj a(bo parambo)
  {
    if (parambo == null) {
      return null;
    }
    int[] arrayOfInt = { 0 };
    byte[] arrayOfByte = new byte[parambo.a()];
    try
    {
      parambo.a(new bb(arrayOfByte, arrayOfInt));
      return aj.a(arrayOfByte, arrayOfInt[0]);
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        co.a().a().a();
      }
    }
  }
  
  static String a(File paramFile)
  {
    return paramFile.getName().substring(0, 35);
  }
  
  private static void a(an paraman, int paramInt, StackTraceElement paramStackTraceElement, boolean paramBoolean)
  {
    int i1 = 4;
    paraman.e(paramInt, 2);
    paraman.f(a(paramStackTraceElement, paramBoolean));
    if (paramStackTraceElement.isNativeMethod())
    {
      paraman.a(1, Math.max(paramStackTraceElement.getLineNumber(), 0));
      paraman.a(2, aj.a(paramStackTraceElement.getClassName() + "." + paramStackTraceElement.getMethodName()));
      if (paramStackTraceElement.getFileName() != null) {
        paraman.a(3, aj.a(paramStackTraceElement.getFileName()));
      }
      if ((!paramStackTraceElement.isNativeMethod()) && (paramStackTraceElement.getLineNumber() > 0)) {
        paraman.a(i1, paramStackTraceElement.getLineNumber());
      }
      if (!paramBoolean) {
        break label142;
      }
    }
    for (;;)
    {
      paraman.a(5, i1);
      return;
      paraman.a(1, 0L);
      break;
      label142:
      i1 = 0;
    }
  }
  
  private static void a(an paraman, File paramFile)
  {
    if (paramFile.exists())
    {
      byte[] arrayOfByte = new byte[(int)paramFile.length()];
      try
      {
        FileInputStream localFileInputStream = new FileInputStream(paramFile);
        int i1 = 0;
        int i2;
        com.a.a.a.ba.a(localFileInputStream);
      }
      finally
      {
        try
        {
          while (i1 < arrayOfByte.length)
          {
            i2 = localFileInputStream.read(arrayOfByte, i1, arrayOfByte.length - i1);
            if (i2 < 0) {
              break;
            }
            i1 += i2;
          }
          com.a.a.a.ba.a(localFileInputStream);
          paraman.a(arrayOfByte);
          return;
        }
        finally
        {
          ci localci;
          for (;;) {}
        }
        localObject1 = finally;
        localFileInputStream = null;
      }
      throw ((Throwable)localObject1);
    }
    localci = co.a().a();
    new StringBuilder("Tried to include a file that doesn't exist: ").append(paramFile.getName());
    localci.a();
  }
  
  private void a(an paraman, String paramString)
  {
    String[] arrayOfString = { "SessionUser", "SessionApp", "SessionOS", "SessionDevice" };
    int i1 = 0;
    if (i1 < 4)
    {
      String str = arrayOfString[i1];
      File[] arrayOfFile = a(new t(paramString + str));
      if (arrayOfFile.length == 0)
      {
        ci localci2 = co.a().a();
        new StringBuilder("Can't find ").append(str).append(" data for session ID ").append(paramString);
        localci2.a();
      }
      for (;;)
      {
        i1++;
        break;
        ci localci1 = co.a().a();
        new StringBuilder("Collecting ").append(str).append(" data for session ID ").append(paramString);
        localci1.b();
        a(paraman, arrayOfFile[0]);
      }
    }
  }
  
  private void a(an paraman, Thread paramThread, Throwable paramThrowable)
  {
    paraman.e(1, 2);
    paraman.f(a(paramThread, paramThrowable));
    a(paraman, paramThread, this.y, 4, true);
    int i1 = this.w.length;
    for (int i2 = 0; i2 < i1; i2++) {
      a(paraman, this.w[i2], (StackTraceElement[])this.x.get(i2), 0, false);
    }
    int i3 = 2;
    int i4 = 1;
    for (;;)
    {
      paraman.e(i3, 2);
      paraman.f(a(paramThrowable, 1));
      paraman.a(1, aj.a(paramThrowable.getClass().getName()));
      String str = paramThrowable.getLocalizedMessage();
      if (str != null) {
        paraman.a(3, aj.a(str));
      }
      StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
      int i5 = arrayOfStackTraceElement.length;
      for (int i6 = 0; i6 < i5; i6++) {
        a(paraman, 4, arrayOfStackTraceElement[i6], true);
      }
      paramThrowable = paramThrowable.getCause();
      if (paramThrowable == null) {
        break label226;
      }
      if (i4 >= 8) {
        break;
      }
      i4++;
      i3 = 6;
    }
    for (int i7 = 0; paramThrowable != null; i7++) {
      paramThrowable = paramThrowable.getCause();
    }
    paraman.a(7, i7);
    label226:
    paraman.e(3, 2);
    paraman.f(j());
    paraman.a(1, g);
    paraman.a(2, g);
    paraman.a(3, 0L);
    paraman.e(4, 2);
    paraman.f(i());
    paraman.a(1, 0L);
    paraman.a(2, 0L);
    paraman.a(3, this.q);
    if (this.r != null) {
      paraman.a(4, this.r);
    }
  }
  
  private static void a(an paraman, Thread paramThread, StackTraceElement[] paramArrayOfStackTraceElement, int paramInt, boolean paramBoolean)
  {
    paraman.e(1, 2);
    paraman.f(a(paramThread, paramArrayOfStackTraceElement, paramInt, paramBoolean));
    paraman.a(1, aj.a(paramThread.getName()));
    paraman.a(2, paramInt);
    int i1 = paramArrayOfStackTraceElement.length;
    for (int i2 = 0; i2 < i1; i2++) {
      a(paraman, 3, paramArrayOfStackTraceElement[i2], paramBoolean);
    }
  }
  
  private static void a(an paraman, Map<String, String> paramMap)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      paraman.e(2, 2);
      paraman.f(a((String)localEntry.getKey(), (String)localEntry.getValue()));
      paraman.a(1, aj.a((String)localEntry.getKey()));
      String str = (String)localEntry.getValue();
      if (str == null) {
        str = "";
      }
      paraman.a(2, aj.a(str));
    }
  }
  
  private static void a(an paraman, File[] paramArrayOfFile, String paramString)
  {
    int i1 = 0;
    Arrays.sort(paramArrayOfFile, com.a.a.a.ba.a);
    int i2 = paramArrayOfFile.length;
    for (;;)
    {
      if (i1 < i2)
      {
        File localFile = paramArrayOfFile[i1];
        try
        {
          ci localci = co.a().a();
          Locale localLocale = Locale.US;
          Object[] arrayOfObject = new Object[2];
          arrayOfObject[0] = paramString;
          arrayOfObject[1] = localFile.getName();
          String.format(localLocale, "Found Non Fatal for session ID %s in %s ", arrayOfObject);
          localci.b();
          a(paraman, localFile);
          i1++;
        }
        catch (Exception localException)
        {
          for (;;)
          {
            co.a().a().a();
          }
        }
      }
    }
  }
  
  private void a(String paramString)
  {
    File[] arrayOfFile = a(new u(paramString));
    int i1 = arrayOfFile.length;
    for (int i2 = 0; i2 < i1; i2++) {
      arrayOfFile[i2].delete();
    }
  }
  
  /* Error */
  private static void a(Throwable paramThrowable, java.io.OutputStream paramOutputStream)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +23 -> 24
    //   4: aconst_null
    //   5: astore_2
    //   6: new 588	java/io/PrintWriter
    //   9: dup
    //   10: aload_1
    //   11: invokespecial 591	java/io/PrintWriter:<init>	(Ljava/io/OutputStream;)V
    //   14: astore_3
    //   15: aload_0
    //   16: aload_3
    //   17: invokestatic 594	com/a/a/az:a	(Ljava/lang/Throwable;Ljava/io/Writer;)V
    //   20: aload_3
    //   21: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   24: return
    //   25: astore 7
    //   27: aconst_null
    //   28: astore_3
    //   29: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   32: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   35: invokeinterface 454 1 0
    //   40: aload_3
    //   41: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   44: return
    //   45: astore 6
    //   47: aload_2
    //   48: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   51: aload 6
    //   53: athrow
    //   54: astore 5
    //   56: aload_3
    //   57: astore_2
    //   58: aload 5
    //   60: astore 6
    //   62: goto -15 -> 47
    //   65: astore 4
    //   67: goto -38 -> 29
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	70	0	paramThrowable	Throwable
    //   0	70	1	paramOutputStream	java.io.OutputStream
    //   5	53	2	localObject1	Object
    //   14	43	3	localPrintWriter	java.io.PrintWriter
    //   65	1	4	localException1	Exception
    //   54	5	5	localObject2	Object
    //   45	7	6	localObject3	Object
    //   60	1	6	localObject4	Object
    //   25	1	7	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   6	15	25	java/lang/Exception
    //   6	15	45	finally
    //   15	20	54	finally
    //   29	40	54	finally
    //   15	20	65	java/lang/Exception
  }
  
  private static void a(Throwable paramThrowable, Writer paramWriter)
  {
    int i1 = 1;
    if (paramThrowable != null) {}
    for (;;)
    {
      String str2;
      try
      {
        String str1 = paramThrowable.getLocalizedMessage();
        if (str1 == null)
        {
          str2 = null;
          break label177;
          paramWriter.write(str4 + paramThrowable.getClass().getName() + ": " + str3 + "\n");
          StackTraceElement[] arrayOfStackTraceElement = paramThrowable.getStackTrace();
          int i2 = arrayOfStackTraceElement.length;
          int i3 = 0;
          if (i3 < i2)
          {
            StackTraceElement localStackTraceElement = arrayOfStackTraceElement[i3];
            paramWriter.write("\tat " + localStackTraceElement.toString() + "\n");
            i3++;
            continue;
          }
        }
        else
        {
          str2 = str1.replaceAll("(\r\n|\n|\f)", " ");
          break label177;
        }
        Throwable localThrowable = paramThrowable.getCause();
        paramThrowable = localThrowable;
        i1 = 0;
      }
      catch (Exception localException)
      {
        co.a().a().a();
      }
      return;
      label177:
      if (str2 != null) {}
      for (String str3 = str2;; str3 = "")
      {
        if (i1 == 0) {
          break label204;
        }
        str4 = "";
        break;
      }
      label204:
      String str4 = "Caused by: ";
    }
  }
  
  private void a(Date paramDate, an paraman, Thread paramThread, Throwable paramThrowable, String paramString)
  {
    long l1 = paramDate.getTime() / 1000L;
    float f1 = com.a.a.a.ba.b(d.a().g);
    int i1 = com.a.a.a.ba.a(this.v);
    boolean bool1 = com.a.a.a.ba.c(d.a().g);
    int i2 = d.a().g.getResources().getConfiguration().orientation;
    long l2 = com.a.a.a.ba.c() - com.a.a.a.ba.a(d.a().g);
    long l3 = com.a.a.a.ba.b(Environment.getDataDirectory().getPath());
    this.t = com.a.a.a.ba.a(d.d(), d.a().g);
    this.x = new LinkedList();
    this.y = paramThrowable.getStackTrace();
    Map localMap1 = Thread.getAllStackTraces();
    this.w = new Thread[localMap1.size()];
    Iterator localIterator = localMap1.entrySet().iterator();
    for (int i3 = 0; localIterator.hasNext(); i3++)
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      this.w[i3] = ((Thread)localEntry.getKey());
      this.x.add(localEntry.getValue());
    }
    aj localaj = a(this.u);
    if (localaj == null) {
      co.a().a().b();
    }
    com.a.a.a.ba.a(this.u);
    this.u = null;
    Object localObject;
    if (!com.a.a.a.ba.a(d.a().g, "com.crashlytics.CollectCustomKeys", true)) {
      localObject = new TreeMap();
    }
    for (;;)
    {
      paraman.e(10, 2);
      int i4 = 0 + an.b(1, l1) + an.b(2, aj.a(paramString));
      int i5 = a(paramThread, paramThrowable, (Map)localObject);
      int i6 = i4 + (i5 + (an.e(3) + an.g(i5)));
      int i7 = a(i1, i2, l2, l3);
      int i8 = i6 + (i7 + (an.e(5) + an.g(i7)));
      if (localaj != null)
      {
        int i10 = an.b(1, localaj);
        i8 += i10 + (an.e(6) + an.g(i10));
      }
      paraman.f(i8);
      paraman.a(1, l1);
      paraman.a(2, aj.a(paramString));
      paraman.e(3, 2);
      paraman.f(a(paramThread, paramThrowable, (Map)localObject));
      a(paraman, paramThread, paramThrowable);
      if ((localObject != null) && (!((Map)localObject).isEmpty())) {
        a(paraman, (Map)localObject);
      }
      if (this.t != null) {
        if (this.t.importance == 100) {
          break label704;
        }
      }
      Map localMap2;
      label704:
      for (boolean bool2 = true;; bool2 = false)
      {
        paraman.a(3, bool2);
        paraman.a(4, d.a().g.getResources().getConfiguration().orientation);
        paraman.e(5, 2);
        paraman.f(a(i1, i2, l2, l3));
        paraman.e(1, 5);
        int i9 = Float.floatToRawIntBits(f1);
        paraman.d(i9 & 0xFF);
        paraman.d(0xFF & i9 >> 8);
        paraman.d(0xFF & i9 >> 16);
        paraman.d(i9 >>> 24);
        paraman.a(i1);
        paraman.a(3, bool1);
        paraman.a(4, i2);
        paraman.a(5, l2);
        paraman.a(6, l3);
        if (localaj != null)
        {
          paraman.e(6, 2);
          paraman.f(an.b(1, localaj));
          paraman.a(1, localaj);
        }
        return;
        localMap2 = Collections.unmodifiableMap(d.a().a);
        if ((localMap2 == null) || (localMap2.size() <= 1)) {
          break label710;
        }
        localObject = new TreeMap(localMap2);
        break;
      }
      label710:
      localObject = localMap2;
    }
  }
  
  private static aj b(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    return aj.a(paramString);
  }
  
  /* Error */
  private static void c(String paramString)
  {
    // Byte code:
    //   0: new 553	com/a/a/al
    //   3: dup
    //   4: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   7: getfield 156	com/a/a/a/cl:c	Ljava/io/File;
    //   10: new 337	java/lang/StringBuilder
    //   13: dup
    //   14: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   17: aload_0
    //   18: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   21: ldc_w 509
    //   24: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   27: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   30: invokespecial 556	com/a/a/al:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   33: astore_1
    //   34: aload_1
    //   35: invokestatic 559	com/a/a/an:a	(Ljava/io/OutputStream;)Lcom/a/a/an;
    //   38: astore 7
    //   40: aload 7
    //   42: astore 6
    //   44: new 719	android/os/StatFs
    //   47: dup
    //   48: invokestatic 645	android/os/Environment:getDataDirectory	()Ljava/io/File;
    //   51: invokevirtual 648	java/io/File:getPath	()Ljava/lang/String;
    //   54: invokespecial 720	android/os/StatFs:<init>	(Ljava/lang/String;)V
    //   57: astore 8
    //   59: invokestatic 722	com/a/a/a/ba:b	()I
    //   62: istore 9
    //   64: getstatic 727	android/os/Build:MODEL	Ljava/lang/String;
    //   67: invokestatic 729	com/a/a/az:b	(Ljava/lang/String;)Lcom/a/a/aj;
    //   70: astore 10
    //   72: getstatic 732	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   75: invokestatic 729	com/a/a/az:b	(Ljava/lang/String;)Lcom/a/a/aj;
    //   78: astore 11
    //   80: getstatic 735	android/os/Build:PRODUCT	Ljava/lang/String;
    //   83: invokestatic 729	com/a/a/az:b	(Ljava/lang/String;)Lcom/a/a/aj;
    //   86: astore 12
    //   88: invokestatic 741	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
    //   91: invokevirtual 744	java/lang/Runtime:availableProcessors	()I
    //   94: istore 13
    //   96: invokestatic 636	com/a/a/a/ba:c	()J
    //   99: lstore 14
    //   101: aload 8
    //   103: invokevirtual 747	android/os/StatFs:getBlockCount	()I
    //   106: i2l
    //   107: aload 8
    //   109: invokevirtual 750	android/os/StatFs:getBlockSize	()I
    //   112: i2l
    //   113: lmul
    //   114: lstore 16
    //   116: invokestatic 752	com/a/a/a/ba:f	()Z
    //   119: istore 18
    //   121: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   124: getfield 755	com/a/a/d:c	Lcom/a/a/a/bm;
    //   127: astore 19
    //   129: ldc -2
    //   131: astore 20
    //   133: aload 19
    //   135: getfield 759	com/a/a/a/bm:a	Z
    //   138: ifeq +47 -> 185
    //   141: aload 19
    //   143: invokevirtual 761	com/a/a/a/bm:e	()Ljava/lang/String;
    //   146: astore 20
    //   148: aload 20
    //   150: ifnonnull +35 -> 185
    //   153: invokestatic 764	com/a/a/a/ba:a	()Landroid/content/SharedPreferences;
    //   156: astore 26
    //   158: aload 26
    //   160: ldc_w 766
    //   163: aconst_null
    //   164: invokeinterface 771 3 0
    //   169: astore 20
    //   171: aload 20
    //   173: ifnonnull +12 -> 185
    //   176: aload 19
    //   178: aload 26
    //   180: invokevirtual 774	com/a/a/a/bm:a	(Landroid/content/SharedPreferences;)Ljava/lang/String;
    //   183: astore 20
    //   185: aload 20
    //   187: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   190: astore 21
    //   192: aload 19
    //   194: invokevirtual 776	com/a/a/a/bm:d	()Ljava/util/Map;
    //   197: astore 22
    //   199: invokestatic 778	com/a/a/a/ba:h	()I
    //   202: istore 23
    //   204: aload 6
    //   206: bipush 9
    //   208: iconst_2
    //   209: invokevirtual 465	com/a/a/an:e	(II)V
    //   212: aload 6
    //   214: iload 9
    //   216: aload 21
    //   218: aload 10
    //   220: iload 13
    //   222: lload 14
    //   224: lload 16
    //   226: aload 22
    //   228: iload 23
    //   230: aload 11
    //   232: aload 12
    //   234: invokestatic 780	com/a/a/az:a	(ILcom/a/a/aj;Lcom/a/a/aj;IJJLjava/util/Map;ILcom/a/a/aj;Lcom/a/a/aj;)I
    //   237: invokevirtual 467	com/a/a/an:f	(I)V
    //   240: aload 6
    //   242: iconst_1
    //   243: aload 21
    //   245: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   248: aload 6
    //   250: iconst_3
    //   251: iload 9
    //   253: invokevirtual 782	com/a/a/an:b	(II)V
    //   256: aload 6
    //   258: iconst_4
    //   259: aload 10
    //   261: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   264: aload 6
    //   266: iconst_5
    //   267: iload 13
    //   269: invokevirtual 475	com/a/a/an:a	(II)V
    //   272: aload 6
    //   274: bipush 6
    //   276: lload 14
    //   278: invokevirtual 470	com/a/a/an:a	(IJ)V
    //   281: aload 6
    //   283: bipush 7
    //   285: lload 16
    //   287: invokevirtual 470	com/a/a/an:a	(IJ)V
    //   290: aload 6
    //   292: bipush 10
    //   294: iload 18
    //   296: invokevirtual 694	com/a/a/an:a	(IZ)V
    //   299: aload 22
    //   301: invokeinterface 287 1 0
    //   306: invokeinterface 293 1 0
    //   311: astore 24
    //   313: aload 24
    //   315: invokeinterface 298 1 0
    //   320: ifeq +126 -> 446
    //   323: aload 24
    //   325: invokeinterface 302 1 0
    //   330: checkcast 304	java/util/Map$Entry
    //   333: astore 25
    //   335: aload 6
    //   337: bipush 11
    //   339: iconst_2
    //   340: invokevirtual 465	com/a/a/an:e	(II)V
    //   343: aload 6
    //   345: aload 25
    //   347: invokeinterface 307 1 0
    //   352: checkcast 309	com/a/a/a/bn
    //   355: aload 25
    //   357: invokeinterface 312 1 0
    //   362: checkcast 185	java/lang/String
    //   365: invokestatic 315	com/a/a/az:a	(Lcom/a/a/a/bn;Ljava/lang/String;)I
    //   368: invokevirtual 467	com/a/a/an:f	(I)V
    //   371: aload 6
    //   373: iconst_1
    //   374: aload 25
    //   376: invokeinterface 307 1 0
    //   381: checkcast 309	com/a/a/a/bn
    //   384: getfield 321	com/a/a/a/bn:f	I
    //   387: invokevirtual 782	com/a/a/an:b	(II)V
    //   390: aload 6
    //   392: iconst_2
    //   393: aload 25
    //   395: invokeinterface 312 1 0
    //   400: checkcast 185	java/lang/String
    //   403: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   406: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   409: goto -96 -> 313
    //   412: astore_2
    //   413: aload 6
    //   415: astore 4
    //   417: aload_1
    //   418: astore_3
    //   419: aload_2
    //   420: aload_3
    //   421: invokestatic 583	com/a/a/az:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   424: aload_2
    //   425: athrow
    //   426: astore 5
    //   428: aload 4
    //   430: astore 6
    //   432: aload_3
    //   433: astore_1
    //   434: aload 6
    //   436: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   439: aload_1
    //   440: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   443: aload 5
    //   445: athrow
    //   446: aload 6
    //   448: bipush 12
    //   450: iload 23
    //   452: invokevirtual 475	com/a/a/an:a	(II)V
    //   455: aload 11
    //   457: ifnull +12 -> 469
    //   460: aload 6
    //   462: bipush 13
    //   464: aload 11
    //   466: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   469: aload 12
    //   471: ifnull +12 -> 483
    //   474: aload 6
    //   476: bipush 14
    //   478: aload 12
    //   480: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   483: aload 6
    //   485: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   488: aload_1
    //   489: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   492: return
    //   493: astore 5
    //   495: aconst_null
    //   496: astore 6
    //   498: aconst_null
    //   499: astore_1
    //   500: goto -66 -> 434
    //   503: astore 5
    //   505: aconst_null
    //   506: astore 6
    //   508: goto -74 -> 434
    //   511: astore 5
    //   513: goto -79 -> 434
    //   516: astore_2
    //   517: aconst_null
    //   518: astore 4
    //   520: aconst_null
    //   521: astore_3
    //   522: goto -103 -> 419
    //   525: astore_2
    //   526: aload_1
    //   527: astore_3
    //   528: aconst_null
    //   529: astore 4
    //   531: goto -112 -> 419
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	534	0	paramString	String
    //   33	494	1	localObject1	Object
    //   412	13	2	localException1	Exception
    //   516	1	2	localException2	Exception
    //   525	1	2	localException3	Exception
    //   418	110	3	localObject2	Object
    //   415	115	4	localObject3	Object
    //   426	18	5	localObject4	Object
    //   493	1	5	localObject5	Object
    //   503	1	5	localObject6	Object
    //   511	1	5	localObject7	Object
    //   42	465	6	localObject8	Object
    //   38	3	7	localan	an
    //   57	51	8	localStatFs	android.os.StatFs
    //   62	190	9	i1	int
    //   70	190	10	localaj1	aj
    //   78	387	11	localaj2	aj
    //   86	393	12	localaj3	aj
    //   94	174	13	i2	int
    //   99	178	14	l1	long
    //   114	172	16	l2	long
    //   119	176	18	bool	boolean
    //   127	66	19	localbm	com.a.a.a.bm
    //   131	55	20	str	String
    //   190	54	21	localaj4	aj
    //   197	103	22	localMap	Map
    //   202	249	23	i3	int
    //   311	13	24	localIterator	Iterator
    //   333	61	25	localEntry	Map.Entry
    //   156	23	26	localSharedPreferences	android.content.SharedPreferences
    // Exception table:
    //   from	to	target	type
    //   44	129	412	java/lang/Exception
    //   133	148	412	java/lang/Exception
    //   153	171	412	java/lang/Exception
    //   176	185	412	java/lang/Exception
    //   185	313	412	java/lang/Exception
    //   313	409	412	java/lang/Exception
    //   446	455	412	java/lang/Exception
    //   460	469	412	java/lang/Exception
    //   474	483	412	java/lang/Exception
    //   419	426	426	finally
    //   0	34	493	finally
    //   34	40	503	finally
    //   44	129	511	finally
    //   133	148	511	finally
    //   153	171	511	finally
    //   176	185	511	finally
    //   185	313	511	finally
    //   313	409	511	finally
    //   446	455	511	finally
    //   460	469	511	finally
    //   474	483	511	finally
    //   0	34	516	java/lang/Exception
    //   34	40	525	java/lang/Exception
  }
  
  /* Error */
  private void d()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 618	java/util/Date
    //   5: dup
    //   6: invokespecial 784	java/util/Date:<init>	()V
    //   9: astore_2
    //   10: new 786	com/a/a/ak
    //   13: dup
    //   14: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   17: getfield 755	com/a/a/d:c	Lcom/a/a/a/bm;
    //   20: invokespecial 789	com/a/a/ak:<init>	(Lcom/a/a/a/bm;)V
    //   23: invokevirtual 790	com/a/a/ak:toString	()Ljava/lang/String;
    //   26: astore_3
    //   27: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   30: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   33: invokeinterface 200 1 0
    //   38: new 553	com/a/a/al
    //   41: dup
    //   42: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   45: getfield 156	com/a/a/a/cl:c	Ljava/io/File;
    //   48: new 337	java/lang/StringBuilder
    //   51: dup
    //   52: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   55: aload_3
    //   56: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   59: ldc_w 792
    //   62: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   65: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   68: invokespecial 556	com/a/a/al:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   71: astore 4
    //   73: aload 4
    //   75: invokestatic 559	com/a/a/an:a	(Ljava/io/OutputStream;)Lcom/a/a/an;
    //   78: astore 9
    //   80: aload 9
    //   82: astore 6
    //   84: aload 6
    //   86: iconst_1
    //   87: aload_0
    //   88: getfield 191	com/a/a/az:n	Ljava/lang/String;
    //   91: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   94: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   97: aload 6
    //   99: iconst_2
    //   100: aload_3
    //   101: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   104: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   107: aload 6
    //   109: iconst_3
    //   110: aload_2
    //   111: invokevirtual 621	java/util/Date:getTime	()J
    //   114: ldc2_w 622
    //   117: ldiv
    //   118: invokevirtual 470	com/a/a/an:a	(IJ)V
    //   121: aload 6
    //   123: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   126: aload 4
    //   128: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   131: new 553	com/a/a/al
    //   134: dup
    //   135: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   138: getfield 156	com/a/a/a/cl:c	Ljava/io/File;
    //   141: new 337	java/lang/StringBuilder
    //   144: dup
    //   145: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   148: aload_3
    //   149: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   152: ldc_w 505
    //   155: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   158: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   161: invokespecial 556	com/a/a/al:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   164: astore 10
    //   166: aload 10
    //   168: invokestatic 559	com/a/a/an:a	(Ljava/io/OutputStream;)Lcom/a/a/an;
    //   171: astore 15
    //   173: aload 15
    //   175: astore 14
    //   177: invokestatic 211	com/a/a/d:d	()Ljava/lang/String;
    //   180: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   183: astore 16
    //   185: invokestatic 794	com/a/a/d:g	()Ljava/lang/String;
    //   188: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   191: astore 17
    //   193: invokestatic 795	com/a/a/d:f	()Ljava/lang/String;
    //   196: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   199: astore 18
    //   201: invokestatic 797	com/a/a/d:h	()Ljava/lang/String;
    //   204: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   207: pop
    //   208: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   211: getfield 241	com/a/a/a/ch:g	Landroid/content/Context;
    //   214: invokevirtual 800	android/content/Context:getPackageCodePath	()Ljava/lang/String;
    //   217: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   220: pop
    //   221: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   224: getfield 755	com/a/a/d:c	Lcom/a/a/a/bm;
    //   227: invokevirtual 802	com/a/a/a/bm:a	()Ljava/lang/String;
    //   230: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   233: astore 21
    //   235: invokestatic 803	com/a/a/d:e	()Ljava/lang/String;
    //   238: invokestatic 808	com/a/a/a/bg:a	(Ljava/lang/String;)Lcom/a/a/a/bg;
    //   241: getfield 810	com/a/a/a/bg:a	I
    //   244: istore 22
    //   246: aload 14
    //   248: bipush 7
    //   250: iconst_2
    //   251: invokevirtual 465	com/a/a/an:e	(II)V
    //   254: iconst_0
    //   255: iconst_1
    //   256: aload 16
    //   258: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   261: iadd
    //   262: iconst_2
    //   263: aload 17
    //   265: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   268: iadd
    //   269: iconst_3
    //   270: aload 18
    //   272: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   275: iadd
    //   276: istore 23
    //   278: invokestatic 811	com/a/a/az:h	()I
    //   281: istore 24
    //   283: aload 14
    //   285: iload 23
    //   287: iload 24
    //   289: iconst_5
    //   290: invokestatic 317	com/a/a/an:e	(I)I
    //   293: iload 24
    //   295: invokestatic 319	com/a/a/an:g	(I)I
    //   298: iadd
    //   299: iadd
    //   300: iadd
    //   301: bipush 6
    //   303: aload 21
    //   305: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   308: iadd
    //   309: bipush 10
    //   311: iload 22
    //   313: invokestatic 281	com/a/a/an:d	(II)I
    //   316: iadd
    //   317: invokevirtual 467	com/a/a/an:f	(I)V
    //   320: aload 14
    //   322: iconst_1
    //   323: aload 16
    //   325: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   328: aload 14
    //   330: iconst_2
    //   331: aload 17
    //   333: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   336: aload 14
    //   338: iconst_3
    //   339: aload 18
    //   341: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   344: aload 14
    //   346: iconst_5
    //   347: iconst_2
    //   348: invokevirtual 465	com/a/a/an:e	(II)V
    //   351: aload 14
    //   353: invokestatic 811	com/a/a/az:h	()I
    //   356: invokevirtual 467	com/a/a/an:f	(I)V
    //   359: aload 14
    //   361: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   364: getfield 241	com/a/a/a/ch:g	Landroid/content/Context;
    //   367: iconst_0
    //   368: invokestatic 816	com/a/a/a/cj:a	(Landroid/content/Context;Z)Ljava/lang/String;
    //   371: invokevirtual 817	com/a/a/an:a	(Ljava/lang/String;)V
    //   374: aload 14
    //   376: bipush 6
    //   378: aload 21
    //   380: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   383: aload 14
    //   385: bipush 10
    //   387: iload 22
    //   389: invokevirtual 782	com/a/a/an:b	(II)V
    //   392: aload 14
    //   394: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   397: aload 10
    //   399: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   402: new 553	com/a/a/al
    //   405: dup
    //   406: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   409: getfield 156	com/a/a/a/cl:c	Ljava/io/File;
    //   412: new 337	java/lang/StringBuilder
    //   415: dup
    //   416: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   419: aload_3
    //   420: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   423: ldc_w 507
    //   426: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   429: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   432: invokespecial 556	com/a/a/al:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   435: astore 25
    //   437: aload 25
    //   439: invokestatic 559	com/a/a/an:a	(Ljava/io/OutputStream;)Lcom/a/a/an;
    //   442: astore_1
    //   443: getstatic 822	android/os/Build$VERSION:RELEASE	Ljava/lang/String;
    //   446: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   449: astore 28
    //   451: getstatic 825	android/os/Build$VERSION:CODENAME	Ljava/lang/String;
    //   454: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   457: astore 29
    //   459: invokestatic 827	com/a/a/a/ba:g	()Z
    //   462: istore 30
    //   464: aload_1
    //   465: bipush 8
    //   467: iconst_2
    //   468: invokevirtual 465	com/a/a/an:e	(II)V
    //   471: aload_1
    //   472: iconst_0
    //   473: iconst_1
    //   474: iconst_3
    //   475: invokestatic 281	com/a/a/an:d	(II)I
    //   478: iadd
    //   479: iconst_2
    //   480: aload 28
    //   482: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   485: iadd
    //   486: iconst_3
    //   487: aload 29
    //   489: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   492: iadd
    //   493: iconst_4
    //   494: invokestatic 269	com/a/a/an:b	(I)I
    //   497: iadd
    //   498: invokevirtual 467	com/a/a/an:f	(I)V
    //   501: aload_1
    //   502: iconst_1
    //   503: iconst_3
    //   504: invokevirtual 782	com/a/a/an:b	(II)V
    //   507: aload_1
    //   508: iconst_2
    //   509: aload 28
    //   511: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   514: aload_1
    //   515: iconst_3
    //   516: aload 29
    //   518: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   521: aload_1
    //   522: iconst_4
    //   523: iload 30
    //   525: invokevirtual 694	com/a/a/an:a	(IZ)V
    //   528: aload_1
    //   529: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   532: aload 25
    //   534: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   537: aload_3
    //   538: invokestatic 829	com/a/a/az:c	(Ljava/lang/String;)V
    //   541: return
    //   542: astore 5
    //   544: aconst_null
    //   545: astore 6
    //   547: aload 5
    //   549: aload_1
    //   550: invokestatic 583	com/a/a/az:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   553: aload 5
    //   555: athrow
    //   556: astore 7
    //   558: aload_1
    //   559: astore 4
    //   561: aload 6
    //   563: astore 8
    //   565: aload 8
    //   567: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   570: aload 4
    //   572: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   575: aload 7
    //   577: athrow
    //   578: astore 11
    //   580: aconst_null
    //   581: astore 12
    //   583: aload 11
    //   585: aload 12
    //   587: invokestatic 583	com/a/a/az:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   590: aload 11
    //   592: athrow
    //   593: astore 13
    //   595: aload 12
    //   597: astore 10
    //   599: aload_1
    //   600: astore 14
    //   602: aload 14
    //   604: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   607: aload 10
    //   609: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   612: aload 13
    //   614: athrow
    //   615: astore 26
    //   617: aconst_null
    //   618: astore 25
    //   620: aload 26
    //   622: aload 25
    //   624: invokestatic 583	com/a/a/az:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   627: aload 26
    //   629: athrow
    //   630: astore 27
    //   632: aload_1
    //   633: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   636: aload 25
    //   638: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   641: aload 27
    //   643: athrow
    //   644: astore 27
    //   646: aconst_null
    //   647: astore 25
    //   649: aconst_null
    //   650: astore_1
    //   651: goto -19 -> 632
    //   654: astore 26
    //   656: goto -36 -> 620
    //   659: astore 13
    //   661: aconst_null
    //   662: astore 14
    //   664: aconst_null
    //   665: astore 10
    //   667: goto -65 -> 602
    //   670: astore 13
    //   672: aconst_null
    //   673: astore 14
    //   675: goto -73 -> 602
    //   678: astore 13
    //   680: goto -78 -> 602
    //   683: astore 11
    //   685: aload 10
    //   687: astore 12
    //   689: aconst_null
    //   690: astore_1
    //   691: goto -108 -> 583
    //   694: astore 11
    //   696: aload 14
    //   698: astore_1
    //   699: aload 10
    //   701: astore 12
    //   703: goto -120 -> 583
    //   706: astore 7
    //   708: aconst_null
    //   709: astore 8
    //   711: aconst_null
    //   712: astore 4
    //   714: goto -149 -> 565
    //   717: astore 7
    //   719: aconst_null
    //   720: astore 8
    //   722: goto -157 -> 565
    //   725: astore 7
    //   727: aload 6
    //   729: astore 8
    //   731: goto -166 -> 565
    //   734: astore 5
    //   736: aload 4
    //   738: astore_1
    //   739: aconst_null
    //   740: astore 6
    //   742: goto -195 -> 547
    //   745: astore 5
    //   747: aload 4
    //   749: astore_1
    //   750: goto -203 -> 547
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	753	0	this	az
    //   1	749	1	localObject1	Object
    //   9	102	2	localDate	Date
    //   26	512	3	str	String
    //   71	677	4	localObject2	Object
    //   542	12	5	localException1	Exception
    //   734	1	5	localException2	Exception
    //   745	1	5	localException3	Exception
    //   82	659	6	localan1	an
    //   556	20	7	localObject3	Object
    //   706	1	7	localObject4	Object
    //   717	1	7	localObject5	Object
    //   725	1	7	localObject6	Object
    //   563	167	8	localan2	an
    //   78	3	9	localan3	an
    //   164	536	10	localObject7	Object
    //   578	13	11	localException4	Exception
    //   683	1	11	localException5	Exception
    //   694	1	11	localException6	Exception
    //   581	121	12	localObject8	Object
    //   593	20	13	localObject9	Object
    //   659	1	13	localObject10	Object
    //   670	1	13	localObject11	Object
    //   678	1	13	localObject12	Object
    //   175	522	14	localObject13	Object
    //   171	3	15	localan4	an
    //   183	141	16	localaj1	aj
    //   191	141	17	localaj2	aj
    //   199	141	18	localaj3	aj
    //   233	146	21	localaj4	aj
    //   244	144	22	i1	int
    //   276	25	23	i2	int
    //   281	19	24	i3	int
    //   435	213	25	localal	al
    //   615	13	26	localException7	Exception
    //   654	1	26	localException8	Exception
    //   630	12	27	localObject14	Object
    //   644	1	27	localObject15	Object
    //   449	61	28	localaj5	aj
    //   457	60	29	localaj6	aj
    //   462	62	30	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   38	73	542	java/lang/Exception
    //   547	556	556	finally
    //   131	166	578	java/lang/Exception
    //   583	593	593	finally
    //   402	437	615	java/lang/Exception
    //   437	528	630	finally
    //   620	630	630	finally
    //   402	437	644	finally
    //   437	528	654	java/lang/Exception
    //   131	166	659	finally
    //   166	173	670	finally
    //   177	392	678	finally
    //   166	173	683	java/lang/Exception
    //   177	392	694	java/lang/Exception
    //   38	73	706	finally
    //   73	80	717	finally
    //   84	121	725	finally
    //   73	80	734	java/lang/Exception
    //   84	121	745	java/lang/Exception
  }
  
  /* Error */
  private void e()
  {
    // Byte code:
    //   0: new 831	java/util/HashSet
    //   3: dup
    //   4: invokespecial 832	java/util/HashSet:<init>	()V
    //   7: astore_1
    //   8: aload_0
    //   9: invokevirtual 835	com/a/a/az:a	()[Ljava/io/File;
    //   12: astore_2
    //   13: aload_2
    //   14: getstatic 67	com/a/a/az:c	Ljava/util/Comparator;
    //   17: invokestatic 541	java/util/Arrays:sort	([Ljava/lang/Object;Ljava/util/Comparator;)V
    //   20: bipush 8
    //   22: aload_2
    //   23: arraylength
    //   24: invokestatic 838	java/lang/Math:min	(II)I
    //   27: istore_3
    //   28: iconst_0
    //   29: istore 4
    //   31: iload 4
    //   33: iload_3
    //   34: if_icmpge +23 -> 57
    //   37: aload_1
    //   38: aload_2
    //   39: iload 4
    //   41: aaload
    //   42: invokestatic 840	com/a/a/az:a	(Ljava/io/File;)Ljava/lang/String;
    //   45: invokeinterface 841 2 0
    //   50: pop
    //   51: iinc 4 1
    //   54: goto -23 -> 31
    //   57: aload_0
    //   58: new 843	com/a/a/s
    //   61: dup
    //   62: iconst_0
    //   63: invokespecial 846	com/a/a/s:<init>	(B)V
    //   66: invokevirtual 515	com/a/a/az:a	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   69: astore 5
    //   71: aload 5
    //   73: arraylength
    //   74: istore 6
    //   76: iconst_0
    //   77: istore 7
    //   79: iload 7
    //   81: iload 6
    //   83: if_icmpge +71 -> 154
    //   86: aload 5
    //   88: iload 7
    //   90: aaload
    //   91: astore 57
    //   93: aload 57
    //   95: invokevirtual 456	java/io/File:getName	()Ljava/lang/String;
    //   98: astore 58
    //   100: getstatic 85	com/a/a/az:e	Ljava/util/regex/Pattern;
    //   103: aload 58
    //   105: invokevirtual 850	java/util/regex/Pattern:matcher	(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
    //   108: astore 59
    //   110: aload 59
    //   112: invokevirtual 855	java/util/regex/Matcher:matches	()Z
    //   115: pop
    //   116: aload_1
    //   117: aload 59
    //   119: iconst_1
    //   120: invokevirtual 859	java/util/regex/Matcher:group	(I)Ljava/lang/String;
    //   123: invokeinterface 862 2 0
    //   128: ifne +20 -> 148
    //   131: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   134: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   137: invokeinterface 200 1 0
    //   142: aload 57
    //   144: invokevirtual 209	java/io/File:delete	()Z
    //   147: pop
    //   148: iinc 7 1
    //   151: goto -72 -> 79
    //   154: aload_0
    //   155: invokespecial 549	com/a/a/az:f	()Ljava/lang/String;
    //   158: astore 8
    //   160: aload 8
    //   162: ifnull +1072 -> 1234
    //   165: aconst_null
    //   166: astore 9
    //   168: new 553	com/a/a/al
    //   171: dup
    //   172: aload_0
    //   173: getfield 158	com/a/a/az:l	Ljava/io/File;
    //   176: new 337	java/lang/StringBuilder
    //   179: dup
    //   180: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   183: aload 8
    //   185: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: ldc_w 503
    //   191: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   197: invokespecial 556	com/a/a/al:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   200: astore 10
    //   202: aload 10
    //   204: invokestatic 559	com/a/a/an:a	(Ljava/io/OutputStream;)Lcom/a/a/an;
    //   207: astore 9
    //   209: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   212: astore 13
    //   214: aload 13
    //   216: getfield 755	com/a/a/d:c	Lcom/a/a/a/bm;
    //   219: getfield 864	com/a/a/a/bm:b	Z
    //   222: ifeq +693 -> 915
    //   225: aload 13
    //   227: getfield 866	com/a/a/d:d	Ljava/lang/String;
    //   230: astore 14
    //   232: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   235: astore 15
    //   237: aload 15
    //   239: getfield 755	com/a/a/d:c	Lcom/a/a/a/bm;
    //   242: getfield 864	com/a/a/a/bm:b	Z
    //   245: ifeq +676 -> 921
    //   248: aload 15
    //   250: getfield 868	com/a/a/d:f	Ljava/lang/String;
    //   253: astore 16
    //   255: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   258: astore 17
    //   260: aload 17
    //   262: getfield 755	com/a/a/d:c	Lcom/a/a/a/bm;
    //   265: getfield 864	com/a/a/a/bm:b	Z
    //   268: ifeq +659 -> 927
    //   271: aload 17
    //   273: getfield 870	com/a/a/d:e	Ljava/lang/String;
    //   276: astore 18
    //   278: aload 14
    //   280: ifnonnull +653 -> 933
    //   283: aload 16
    //   285: ifnonnull +648 -> 933
    //   288: aload 18
    //   290: ifnonnull +643 -> 933
    //   293: aload 9
    //   295: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   298: aload 10
    //   300: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   303: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   306: pop
    //   307: invokestatic 875	com/a/a/a/as:a	()Lcom/a/a/a/ar;
    //   310: new 877	com/a/a/as
    //   313: dup
    //   314: invokespecial 878	com/a/a/as:<init>	()V
    //   317: aconst_null
    //   318: invokevirtual 883	com/a/a/a/ar:a	(Lcom/a/a/a/at;Ljava/lang/Object;)Ljava/lang/Object;
    //   321: checkcast 885	com/a/a/a/aq
    //   324: astore 24
    //   326: aload 24
    //   328: ifnull +894 -> 1222
    //   331: aload 24
    //   333: getfield 886	com/a/a/a/aq:a	I
    //   336: istore 25
    //   338: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   341: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   344: invokeinterface 200 1 0
    //   349: aload_0
    //   350: invokevirtual 835	com/a/a/az:a	()[Ljava/io/File;
    //   353: astore 26
    //   355: aload 26
    //   357: ifnull +876 -> 1233
    //   360: aload 26
    //   362: arraylength
    //   363: ifle +870 -> 1233
    //   366: aload 26
    //   368: arraylength
    //   369: istore 27
    //   371: iconst_0
    //   372: istore 28
    //   374: iload 28
    //   376: iload 27
    //   378: if_icmpge +855 -> 1233
    //   381: aload 26
    //   383: iload 28
    //   385: aaload
    //   386: astore 29
    //   388: aload 29
    //   390: invokestatic 840	com/a/a/az:a	(Ljava/io/File;)Ljava/lang/String;
    //   393: astore 30
    //   395: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   398: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   401: invokeinterface 200 1 0
    //   406: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   409: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   412: invokeinterface 200 1 0
    //   417: aload_0
    //   418: new 511	com/a/a/t
    //   421: dup
    //   422: new 337	java/lang/StringBuilder
    //   425: dup
    //   426: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   429: aload 30
    //   431: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   434: ldc_w 555
    //   437: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   440: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   443: invokespecial 512	com/a/a/t:<init>	(Ljava/lang/String;)V
    //   446: invokevirtual 515	com/a/a/az:a	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   449: astore 31
    //   451: aload 31
    //   453: ifnull +665 -> 1118
    //   456: aload 31
    //   458: arraylength
    //   459: ifle +659 -> 1118
    //   462: iconst_1
    //   463: istore 32
    //   465: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   468: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   471: astore 33
    //   473: getstatic 173	java/util/Locale:US	Ljava/util/Locale;
    //   476: astore 34
    //   478: iconst_2
    //   479: anewarray 4	java/lang/Object
    //   482: astore 35
    //   484: aload 35
    //   486: iconst_0
    //   487: aload 30
    //   489: aastore
    //   490: aload 35
    //   492: iconst_1
    //   493: iload 32
    //   495: invokestatic 892	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   498: aastore
    //   499: aload 34
    //   501: ldc_w 894
    //   504: aload 35
    //   506: invokestatic 189	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   509: pop
    //   510: aload 33
    //   512: invokeinterface 200 1 0
    //   517: aload_0
    //   518: new 511	com/a/a/t
    //   521: dup
    //   522: new 337	java/lang/StringBuilder
    //   525: dup
    //   526: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   529: aload 30
    //   531: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   534: ldc_w 896
    //   537: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   540: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   543: invokespecial 512	com/a/a/t:<init>	(Ljava/lang/String;)V
    //   546: invokevirtual 515	com/a/a/az:a	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   549: astore 37
    //   551: aload 37
    //   553: ifnull +571 -> 1124
    //   556: aload 37
    //   558: arraylength
    //   559: ifle +565 -> 1124
    //   562: iconst_1
    //   563: istore 38
    //   565: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   568: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   571: astore 39
    //   573: getstatic 173	java/util/Locale:US	Ljava/util/Locale;
    //   576: astore 40
    //   578: iconst_2
    //   579: anewarray 4	java/lang/Object
    //   582: astore 41
    //   584: aload 41
    //   586: iconst_0
    //   587: aload 30
    //   589: aastore
    //   590: aload 41
    //   592: iconst_1
    //   593: iload 38
    //   595: invokestatic 892	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   598: aastore
    //   599: aload 40
    //   601: ldc_w 898
    //   604: aload 41
    //   606: invokestatic 189	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   609: pop
    //   610: aload 39
    //   612: invokeinterface 200 1 0
    //   617: iload 32
    //   619: ifne +8 -> 627
    //   622: iload 38
    //   624: ifeq +584 -> 1208
    //   627: aconst_null
    //   628: astore 43
    //   630: new 553	com/a/a/al
    //   633: dup
    //   634: aload_0
    //   635: getfield 158	com/a/a/az:l	Ljava/io/File;
    //   638: aload 30
    //   640: invokespecial 556	com/a/a/al:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   643: astore 44
    //   645: aload 44
    //   647: invokestatic 559	com/a/a/an:a	(Ljava/io/OutputStream;)Lcom/a/a/an;
    //   650: astore 50
    //   652: aload 50
    //   654: astore 43
    //   656: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   659: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   662: invokeinterface 200 1 0
    //   667: aload 43
    //   669: aload 29
    //   671: invokestatic 523	com/a/a/az:a	(Lcom/a/a/an;Ljava/io/File;)V
    //   674: aload 43
    //   676: iconst_4
    //   677: new 618	java/util/Date
    //   680: dup
    //   681: invokespecial 784	java/util/Date:<init>	()V
    //   684: invokevirtual 621	java/util/Date:getTime	()J
    //   687: ldc2_w 622
    //   690: ldiv
    //   691: invokevirtual 470	com/a/a/an:a	(IJ)V
    //   694: aload 43
    //   696: iconst_5
    //   697: iload 32
    //   699: invokevirtual 694	com/a/a/an:a	(IZ)V
    //   702: aload_0
    //   703: aload 43
    //   705: aload 30
    //   707: invokespecial 900	com/a/a/az:a	(Lcom/a/a/an;Ljava/lang/String;)V
    //   710: iload 38
    //   712: ifeq +140 -> 852
    //   715: aload 37
    //   717: arraylength
    //   718: iload 25
    //   720: if_icmple +585 -> 1305
    //   723: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   726: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   729: astore 52
    //   731: getstatic 173	java/util/Locale:US	Ljava/util/Locale;
    //   734: astore 53
    //   736: iconst_1
    //   737: anewarray 4	java/lang/Object
    //   740: astore 54
    //   742: aload 54
    //   744: iconst_0
    //   745: iload 25
    //   747: invokestatic 905	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   750: aastore
    //   751: aload 53
    //   753: ldc_w 907
    //   756: aload 54
    //   758: invokestatic 189	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   761: pop
    //   762: aload 52
    //   764: invokeinterface 200 1 0
    //   769: aload_0
    //   770: getfield 158	com/a/a/az:l	Ljava/io/File;
    //   773: new 511	com/a/a/t
    //   776: dup
    //   777: new 337	java/lang/StringBuilder
    //   780: dup
    //   781: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   784: aload 30
    //   786: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   789: ldc_w 896
    //   792: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   795: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   798: invokespecial 512	com/a/a/t:<init>	(Ljava/lang/String;)V
    //   801: iload 25
    //   803: getstatic 72	com/a/a/az:d	Ljava/util/Comparator;
    //   806: invokestatic 576	com/a/a/ah:a	(Ljava/io/File;Ljava/io/FilenameFilter;ILjava/util/Comparator;)V
    //   809: aload_0
    //   810: new 511	com/a/a/t
    //   813: dup
    //   814: new 337	java/lang/StringBuilder
    //   817: dup
    //   818: invokespecial 338	java/lang/StringBuilder:<init>	()V
    //   821: aload 30
    //   823: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   826: ldc_w 896
    //   829: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   832: invokevirtual 353	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   835: invokespecial 512	com/a/a/t:<init>	(Ljava/lang/String;)V
    //   838: invokevirtual 515	com/a/a/az:a	(Ljava/io/FilenameFilter;)[Ljava/io/File;
    //   841: astore 51
    //   843: aload 43
    //   845: aload 51
    //   847: aload 30
    //   849: invokestatic 909	com/a/a/az:a	(Lcom/a/a/an;[Ljava/io/File;Ljava/lang/String;)V
    //   852: iload 32
    //   854: ifeq +12 -> 866
    //   857: aload 43
    //   859: aload 31
    //   861: iconst_0
    //   862: aaload
    //   863: invokestatic 523	com/a/a/az:a	(Lcom/a/a/an;Ljava/io/File;)V
    //   866: aload 43
    //   868: bipush 11
    //   870: iconst_1
    //   871: invokevirtual 475	com/a/a/an:a	(II)V
    //   874: aload 43
    //   876: bipush 12
    //   878: iconst_3
    //   879: invokevirtual 782	com/a/a/an:b	(II)V
    //   882: aload 43
    //   884: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   887: aload 44
    //   889: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   892: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   895: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   898: invokeinterface 200 1 0
    //   903: aload_0
    //   904: aload 30
    //   906: invokespecial 910	com/a/a/az:a	(Ljava/lang/String;)V
    //   909: iinc 28 1
    //   912: goto -538 -> 374
    //   915: aconst_null
    //   916: astore 14
    //   918: goto -686 -> 232
    //   921: aconst_null
    //   922: astore 16
    //   924: goto -669 -> 255
    //   927: aconst_null
    //   928: astore 18
    //   930: goto -652 -> 278
    //   933: aload 14
    //   935: ifnonnull +7 -> 942
    //   938: ldc -2
    //   940: astore 14
    //   942: aload 14
    //   944: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   947: astore 19
    //   949: aload 16
    //   951: ifnonnull +113 -> 1064
    //   954: aconst_null
    //   955: astore 20
    //   957: goto +355 -> 1312
    //   960: iconst_0
    //   961: iconst_1
    //   962: aload 19
    //   964: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   967: iadd
    //   968: istore 22
    //   970: aload 20
    //   972: ifnull +14 -> 986
    //   975: iload 22
    //   977: iconst_2
    //   978: aload 20
    //   980: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   983: iadd
    //   984: istore 22
    //   986: aload 21
    //   988: ifnull +14 -> 1002
    //   991: iload 22
    //   993: iconst_3
    //   994: aload 21
    //   996: invokestatic 279	com/a/a/an:b	(ILcom/a/a/aj;)I
    //   999: iadd
    //   1000: istore 22
    //   1002: aload 9
    //   1004: bipush 6
    //   1006: iconst_2
    //   1007: invokevirtual 465	com/a/a/an:e	(II)V
    //   1010: aload 9
    //   1012: iload 22
    //   1014: invokevirtual 467	com/a/a/an:f	(I)V
    //   1017: aload 9
    //   1019: iconst_1
    //   1020: aload 19
    //   1022: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   1025: aload 20
    //   1027: ifnull +11 -> 1038
    //   1030: aload 9
    //   1032: iconst_2
    //   1033: aload 20
    //   1035: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   1038: aload 21
    //   1040: ifnull +11 -> 1051
    //   1043: aload 9
    //   1045: iconst_3
    //   1046: aload 21
    //   1048: invokevirtual 473	com/a/a/an:a	(ILcom/a/a/aj;)V
    //   1051: aload 9
    //   1053: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   1056: aload 10
    //   1058: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   1061: goto -758 -> 303
    //   1064: aload 16
    //   1066: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   1069: astore 20
    //   1071: goto +241 -> 1312
    //   1074: aload 18
    //   1076: invokestatic 104	com/a/a/aj:a	(Ljava/lang/String;)Lcom/a/a/aj;
    //   1079: astore 56
    //   1081: aload 56
    //   1083: astore 21
    //   1085: goto -125 -> 960
    //   1088: astore 11
    //   1090: aconst_null
    //   1091: astore 10
    //   1093: aload 11
    //   1095: aload 10
    //   1097: invokestatic 583	com/a/a/az:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   1100: aload 11
    //   1102: athrow
    //   1103: astore 12
    //   1105: aload 9
    //   1107: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   1110: aload 10
    //   1112: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   1115: aload 12
    //   1117: athrow
    //   1118: iconst_0
    //   1119: istore 32
    //   1121: goto -656 -> 465
    //   1124: iconst_0
    //   1125: istore 38
    //   1127: goto -562 -> 565
    //   1130: astore 45
    //   1132: aconst_null
    //   1133: astore 47
    //   1135: aconst_null
    //   1136: astore 46
    //   1138: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   1141: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   1144: invokeinterface 454 1 0
    //   1149: aload 45
    //   1151: aload 46
    //   1153: invokestatic 583	com/a/a/az:a	(Ljava/lang/Throwable;Ljava/io/OutputStream;)V
    //   1156: aload 47
    //   1158: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   1161: aload 46
    //   1163: ifnull -271 -> 892
    //   1166: aload 46
    //   1168: invokevirtual 911	com/a/a/al:a	()V
    //   1171: goto -279 -> 892
    //   1174: astore 49
    //   1176: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   1179: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   1182: invokeinterface 454 1 0
    //   1187: goto -295 -> 892
    //   1190: astore 48
    //   1192: aconst_null
    //   1193: astore 44
    //   1195: aload 43
    //   1197: invokestatic 567	com/a/a/a/ba:a	(Ljava/io/Flushable;)V
    //   1200: aload 44
    //   1202: invokestatic 494	com/a/a/a/ba:a	(Ljava/io/Closeable;)V
    //   1205: aload 48
    //   1207: athrow
    //   1208: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   1211: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   1214: invokeinterface 200 1 0
    //   1219: goto -327 -> 892
    //   1222: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   1225: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   1228: invokeinterface 200 1 0
    //   1233: return
    //   1234: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   1237: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   1240: invokeinterface 200 1 0
    //   1245: return
    //   1246: astore 48
    //   1248: goto -53 -> 1195
    //   1251: astore 48
    //   1253: aload 46
    //   1255: astore 44
    //   1257: aload 47
    //   1259: astore 43
    //   1261: goto -66 -> 1195
    //   1264: astore 45
    //   1266: aload 44
    //   1268: astore 46
    //   1270: aconst_null
    //   1271: astore 47
    //   1273: goto -135 -> 1138
    //   1276: astore 45
    //   1278: aload 43
    //   1280: astore 47
    //   1282: aload 44
    //   1284: astore 46
    //   1286: goto -148 -> 1138
    //   1289: astore 12
    //   1291: aconst_null
    //   1292: astore 9
    //   1294: aconst_null
    //   1295: astore 10
    //   1297: goto -192 -> 1105
    //   1300: astore 11
    //   1302: goto -209 -> 1093
    //   1305: aload 37
    //   1307: astore 51
    //   1309: goto -466 -> 843
    //   1312: aload 18
    //   1314: ifnonnull -240 -> 1074
    //   1317: aconst_null
    //   1318: astore 21
    //   1320: goto -360 -> 960
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1323	0	this	az
    //   7	110	1	localHashSet	java.util.HashSet
    //   12	27	2	arrayOfFile1	File[]
    //   27	8	3	i1	int
    //   29	23	4	i2	int
    //   69	18	5	arrayOfFile2	File[]
    //   74	10	6	i3	int
    //   77	72	7	i4	int
    //   158	26	8	str1	String
    //   166	1127	9	localan1	an
    //   200	1096	10	localal	al
    //   1088	13	11	localException1	Exception
    //   1300	1	11	localException2	Exception
    //   1103	13	12	localObject1	Object
    //   1289	1	12	localObject2	Object
    //   212	14	13	locald1	d
    //   230	713	14	str2	String
    //   235	14	15	locald2	d
    //   253	812	16	str3	String
    //   258	14	17	locald3	d
    //   276	1037	18	str4	String
    //   947	74	19	localaj1	aj
    //   955	115	20	localaj2	aj
    //   986	333	21	localObject3	Object
    //   968	45	22	i5	int
    //   324	8	24	localaq	com.a.a.a.aq
    //   336	466	25	i6	int
    //   353	29	26	arrayOfFile3	File[]
    //   369	10	27	i7	int
    //   372	538	28	i8	int
    //   386	284	29	localFile1	File
    //   393	512	30	str5	String
    //   449	411	31	arrayOfFile4	File[]
    //   463	657	32	bool1	boolean
    //   471	40	33	localci1	ci
    //   476	24	34	localLocale1	Locale
    //   482	23	35	arrayOfObject1	Object[]
    //   549	757	37	arrayOfFile5	File[]
    //   563	563	38	bool2	boolean
    //   571	40	39	localci2	ci
    //   576	24	40	localLocale2	Locale
    //   582	23	41	arrayOfObject2	Object[]
    //   628	651	43	localObject4	Object
    //   643	640	44	localObject5	Object
    //   1130	20	45	localException3	Exception
    //   1264	1	45	localException4	Exception
    //   1276	1	45	localException5	Exception
    //   1136	149	46	localObject6	Object
    //   1133	148	47	localObject7	Object
    //   1190	16	48	localObject8	Object
    //   1246	1	48	localObject9	Object
    //   1251	1	48	localObject10	Object
    //   1174	1	49	localIOException	IOException
    //   650	3	50	localan2	an
    //   841	467	51	arrayOfFile6	File[]
    //   729	34	52	localci3	ci
    //   734	18	53	localLocale3	Locale
    //   740	17	54	arrayOfObject3	Object[]
    //   1079	3	56	localaj3	aj
    //   91	52	57	localFile2	File
    //   98	6	58	str6	String
    //   108	10	59	localMatcher	java.util.regex.Matcher
    // Exception table:
    //   from	to	target	type
    //   168	202	1088	java/lang/Exception
    //   202	232	1103	finally
    //   232	255	1103	finally
    //   255	278	1103	finally
    //   942	949	1103	finally
    //   960	970	1103	finally
    //   975	986	1103	finally
    //   991	1002	1103	finally
    //   1002	1025	1103	finally
    //   1030	1038	1103	finally
    //   1043	1051	1103	finally
    //   1064	1071	1103	finally
    //   1074	1081	1103	finally
    //   1093	1103	1103	finally
    //   630	645	1130	java/lang/Exception
    //   1166	1171	1174	java/io/IOException
    //   630	645	1190	finally
    //   645	652	1246	finally
    //   656	710	1246	finally
    //   715	843	1246	finally
    //   843	852	1246	finally
    //   857	866	1246	finally
    //   866	882	1246	finally
    //   1138	1156	1251	finally
    //   645	652	1264	java/lang/Exception
    //   656	710	1276	java/lang/Exception
    //   715	843	1276	java/lang/Exception
    //   843	852	1276	java/lang/Exception
    //   857	866	1276	java/lang/Exception
    //   866	882	1276	java/lang/Exception
    //   168	202	1289	finally
    //   202	232	1300	java/lang/Exception
    //   232	255	1300	java/lang/Exception
    //   255	278	1300	java/lang/Exception
    //   942	949	1300	java/lang/Exception
    //   960	970	1300	java/lang/Exception
    //   975	986	1300	java/lang/Exception
    //   991	1002	1300	java/lang/Exception
    //   1002	1025	1300	java/lang/Exception
    //   1030	1038	1300	java/lang/Exception
    //   1043	1051	1300	java/lang/Exception
    //   1064	1071	1300	java/lang/Exception
    //   1074	1081	1300	java/lang/Exception
  }
  
  private String f()
  {
    File[] arrayOfFile = a(new t("BeginSession"));
    Arrays.sort(arrayOfFile, c);
    if (arrayOfFile.length > 0) {
      return a(arrayOfFile[0]);
    }
    return null;
  }
  
  private void g()
  {
    for (File localFile : a(a))
    {
      co.a().a().b();
      new Thread(new f(localFile), "Crashlytics Report Uploader").start();
    }
  }
  
  private static int h()
  {
    return 0 + an.b(1, aj.a(cj.a(d.a().g, co.a().b)));
  }
  
  private int i()
  {
    int i1 = 0 + an.b(1, 0L) + an.b(2, 0L) + an.b(3, this.q);
    if (this.r != null) {
      i1 += an.b(4, this.r);
    }
    return i1;
  }
  
  private static int j()
  {
    return 0 + an.b(1, g) + an.b(2, g) + an.b(3, 0L);
  }
  
  final <T> T a(Callable<T> paramCallable)
  {
    try
    {
      Object localObject = this.s.submit(paramCallable).get();
      return (T)localObject;
    }
    catch (RejectedExecutionException localRejectedExecutionException)
    {
      co.a().a().b();
      return null;
    }
    catch (Exception localException)
    {
      co.a().a().a();
    }
    return null;
  }
  
  final Future<?> a(Runnable paramRunnable)
  {
    try
    {
      Future localFuture = this.s.submit(new i(paramRunnable));
      return localFuture;
    }
    catch (RejectedExecutionException localRejectedExecutionException)
    {
      co.a().a().b();
    }
    return null;
  }
  
  final File[] a()
  {
    return a(new t("BeginSession"));
  }
  
  final File[] a(FilenameFilter paramFilenameFilter)
  {
    File[] arrayOfFile = this.l.listFiles(paramFilenameFilter);
    if (arrayOfFile == null) {
      arrayOfFile = new File[0];
    }
    return arrayOfFile;
  }
  
  final <T> Future<T> b(Callable<T> paramCallable)
  {
    try
    {
      Future localFuture = this.s.submit(new k(paramCallable));
      return localFuture;
    }
    catch (RejectedExecutionException localRejectedExecutionException)
    {
      co.a().a().b();
    }
    return null;
  }
  
  /* Error */
  public final void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 147	com/a/a/az:b	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   6: iconst_1
    //   7: invokevirtual 250	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   10: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   13: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   16: astore 6
    //   18: new 337	java/lang/StringBuilder
    //   21: dup
    //   22: ldc_w 956
    //   25: invokespecial 500	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   28: aload_2
    //   29: invokevirtual 959	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   32: ldc_w 961
    //   35: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   38: aload_1
    //   39: invokevirtual 413	java/lang/Thread:getName	()Ljava/lang/String;
    //   42: invokevirtual 345	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: pop
    //   46: aload 6
    //   48: invokeinterface 200 1 0
    //   53: aload_0
    //   54: getfield 141	com/a/a/az:i	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   57: iconst_1
    //   58: invokevirtual 965	java/util/concurrent/atomic/AtomicBoolean:getAndSet	(Z)Z
    //   61: ifne +40 -> 101
    //   64: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   67: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   70: invokeinterface 200 1 0
    //   75: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   78: getfield 241	com/a/a/a/ch:g	Landroid/content/Context;
    //   81: aload_0
    //   82: getfield 222	com/a/a/az:p	Landroid/content/BroadcastReceiver;
    //   85: invokevirtual 969	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
    //   88: invokestatic 178	com/a/a/d:a	()Lcom/a/a/d;
    //   91: getfield 241	com/a/a/a/ch:g	Landroid/content/Context;
    //   94: aload_0
    //   95: getfield 234	com/a/a/az:o	Landroid/content/BroadcastReceiver;
    //   98: invokevirtual 969	android/content/Context:unregisterReceiver	(Landroid/content/BroadcastReceiver;)V
    //   101: aload_0
    //   102: new 971	com/a/a/p
    //   105: dup
    //   106: aload_0
    //   107: new 618	java/util/Date
    //   110: dup
    //   111: invokespecial 784	java/util/Date:<init>	()V
    //   114: aload_1
    //   115: aload_2
    //   116: invokespecial 973	com/a/a/p:<init>	(Lcom/a/a/az;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;)V
    //   119: invokevirtual 975	com/a/a/az:a	(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    //   122: pop
    //   123: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   126: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   129: invokeinterface 200 1 0
    //   134: aload_0
    //   135: getfield 143	com/a/a/az:k	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   138: aload_1
    //   139: aload_2
    //   140: invokeinterface 977 3 0
    //   145: aload_0
    //   146: getfield 147	com/a/a/az:b	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   149: iconst_0
    //   150: invokevirtual 250	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   153: aload_0
    //   154: monitorexit
    //   155: return
    //   156: astore 5
    //   158: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   161: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   164: invokeinterface 454 1 0
    //   169: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   172: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   175: invokeinterface 200 1 0
    //   180: aload_0
    //   181: getfield 143	com/a/a/az:k	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   184: aload_1
    //   185: aload_2
    //   186: invokeinterface 977 3 0
    //   191: aload_0
    //   192: getfield 147	com/a/a/az:b	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   195: iconst_0
    //   196: invokevirtual 250	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   199: goto -46 -> 153
    //   202: astore_3
    //   203: aload_0
    //   204: monitorexit
    //   205: aload_3
    //   206: athrow
    //   207: astore 4
    //   209: invokestatic 152	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   212: invokevirtual 196	com/a/a/a/cl:a	()Lcom/a/a/a/ci;
    //   215: invokeinterface 200 1 0
    //   220: aload_0
    //   221: getfield 143	com/a/a/az:k	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   224: aload_1
    //   225: aload_2
    //   226: invokeinterface 977 3 0
    //   231: aload_0
    //   232: getfield 147	com/a/a/az:b	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   235: iconst_0
    //   236: invokevirtual 250	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   239: aload 4
    //   241: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	242	0	this	az
    //   0	242	1	paramThread	Thread
    //   0	242	2	paramThrowable	Throwable
    //   202	4	3	localObject1	Object
    //   207	33	4	localObject2	Object
    //   156	1	5	localException	Exception
    //   16	31	6	localci	ci
    // Exception table:
    //   from	to	target	type
    //   10	101	156	java/lang/Exception
    //   101	123	156	java/lang/Exception
    //   2	10	202	finally
    //   123	153	202	finally
    //   169	199	202	finally
    //   209	242	202	finally
    //   10	101	207	finally
    //   101	123	207	finally
    //   158	169	207	finally
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/az.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */