package com.a.a;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import com.a.a.a.ba;
import com.a.a.a.bt;
import com.a.a.a.bw;
import com.a.a.a.ch;
import com.a.a.a.ci;
import com.a.a.a.cj;
import com.a.a.a.cl;
import com.a.a.a.co;
import com.a.a.a.y;
import java.io.Closeable;
import java.io.InputStream;
import java.net.HttpURLConnection;

abstract class ac
  extends y
{
  public ac(String paramString1, String paramString2, bt parambt, int paramInt)
  {
    super(paramString1, paramString2, parambt, paramInt);
  }
  
  private static bw a(bw parambw, ah paramah)
  {
    bw localbw = parambw.c("app[identifier]", paramah.b).c("app[name]", paramah.f).c("app[display_version]", paramah.c).c("app[build_version]", paramah.d).a("app[source]", Integer.valueOf(paramah.g)).c("app[minimum_sdk_version]", paramah.h).c("app[built_sdk_version]", paramah.i);
    if (!ba.e(paramah.e)) {
      localbw.c("app[instance_identifier]", paramah.e);
    }
    if (paramah.j != null) {
      localObject1 = null;
    }
    try
    {
      InputStream localInputStream = co.a().g.getResources().openRawResource(paramah.j.b);
      localObject1 = localInputStream;
      ci localci;
      ba.a((Closeable)localObject3);
    }
    catch (Resources.NotFoundException localNotFoundException)
    {
      try
      {
        localbw.c("app[icon][hash]", paramah.j.a).a("app[icon][data]", "icon.png", "application/octet-stream", (InputStream)localObject1).a("app[icon][width]", Integer.valueOf(paramah.j.c)).a("app[icon][height]", Integer.valueOf(paramah.j.d));
        ba.a((Closeable)localObject1);
        return localbw;
      }
      finally
      {
        for (;;)
        {
          Object localObject3 = localObject1;
          Object localObject4 = localObject5;
        }
      }
      localNotFoundException = localNotFoundException;
      localci = co.a().a();
      new StringBuilder("Failed to find app icon with resource ID: ").append(paramah.j.b);
      localci.a();
      ba.a((Closeable)localObject1);
      return localbw;
    }
    finally
    {
      localObject2 = finally;
      localObject3 = null;
      localObject4 = localObject2;
    }
    throw ((Throwable)localObject4);
  }
  
  public final boolean a(ah paramah)
  {
    bw localbw1 = a().a("X-CRASHLYTICS-API-KEY", paramah.a).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    co.a();
    bw localbw2 = a(localbw1.a("X-CRASHLYTICS-API-CLIENT-VERSION", "1.1.13.29"), paramah);
    ci localci1 = co.a().a();
    new StringBuilder("Sending app info to ").append(this.a);
    localci1.b();
    if (paramah.j != null)
    {
      ci localci3 = co.a().a();
      new StringBuilder("App icon hash is ").append(paramah.j.a);
      localci3.b();
      ci localci4 = co.a().a();
      new StringBuilder("App icon size is ").append(paramah.j.c).append("x").append(paramah.j.d);
      localci4.b();
    }
    int i = localbw2.b();
    if ("POST".equals(localbw2.a().getRequestMethod())) {}
    for (String str = "Create";; str = "Update")
    {
      ci localci2 = co.a().a();
      new StringBuilder().append(str).append(" app request ID: ").append(localbw2.b("X-REQUEST-ID"));
      localci2.b();
      co.a().a().b();
      if (cj.a(i) != 0) {
        break;
      }
      return true;
    }
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/ac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */