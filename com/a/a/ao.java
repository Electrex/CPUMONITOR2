package com.a.a;

import java.io.IOException;

final class ao
  extends IOException
{
  private static final long serialVersionUID = -6947486886997889499L;
  
  ao()
  {
    super("CodedOutputStream was writing to a flat byte array and ran out of space.");
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/ao.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */