package com.a.a;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Looper;
import com.a.a.a.al;
import com.a.a.a.ao;
import com.a.a.a.as;
import com.a.a.a.aw;
import com.a.a.a.ba;
import com.a.a.a.bd;
import com.a.a.a.be;
import com.a.a.a.bg;
import com.a.a.a.bm;
import com.a.a.a.bt;
import com.a.a.a.ch;
import com.a.a.a.ci;
import com.a.a.a.cj;
import com.a.a.a.ck;
import com.a.a.a.cl;
import com.a.a.a.co;
import com.a.a.a.n;
import com.a.a.a.o;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class d
  extends ck
{
  private static ContextWrapper j;
  private static String k;
  private static String l;
  private static String m;
  private static String n;
  private static String o;
  private static String p;
  private static String q;
  private static boolean r = false;
  private static r s = null;
  private static bt t;
  private static float u;
  private static d v;
  final ConcurrentHashMap<String, String> a = new ConcurrentHashMap();
  az b;
  bm c = null;
  String d = null;
  String e = null;
  String f = null;
  private final long h = System.currentTimeMillis();
  private String i;
  
  private ah a(aa paramaa)
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = this.i;
    String str = ba.a(arrayOfString);
    int i1 = bg.a(l).a;
    return new ah(p, k, o, n, str, m, i1, q, "0", paramaa);
  }
  
  /* Error */
  public static d a()
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: invokestatic 116	com/a/a/a/co:a	()Lcom/a/a/a/cl;
    //   6: ldc 2
    //   8: invokevirtual 121	com/a/a/a/cl:a	(Ljava/lang/Class;)Lcom/a/a/a/ck;
    //   11: checkcast 2	com/a/a/d
    //   14: astore_1
    //   15: aload_1
    //   16: ifnull +8 -> 24
    //   19: ldc 2
    //   21: monitorexit
    //   22: aload_1
    //   23: areturn
    //   24: getstatic 123	com/a/a/d:v	Lcom/a/a/d;
    //   27: ifnonnull +13 -> 40
    //   30: new 2	com/a/a/d
    //   33: dup
    //   34: invokespecial 124	com/a/a/d:<init>	()V
    //   37: putstatic 123	com/a/a/d:v	Lcom/a/a/d;
    //   40: getstatic 123	com/a/a/d:v	Lcom/a/a/d;
    //   43: astore_1
    //   44: goto -25 -> 19
    //   47: astore_0
    //   48: ldc 2
    //   50: monitorexit
    //   51: aload_0
    //   52: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   47	5	0	localObject	Object
    //   14	30	1	locald	d
    // Exception table:
    //   from	to	target	type
    //   3	15	47	finally
    //   24	40	47	finally
    //   40	44	47	finally
  }
  
  public static void a(Context paramContext)
  {
    u = 1.0F;
    if (!ba.d(paramContext))
    {
      cl localcl = co.a();
      com.a.a.a.a locala = new com.a.a.a.a();
      localcl.a.set(locala);
    }
    ck[] arrayOfck = new ck[2];
    arrayOfck[0] = a();
    arrayOfck[1] = new com.a.a.a.c();
    cl.a(paramContext, arrayOfck);
  }
  
  static void a(String paramString)
  {
    com.a.a.a.c localc = (com.a.a.a.c)co.a().a(com.a.a.a.c.class);
    n localn;
    o localo;
    if (localc != null)
    {
      be localbe = new be(paramString);
      if (localc.a != null)
      {
        localn = localc.a;
        String str = localbe.a;
        if (Looper.myLooper() == Looper.getMainLooper()) {
          throw new IllegalStateException("onCrash called from main thread!!!");
        }
        localo = new o(localn, str);
      }
    }
    try
    {
      localn.i.submit(localo).get();
      return;
    }
    catch (Exception localException)
    {
      ba.e();
    }
  }
  
  private void a(String paramString, Context paramContext, float paramFloat)
  {
    ai localai;
    for (;;)
    {
      PackageManager localPackageManager;
      ci localci2;
      PackageInfo localPackageInfo;
      try
      {
        if (j != null)
        {
          co.a().a().b();
          return;
        }
        p = paramString;
        j = new ContextWrapper(paramContext.getApplicationContext());
        t = new bt(co.a().a());
        ci localci1 = co.a().a();
        StringBuilder localStringBuilder = new StringBuilder("Initializing Crashlytics ");
        a();
        co.a();
        localStringBuilder.append("1.1.13.29");
        localci1.c();
      }
      finally {}
      try
      {
        k = j.getPackageName();
        localPackageManager = j.getPackageManager();
        l = localPackageManager.getInstallerPackageName(k);
        localci2 = co.a().a();
        new StringBuilder("Installer package name is: ").append(l);
        localci2.b();
        localPackageInfo = localPackageManager.getPackageInfo(k, 0);
        n = Integer.toString(localPackageInfo.versionCode);
        if (localPackageInfo.versionName != null) {
          break label345;
        }
        str2 = "0.0";
        o = str2;
        m = paramContext.getPackageManager().getApplicationLabel(paramContext.getApplicationInfo()).toString();
        q = Integer.toString(paramContext.getApplicationInfo().targetSdkVersion);
        this.i = ba.i(paramContext);
      }
      catch (Exception localException1)
      {
        co.a().a().a();
        continue;
        if (localai.b) {
          break label390;
        }
        co.a().a().b();
      }
      this.c = new bm(j);
      this.c.f();
      localai = new ai(this.i, ba.a(j, "com.crashlytics.RequireBuildId", true));
      String str1 = k;
      if ((!ba.e(localai.a)) || (!localai.b)) {
        break;
      }
      String.format("https://crashlytics.com/register/%s/android/%s", new Object[] { paramString, str1 });
      throw new e(paramString, str1);
      label345:
      String str2 = localPackageInfo.versionName;
    }
    for (;;)
    {
      try
      {
        label390:
        co.a().a().b();
        this.b = new az(Thread.getDefaultUncaughtExceptionHandler(), this.i);
        az localaz1 = this.b;
        boolean bool2 = ((Boolean)localaz1.a(new c(localaz1))).booleanValue();
        bool1 = bool2;
      }
      catch (Exception localException2)
      {
        az localaz2;
        az localaz3;
        az localaz4;
        CountDownLatch localCountDownLatch;
        boolean bool1 = false;
        continue;
      }
      try
      {
        localaz2 = this.b;
        localaz2.b(new a(localaz2));
        localaz3 = this.b;
        localaz3.b(new bc(localaz3));
        localaz4 = this.b;
        localaz4.a(new g(localaz4));
        Thread.setDefaultUncaughtExceptionHandler(this.b);
        co.a().a().b();
        localCountDownLatch = new CountDownLatch(1);
        new Thread(new ax(this, paramContext, paramFloat, localCountDownLatch), "Crashlytics Initializer").start();
        if (!bool1) {
          break;
        }
        co.a().a().b();
        try
        {
          if (localCountDownLatch.await(4000L, TimeUnit.MILLISECONDS)) {
            break;
          }
          co.a().a().d();
        }
        catch (InterruptedException localInterruptedException)
        {
          co.a().a().a();
        }
      }
      catch (Exception localException3)
      {
        continue;
      }
      co.a().a().a();
    }
  }
  
  private boolean a(Context paramContext, float paramFloat)
  {
    bool1 = true;
    str = ba.g(this.g);
    try
    {
      as.a().a(paramContext, t, n, o, i()).b();
      aw localaw2 = as.a().a();
      localaw1 = localaw2;
    }
    catch (Exception localException3)
    {
      for (;;)
      {
        try
        {
          localal = localaw1.a;
          if ("new".equals(localal.a))
          {
            ah localah2 = a(aa.a(this.g, str));
            if (new v(i(), localal.b, t).a(localah2))
            {
              boolean bool5 = as.a().c();
              bool4 = bool5;
              bool3 = bool4;
            }
          }
        }
        catch (Exception localException3)
        {
          try
          {
            aw localaw1;
            al localal;
            boolean bool4;
            bool2 = localaw1.d.b;
            if ((bool3) && (bool2))
            {
              try
              {
                az localaz = this.b;
                bool1 = true & ((Boolean)localaz.a(new q(localaz))).booleanValue();
                x localx = m();
                i1 = 0;
                if (localx != null) {
                  new ad(localx).a(paramFloat);
                }
              }
              catch (Exception localException2)
              {
                ah localah1;
                co.a().a().a();
                i1 = 0;
                continue;
              }
              if (i1 != 0) {
                co.a().a().b();
              }
              return bool1;
              localException1 = localException1;
              co.a().a().a();
              localaw1 = null;
              continue;
              co.a().a().a();
              bool4 = false;
              continue;
              if ("configured".equals(localal.a))
              {
                bool4 = as.a().c();
                continue;
              }
              if (localal.d)
              {
                co.a().a().b();
                localah1 = a(aa.a(this.g, str));
                new ag(i(), localal.b, t).a(localah1);
              }
              bool4 = bool1;
              continue;
              localException3 = localException3;
              co.a().a().a();
              bool3 = false;
              continue;
            }
          }
          catch (Exception localException4)
          {
            co.a().a().a();
            bool2 = false;
            continue;
            int i1 = bool1;
            continue;
          }
        }
        boolean bool2 = false;
        boolean bool3 = false;
      }
    }
    if (localaw1 == null) {}
  }
  
  public static String c()
  {
    co.a();
    return "1.1.13.29";
  }
  
  static String d()
  {
    return k;
  }
  
  static String e()
  {
    return l;
  }
  
  static String f()
  {
    return o;
  }
  
  static String g()
  {
    return n;
  }
  
  static String h()
  {
    return m;
  }
  
  static String i()
  {
    return ba.a(j, "com.crashlytics.ApiEndpoint");
  }
  
  static boolean k()
  {
    return ba.a().getBoolean("always_send_reports_opt_in", false);
  }
  
  static void l()
  {
    ba.a().edit().putBoolean("always_send_reports_opt_in", true).commit();
  }
  
  static x m()
  {
    return (x)as.a().a(new ar(), null);
  }
  
  protected final void b()
  {
    Context localContext = this.g;
    String str = cj.a(localContext, false);
    if (str == null) {
      return;
    }
    try
    {
      a(str, localContext, u);
      return;
    }
    catch (e locale)
    {
      throw locale;
    }
    catch (Exception localException)
    {
      co.a().a().a();
    }
  }
  
  final boolean j()
  {
    return ((Boolean)as.a().a(new ap(this), Boolean.valueOf(false))).booleanValue();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */