package com.a.a;

import com.a.a.a.ch;
import com.a.a.a.ci;
import com.a.a.a.cj;
import com.a.a.a.cl;
import com.a.a.a.co;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

final class ad
{
  static final Map<String, String> a = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", "1");
  private static final FilenameFilter b = new ae();
  private static final short[] c = { 10, 20, 30, 60, 120, 300 };
  private final Object d = new Object();
  private final x e;
  private Thread f;
  
  public ad(x paramx)
  {
    if (paramx == null) {
      throw new IllegalArgumentException("createReportCall must not be null.");
    }
    this.e = paramx;
  }
  
  final List<ab> a()
  {
    co.a().a().b();
    LinkedList localLinkedList;
    synchronized (this.d)
    {
      File[] arrayOfFile = co.a().c.listFiles(b);
      localLinkedList = new LinkedList();
      int i = arrayOfFile.length;
      int j = 0;
      if (j < i)
      {
        File localFile = arrayOfFile[j];
        ci localci = co.a().a();
        new StringBuilder("Found crash report ").append(localFile.getPath());
        localci.b();
        localLinkedList.add(new ab(localFile));
        j++;
      }
    }
    return localLinkedList;
  }
  
  public final void a(float paramFloat)
  {
    try
    {
      if (this.f == null)
      {
        this.f = new Thread(new af(this, paramFloat), "Crashlytics Report Uploader");
        this.f.start();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  final boolean a(ab paramab)
  {
    synchronized (this.d)
    {
      try
      {
        w localw = new w(cj.a(co.a().g, co.a().b), paramab);
        boolean bool2 = this.e.a(localw);
        ci localci2 = co.a().a();
        StringBuilder localStringBuilder = new StringBuilder("Crashlytics report upload ");
        if (!bool2) {
          break label117;
        }
        str = "complete: ";
        localStringBuilder.append(str).append(paramab.a.getName());
        localci2.c();
        bool1 = false;
        if (bool2)
        {
          paramab.a();
          bool1 = true;
        }
      }
      catch (Exception localException)
      {
        for (;;)
        {
          String str;
          label117:
          ci localci1 = co.a().a();
          new StringBuilder("Error occurred sending report ").append(paramab);
          localci1.a();
          boolean bool1 = false;
        }
      }
      return bool1;
      str = "FAILED: ";
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/ad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */