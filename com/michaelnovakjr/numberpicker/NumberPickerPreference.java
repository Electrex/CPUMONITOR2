package com.michaelnovakjr.numberpicker;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.h;
import com.cgollner.systemmonitor.b.a.j;

public class NumberPickerPreference
  extends DialogPreference
{
  private NumberPicker a;
  private int b;
  private int c;
  private int d;
  private String e;
  private String f;
  
  public NumberPickerPreference(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public NumberPickerPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 16842897);
  }
  
  public NumberPickerPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    if (paramAttributeSet == null) {
      return;
    }
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, a.j.numberpicker);
    this.b = localTypedArray.getInteger(a.j.numberpicker_startRange, 1);
    this.c = localTypedArray.getInteger(a.j.numberpicker_endRange, 200);
    this.d = localTypedArray.getInteger(a.j.numberpicker_defaultValue, 1);
    this.e = localTypedArray.getString(a.j.numberpicker_summary_uni);
    if (this.e == null) {
      this.e = paramContext.getString(a.h.second);
    }
    this.f = localTypedArray.getString(a.j.numberpicker_summary_plural);
    if (this.f == null) {
      this.f = paramContext.getString(a.h.seconds);
    }
    localTypedArray.recycle();
    setDialogLayoutResource(a.f.pref_number_picker);
    a();
  }
  
  private void a()
  {
    if (b() == 1) {}
    for (String str = this.e;; str = this.f)
    {
      setSummary(b() + " " + str);
      return;
    }
  }
  
  private int b()
  {
    return PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(getKey(), this.d);
  }
  
  protected void onBindDialogView(View paramView)
  {
    super.onBindDialogView(paramView);
    this.a = ((NumberPicker)paramView.findViewById(a.e.pref_num_picker));
    this.a.a(this.b, this.c);
    this.a.setCurrent(b());
  }
  
  protected void onDialogClosed(boolean paramBoolean)
  {
    super.onDialogClosed(paramBoolean);
    int i = b();
    int j = this.a.getCurrent();
    if ((paramBoolean) && (j != i) && (callChangeListener(Integer.valueOf(j))))
    {
      persistInt(j);
      a();
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/michaelnovakjr/numberpicker/NumberPickerPreference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */