package com.michaelnovakjr.numberpicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.j;
import java.util.Formatter;
import java.util.Locale;

public class NumberPicker
  extends LinearLayout
  implements View.OnClickListener, View.OnFocusChangeListener, View.OnLongClickListener, TextView.OnEditorActionListener
{
  public static final a a = new a()
  {
    final StringBuilder a = new StringBuilder();
    final Formatter b = new Formatter(this.a);
    final Object[] c = new Object[1];
    
    public final String a(int paramAnonymousInt)
    {
      this.c[0] = Integer.valueOf(paramAnonymousInt);
      this.a.delete(0, this.a.length());
      this.b.format("%02d", this.c);
      return this.b.toString();
    }
  };
  private static final char[] r = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57 };
  private static final char[] s = { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45 };
  protected int b;
  protected int c;
  protected int d;
  protected int e;
  protected d f;
  protected a g;
  protected boolean h;
  protected long i = 300L;
  boolean j;
  boolean k;
  private final Handler l;
  private final Runnable m = new Runnable()
  {
    public final void run()
    {
      if (NumberPicker.a(NumberPicker.this))
      {
        NumberPicker.this.a(1 + NumberPicker.this.d);
        NumberPicker.b(NumberPicker.this).postDelayed(this, NumberPicker.this.i);
      }
      while (!NumberPicker.c(NumberPicker.this)) {
        return;
      }
      NumberPicker.this.a(-1 + NumberPicker.this.d);
      NumberPicker.b(NumberPicker.this).postDelayed(this, NumberPicker.this.i);
    }
  };
  private final EditText n;
  private final InputFilter o;
  private String[] p;
  private int q;
  private NumberPickerButton t;
  private NumberPickerButton u;
  
  public NumberPicker(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public NumberPicker(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public NumberPicker(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet);
    setOrientation(1);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(a.f.number_picker, this, true);
    this.l = new Handler();
    b localb = new b((byte)0);
    this.o = new c((byte)0);
    this.t = ((NumberPickerButton)findViewById(a.e.increment));
    this.t.setOnClickListener(this);
    this.t.setOnLongClickListener(this);
    this.t.setNumberPicker(this);
    this.u = ((NumberPickerButton)findViewById(a.e.decrement));
    this.u.setOnClickListener(this);
    this.u.setOnLongClickListener(this);
    this.u.setNumberPicker(this);
    this.n = ((EditText)findViewById(a.e.numpicker_input));
    this.n.setOnFocusChangeListener(this);
    this.n.setOnEditorActionListener(this);
    this.n.setFilters(new InputFilter[] { localb });
    if (!isEnabled()) {
      setEnabled(false);
    }
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, a.j.numberpicker);
    this.h = localTypedArray.getBoolean(a.j.numberpicker_wrap, true);
    int i1 = localTypedArray.getInt(a.j.numberpicker_startRange, 0);
    int i2 = localTypedArray.getInt(a.j.numberpicker_endRange, 200);
    int i3 = localTypedArray.getInt(a.j.numberpicker_defaultValue, 0);
    localTypedArray.recycle();
    if (i2 < i1) {}
    for (;;)
    {
      b(i2, i1);
      setCurrentInternal(a(i3, i2, i1));
      d();
      e();
      return;
      int i4 = i2;
      i2 = i1;
      i1 = i4;
    }
  }
  
  private static int a(int paramInt1, int paramInt2, int paramInt3)
  {
    return Math.min(Math.max(paramInt1, paramInt2), paramInt3);
  }
  
  private int a(String paramString)
  {
    String[] arrayOfString = this.p;
    int i1 = 0;
    if (arrayOfString == null)
    {
      if (paramString.equals("-")) {
        return 0;
      }
      try
      {
        int i3 = Integer.parseInt(paramString);
        return i3;
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        return this.d;
      }
    }
    do
    {
      i1++;
      if (i1 >= this.p.length) {
        break;
      }
      paramString = paramString.toLowerCase(Locale.US);
    } while (!this.p[i1].toLowerCase(Locale.US).startsWith(paramString));
    return i1 + this.b;
    try
    {
      int i2 = Integer.parseInt(paramString);
      return i2;
    }
    catch (NumberFormatException localNumberFormatException1) {}
    return this.b;
  }
  
  private void a(View paramView)
  {
    String str = String.valueOf(((TextView)paramView).getText());
    if ("".equals(str))
    {
      e();
      return;
    }
    int i1 = a(a(str.toString()), this.b, this.c);
    if (this.d != i1)
    {
      this.e = this.d;
      this.d = i1;
    }
    e();
  }
  
  private void b(int paramInt1, int paramInt2)
  {
    if (paramInt2 < paramInt1) {
      throw new IllegalArgumentException("End value cannot be less than the start value.");
    }
    this.b = paramInt1;
    this.c = paramInt2;
    if (this.d < paramInt1) {
      this.d = paramInt1;
    }
    for (;;)
    {
      this.q = Integer.toString(Math.max(Math.abs(this.b), Math.abs(this.c))).length();
      d();
      return;
      if (this.d > paramInt2) {
        this.d = paramInt2;
      }
    }
  }
  
  private boolean c()
  {
    return this.b < 0;
  }
  
  private void d()
  {
    int i1 = 2;
    if (c()) {
      i1 = 4098;
    }
    this.n.setInputType(i1);
  }
  
  private void e()
  {
    int i2;
    int i3;
    int i4;
    String str;
    if (this.p == null)
    {
      i2 = this.n.getSelectionStart();
      i3 = this.n.getText().length();
      EditText localEditText = this.n;
      i4 = this.d;
      if (this.g != null)
      {
        str = this.g.a(i4);
        localEditText.setText(str);
      }
    }
    for (int i1 = Math.max(this.n.getText().length() - (i3 - Math.max(i2, 0)), 0);; i1 = this.n.getText().length())
    {
      this.n.setSelection(i1);
      return;
      str = String.valueOf(i4);
      break;
      this.n.setText(this.p[(this.d - this.b)]);
    }
  }
  
  private void setCurrentInternal(int paramInt)
  {
    if (this.b > paramInt) {
      throw new IllegalArgumentException("Current value cannot be less than the range start.");
    }
    if (this.c < paramInt) {
      throw new IllegalArgumentException("Current value cannot be greater than the range end.");
    }
    this.d = paramInt;
  }
  
  protected final void a(int paramInt)
  {
    int i1;
    if (paramInt > this.c) {
      if (this.h) {
        i1 = this.b;
      }
    }
    for (;;)
    {
      this.e = this.d;
      this.d = i1;
      e();
      return;
      i1 = this.c;
      continue;
      if (paramInt < this.b)
      {
        if (this.h) {
          i1 = this.c;
        } else {
          i1 = this.b;
        }
      }
      else {
        i1 = paramInt;
      }
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    b(paramInt1, paramInt2);
    e();
  }
  
  public int getCurrent()
  {
    return this.d;
  }
  
  public void onClick(View paramView)
  {
    a(this.n);
    if (!this.n.hasFocus()) {
      this.n.requestFocus();
    }
    if (a.e.increment == paramView.getId()) {
      a(1 + this.d);
    }
    while (a.e.decrement != paramView.getId()) {
      return;
    }
    a(-1 + this.d);
  }
  
  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramTextView == this.n) {
      a(paramTextView);
    }
    return false;
  }
  
  public void onFocusChange(View paramView, boolean paramBoolean)
  {
    if (!paramBoolean) {
      a(paramView);
    }
  }
  
  public boolean onLongClick(View paramView)
  {
    this.n.clearFocus();
    this.n.requestFocus();
    if (a.e.increment == paramView.getId())
    {
      this.j = true;
      this.l.post(this.m);
    }
    while (a.e.decrement != paramView.getId()) {
      return true;
    }
    this.k = true;
    this.l.post(this.m);
    return true;
  }
  
  public void setCurrent(int paramInt)
  {
    setCurrentInternal(paramInt);
    e();
  }
  
  public void setCurrentAndNotify(int paramInt)
  {
    setCurrentInternal(paramInt);
    e();
  }
  
  public void setEnabled(boolean paramBoolean)
  {
    super.setEnabled(paramBoolean);
    this.t.setEnabled(paramBoolean);
    this.u.setEnabled(paramBoolean);
    this.n.setEnabled(paramBoolean);
  }
  
  public void setFormatter(a parama)
  {
    this.g = parama;
  }
  
  public void setOnChangeListener(d paramd)
  {
    this.f = paramd;
  }
  
  public void setSpeed(long paramLong)
  {
    this.i = paramLong;
  }
  
  public void setWrap(boolean paramBoolean)
  {
    this.h = paramBoolean;
  }
  
  public static abstract interface a
  {
    public abstract String a(int paramInt);
  }
  
  private final class b
    implements InputFilter
  {
    private b() {}
    
    public final CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
    {
      int i = 0;
      if (NumberPicker.d(NumberPicker.this) == null)
      {
        localObject = NumberPicker.e(NumberPicker.this).filter(paramCharSequence, paramInt1, paramInt2, paramSpanned, paramInt3, paramInt4);
        return (CharSequence)localObject;
      }
      Object localObject = String.valueOf(paramCharSequence.subSequence(paramInt1, paramInt2));
      String str = String.valueOf(String.valueOf(paramSpanned.subSequence(0, paramInt3)) + localObject + paramSpanned.subSequence(paramInt4, paramSpanned.length())).toLowerCase(Locale.US);
      String[] arrayOfString = NumberPicker.d(NumberPicker.this);
      int j = arrayOfString.length;
      for (;;)
      {
        if (i >= j) {
          break label159;
        }
        if (arrayOfString[i].toLowerCase(Locale.US).startsWith(str)) {
          break;
        }
        i++;
      }
      label159:
      return "";
    }
  }
  
  private final class c
    extends NumberKeyListener
  {
    private c() {}
    
    private static int a(String paramString)
    {
      int i = 0;
      int j = paramString.length();
      for (int k = 0; i < j; k++)
      {
        int m = paramString.indexOf('-', i);
        if (m < 0) {
          break;
        }
        i += m + 1;
      }
      return k;
    }
    
    public final CharSequence filter(CharSequence paramCharSequence, int paramInt1, int paramInt2, Spanned paramSpanned, int paramInt3, int paramInt4)
    {
      Object localObject = super.filter(paramCharSequence, paramInt1, paramInt2, paramSpanned, paramInt3, paramInt4);
      if (localObject == null) {
        localObject = paramCharSequence.subSequence(paramInt1, paramInt2);
      }
      String str = String.valueOf(paramSpanned.subSequence(0, paramInt3)) + localObject + paramSpanned.subSequence(paramInt4, paramSpanned.length());
      if ("".equals(str)) {
        localObject = str;
      }
      label240:
      do
      {
        int i;
        do
        {
          do
          {
            return (CharSequence)localObject;
            if (NumberPicker.f(NumberPicker.this))
            {
              if ((((CharSequence)localObject).length() > 0) && (((CharSequence)localObject).charAt(0) == '-') && (paramInt3 != 0)) {
                return "";
              }
              int j = a(str);
              if (j > 1) {
                return "";
              }
              if ((j > 0) && (str.charAt(0) != '-')) {
                return "";
              }
            }
            if (NumberPicker.d(NumberPicker.this) != null) {
              break label240;
            }
            i = str.length();
            if ((i <= 0) || (str.charAt(0) != '-')) {
              break;
            }
          } while (i <= 1 + NumberPicker.g(NumberPicker.this));
          return "";
        } while (i <= NumberPicker.g(NumberPicker.this));
        return "";
      } while (NumberPicker.a(NumberPicker.this, str) <= NumberPicker.this.c);
      return "";
    }
    
    protected final char[] getAcceptedChars()
    {
      if (NumberPicker.f(NumberPicker.this)) {
        return NumberPicker.a();
      }
      return NumberPicker.b();
    }
    
    public final int getInputType()
    {
      if (NumberPicker.f(NumberPicker.this)) {
        return 4098;
      }
      return 2;
    }
  }
  
  public static abstract interface d {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/michaelnovakjr/numberpicker/NumberPicker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */