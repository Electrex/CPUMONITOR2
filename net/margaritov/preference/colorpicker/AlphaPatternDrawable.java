package net.margaritov.preference.colorpicker;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class AlphaPatternDrawable
  extends Drawable
{
  private Bitmap mBitmap;
  private Paint mPaint = new Paint();
  private Paint mPaintGray = new Paint();
  private Paint mPaintWhite = new Paint();
  private int mRectangleSize = 10;
  private int numRectanglesHorizontal;
  private int numRectanglesVertical;
  
  public AlphaPatternDrawable(int paramInt)
  {
    this.mRectangleSize = paramInt;
    this.mPaintWhite.setColor(-1);
    this.mPaintGray.setColor(-3421237);
  }
  
  private void generatePatternBitmap()
  {
    if ((getBounds().width() <= 0) || (getBounds().height() <= 0)) {
      return;
    }
    this.mBitmap = Bitmap.createBitmap(getBounds().width(), getBounds().height(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(this.mBitmap);
    Rect localRect = new Rect();
    int i = 0;
    int j = 1;
    label70:
    if (i <= this.numRectanglesVertical)
    {
      int k = 0;
      int m = j;
      if (k <= this.numRectanglesHorizontal)
      {
        localRect.top = (i * this.mRectangleSize);
        localRect.left = (k * this.mRectangleSize);
        localRect.bottom = (localRect.top + this.mRectangleSize);
        localRect.right = (localRect.left + this.mRectangleSize);
        Paint localPaint;
        if (m != 0)
        {
          localPaint = this.mPaintWhite;
          label152:
          localCanvas.drawRect(localRect, localPaint);
          if (m != 0) {
            break label182;
          }
        }
        label182:
        for (m = 1;; m = 0)
        {
          k++;
          break;
          localPaint = this.mPaintGray;
          break label152;
        }
      }
      if (j != 0) {
        break label202;
      }
    }
    label202:
    for (j = 1;; j = 0)
    {
      i++;
      break label70;
      break;
    }
  }
  
  public void draw(Canvas paramCanvas)
  {
    paramCanvas.drawBitmap(this.mBitmap, null, getBounds(), this.mPaint);
  }
  
  public int getOpacity()
  {
    return 0;
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    super.onBoundsChange(paramRect);
    int i = paramRect.height();
    this.numRectanglesHorizontal = ((int)Math.ceil(paramRect.width() / this.mRectangleSize));
    this.numRectanglesVertical = ((int)Math.ceil(i / this.mRectangleSize));
    generatePatternBitmap();
  }
  
  public void setAlpha(int paramInt)
  {
    throw new UnsupportedOperationException("Alpha is not supported by this drawwable.");
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    throw new UnsupportedOperationException("ColorFilter is not supported by this drawwable.");
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/net/margaritov/preference/colorpicker/AlphaPatternDrawable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */