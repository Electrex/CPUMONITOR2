package net.margaritov.preference.colorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;

public class ColorPickerView
  extends View
{
  private static final float BORDER_WIDTH_PX = 1.0F;
  private static final int PANEL_ALPHA = 2;
  private static final int PANEL_HUE = 1;
  private static final int PANEL_SAT_VAL;
  private float ALPHA_PANEL_HEIGHT = 20.0F;
  private float HUE_PANEL_WIDTH = 30.0F;
  private float PALETTE_CIRCLE_TRACKER_RADIUS = 5.0F;
  private float PANEL_SPACING = 10.0F;
  private float RECTANGLE_TRACKER_OFFSET = 2.0F;
  private int mAlpha = 255;
  private Paint mAlphaPaint;
  private AlphaPatternDrawable mAlphaPattern;
  private RectF mAlphaRect;
  private Shader mAlphaShader;
  private String mAlphaSliderText = "";
  private Paint mAlphaTextPaint;
  private int mBorderColor = -9539986;
  private Paint mBorderPaint;
  private float mDensity = 1.0F;
  private float mDrawingOffset;
  private RectF mDrawingRect;
  private float mHue = 360.0F;
  private Paint mHuePaint;
  private RectF mHueRect;
  private Shader mHueShader;
  private Paint mHueTrackerPaint;
  private int mLastTouchedPanel = 0;
  private OnColorChangedListener mListener;
  private float mSat = 0.0F;
  private Shader mSatShader;
  private Paint mSatValPaint;
  private RectF mSatValRect;
  private Paint mSatValTrackerPaint;
  private boolean mShowAlphaPanel = false;
  private int mSliderTrackerColor = -14935012;
  private Point mStartTouchPoint = null;
  private float mVal = 0.0F;
  private Shader mValShader;
  
  public ColorPickerView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ColorPickerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ColorPickerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init();
  }
  
  private Point alphaToPoint(int paramInt)
  {
    RectF localRectF = this.mAlphaRect;
    float f = localRectF.width();
    Point localPoint = new Point();
    localPoint.x = ((int)(f - f * paramInt / 255.0F + localRectF.left));
    localPoint.y = ((int)localRectF.top);
    return localPoint;
  }
  
  private int[] buildHueColorArray()
  {
    int[] arrayOfInt = new int['ũ'];
    int i = 360;
    for (int j = 0; i >= 0; j++)
    {
      float[] arrayOfFloat = new float[3];
      arrayOfFloat[0] = i;
      arrayOfFloat[1] = 1.0F;
      arrayOfFloat[2] = 1.0F;
      arrayOfInt[j] = Color.HSVToColor(arrayOfFloat);
      i--;
    }
    return arrayOfInt;
  }
  
  private float calculateRequiredOffset()
  {
    return 1.5F * Math.max(Math.max(this.PALETTE_CIRCLE_TRACKER_RADIUS, this.RECTANGLE_TRACKER_OFFSET), 1.0F * this.mDensity);
  }
  
  private int chooseHeight(int paramInt1, int paramInt2)
  {
    if ((paramInt1 == Integer.MIN_VALUE) || (paramInt1 == 1073741824)) {
      return paramInt2;
    }
    return getPrefferedHeight();
  }
  
  private int chooseWidth(int paramInt1, int paramInt2)
  {
    if ((paramInt1 == Integer.MIN_VALUE) || (paramInt1 == 1073741824)) {
      return paramInt2;
    }
    return getPrefferedWidth();
  }
  
  private void drawAlphaPanel(Canvas paramCanvas)
  {
    if ((!this.mShowAlphaPanel) || (this.mAlphaRect == null) || (this.mAlphaPattern == null)) {
      return;
    }
    RectF localRectF1 = this.mAlphaRect;
    this.mBorderPaint.setColor(this.mBorderColor);
    paramCanvas.drawRect(localRectF1.left - 1.0F, localRectF1.top - 1.0F, 1.0F + localRectF1.right, 1.0F + localRectF1.bottom, this.mBorderPaint);
    this.mAlphaPattern.draw(paramCanvas);
    float[] arrayOfFloat = new float[3];
    arrayOfFloat[0] = this.mHue;
    arrayOfFloat[1] = this.mSat;
    arrayOfFloat[2] = this.mVal;
    int i = Color.HSVToColor(arrayOfFloat);
    int j = Color.HSVToColor(0, arrayOfFloat);
    this.mAlphaShader = new LinearGradient(localRectF1.left, localRectF1.top, localRectF1.right, localRectF1.top, i, j, Shader.TileMode.CLAMP);
    this.mAlphaPaint.setShader(this.mAlphaShader);
    paramCanvas.drawRect(localRectF1, this.mAlphaPaint);
    if ((this.mAlphaSliderText != null) && (this.mAlphaSliderText != "")) {
      paramCanvas.drawText(this.mAlphaSliderText, localRectF1.centerX(), localRectF1.centerY() + 4.0F * this.mDensity, this.mAlphaTextPaint);
    }
    float f = 4.0F * this.mDensity / 2.0F;
    Point localPoint = alphaToPoint(this.mAlpha);
    RectF localRectF2 = new RectF();
    localRectF2.left = (localPoint.x - f);
    localRectF2.right = (f + localPoint.x);
    localRectF1.top -= this.RECTANGLE_TRACKER_OFFSET;
    localRectF1.bottom += this.RECTANGLE_TRACKER_OFFSET;
    paramCanvas.drawRoundRect(localRectF2, 2.0F, 2.0F, this.mHueTrackerPaint);
  }
  
  private void drawHuePanel(Canvas paramCanvas)
  {
    RectF localRectF1 = this.mHueRect;
    this.mBorderPaint.setColor(this.mBorderColor);
    paramCanvas.drawRect(localRectF1.left - 1.0F, localRectF1.top - 1.0F, 1.0F + localRectF1.right, 1.0F + localRectF1.bottom, this.mBorderPaint);
    if (this.mHueShader == null)
    {
      this.mHueShader = new LinearGradient(localRectF1.left, localRectF1.top, localRectF1.left, localRectF1.bottom, buildHueColorArray(), null, Shader.TileMode.CLAMP);
      this.mHuePaint.setShader(this.mHueShader);
    }
    paramCanvas.drawRect(localRectF1, this.mHuePaint);
    float f = 4.0F * this.mDensity / 2.0F;
    Point localPoint = hueToPoint(this.mHue);
    RectF localRectF2 = new RectF();
    localRectF1.left -= this.RECTANGLE_TRACKER_OFFSET;
    localRectF1.right += this.RECTANGLE_TRACKER_OFFSET;
    localRectF2.top = (localPoint.y - f);
    localRectF2.bottom = (f + localPoint.y);
    paramCanvas.drawRoundRect(localRectF2, 2.0F, 2.0F, this.mHueTrackerPaint);
  }
  
  private void drawSatValPanel(Canvas paramCanvas)
  {
    RectF localRectF = this.mSatValRect;
    this.mBorderPaint.setColor(this.mBorderColor);
    paramCanvas.drawRect(this.mDrawingRect.left, this.mDrawingRect.top, 1.0F + localRectF.right, 1.0F + localRectF.bottom, this.mBorderPaint);
    if (this.mValShader == null) {
      this.mValShader = new LinearGradient(localRectF.left, localRectF.top, localRectF.left, localRectF.bottom, -1, -16777216, Shader.TileMode.CLAMP);
    }
    float[] arrayOfFloat = new float[3];
    arrayOfFloat[0] = this.mHue;
    arrayOfFloat[1] = 1.0F;
    arrayOfFloat[2] = 1.0F;
    int i = Color.HSVToColor(arrayOfFloat);
    this.mSatShader = new LinearGradient(localRectF.left, localRectF.top, localRectF.right, localRectF.top, -1, i, Shader.TileMode.CLAMP);
    ComposeShader localComposeShader = new ComposeShader(this.mValShader, this.mSatShader, PorterDuff.Mode.MULTIPLY);
    this.mSatValPaint.setShader(localComposeShader);
    paramCanvas.drawRect(localRectF, this.mSatValPaint);
    Point localPoint = satValToPoint(this.mSat, this.mVal);
    this.mSatValTrackerPaint.setColor(-16777216);
    paramCanvas.drawCircle(localPoint.x, localPoint.y, this.PALETTE_CIRCLE_TRACKER_RADIUS - 1.0F * this.mDensity, this.mSatValTrackerPaint);
    this.mSatValTrackerPaint.setColor(-2236963);
    paramCanvas.drawCircle(localPoint.x, localPoint.y, this.PALETTE_CIRCLE_TRACKER_RADIUS, this.mSatValTrackerPaint);
  }
  
  private int getPrefferedHeight()
  {
    int i = (int)(200.0F * this.mDensity);
    if (this.mShowAlphaPanel) {
      i = (int)(i + (this.PANEL_SPACING + this.ALPHA_PANEL_HEIGHT));
    }
    return i;
  }
  
  private int getPrefferedWidth()
  {
    int i = getPrefferedHeight();
    if (this.mShowAlphaPanel) {
      i = (int)(i - (this.PANEL_SPACING + this.ALPHA_PANEL_HEIGHT));
    }
    return (int)(i + this.HUE_PANEL_WIDTH + this.PANEL_SPACING);
  }
  
  private Point hueToPoint(float paramFloat)
  {
    RectF localRectF = this.mHueRect;
    float f = localRectF.height();
    Point localPoint = new Point();
    localPoint.y = ((int)(f - paramFloat * f / 360.0F + localRectF.top));
    localPoint.x = ((int)localRectF.left);
    return localPoint;
  }
  
  private void init()
  {
    this.mDensity = getContext().getResources().getDisplayMetrics().density;
    this.PALETTE_CIRCLE_TRACKER_RADIUS *= this.mDensity;
    this.RECTANGLE_TRACKER_OFFSET *= this.mDensity;
    this.HUE_PANEL_WIDTH *= this.mDensity;
    this.ALPHA_PANEL_HEIGHT *= this.mDensity;
    this.PANEL_SPACING *= this.mDensity;
    this.mDrawingOffset = calculateRequiredOffset();
    initPaintTools();
    setFocusable(true);
    setFocusableInTouchMode(true);
  }
  
  private void initPaintTools()
  {
    this.mSatValPaint = new Paint();
    this.mSatValTrackerPaint = new Paint();
    this.mHuePaint = new Paint();
    this.mHueTrackerPaint = new Paint();
    this.mAlphaPaint = new Paint();
    this.mAlphaTextPaint = new Paint();
    this.mBorderPaint = new Paint();
    this.mSatValTrackerPaint.setStyle(Paint.Style.STROKE);
    this.mSatValTrackerPaint.setStrokeWidth(2.0F * this.mDensity);
    this.mSatValTrackerPaint.setAntiAlias(true);
    this.mHueTrackerPaint.setColor(this.mSliderTrackerColor);
    this.mHueTrackerPaint.setStyle(Paint.Style.STROKE);
    this.mHueTrackerPaint.setStrokeWidth(2.0F * this.mDensity);
    this.mHueTrackerPaint.setAntiAlias(true);
    this.mAlphaTextPaint.setColor(-14935012);
    this.mAlphaTextPaint.setTextSize(14.0F * this.mDensity);
    this.mAlphaTextPaint.setAntiAlias(true);
    this.mAlphaTextPaint.setTextAlign(Paint.Align.CENTER);
    this.mAlphaTextPaint.setFakeBoldText(true);
  }
  
  private boolean moveTrackersIfNeeded(MotionEvent paramMotionEvent)
  {
    int i = 1;
    if (this.mStartTouchPoint == null) {
      return false;
    }
    int j = this.mStartTouchPoint.x;
    int k = this.mStartTouchPoint.y;
    if (this.mHueRect.contains(j, k))
    {
      this.mLastTouchedPanel = i;
      this.mHue = pointToHue(paramMotionEvent.getY());
    }
    for (;;)
    {
      return i;
      if (this.mSatValRect.contains(j, k))
      {
        this.mLastTouchedPanel = 0;
        float[] arrayOfFloat = pointToSatVal(paramMotionEvent.getX(), paramMotionEvent.getY());
        this.mSat = arrayOfFloat[0];
        this.mVal = arrayOfFloat[i];
      }
      else if ((this.mAlphaRect != null) && (this.mAlphaRect.contains(j, k)))
      {
        this.mLastTouchedPanel = 2;
        this.mAlpha = pointToAlpha((int)paramMotionEvent.getX());
      }
      else
      {
        i = 0;
      }
    }
  }
  
  private int pointToAlpha(int paramInt)
  {
    RectF localRectF = this.mAlphaRect;
    int i = (int)localRectF.width();
    int j;
    if (paramInt < localRectF.left) {
      j = 0;
    }
    for (;;)
    {
      return 255 - j * 255 / i;
      if (paramInt > localRectF.right) {
        j = i;
      } else {
        j = paramInt - (int)localRectF.left;
      }
    }
  }
  
  private float pointToHue(float paramFloat)
  {
    RectF localRectF = this.mHueRect;
    float f1 = localRectF.height();
    float f2;
    if (paramFloat < localRectF.top) {
      f2 = 0.0F;
    }
    for (;;)
    {
      return 360.0F - f2 * 360.0F / f1;
      if (paramFloat > localRectF.bottom) {
        f2 = f1;
      } else {
        f2 = paramFloat - localRectF.top;
      }
    }
  }
  
  private float[] pointToSatVal(float paramFloat1, float paramFloat2)
  {
    RectF localRectF = this.mSatValRect;
    float[] arrayOfFloat = new float[2];
    float f1 = localRectF.width();
    float f2 = localRectF.height();
    float f3;
    float f4;
    if (paramFloat1 < localRectF.left)
    {
      f3 = 0.0F;
      boolean bool = paramFloat2 < localRectF.top;
      f4 = 0.0F;
      if (!bool) {
        break label104;
      }
    }
    for (;;)
    {
      arrayOfFloat[0] = (f3 * (1.0F / f1));
      arrayOfFloat[1] = (1.0F - f4 * (1.0F / f2));
      return arrayOfFloat;
      if (paramFloat1 > localRectF.right)
      {
        f3 = f1;
        break;
      }
      f3 = paramFloat1 - localRectF.left;
      break;
      label104:
      if (paramFloat2 > localRectF.bottom) {
        f4 = f2;
      } else {
        f4 = paramFloat2 - localRectF.top;
      }
    }
  }
  
  private Point satValToPoint(float paramFloat1, float paramFloat2)
  {
    RectF localRectF = this.mSatValRect;
    float f1 = localRectF.height();
    float f2 = localRectF.width();
    Point localPoint = new Point();
    localPoint.x = ((int)(f2 * paramFloat1 + localRectF.left));
    localPoint.y = ((int)(f1 * (1.0F - paramFloat2) + localRectF.top));
    return localPoint;
  }
  
  private void setUpAlphaRect()
  {
    if (!this.mShowAlphaPanel) {
      return;
    }
    RectF localRectF = this.mDrawingRect;
    float f1 = 1.0F + localRectF.left;
    float f2 = 1.0F + (localRectF.bottom - this.ALPHA_PANEL_HEIGHT);
    float f3 = localRectF.bottom - 1.0F;
    this.mAlphaRect = new RectF(f1, f2, localRectF.right - 1.0F, f3);
    this.mAlphaPattern = new AlphaPatternDrawable((int)(5.0F * this.mDensity));
    this.mAlphaPattern.setBounds(Math.round(this.mAlphaRect.left), Math.round(this.mAlphaRect.top), Math.round(this.mAlphaRect.right), Math.round(this.mAlphaRect.bottom));
  }
  
  private void setUpHueRect()
  {
    RectF localRectF = this.mDrawingRect;
    float f1 = 1.0F + (localRectF.right - this.HUE_PANEL_WIDTH);
    float f2 = 1.0F + localRectF.top;
    float f3 = localRectF.bottom - 1.0F;
    if (this.mShowAlphaPanel) {}
    for (float f4 = this.PANEL_SPACING + this.ALPHA_PANEL_HEIGHT;; f4 = 0.0F)
    {
      float f5 = f3 - f4;
      this.mHueRect = new RectF(f1, f2, localRectF.right - 1.0F, f5);
      return;
    }
  }
  
  private void setUpSatValRect()
  {
    RectF localRectF = this.mDrawingRect;
    float f1 = localRectF.height() - 2.0F;
    if (this.mShowAlphaPanel) {
      f1 -= this.PANEL_SPACING + this.ALPHA_PANEL_HEIGHT;
    }
    float f2 = 1.0F + localRectF.left;
    float f3 = 1.0F + localRectF.top;
    float f4 = f3 + f1;
    this.mSatValRect = new RectF(f2, f3, f1 + f2, f4);
  }
  
  public String getAlphaSliderText()
  {
    return this.mAlphaSliderText;
  }
  
  public int getBorderColor()
  {
    return this.mBorderColor;
  }
  
  public int getColor()
  {
    int i = this.mAlpha;
    float[] arrayOfFloat = new float[3];
    arrayOfFloat[0] = this.mHue;
    arrayOfFloat[1] = this.mSat;
    arrayOfFloat[2] = this.mVal;
    return Color.HSVToColor(i, arrayOfFloat);
  }
  
  public float getDrawingOffset()
  {
    return this.mDrawingOffset;
  }
  
  public int getSliderTrackerColor()
  {
    return this.mSliderTrackerColor;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    if ((this.mDrawingRect.width() <= 0.0F) || (this.mDrawingRect.height() <= 0.0F)) {
      return;
    }
    drawSatValPanel(paramCanvas);
    drawHuePanel(paramCanvas);
    drawAlphaPanel(paramCanvas);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.getMode(paramInt1);
    int j = View.MeasureSpec.getMode(paramInt2);
    int k = View.MeasureSpec.getSize(paramInt1);
    int m = View.MeasureSpec.getSize(paramInt2);
    int n = chooseWidth(i, k);
    int i1 = chooseHeight(j, m);
    int i4;
    int i3;
    if (!this.mShowAlphaPanel)
    {
      i4 = (int)(n - this.PANEL_SPACING - this.HUE_PANEL_WIDTH);
      if ((i4 > i1) || (getTag().equals("landscape")))
      {
        i3 = (int)(i1 + this.PANEL_SPACING + this.HUE_PANEL_WIDTH);
        i4 = i1;
      }
    }
    for (;;)
    {
      setMeasuredDimension(i3, i4);
      return;
      i3 = n;
      continue;
      int i2 = (int)(i1 - this.ALPHA_PANEL_HEIGHT + this.HUE_PANEL_WIDTH);
      if (i2 > n)
      {
        i4 = (int)(n - this.HUE_PANEL_WIDTH + this.ALPHA_PANEL_HEIGHT);
        i3 = n;
      }
      else
      {
        i3 = i2;
        i4 = i1;
      }
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    this.mDrawingRect = new RectF();
    this.mDrawingRect.left = (this.mDrawingOffset + getPaddingLeft());
    this.mDrawingRect.right = (paramInt1 - this.mDrawingOffset - getPaddingRight());
    this.mDrawingRect.top = (this.mDrawingOffset + getPaddingTop());
    this.mDrawingRect.bottom = (paramInt2 - this.mDrawingOffset - getPaddingBottom());
    setUpSatValRect();
    setUpHueRect();
    setUpAlphaRect();
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool;
    switch (paramMotionEvent.getAction())
    {
    default: 
      bool = false;
    }
    while (bool)
    {
      if (this.mListener != null)
      {
        OnColorChangedListener localOnColorChangedListener = this.mListener;
        int i = this.mAlpha;
        float[] arrayOfFloat = new float[3];
        arrayOfFloat[0] = this.mHue;
        arrayOfFloat[1] = this.mSat;
        arrayOfFloat[2] = this.mVal;
        localOnColorChangedListener.onColorChanged(Color.HSVToColor(i, arrayOfFloat));
      }
      invalidate();
      return true;
      this.mStartTouchPoint = new Point((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
      bool = moveTrackersIfNeeded(paramMotionEvent);
      continue;
      bool = moveTrackersIfNeeded(paramMotionEvent);
      continue;
      this.mStartTouchPoint = null;
      bool = moveTrackersIfNeeded(paramMotionEvent);
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent)
  {
    float f1 = paramMotionEvent.getX();
    float f2 = paramMotionEvent.getY();
    if (paramMotionEvent.getAction() == 2) {}
    int i;
    float f5;
    float f6;
    label158:
    float f7;
    switch (this.mLastTouchedPanel)
    {
    default: 
      i = 0;
      if (i != 0)
      {
        if (this.mListener != null)
        {
          OnColorChangedListener localOnColorChangedListener = this.mListener;
          int j = this.mAlpha;
          float[] arrayOfFloat = new float[3];
          arrayOfFloat[0] = this.mHue;
          arrayOfFloat[1] = this.mSat;
          arrayOfFloat[2] = this.mVal;
          localOnColorChangedListener.onColorChanged(Color.HSVToColor(j, arrayOfFloat));
        }
        invalidate();
        return true;
      }
      break;
    case 0: 
      f5 = this.mSat + f1 / 50.0F;
      f6 = this.mVal - f2 / 50.0F;
      if (f5 < 0.0F)
      {
        f5 = 0.0F;
        boolean bool2 = f6 < 0.0F;
        f7 = 0.0F;
        if (!bool2) {
          break label203;
        }
      }
      break;
    }
    for (;;)
    {
      this.mSat = f5;
      this.mVal = f7;
      i = 1;
      break;
      if (f5 <= 1.0F) {
        break label158;
      }
      f5 = 1.0F;
      break label158;
      label203:
      if (f6 > 1.0F)
      {
        f7 = 1.0F;
        continue;
        float f3 = this.mHue - f2 * 10.0F;
        boolean bool1 = f3 < 0.0F;
        float f4 = 0.0F;
        if (bool1) {}
        for (;;)
        {
          this.mHue = f4;
          i = 1;
          break;
          if (f3 > 360.0F)
          {
            f4 = 360.0F;
            continue;
            if ((!this.mShowAlphaPanel) || (this.mAlphaRect == null))
            {
              i = 0;
              break;
            }
            int k = (int)(this.mAlpha - f1 * 10.0F);
            if (k < 0) {
              k = 0;
            }
            for (;;)
            {
              this.mAlpha = k;
              i = 1;
              break;
              if (k > 255) {
                k = 255;
              }
            }
            return super.onTrackballEvent(paramMotionEvent);
          }
          f4 = f3;
        }
      }
      else
      {
        f7 = f6;
      }
    }
  }
  
  public void setAlphaSliderText(int paramInt)
  {
    setAlphaSliderText(getContext().getString(paramInt));
  }
  
  public void setAlphaSliderText(String paramString)
  {
    this.mAlphaSliderText = paramString;
    invalidate();
  }
  
  public void setAlphaSliderVisible(boolean paramBoolean)
  {
    if (this.mShowAlphaPanel != paramBoolean)
    {
      this.mShowAlphaPanel = paramBoolean;
      this.mValShader = null;
      this.mSatShader = null;
      this.mHueShader = null;
      this.mAlphaShader = null;
      requestLayout();
    }
  }
  
  public void setBorderColor(int paramInt)
  {
    this.mBorderColor = paramInt;
    invalidate();
  }
  
  public void setColor(int paramInt)
  {
    setColor(paramInt, false);
  }
  
  public void setColor(int paramInt, boolean paramBoolean)
  {
    int i = Color.alpha(paramInt);
    int j = Color.red(paramInt);
    int k = Color.blue(paramInt);
    int m = Color.green(paramInt);
    float[] arrayOfFloat1 = new float[3];
    Color.RGBToHSV(j, m, k, arrayOfFloat1);
    this.mAlpha = i;
    this.mHue = arrayOfFloat1[0];
    this.mSat = arrayOfFloat1[1];
    this.mVal = arrayOfFloat1[2];
    if ((paramBoolean) && (this.mListener != null))
    {
      OnColorChangedListener localOnColorChangedListener = this.mListener;
      int n = this.mAlpha;
      float[] arrayOfFloat2 = new float[3];
      arrayOfFloat2[0] = this.mHue;
      arrayOfFloat2[1] = this.mSat;
      arrayOfFloat2[2] = this.mVal;
      localOnColorChangedListener.onColorChanged(Color.HSVToColor(n, arrayOfFloat2));
    }
    invalidate();
  }
  
  public void setOnColorChangedListener(OnColorChangedListener paramOnColorChangedListener)
  {
    this.mListener = paramOnColorChangedListener;
  }
  
  public void setSliderTrackerColor(int paramInt)
  {
    this.mSliderTrackerColor = paramInt;
    this.mHueTrackerPaint.setColor(this.mSliderTrackerColor);
    invalidate();
  }
  
  public static abstract interface OnColorChangedListener
  {
    public abstract void onColorChanged(int paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/net/margaritov/preference/colorpicker/ColorPickerView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */