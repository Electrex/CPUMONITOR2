package net.margaritov.preference.colorpicker;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import com.cgollner.systemmonitor.b.a.e;
import com.cgollner.systemmonitor.b.a.f;
import com.cgollner.systemmonitor.b.a.h;

public class ColorPickerDialog
  extends Dialog
  implements View.OnClickListener, ColorPickerView.OnColorChangedListener
{
  private ColorPickerView mColorPicker;
  private OnColorChangedListener mListener;
  private ColorPickerPanelView mNewColor;
  private ColorPickerPanelView mOldColor;
  
  public ColorPickerDialog(Context paramContext, int paramInt)
  {
    super(paramContext);
    init(paramInt);
  }
  
  private void init(int paramInt)
  {
    getWindow().setFormat(1);
    setUp(paramInt);
  }
  
  private void setUp(int paramInt)
  {
    View localView = ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(a.f.dialog_color_picker, null);
    setContentView(localView);
    setTitle(a.h.dialog_color_picker);
    this.mColorPicker = ((ColorPickerView)localView.findViewById(a.e.color_picker_view));
    this.mOldColor = ((ColorPickerPanelView)localView.findViewById(a.e.old_color_panel));
    this.mNewColor = ((ColorPickerPanelView)localView.findViewById(a.e.new_color_panel));
    ((LinearLayout)this.mOldColor.getParent()).setPadding(Math.round(this.mColorPicker.getDrawingOffset()), 0, Math.round(this.mColorPicker.getDrawingOffset()), 0);
    this.mOldColor.setOnClickListener(this);
    this.mNewColor.setOnClickListener(this);
    this.mColorPicker.setOnColorChangedListener(this);
    this.mOldColor.setColor(paramInt);
    this.mColorPicker.setColor(paramInt, true);
  }
  
  public int getColor()
  {
    return this.mColorPicker.getColor();
  }
  
  public void onClick(View paramView)
  {
    if ((paramView.getId() == a.e.new_color_panel) && (this.mListener != null)) {
      this.mListener.onColorChanged(this.mNewColor.getColor());
    }
    dismiss();
  }
  
  public void onColorChanged(int paramInt)
  {
    this.mNewColor.setColor(paramInt);
  }
  
  public void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    this.mOldColor.setColor(paramBundle.getInt("old_color"));
    this.mColorPicker.setColor(paramBundle.getInt("new_color"), true);
  }
  
  public Bundle onSaveInstanceState()
  {
    Bundle localBundle = super.onSaveInstanceState();
    localBundle.putInt("old_color", this.mOldColor.getColor());
    localBundle.putInt("new_color", this.mNewColor.getColor());
    return localBundle;
  }
  
  public void setAlphaSliderVisible(boolean paramBoolean)
  {
    this.mColorPicker.setAlphaSliderVisible(paramBoolean);
  }
  
  public void setOnColorChangedListener(OnColorChangedListener paramOnColorChangedListener)
  {
    this.mListener = paramOnColorChangedListener;
  }
  
  public static abstract interface OnColorChangedListener
  {
    public abstract void onColorChanged(int paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/net/margaritov/preference/colorpicker/ColorPickerDialog.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */