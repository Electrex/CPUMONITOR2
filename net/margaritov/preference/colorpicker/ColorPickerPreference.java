package net.margaritov.preference.colorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.preference.Preference;
import android.preference.Preference.BaseSavedState;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ColorPickerPreference
  extends Preference
  implements Preference.OnPreferenceClickListener, ColorPickerDialog.OnColorChangedListener
{
  private boolean mAlphaSliderEnabled = false;
  private float mDensity = 0.0F;
  ColorPickerDialog mDialog;
  private int mValue = -16777216;
  View mView;
  
  public ColorPickerPreference(Context paramContext)
  {
    super(paramContext);
    init(paramContext, null);
  }
  
  public ColorPickerPreference(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext, paramAttributeSet);
  }
  
  public ColorPickerPreference(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext, paramAttributeSet);
  }
  
  public static String convertToARGB(int paramInt)
  {
    String str1 = Integer.toHexString(Color.alpha(paramInt));
    String str2 = Integer.toHexString(Color.red(paramInt));
    String str3 = Integer.toHexString(Color.green(paramInt));
    String str4 = Integer.toHexString(Color.blue(paramInt));
    if (str1.length() == 1) {
      str1 = "0" + str1;
    }
    if (str2.length() == 1) {
      str2 = "0" + str2;
    }
    if (str3.length() == 1) {
      str3 = "0" + str3;
    }
    if (str4.length() == 1) {
      str4 = "0" + str4;
    }
    return "#" + str1 + str2 + str3 + str4;
  }
  
  public static int convertToColorInt(String paramString)
  {
    int i = -1;
    if (paramString.startsWith("#")) {
      paramString = paramString.replace("#", "");
    }
    int m;
    int k;
    int j;
    if (paramString.length() == 8)
    {
      m = Integer.parseInt(paramString.substring(0, 2), 16);
      k = Integer.parseInt(paramString.substring(2, 4), 16);
      j = Integer.parseInt(paramString.substring(4, 6), 16);
      i = Integer.parseInt(paramString.substring(6, 8), 16);
    }
    for (;;)
    {
      return Color.argb(m, k, j, i);
      if (paramString.length() == 6)
      {
        m = 255;
        k = Integer.parseInt(paramString.substring(0, 2), 16);
        j = Integer.parseInt(paramString.substring(2, 4), 16);
        i = Integer.parseInt(paramString.substring(4, 6), 16);
      }
      else
      {
        j = i;
        k = i;
        m = i;
      }
    }
  }
  
  private Bitmap getPreviewBitmap()
  {
    int i = (int)(31.0F * this.mDensity);
    int j = this.mValue;
    Bitmap localBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
    int k = localBitmap.getWidth();
    int m = localBitmap.getHeight();
    for (int n = 0; n < k; n++)
    {
      int i1 = n;
      if (i1 < m)
      {
        if ((n <= 1) || (i1 <= 1) || (n >= k - 2) || (i1 >= m - 2)) {}
        for (int i2 = -7829368;; i2 = j)
        {
          localBitmap.setPixel(n, i1, i2);
          if (n != i1) {
            localBitmap.setPixel(i1, n, i2);
          }
          i1++;
          break;
        }
      }
    }
    return localBitmap;
  }
  
  private void init(Context paramContext, AttributeSet paramAttributeSet)
  {
    this.mDensity = getContext().getResources().getDisplayMetrics().density;
    setOnPreferenceClickListener(this);
    if (paramAttributeSet != null) {
      this.mAlphaSliderEnabled = paramAttributeSet.getAttributeBooleanValue(null, "alphaSlider", false);
    }
  }
  
  private void setPreviewColor()
  {
    if (this.mView == null) {}
    ImageView localImageView;
    LinearLayout localLinearLayout;
    do
    {
      return;
      localImageView = new ImageView(getContext());
      localLinearLayout = (LinearLayout)this.mView.findViewById(16908312);
    } while (localLinearLayout == null);
    localLinearLayout.setVisibility(0);
    localLinearLayout.setPadding(localLinearLayout.getPaddingLeft(), localLinearLayout.getPaddingTop(), (int)(8.0F * this.mDensity), localLinearLayout.getPaddingBottom());
    int i = localLinearLayout.getChildCount();
    if (i > 0) {
      localLinearLayout.removeViews(0, i);
    }
    localLinearLayout.addView(localImageView);
    localLinearLayout.setMinimumWidth(0);
    localImageView.setBackgroundDrawable(new AlphaPatternDrawable((int)(5.0F * this.mDensity)));
    localImageView.setImageBitmap(getPreviewBitmap());
  }
  
  protected void onBindView(View paramView)
  {
    super.onBindView(paramView);
    this.mView = paramView;
    setPreviewColor();
  }
  
  public void onColorChanged(int paramInt)
  {
    if (isPersistent()) {
      persistInt(paramInt);
    }
    this.mValue = paramInt;
    setPreviewColor();
    try
    {
      getOnPreferenceChangeListener().onPreferenceChange(this, Integer.valueOf(paramInt));
      return;
    }
    catch (NullPointerException localNullPointerException) {}
  }
  
  protected Object onGetDefaultValue(TypedArray paramTypedArray, int paramInt)
  {
    return Integer.valueOf(paramTypedArray.getColor(paramInt, -16777216));
  }
  
  public boolean onPreferenceClick(Preference paramPreference)
  {
    showDialog(null);
    return false;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if ((paramParcelable == null) || (!(paramParcelable instanceof SavedState)))
    {
      super.onRestoreInstanceState(paramParcelable);
      return;
    }
    SavedState localSavedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(localSavedState.getSuperState());
    showDialog(localSavedState.dialogBundle);
  }
  
  protected Parcelable onSaveInstanceState()
  {
    Parcelable localParcelable = super.onSaveInstanceState();
    if ((this.mDialog == null) || (!this.mDialog.isShowing())) {
      return localParcelable;
    }
    SavedState localSavedState = new SavedState(localParcelable);
    localSavedState.dialogBundle = this.mDialog.onSaveInstanceState();
    return localSavedState;
  }
  
  protected void onSetInitialValue(boolean paramBoolean, Object paramObject)
  {
    if (paramBoolean) {}
    for (int i = getPersistedInt(this.mValue);; i = ((Integer)paramObject).intValue())
    {
      onColorChanged(i);
      return;
    }
  }
  
  public void setAlphaSliderEnabled(boolean paramBoolean)
  {
    this.mAlphaSliderEnabled = paramBoolean;
  }
  
  protected void showDialog(Bundle paramBundle)
  {
    this.mDialog = new ColorPickerDialog(getContext(), this.mValue);
    this.mDialog.setOnColorChangedListener(this);
    if (this.mAlphaSliderEnabled) {
      this.mDialog.setAlphaSliderVisible(true);
    }
    if (paramBundle != null) {
      this.mDialog.onRestoreInstanceState(paramBundle);
    }
    this.mDialog.show();
  }
  
  private static class SavedState
    extends Preference.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public final ColorPickerPreference.SavedState createFromParcel(Parcel paramAnonymousParcel)
      {
        return new ColorPickerPreference.SavedState(paramAnonymousParcel);
      }
      
      public final ColorPickerPreference.SavedState[] newArray(int paramAnonymousInt)
      {
        return new ColorPickerPreference.SavedState[paramAnonymousInt];
      }
    };
    Bundle dialogBundle;
    
    public SavedState(Parcel paramParcel)
    {
      super();
      this.dialogBundle = paramParcel.readBundle();
    }
    
    public SavedState(Parcelable paramParcelable)
    {
      super();
    }
    
    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeBundle(this.dialogBundle);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/net/margaritov/preference/colorpicker/ColorPickerPreference.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */