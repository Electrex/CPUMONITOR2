package android.support.v7.view;

public abstract interface CollapsibleActionView
{
  public abstract void onActionViewCollapsed();
  
  public abstract void onActionViewExpanded();
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/view/CollapsibleActionView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */