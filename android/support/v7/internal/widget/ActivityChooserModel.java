package android.support.v7.internal.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;
import android.text.TextUtils;
import android.util.Xml;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlSerializer;

public class ActivityChooserModel
  extends DataSetObservable
{
  private static final String ATTRIBUTE_ACTIVITY = "activity";
  private static final String ATTRIBUTE_TIME = "time";
  private static final String ATTRIBUTE_WEIGHT = "weight";
  private static final boolean DEBUG = false;
  private static final int DEFAULT_ACTIVITY_INFLATION = 5;
  private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0F;
  public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
  public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
  private static final String HISTORY_FILE_EXTENSION = ".xml";
  private static final int INVALID_INDEX = -1;
  private static final String LOG_TAG = ActivityChooserModel.class.getSimpleName();
  private static final String TAG_HISTORICAL_RECORD = "historical-record";
  private static final String TAG_HISTORICAL_RECORDS = "historical-records";
  private static final Map<String, ActivityChooserModel> sDataModelRegistry = new HashMap();
  private static final Object sRegistryLock = new Object();
  private final List<ActivityResolveInfo> mActivities = new ArrayList();
  private OnChooseActivityListener mActivityChoserModelPolicy;
  private ActivitySorter mActivitySorter = new DefaultSorter(null);
  private boolean mCanReadHistoricalData = true;
  private final Context mContext;
  private final List<HistoricalRecord> mHistoricalRecords = new ArrayList();
  private boolean mHistoricalRecordsChanged = true;
  private final String mHistoryFileName;
  private int mHistoryMaxSize = 50;
  private final Object mInstanceLock = new Object();
  private Intent mIntent;
  private boolean mReadShareHistoryCalled = false;
  private boolean mReloadActivities = false;
  
  private ActivityChooserModel(Context paramContext, String paramString)
  {
    this.mContext = paramContext.getApplicationContext();
    if ((!TextUtils.isEmpty(paramString)) && (!paramString.endsWith(".xml")))
    {
      this.mHistoryFileName = (paramString + ".xml");
      return;
    }
    this.mHistoryFileName = paramString;
  }
  
  private boolean addHisoricalRecord(HistoricalRecord paramHistoricalRecord)
  {
    boolean bool = this.mHistoricalRecords.add(paramHistoricalRecord);
    if (bool)
    {
      this.mHistoricalRecordsChanged = true;
      pruneExcessiveHistoricalRecordsIfNeeded();
      persistHistoricalDataIfNeeded();
      sortActivitiesIfNeeded();
      notifyChanged();
    }
    return bool;
  }
  
  private void ensureConsistentState()
  {
    boolean bool = loadActivitiesIfNeeded() | readHistoricalDataIfNeeded();
    pruneExcessiveHistoricalRecordsIfNeeded();
    if (bool)
    {
      sortActivitiesIfNeeded();
      notifyChanged();
    }
  }
  
  public static ActivityChooserModel get(Context paramContext, String paramString)
  {
    synchronized (sRegistryLock)
    {
      ActivityChooserModel localActivityChooserModel = (ActivityChooserModel)sDataModelRegistry.get(paramString);
      if (localActivityChooserModel == null)
      {
        localActivityChooserModel = new ActivityChooserModel(paramContext, paramString);
        sDataModelRegistry.put(paramString, localActivityChooserModel);
      }
      return localActivityChooserModel;
    }
  }
  
  private boolean loadActivitiesIfNeeded()
  {
    boolean bool1 = this.mReloadActivities;
    boolean bool2 = false;
    if (bool1)
    {
      Intent localIntent = this.mIntent;
      bool2 = false;
      if (localIntent != null)
      {
        this.mReloadActivities = false;
        this.mActivities.clear();
        List localList = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0);
        int i = localList.size();
        for (int j = 0; j < i; j++)
        {
          ResolveInfo localResolveInfo = (ResolveInfo)localList.get(j);
          this.mActivities.add(new ActivityResolveInfo(localResolveInfo));
        }
        bool2 = true;
      }
    }
    return bool2;
  }
  
  private void persistHistoricalDataIfNeeded()
  {
    if (!this.mReadShareHistoryCalled) {
      throw new IllegalStateException("No preceding call to #readHistoricalData");
    }
    if (!this.mHistoricalRecordsChanged) {}
    do
    {
      return;
      this.mHistoricalRecordsChanged = false;
    } while (TextUtils.isEmpty(this.mHistoryFileName));
    PersistHistoryAsyncTask localPersistHistoryAsyncTask = new PersistHistoryAsyncTask(null);
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.mHistoricalRecords;
    arrayOfObject[1] = this.mHistoryFileName;
    AsyncTaskCompat.executeParallel(localPersistHistoryAsyncTask, arrayOfObject);
  }
  
  private void pruneExcessiveHistoricalRecordsIfNeeded()
  {
    int i = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
    if (i <= 0) {}
    for (;;)
    {
      return;
      this.mHistoricalRecordsChanged = true;
      for (int j = 0; j < i; j++) {
        this.mHistoricalRecords.remove(0);
      }
    }
  }
  
  private boolean readHistoricalDataIfNeeded()
  {
    if ((this.mCanReadHistoricalData) && (this.mHistoricalRecordsChanged) && (!TextUtils.isEmpty(this.mHistoryFileName)))
    {
      this.mCanReadHistoricalData = false;
      this.mReadShareHistoryCalled = true;
      readHistoricalDataImpl();
      return true;
    }
    return false;
  }
  
  /* Error */
  private void readHistoricalDataImpl()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 123	android/support/v7/internal/widget/ActivityChooserModel:mContext	Landroid/content/Context;
    //   4: aload_0
    //   5: getfield 147	android/support/v7/internal/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   8: invokevirtual 256	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   11: astore_2
    //   12: invokestatic 262	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
    //   15: astore 11
    //   17: aload 11
    //   19: aload_2
    //   20: aconst_null
    //   21: invokeinterface 268 3 0
    //   26: iconst_0
    //   27: istore 12
    //   29: iload 12
    //   31: iconst_1
    //   32: if_icmpeq +21 -> 53
    //   35: iload 12
    //   37: iconst_2
    //   38: if_icmpeq +15 -> 53
    //   41: aload 11
    //   43: invokeinterface 271 1 0
    //   48: istore 12
    //   50: goto -21 -> 29
    //   53: ldc 40
    //   55: aload 11
    //   57: invokeinterface 274 1 0
    //   62: invokevirtual 277	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   65: ifne +43 -> 108
    //   68: new 250	org/xmlpull/v1/XmlPullParserException
    //   71: dup
    //   72: ldc_w 279
    //   75: invokespecial 280	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
    //   78: athrow
    //   79: astore 8
    //   81: new 137	java/lang/StringBuilder
    //   84: dup
    //   85: ldc_w 282
    //   88: invokespecial 283	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   91: aload_0
    //   92: getfield 147	android/support/v7/internal/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   95: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   98: pop
    //   99: aload_2
    //   100: ifnull +7 -> 107
    //   103: aload_2
    //   104: invokevirtual 288	java/io/FileInputStream:close	()V
    //   107: return
    //   108: aload_0
    //   109: getfield 98	android/support/v7/internal/widget/ActivityChooserModel:mHistoricalRecords	Ljava/util/List;
    //   112: astore 13
    //   114: aload 13
    //   116: invokeinterface 200 1 0
    //   121: aload 11
    //   123: invokeinterface 271 1 0
    //   128: istore 14
    //   130: iload 14
    //   132: iconst_1
    //   133: if_icmpeq +138 -> 271
    //   136: iload 14
    //   138: iconst_3
    //   139: if_icmpeq -18 -> 121
    //   142: iload 14
    //   144: iconst_4
    //   145: if_icmpeq -24 -> 121
    //   148: ldc 37
    //   150: aload 11
    //   152: invokeinterface 274 1 0
    //   157: invokevirtual 277	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   160: ifne +46 -> 206
    //   163: new 250	org/xmlpull/v1/XmlPullParserException
    //   166: dup
    //   167: ldc_w 290
    //   170: invokespecial 280	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
    //   173: athrow
    //   174: astore 5
    //   176: new 137	java/lang/StringBuilder
    //   179: dup
    //   180: ldc_w 282
    //   183: invokespecial 283	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   186: aload_0
    //   187: getfield 147	android/support/v7/internal/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   190: invokevirtual 142	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   193: pop
    //   194: aload_2
    //   195: ifnull -88 -> 107
    //   198: aload_2
    //   199: invokevirtual 288	java/io/FileInputStream:close	()V
    //   202: return
    //   203: astore 7
    //   205: return
    //   206: aload 13
    //   208: new 292	android/support/v7/internal/widget/ActivityChooserModel$HistoricalRecord
    //   211: dup
    //   212: aload 11
    //   214: aconst_null
    //   215: ldc 8
    //   217: invokeinterface 296 3 0
    //   222: aload 11
    //   224: aconst_null
    //   225: ldc 11
    //   227: invokeinterface 296 3 0
    //   232: invokestatic 302	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   235: aload 11
    //   237: aconst_null
    //   238: ldc 14
    //   240: invokeinterface 296 3 0
    //   245: invokestatic 308	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   248: invokespecial 311	android/support/v7/internal/widget/ActivityChooserModel$HistoricalRecord:<init>	(Ljava/lang/String;JF)V
    //   251: invokeinterface 162 2 0
    //   256: pop
    //   257: goto -136 -> 121
    //   260: astore_3
    //   261: aload_2
    //   262: ifnull +7 -> 269
    //   265: aload_2
    //   266: invokevirtual 288	java/io/FileInputStream:close	()V
    //   269: aload_3
    //   270: athrow
    //   271: aload_2
    //   272: ifnull -165 -> 107
    //   275: aload_2
    //   276: invokevirtual 288	java/io/FileInputStream:close	()V
    //   279: return
    //   280: astore 15
    //   282: return
    //   283: astore 10
    //   285: return
    //   286: astore 4
    //   288: goto -19 -> 269
    //   291: astore_1
    //   292: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	293	0	this	ActivityChooserModel
    //   291	1	1	localFileNotFoundException	FileNotFoundException
    //   11	265	2	localFileInputStream	java.io.FileInputStream
    //   260	10	3	localObject	Object
    //   286	1	4	localIOException1	IOException
    //   174	1	5	localIOException2	IOException
    //   203	1	7	localIOException3	IOException
    //   79	1	8	localXmlPullParserException	org.xmlpull.v1.XmlPullParserException
    //   283	1	10	localIOException4	IOException
    //   15	221	11	localXmlPullParser	org.xmlpull.v1.XmlPullParser
    //   27	22	12	i	int
    //   112	95	13	localList	List
    //   128	18	14	j	int
    //   280	1	15	localIOException5	IOException
    // Exception table:
    //   from	to	target	type
    //   12	26	79	org/xmlpull/v1/XmlPullParserException
    //   41	50	79	org/xmlpull/v1/XmlPullParserException
    //   53	79	79	org/xmlpull/v1/XmlPullParserException
    //   108	121	79	org/xmlpull/v1/XmlPullParserException
    //   121	130	79	org/xmlpull/v1/XmlPullParserException
    //   148	174	79	org/xmlpull/v1/XmlPullParserException
    //   206	257	79	org/xmlpull/v1/XmlPullParserException
    //   12	26	174	java/io/IOException
    //   41	50	174	java/io/IOException
    //   53	79	174	java/io/IOException
    //   108	121	174	java/io/IOException
    //   121	130	174	java/io/IOException
    //   148	174	174	java/io/IOException
    //   206	257	174	java/io/IOException
    //   198	202	203	java/io/IOException
    //   12	26	260	finally
    //   41	50	260	finally
    //   53	79	260	finally
    //   81	99	260	finally
    //   108	121	260	finally
    //   121	130	260	finally
    //   148	174	260	finally
    //   176	194	260	finally
    //   206	257	260	finally
    //   275	279	280	java/io/IOException
    //   103	107	283	java/io/IOException
    //   265	269	286	java/io/IOException
    //   0	12	291	java/io/FileNotFoundException
  }
  
  private boolean sortActivitiesIfNeeded()
  {
    if ((this.mActivitySorter != null) && (this.mIntent != null) && (!this.mActivities.isEmpty()) && (!this.mHistoricalRecords.isEmpty()))
    {
      this.mActivitySorter.sort(this.mIntent, this.mActivities, Collections.unmodifiableList(this.mHistoricalRecords));
      return true;
    }
    return false;
  }
  
  public Intent chooseActivity(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mIntent == null) {
        return null;
      }
      ensureConsistentState();
      ActivityResolveInfo localActivityResolveInfo = (ActivityResolveInfo)this.mActivities.get(paramInt);
      ComponentName localComponentName = new ComponentName(localActivityResolveInfo.resolveInfo.activityInfo.packageName, localActivityResolveInfo.resolveInfo.activityInfo.name);
      Intent localIntent1 = new Intent(this.mIntent);
      localIntent1.setComponent(localComponentName);
      if (this.mActivityChoserModelPolicy != null)
      {
        Intent localIntent2 = new Intent(localIntent1);
        if (this.mActivityChoserModelPolicy.onChooseActivity(this, localIntent2)) {
          return null;
        }
      }
      addHisoricalRecord(new HistoricalRecord(localComponentName, System.currentTimeMillis(), 1.0F));
      return localIntent1;
    }
  }
  
  public ResolveInfo getActivity(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      ResolveInfo localResolveInfo = ((ActivityResolveInfo)this.mActivities.get(paramInt)).resolveInfo;
      return localResolveInfo;
    }
  }
  
  public int getActivityCount()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      int i = this.mActivities.size();
      return i;
    }
  }
  
  public int getActivityIndex(ResolveInfo paramResolveInfo)
  {
    for (;;)
    {
      int j;
      synchronized (this.mInstanceLock)
      {
        ensureConsistentState();
        List localList = this.mActivities;
        int i = localList.size();
        j = 0;
        if (j < i)
        {
          if (((ActivityResolveInfo)localList.get(j)).resolveInfo == paramResolveInfo) {
            return j;
          }
        }
        else {
          return -1;
        }
      }
      j++;
    }
  }
  
  public ResolveInfo getDefaultActivity()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      if (!this.mActivities.isEmpty())
      {
        ResolveInfo localResolveInfo = ((ActivityResolveInfo)this.mActivities.get(0)).resolveInfo;
        return localResolveInfo;
      }
      return null;
    }
  }
  
  public int getHistoryMaxSize()
  {
    synchronized (this.mInstanceLock)
    {
      int i = this.mHistoryMaxSize;
      return i;
    }
  }
  
  public int getHistorySize()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      int i = this.mHistoricalRecords.size();
      return i;
    }
  }
  
  public Intent getIntent()
  {
    synchronized (this.mInstanceLock)
    {
      Intent localIntent = this.mIntent;
      return localIntent;
    }
  }
  
  public void setActivitySorter(ActivitySorter paramActivitySorter)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mActivitySorter == paramActivitySorter) {
        return;
      }
      this.mActivitySorter = paramActivitySorter;
      if (sortActivitiesIfNeeded()) {
        notifyChanged();
      }
      return;
    }
  }
  
  public void setDefaultActivity(int paramInt)
  {
    for (;;)
    {
      synchronized (this.mInstanceLock)
      {
        ensureConsistentState();
        ActivityResolveInfo localActivityResolveInfo1 = (ActivityResolveInfo)this.mActivities.get(paramInt);
        ActivityResolveInfo localActivityResolveInfo2 = (ActivityResolveInfo)this.mActivities.get(0);
        if (localActivityResolveInfo2 != null)
        {
          f = 5.0F + (localActivityResolveInfo2.weight - localActivityResolveInfo1.weight);
          addHisoricalRecord(new HistoricalRecord(new ComponentName(localActivityResolveInfo1.resolveInfo.activityInfo.packageName, localActivityResolveInfo1.resolveInfo.activityInfo.name), System.currentTimeMillis(), f));
          return;
        }
      }
      float f = 1.0F;
    }
  }
  
  public void setHistoryMaxSize(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mHistoryMaxSize == paramInt) {
        return;
      }
      this.mHistoryMaxSize = paramInt;
      pruneExcessiveHistoricalRecordsIfNeeded();
      if (sortActivitiesIfNeeded()) {
        notifyChanged();
      }
      return;
    }
  }
  
  public void setIntent(Intent paramIntent)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mIntent == paramIntent) {
        return;
      }
      this.mIntent = paramIntent;
      this.mReloadActivities = true;
      ensureConsistentState();
      return;
    }
  }
  
  public void setOnChooseActivityListener(OnChooseActivityListener paramOnChooseActivityListener)
  {
    synchronized (this.mInstanceLock)
    {
      this.mActivityChoserModelPolicy = paramOnChooseActivityListener;
      return;
    }
  }
  
  public static abstract interface ActivityChooserModelClient
  {
    public abstract void setActivityChooserModel(ActivityChooserModel paramActivityChooserModel);
  }
  
  public final class ActivityResolveInfo
    implements Comparable<ActivityResolveInfo>
  {
    public final ResolveInfo resolveInfo;
    public float weight;
    
    public ActivityResolveInfo(ResolveInfo paramResolveInfo)
    {
      this.resolveInfo = paramResolveInfo;
    }
    
    public final int compareTo(ActivityResolveInfo paramActivityResolveInfo)
    {
      return Float.floatToIntBits(paramActivityResolveInfo.weight) - Float.floatToIntBits(this.weight);
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      ActivityResolveInfo localActivityResolveInfo;
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        localActivityResolveInfo = (ActivityResolveInfo)paramObject;
      } while (Float.floatToIntBits(this.weight) == Float.floatToIntBits(localActivityResolveInfo.weight));
      return false;
    }
    
    public final int hashCode()
    {
      return 31 + Float.floatToIntBits(this.weight);
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append("resolveInfo:").append(this.resolveInfo.toString());
      localStringBuilder.append("; weight:").append(new BigDecimal(this.weight));
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface ActivitySorter
  {
    public abstract void sort(Intent paramIntent, List<ActivityChooserModel.ActivityResolveInfo> paramList, List<ActivityChooserModel.HistoricalRecord> paramList1);
  }
  
  private final class DefaultSorter
    implements ActivityChooserModel.ActivitySorter
  {
    private static final float WEIGHT_DECAY_COEFFICIENT = 0.95F;
    private final Map<String, ActivityChooserModel.ActivityResolveInfo> mPackageNameToActivityMap = new HashMap();
    
    private DefaultSorter() {}
    
    public final void sort(Intent paramIntent, List<ActivityChooserModel.ActivityResolveInfo> paramList, List<ActivityChooserModel.HistoricalRecord> paramList1)
    {
      Map localMap = this.mPackageNameToActivityMap;
      localMap.clear();
      int i = paramList.size();
      for (int j = 0; j < i; j++)
      {
        ActivityChooserModel.ActivityResolveInfo localActivityResolveInfo2 = (ActivityChooserModel.ActivityResolveInfo)paramList.get(j);
        localActivityResolveInfo2.weight = 0.0F;
        localMap.put(localActivityResolveInfo2.resolveInfo.activityInfo.packageName, localActivityResolveInfo2);
      }
      int k = -1 + paramList1.size();
      float f1 = 1.0F;
      int m = k;
      if (m >= 0)
      {
        ActivityChooserModel.HistoricalRecord localHistoricalRecord = (ActivityChooserModel.HistoricalRecord)paramList1.get(m);
        ActivityChooserModel.ActivityResolveInfo localActivityResolveInfo1 = (ActivityChooserModel.ActivityResolveInfo)localMap.get(localHistoricalRecord.activity.getPackageName());
        if (localActivityResolveInfo1 == null) {
          break label178;
        }
        localActivityResolveInfo1.weight += f1 * localHistoricalRecord.weight;
      }
      label178:
      for (float f2 = 0.95F * f1;; f2 = f1)
      {
        m--;
        f1 = f2;
        break;
        Collections.sort(paramList);
        return;
      }
    }
  }
  
  public static final class HistoricalRecord
  {
    public final ComponentName activity;
    public final long time;
    public final float weight;
    
    public HistoricalRecord(ComponentName paramComponentName, long paramLong, float paramFloat)
    {
      this.activity = paramComponentName;
      this.time = paramLong;
      this.weight = paramFloat;
    }
    
    public HistoricalRecord(String paramString, long paramLong, float paramFloat)
    {
      this(ComponentName.unflattenFromString(paramString), paramLong, paramFloat);
    }
    
    public final boolean equals(Object paramObject)
    {
      if (this == paramObject) {}
      HistoricalRecord localHistoricalRecord;
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        localHistoricalRecord = (HistoricalRecord)paramObject;
        if (this.activity == null)
        {
          if (localHistoricalRecord.activity != null) {
            return false;
          }
        }
        else if (!this.activity.equals(localHistoricalRecord.activity)) {
          return false;
        }
        if (this.time != localHistoricalRecord.time) {
          return false;
        }
      } while (Float.floatToIntBits(this.weight) == Float.floatToIntBits(localHistoricalRecord.weight));
      return false;
    }
    
    public final int hashCode()
    {
      if (this.activity == null) {}
      for (int i = 0;; i = this.activity.hashCode()) {
        return 31 * (31 * (i + 31) + (int)(this.time ^ this.time >>> 32)) + Float.floatToIntBits(this.weight);
      }
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append("; activity:").append(this.activity);
      localStringBuilder.append("; time:").append(this.time);
      localStringBuilder.append("; weight:").append(new BigDecimal(this.weight));
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface OnChooseActivityListener
  {
    public abstract boolean onChooseActivity(ActivityChooserModel paramActivityChooserModel, Intent paramIntent);
  }
  
  private final class PersistHistoryAsyncTask
    extends AsyncTask<Object, Void, Void>
  {
    private PersistHistoryAsyncTask() {}
    
    public final Void doInBackground(Object... paramVarArgs)
    {
      int i = 0;
      List localList = (List)paramVarArgs[0];
      String str = (String)paramVarArgs[1];
      FileOutputStream localFileOutputStream;
      for (;;)
      {
        try
        {
          localFileOutputStream = ActivityChooserModel.this.mContext.openFileOutput(str, 0);
          localXmlSerializer = Xml.newSerializer();
          int j;
          ActivityChooserModel.HistoricalRecord localHistoricalRecord;
          localXmlSerializer.endTag(null, "historical-records");
        }
        catch (FileNotFoundException localFileNotFoundException)
        {
          try
          {
            localXmlSerializer.setOutput(localFileOutputStream, null);
            localXmlSerializer.startDocument("UTF-8", Boolean.valueOf(true));
            localXmlSerializer.startTag(null, "historical-records");
            j = localList.size();
            if (i >= j) {
              break label190;
            }
            localHistoricalRecord = (ActivityChooserModel.HistoricalRecord)localList.remove(0);
            localXmlSerializer.startTag(null, "historical-record");
            localXmlSerializer.attribute(null, "activity", localHistoricalRecord.activity.flattenToString());
            localXmlSerializer.attribute(null, "time", String.valueOf(localHistoricalRecord.time));
            localXmlSerializer.attribute(null, "weight", String.valueOf(localHistoricalRecord.weight));
            localXmlSerializer.endTag(null, "historical-record");
            i++;
            continue;
            localFileNotFoundException = localFileNotFoundException;
          }
          catch (IllegalArgumentException localIllegalArgumentException)
          {
            XmlSerializer localXmlSerializer;
            new StringBuilder("Error writing historical recrod file: ").append(ActivityChooserModel.this.mHistoryFileName);
            ActivityChooserModel.access$502(ActivityChooserModel.this, true);
            if (localFileOutputStream == null) {
              continue;
            }
            try
            {
              localFileOutputStream.close();
              return null;
            }
            catch (IOException localIOException5)
            {
              return null;
            }
          }
          catch (IllegalStateException localIllegalStateException)
          {
            new StringBuilder("Error writing historical recrod file: ").append(ActivityChooserModel.this.mHistoryFileName);
            ActivityChooserModel.access$502(ActivityChooserModel.this, true);
            if (localFileOutputStream == null) {
              continue;
            }
            try
            {
              localFileOutputStream.close();
              return null;
            }
            catch (IOException localIOException4)
            {
              return null;
            }
          }
          catch (IOException localIOException2)
          {
            new StringBuilder("Error writing historical recrod file: ").append(ActivityChooserModel.this.mHistoryFileName);
            ActivityChooserModel.access$502(ActivityChooserModel.this, true);
            if (localFileOutputStream == null) {
              continue;
            }
            try
            {
              localFileOutputStream.close();
              return null;
            }
            catch (IOException localIOException3)
            {
              return null;
            }
          }
          finally
          {
            ActivityChooserModel.access$502(ActivityChooserModel.this, true);
            if (localFileOutputStream == null) {
              break label407;
            }
          }
          return null;
        }
        label190:
        localXmlSerializer.endDocument();
        ActivityChooserModel.access$502(ActivityChooserModel.this, true);
        if (localFileOutputStream != null) {
          try
          {
            localFileOutputStream.close();
            return null;
          }
          catch (IOException localIOException6)
          {
            return null;
          }
        }
      }
      try
      {
        localFileOutputStream.close();
        label407:
        throw ((Throwable)localObject);
      }
      catch (IOException localIOException1)
      {
        for (;;) {}
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/ActivityChooserModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */