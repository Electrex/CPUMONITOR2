package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.IntDef;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.styleable;
import android.support.v7.internal.widget.TintTypedArray;
import android.support.v7.internal.widget.ViewUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LinearLayoutCompat
  extends ViewGroup
{
  public static final int HORIZONTAL = 0;
  private static final int INDEX_BOTTOM = 2;
  private static final int INDEX_CENTER_VERTICAL = 0;
  private static final int INDEX_FILL = 3;
  private static final int INDEX_TOP = 1;
  public static final int SHOW_DIVIDER_BEGINNING = 1;
  public static final int SHOW_DIVIDER_END = 4;
  public static final int SHOW_DIVIDER_MIDDLE = 2;
  public static final int SHOW_DIVIDER_NONE = 0;
  public static final int VERTICAL = 1;
  private static final int VERTICAL_GRAVITY_COUNT = 4;
  private boolean mBaselineAligned = true;
  private int mBaselineAlignedChildIndex = -1;
  private int mBaselineChildTop = 0;
  private Drawable mDivider;
  private int mDividerHeight;
  private int mDividerPadding;
  private int mDividerWidth;
  private int mGravity = 8388659;
  private int[] mMaxAscent;
  private int[] mMaxDescent;
  private int mOrientation;
  private int mShowDividers;
  private int mTotalLength;
  private boolean mUseLargestChild;
  private float mWeightSum;
  
  public LinearLayoutCompat(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public LinearLayoutCompat(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public LinearLayoutCompat(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    TintTypedArray localTintTypedArray = TintTypedArray.obtainStyledAttributes(paramContext, paramAttributeSet, R.styleable.LinearLayoutCompat, paramInt, 0);
    int i = localTintTypedArray.getInt(R.styleable.LinearLayoutCompat_android_orientation, -1);
    if (i >= 0) {
      setOrientation(i);
    }
    int j = localTintTypedArray.getInt(R.styleable.LinearLayoutCompat_android_gravity, -1);
    if (j >= 0) {
      setGravity(j);
    }
    boolean bool = localTintTypedArray.getBoolean(R.styleable.LinearLayoutCompat_android_baselineAligned, true);
    if (!bool) {
      setBaselineAligned(bool);
    }
    this.mWeightSum = localTintTypedArray.getFloat(R.styleable.LinearLayoutCompat_android_weightSum, -1.0F);
    this.mBaselineAlignedChildIndex = localTintTypedArray.getInt(R.styleable.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
    this.mUseLargestChild = localTintTypedArray.getBoolean(R.styleable.LinearLayoutCompat_measureWithLargestChild, false);
    setDividerDrawable(localTintTypedArray.getDrawable(R.styleable.LinearLayoutCompat_divider));
    this.mShowDividers = localTintTypedArray.getInt(R.styleable.LinearLayoutCompat_showDividers, 0);
    this.mDividerPadding = localTintTypedArray.getDimensionPixelSize(R.styleable.LinearLayoutCompat_dividerPadding, 0);
    localTintTypedArray.recycle();
  }
  
  private void forceUniformHeight(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
    for (int j = 0; j < paramInt1; j++)
    {
      View localView = getVirtualChildAt(j);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.height == -1)
        {
          int k = localLayoutParams.width;
          localLayoutParams.width = localView.getMeasuredWidth();
          measureChildWithMargins(localView, paramInt2, 0, i, 0);
          localLayoutParams.width = k;
        }
      }
    }
  }
  
  private void forceUniformWidth(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
    for (int j = 0; j < paramInt1; j++)
    {
      View localView = getVirtualChildAt(j);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.width == -1)
        {
          int k = localLayoutParams.height;
          localLayoutParams.height = localView.getMeasuredHeight();
          measureChildWithMargins(localView, i, 0, paramInt2, 0);
          localLayoutParams.height = k;
        }
      }
    }
  }
  
  private void setChildFrame(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramView.layout(paramInt1, paramInt2, paramInt1 + paramInt3, paramInt2 + paramInt4);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  void drawDividersHorizontal(Canvas paramCanvas)
  {
    int i = getVirtualChildCount();
    boolean bool = ViewUtils.isLayoutRtl(this);
    int j = 0;
    if (j < i)
    {
      View localView2 = getVirtualChildAt(j);
      LayoutParams localLayoutParams2;
      if ((localView2 != null) && (localView2.getVisibility() != 8) && (hasDividerBeforeChildAt(j)))
      {
        localLayoutParams2 = (LayoutParams)localView2.getLayoutParams();
        if (!bool) {
          break label91;
        }
      }
      label91:
      for (int m = localView2.getRight() + localLayoutParams2.rightMargin;; m = localView2.getLeft() - localLayoutParams2.leftMargin - this.mDividerWidth)
      {
        drawVerticalDivider(paramCanvas, m);
        j++;
        break;
      }
    }
    View localView1;
    int k;
    if (hasDividerBeforeChildAt(i))
    {
      localView1 = getVirtualChildAt(i - 1);
      if (localView1 != null) {
        break label171;
      }
      if (!bool) {
        break label152;
      }
      k = getPaddingLeft();
    }
    for (;;)
    {
      drawVerticalDivider(paramCanvas, k);
      return;
      label152:
      k = getWidth() - getPaddingRight() - this.mDividerWidth;
      continue;
      label171:
      LayoutParams localLayoutParams1 = (LayoutParams)localView1.getLayoutParams();
      if (bool) {
        k = localView1.getLeft() - localLayoutParams1.leftMargin - this.mDividerWidth;
      } else {
        k = localView1.getRight() + localLayoutParams1.rightMargin;
      }
    }
  }
  
  void drawDividersVertical(Canvas paramCanvas)
  {
    int i = getVirtualChildCount();
    for (int j = 0; j < i; j++)
    {
      View localView2 = getVirtualChildAt(j);
      if ((localView2 != null) && (localView2.getVisibility() != 8) && (hasDividerBeforeChildAt(j)))
      {
        LayoutParams localLayoutParams2 = (LayoutParams)localView2.getLayoutParams();
        drawHorizontalDivider(paramCanvas, localView2.getTop() - localLayoutParams2.topMargin - this.mDividerHeight);
      }
    }
    View localView1;
    if (hasDividerBeforeChildAt(i))
    {
      localView1 = getVirtualChildAt(i - 1);
      if (localView1 != null) {
        break label125;
      }
    }
    label125:
    LayoutParams localLayoutParams1;
    for (int k = getHeight() - getPaddingBottom() - this.mDividerHeight;; k = localView1.getBottom() + localLayoutParams1.bottomMargin)
    {
      drawHorizontalDivider(paramCanvas, k);
      return;
      localLayoutParams1 = (LayoutParams)localView1.getLayoutParams();
    }
  }
  
  void drawHorizontalDivider(Canvas paramCanvas, int paramInt)
  {
    this.mDivider.setBounds(getPaddingLeft() + this.mDividerPadding, paramInt, getWidth() - getPaddingRight() - this.mDividerPadding, paramInt + this.mDividerHeight);
    this.mDivider.draw(paramCanvas);
  }
  
  void drawVerticalDivider(Canvas paramCanvas, int paramInt)
  {
    this.mDivider.setBounds(paramInt, getPaddingTop() + this.mDividerPadding, paramInt + this.mDividerWidth, getHeight() - getPaddingBottom() - this.mDividerPadding);
    this.mDivider.draw(paramCanvas);
  }
  
  protected LayoutParams generateDefaultLayoutParams()
  {
    if (this.mOrientation == 0) {
      return new LayoutParams(-2, -2);
    }
    if (this.mOrientation == 1) {
      return new LayoutParams(-1, -2);
    }
    return null;
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new LayoutParams(paramLayoutParams);
  }
  
  public int getBaseline()
  {
    int i = -1;
    if (this.mBaselineAlignedChildIndex < 0) {
      i = super.getBaseline();
    }
    View localView;
    int j;
    do
    {
      return i;
      if (getChildCount() <= this.mBaselineAlignedChildIndex) {
        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
      }
      localView = getChildAt(this.mBaselineAlignedChildIndex);
      j = localView.getBaseline();
      if (j != i) {
        break;
      }
    } while (this.mBaselineAlignedChildIndex == 0);
    throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
    int k = this.mBaselineChildTop;
    int n;
    if (this.mOrientation == 1)
    {
      n = 0x70 & this.mGravity;
      if (n == 48) {}
    }
    int m;
    switch (n)
    {
    default: 
      m = k;
    }
    for (;;)
    {
      return j + (m + ((LayoutParams)localView.getLayoutParams()).topMargin);
      m = getBottom() - getTop() - getPaddingBottom() - this.mTotalLength;
      continue;
      m = k + (getBottom() - getTop() - getPaddingTop() - getPaddingBottom() - this.mTotalLength) / 2;
    }
  }
  
  public int getBaselineAlignedChildIndex()
  {
    return this.mBaselineAlignedChildIndex;
  }
  
  int getChildrenSkipCount(View paramView, int paramInt)
  {
    return 0;
  }
  
  public Drawable getDividerDrawable()
  {
    return this.mDivider;
  }
  
  public int getDividerPadding()
  {
    return this.mDividerPadding;
  }
  
  public int getDividerWidth()
  {
    return this.mDividerWidth;
  }
  
  int getLocationOffset(View paramView)
  {
    return 0;
  }
  
  int getNextLocationOffset(View paramView)
  {
    return 0;
  }
  
  public int getOrientation()
  {
    return this.mOrientation;
  }
  
  public int getShowDividers()
  {
    return this.mShowDividers;
  }
  
  View getVirtualChildAt(int paramInt)
  {
    return getChildAt(paramInt);
  }
  
  int getVirtualChildCount()
  {
    return getChildCount();
  }
  
  public float getWeightSum()
  {
    return this.mWeightSum;
  }
  
  protected boolean hasDividerBeforeChildAt(int paramInt)
  {
    if (paramInt == 0) {
      if ((0x1 & this.mShowDividers) == 0) {}
    }
    do
    {
      return true;
      return false;
      if (paramInt != getChildCount()) {
        break;
      }
    } while ((0x4 & this.mShowDividers) != 0);
    return false;
    if ((0x2 & this.mShowDividers) != 0) {
      for (int i = paramInt - 1;; i--)
      {
        if (i < 0) {
          break label74;
        }
        if (getChildAt(i).getVisibility() != 8) {
          break;
        }
      }
    }
    return false;
    label74:
    return false;
  }
  
  public boolean isBaselineAligned()
  {
    return this.mBaselineAligned;
  }
  
  public boolean isMeasureWithLargestChildEnabled()
  {
    return this.mUseLargestChild;
  }
  
  void layoutHorizontal(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    boolean bool1 = ViewUtils.isLayoutRtl(this);
    int i = getPaddingTop();
    int j = paramInt4 - paramInt2;
    int k = j - getPaddingBottom();
    int m = j - i - getPaddingBottom();
    int n = getVirtualChildCount();
    int i1 = 0x800007 & this.mGravity;
    int i2 = 0x70 & this.mGravity;
    boolean bool2 = this.mBaselineAligned;
    int[] arrayOfInt1 = this.mMaxAscent;
    int[] arrayOfInt2 = this.mMaxDescent;
    int i3;
    int i17;
    int i4;
    switch (GravityCompat.getAbsoluteGravity(i1, ViewCompat.getLayoutDirection(this)))
    {
    default: 
      i3 = getPaddingLeft();
      if (bool1)
      {
        i17 = n - 1;
        i4 = -1;
      }
      break;
    }
    for (int i5 = i17;; i5 = 0)
    {
      int i6 = 0;
      label143:
      int i7;
      View localView;
      int i8;
      if (i6 < n)
      {
        i7 = i5 + i4 * i6;
        localView = getVirtualChildAt(i7);
        if (localView == null)
        {
          i3 += measureNullChild(i7);
          i8 = i6;
        }
      }
      for (;;)
      {
        i6 = i8 + 1;
        break label143;
        i3 = paramInt3 + getPaddingLeft() - paramInt1 - this.mTotalLength;
        break;
        i3 = getPaddingLeft() + (paramInt3 - paramInt1 - this.mTotalLength) / 2;
        break;
        if (localView.getVisibility() != 8)
        {
          int i9 = localView.getMeasuredWidth();
          int i10 = localView.getMeasuredHeight();
          LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
          if ((bool2) && (localLayoutParams.height != -1)) {}
          for (int i11 = localView.getBaseline();; i11 = -1)
          {
            int i12 = localLayoutParams.gravity;
            if (i12 < 0) {
              i12 = i2;
            }
            int i13;
            switch (i12 & 0x70)
            {
            default: 
              i13 = i;
              label348:
              if (!hasDividerBeforeChildAt(i7)) {
                break;
              }
            }
            for (int i15 = i3 + this.mDividerWidth;; i15 = i3)
            {
              int i16 = i15 + localLayoutParams.leftMargin;
              setChildFrame(localView, i16 + getLocationOffset(localView), i13, i9, i10);
              i3 = i16 + (i9 + localLayoutParams.rightMargin + getNextLocationOffset(localView));
              i8 = i6 + getChildrenSkipCount(localView, i7);
              break;
              i13 = i + localLayoutParams.topMargin;
              if (i11 == -1) {
                break label348;
              }
              i13 += arrayOfInt1[1] - i11;
              break label348;
              i13 = i + (m - i10) / 2 + localLayoutParams.topMargin - localLayoutParams.bottomMargin;
              break label348;
              i13 = k - i10 - localLayoutParams.bottomMargin;
              if (i11 == -1) {
                break label348;
              }
              int i14 = localView.getMeasuredHeight() - i11;
              i13 -= arrayOfInt2[2] - i14;
              break label348;
              return;
            }
          }
        }
        i8 = i6;
      }
      i4 = 1;
    }
  }
  
  void layoutVertical(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = getPaddingLeft();
    int j = paramInt3 - paramInt1;
    int k = j - getPaddingRight();
    int m = j - i - getPaddingRight();
    int n = getVirtualChildCount();
    int i1 = 0x70 & this.mGravity;
    int i2 = 0x800007 & this.mGravity;
    int i3;
    int i4;
    int i5;
    label97:
    View localView;
    int i6;
    switch (i1)
    {
    default: 
      i3 = getPaddingTop();
      i4 = 0;
      i5 = i3;
      if (i4 < n)
      {
        localView = getVirtualChildAt(i4);
        if (localView == null)
        {
          i5 += measureNullChild(i4);
          i6 = i4;
        }
      }
      break;
    }
    for (;;)
    {
      i4 = i6 + 1;
      break label97;
      i3 = paramInt4 + getPaddingTop() - paramInt2 - this.mTotalLength;
      break;
      i3 = getPaddingTop() + (paramInt4 - paramInt2 - this.mTotalLength) / 2;
      break;
      if (localView.getVisibility() != 8)
      {
        int i7 = localView.getMeasuredWidth();
        int i8 = localView.getMeasuredHeight();
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        int i9 = localLayoutParams.gravity;
        if (i9 < 0) {
          i9 = i2;
        }
        int i10;
        switch (0x7 & GravityCompat.getAbsoluteGravity(i9, ViewCompat.getLayoutDirection(this)))
        {
        default: 
          i10 = i + localLayoutParams.leftMargin;
          label278:
          if (!hasDividerBeforeChildAt(i4)) {
            break;
          }
        }
        for (int i11 = i5 + this.mDividerHeight;; i11 = i5)
        {
          int i12 = i11 + localLayoutParams.topMargin;
          setChildFrame(localView, i10, i12 + getLocationOffset(localView), i7, i8);
          i5 = i12 + (i8 + localLayoutParams.bottomMargin + getNextLocationOffset(localView));
          i6 = i4 + getChildrenSkipCount(localView, i4);
          break;
          i10 = i + (m - i7) / 2 + localLayoutParams.leftMargin - localLayoutParams.rightMargin;
          break label278;
          i10 = k - i7 - localLayoutParams.rightMargin;
          break label278;
          return;
        }
      }
      i6 = i4;
    }
  }
  
  void measureChildBeforeLayout(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    measureChildWithMargins(paramView, paramInt2, paramInt3, paramInt4, paramInt5);
  }
  
  void measureHorizontal(int paramInt1, int paramInt2)
  {
    this.mTotalLength = 0;
    int i = 0;
    int j = 0;
    int k = 0;
    int m = 0;
    int n = 1;
    float f1 = 0.0F;
    int i1 = getVirtualChildCount();
    int i2 = View.MeasureSpec.getMode(paramInt1);
    int i3 = View.MeasureSpec.getMode(paramInt2);
    int i4 = 0;
    int i5 = 0;
    if ((this.mMaxAscent == null) || (this.mMaxDescent == null))
    {
      this.mMaxAscent = new int[4];
      this.mMaxDescent = new int[4];
    }
    int[] arrayOfInt1 = this.mMaxAscent;
    int[] arrayOfInt2 = this.mMaxDescent;
    arrayOfInt1[3] = -1;
    arrayOfInt1[2] = -1;
    arrayOfInt1[1] = -1;
    arrayOfInt1[0] = -1;
    arrayOfInt2[3] = -1;
    arrayOfInt2[2] = -1;
    arrayOfInt2[1] = -1;
    arrayOfInt2[0] = -1;
    boolean bool1 = this.mBaselineAligned;
    boolean bool2 = this.mUseLargestChild;
    if (i2 == 1073741824) {}
    int i7;
    int i8;
    View localView5;
    for (int i6 = 1;; i6 = 0)
    {
      i7 = Integer.MIN_VALUE;
      for (i8 = 0;; i8++)
      {
        if (i8 >= i1) {
          break label908;
        }
        localView5 = getVirtualChildAt(i8);
        if (localView5 != null) {
          break;
        }
        this.mTotalLength += measureNullChild(i8);
      }
    }
    LayoutParams localLayoutParams3;
    float f8;
    label305:
    label326:
    int i59;
    int i60;
    label348:
    int i61;
    int i62;
    int i64;
    int i71;
    label419:
    int i65;
    int i66;
    label491:
    int i68;
    label510:
    int i48;
    int i49;
    int i50;
    float f7;
    int i47;
    int i52;
    int i51;
    int i53;
    if (localView5.getVisibility() != 8)
    {
      if (hasDividerBeforeChildAt(i8)) {
        this.mTotalLength += this.mDividerWidth;
      }
      localLayoutParams3 = (LayoutParams)localView5.getLayoutParams();
      f8 = f1 + localLayoutParams3.weight;
      if ((i2 == 1073741824) && (localLayoutParams3.width == 0) && (localLayoutParams3.weight > 0.0F)) {
        if (i6 != 0)
        {
          this.mTotalLength += localLayoutParams3.leftMargin + localLayoutParams3.rightMargin;
          if (!bool1) {
            break label634;
          }
          int i74 = View.MeasureSpec.makeMeasureSpec(0, 0);
          localView5.measure(i74, i74);
          if ((i3 == 1073741824) || (localLayoutParams3.height != -1)) {
            break label2274;
          }
          i59 = 1;
          i60 = 1;
          i61 = localLayoutParams3.topMargin + localLayoutParams3.bottomMargin;
          i62 = i61 + localView5.getMeasuredHeight();
          int i63 = ViewCompat.getMeasuredState(localView5);
          i64 = ViewUtils.combineMeasuredStates(j, i63);
          if (bool1)
          {
            int i70 = localView5.getBaseline();
            if (i70 != -1)
            {
              if (localLayoutParams3.gravity >= 0) {
                break label825;
              }
              i71 = this.mGravity;
              int i72 = (0xFFFFFFFE & (i71 & 0x70) >> 4) >> 1;
              arrayOfInt1[i72] = Math.max(arrayOfInt1[i72], i70);
              arrayOfInt2[i72] = Math.max(arrayOfInt2[i72], i62 - i70);
            }
          }
          i65 = Math.max(i, i62);
          if ((n == 0) || (localLayoutParams3.height != -1)) {
            break label835;
          }
          i66 = 1;
          if (localLayoutParams3.weight <= 0.0F) {
            break label848;
          }
          if (i60 == 0) {
            break label841;
          }
          i68 = i61;
          int i69 = Math.max(m, i68);
          i48 = i66;
          i49 = i69;
          i50 = k;
          f7 = f8;
          i47 = i7;
          i52 = i64;
          i4 = i59;
          i51 = i5;
          i53 = i65;
        }
      }
    }
    for (;;)
    {
      i8 += getChildrenSkipCount(localView5, i8);
      n = i48;
      m = i49;
      k = i50;
      j = i52;
      i = i53;
      i7 = i47;
      i5 = i51;
      f1 = f7;
      break;
      int i73 = this.mTotalLength;
      this.mTotalLength = Math.max(i73, i73 + localLayoutParams3.leftMargin + localLayoutParams3.rightMargin);
      break label305;
      label634:
      i5 = 1;
      break label326;
      int i54 = Integer.MIN_VALUE;
      if ((localLayoutParams3.width == 0) && (localLayoutParams3.weight > 0.0F))
      {
        i54 = 0;
        localLayoutParams3.width = -2;
      }
      int i55 = i54;
      int i56;
      label690:
      int i57;
      if (f8 == 0.0F)
      {
        i56 = this.mTotalLength;
        measureChildBeforeLayout(localView5, i8, paramInt1, i56, paramInt2, 0);
        if (i55 != Integer.MIN_VALUE) {
          localLayoutParams3.width = i55;
        }
        i57 = localView5.getMeasuredWidth();
        if (i6 == 0) {
          break label783;
        }
      }
      label783:
      int i58;
      for (this.mTotalLength += i57 + localLayoutParams3.leftMargin + localLayoutParams3.rightMargin + getNextLocationOffset(localView5);; this.mTotalLength = Math.max(i58, i58 + i57 + localLayoutParams3.leftMargin + localLayoutParams3.rightMargin + getNextLocationOffset(localView5)))
      {
        if (!bool2) {
          break label823;
        }
        i7 = Math.max(i57, i7);
        break;
        i56 = 0;
        break label690;
        i58 = this.mTotalLength;
      }
      label823:
      break label326;
      label825:
      i71 = localLayoutParams3.gravity;
      break label419;
      label835:
      i66 = 0;
      break label491;
      label841:
      i68 = i62;
      break label510;
      label848:
      if (i60 != 0) {}
      for (;;)
      {
        int i67 = Math.max(k, i61);
        i48 = i66;
        i49 = m;
        i50 = i67;
        f7 = f8;
        i47 = i7;
        i52 = i64;
        i4 = i59;
        i51 = i5;
        i53 = i65;
        break;
        i61 = i62;
      }
      label908:
      if ((this.mTotalLength > 0) && (hasDividerBeforeChildAt(i1))) {
        this.mTotalLength += this.mDividerWidth;
      }
      int i9;
      if ((arrayOfInt1[1] != -1) || (arrayOfInt1[0] != -1) || (arrayOfInt1[2] != -1) || (arrayOfInt1[3] != -1)) {
        i9 = Math.max(arrayOfInt1[3], Math.max(arrayOfInt1[0], Math.max(arrayOfInt1[1], arrayOfInt1[2]))) + Math.max(arrayOfInt2[3], Math.max(arrayOfInt2[0], Math.max(arrayOfInt2[1], arrayOfInt2[2])));
      }
      for (int i10 = Math.max(i, i9);; i10 = i)
      {
        if ((bool2) && ((i2 == Integer.MIN_VALUE) || (i2 == 0)))
        {
          this.mTotalLength = 0;
          int i44 = 0;
          if (i44 < i1)
          {
            View localView4 = getVirtualChildAt(i44);
            int i46;
            if (localView4 == null)
            {
              this.mTotalLength += measureNullChild(i44);
              i46 = i44;
            }
            for (;;)
            {
              i44 = i46 + 1;
              break;
              if (localView4.getVisibility() == 8)
              {
                i46 = i44 + getChildrenSkipCount(localView4, i44);
              }
              else
              {
                LayoutParams localLayoutParams2 = (LayoutParams)localView4.getLayoutParams();
                if (i6 != 0)
                {
                  this.mTotalLength += i7 + localLayoutParams2.leftMargin + localLayoutParams2.rightMargin + getNextLocationOffset(localView4);
                  i46 = i44;
                }
                else
                {
                  int i45 = this.mTotalLength;
                  this.mTotalLength = Math.max(i45, i45 + i7 + localLayoutParams2.leftMargin + localLayoutParams2.rightMargin + getNextLocationOffset(localView4));
                  i46 = i44;
                }
              }
            }
          }
        }
        this.mTotalLength += getPaddingLeft() + getPaddingRight();
        int i11 = ViewCompat.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumWidth()), paramInt1, 0);
        int i12 = (0xFFFFFF & i11) - this.mTotalLength;
        int i13;
        float f2;
        int i14;
        int i15;
        int i16;
        int i17;
        View localView1;
        LayoutParams localLayoutParams1;
        int i39;
        View localView2;
        label1525:
        int i27;
        float f5;
        int i26;
        label1563:
        label1601:
        int i29;
        label1620:
        int i31;
        int i33;
        label1661:
        int i35;
        label1687:
        int i37;
        label1719:
        float f3;
        int i22;
        int i24;
        int i23;
        int i25;
        int i21;
        if ((i5 != 0) || ((i12 != 0) && (f1 > 0.0F)))
        {
          if (this.mWeightSum > 0.0F) {
            f1 = this.mWeightSum;
          }
          arrayOfInt1[3] = -1;
          arrayOfInt1[2] = -1;
          arrayOfInt1[1] = -1;
          arrayOfInt1[0] = -1;
          arrayOfInt2[3] = -1;
          arrayOfInt2[2] = -1;
          arrayOfInt2[1] = -1;
          arrayOfInt2[0] = -1;
          i13 = -1;
          this.mTotalLength = 0;
          f2 = f1;
          i14 = n;
          i15 = k;
          i16 = j;
          i17 = 0;
          if (i17 < i1)
          {
            localView1 = getVirtualChildAt(i17);
            if ((localView1 == null) || (localView1.getVisibility() == 8)) {
              break label2241;
            }
            localLayoutParams1 = (LayoutParams)localView1.getLayoutParams();
            float f4 = localLayoutParams1.weight;
            if (f4 <= 0.0F) {
              break label2226;
            }
            i39 = (int)(f4 * i12 / f2);
            float f6 = f2 - f4;
            int i40 = i12 - i39;
            int i41 = getChildMeasureSpec(paramInt2, getPaddingTop() + getPaddingBottom() + localLayoutParams1.topMargin + localLayoutParams1.bottomMargin, localLayoutParams1.height);
            if ((localLayoutParams1.width != 0) || (i2 != 1073741824))
            {
              i39 += localView1.getMeasuredWidth();
              if (i39 < 0) {
                i39 = 0;
              }
              localView2 = localView1;
              localView2.measure(View.MeasureSpec.makeMeasureSpec(i39, 1073741824), i41);
              i27 = ViewUtils.combineMeasuredStates(i16, 0xFF000000 & ViewCompat.getMeasuredState(localView1));
              f5 = f6;
              i26 = i40;
              if (i6 == 0) {
                break label1842;
              }
              this.mTotalLength += localView1.getMeasuredWidth() + localLayoutParams1.leftMargin + localLayoutParams1.rightMargin + getNextLocationOffset(localView1);
              if ((i3 == 1073741824) || (localLayoutParams1.height != -1)) {
                break label1887;
              }
              i29 = 1;
              int i30 = localLayoutParams1.topMargin + localLayoutParams1.bottomMargin;
              i31 = i30 + localView1.getMeasuredHeight();
              int i32 = Math.max(i13, i31);
              if (i29 == 0) {
                break label1893;
              }
              i33 = i30;
              int i34 = Math.max(i15, i33);
              if ((i14 == 0) || (localLayoutParams1.height != -1)) {
                break label1900;
              }
              i35 = 1;
              if (bool1)
              {
                int i36 = localView1.getBaseline();
                if (i36 != -1)
                {
                  if (localLayoutParams1.gravity >= 0) {
                    break label1906;
                  }
                  i37 = this.mGravity;
                  int i38 = (0xFFFFFFFE & (i37 & 0x70) >> 4) >> 1;
                  arrayOfInt1[i38] = Math.max(arrayOfInt1[i38], i36);
                  arrayOfInt2[i38] = Math.max(arrayOfInt2[i38], i31 - i36);
                }
              }
              f3 = f5;
              i22 = i34;
              i24 = i27;
              i23 = i35;
              i25 = i32;
              i21 = i26;
            }
          }
        }
        for (;;)
        {
          i17++;
          i14 = i23;
          i15 = i22;
          i13 = i25;
          i16 = i24;
          f2 = f3;
          i12 = i21;
          break;
          if (i39 > 0)
          {
            localView2 = localView1;
            break label1525;
          }
          localView2 = localView1;
          i39 = 0;
          break label1525;
          label1842:
          int i28 = this.mTotalLength;
          this.mTotalLength = Math.max(i28, i28 + localView1.getMeasuredWidth() + localLayoutParams1.leftMargin + localLayoutParams1.rightMargin + getNextLocationOffset(localView1));
          break label1601;
          label1887:
          i29 = 0;
          break label1620;
          label1893:
          i33 = i31;
          break label1661;
          label1900:
          i35 = 0;
          break label1687;
          label1906:
          i37 = localLayoutParams1.gravity;
          break label1719;
          this.mTotalLength += getPaddingLeft() + getPaddingRight();
          if ((arrayOfInt1[1] != -1) || (arrayOfInt1[0] != -1) || (arrayOfInt1[2] != -1) || (arrayOfInt1[3] != -1)) {
            i13 = Math.max(i13, Math.max(arrayOfInt1[3], Math.max(arrayOfInt1[0], Math.max(arrayOfInt1[1], arrayOfInt1[2]))) + Math.max(arrayOfInt2[3], Math.max(arrayOfInt2[0], Math.max(arrayOfInt2[1], arrayOfInt2[2]))));
          }
          n = i14;
          int i18 = i15;
          j = i16;
          for (int i19 = i13;; i19 = i10)
          {
            if ((n == 0) && (i3 != 1073741824)) {}
            int i42;
            for (;;)
            {
              int i20 = Math.max(i18 + (getPaddingTop() + getPaddingBottom()), getSuggestedMinimumHeight());
              setMeasuredDimension(i11 | 0xFF000000 & j, ViewCompat.resolveSizeAndState(i20, paramInt2, j << 16));
              if (i4 != 0) {
                forceUniformHeight(i1, paramInt1);
              }
              return;
              i42 = Math.max(k, m);
              if ((!bool2) || (i2 == 1073741824)) {
                break;
              }
              for (int i43 = 0; i43 < i1; i43++)
              {
                View localView3 = getVirtualChildAt(i43);
                if ((localView3 != null) && (localView3.getVisibility() != 8) && (((LayoutParams)localView3.getLayoutParams()).weight > 0.0F)) {
                  localView3.measure(View.MeasureSpec.makeMeasureSpec(i7, 1073741824), View.MeasureSpec.makeMeasureSpec(localView3.getMeasuredHeight(), 1073741824));
                }
              }
              i18 = i19;
            }
            i18 = i42;
          }
          label2226:
          i26 = i12;
          i27 = i16;
          f5 = f2;
          break label1563;
          label2241:
          f3 = f2;
          i21 = i12;
          i22 = i15;
          i23 = i14;
          i24 = i16;
          i25 = i13;
        }
      }
      label2274:
      i59 = i4;
      i60 = 0;
      break label348;
      i47 = i7;
      f7 = f1;
      i48 = n;
      i49 = m;
      i50 = k;
      i51 = i5;
      i52 = j;
      i53 = i;
    }
  }
  
  int measureNullChild(int paramInt)
  {
    return 0;
  }
  
  void measureVertical(int paramInt1, int paramInt2)
  {
    this.mTotalLength = 0;
    int i = 0;
    int j = 0;
    int k = 0;
    int m = 0;
    int n = 1;
    float f1 = 0.0F;
    int i1 = getVirtualChildCount();
    int i2 = View.MeasureSpec.getMode(paramInt1);
    int i3 = View.MeasureSpec.getMode(paramInt2);
    int i4 = 0;
    int i5 = 0;
    int i6 = this.mBaselineAlignedChildIndex;
    boolean bool = this.mUseLargestChild;
    int i7 = Integer.MIN_VALUE;
    int i8 = 0;
    View localView5;
    float f7;
    int i51;
    int i52;
    label435:
    int i53;
    int i54;
    int i55;
    int i57;
    int i58;
    label499:
    int i60;
    label518:
    int i40;
    int i41;
    int i42;
    float f6;
    int i39;
    int i44;
    int i43;
    int i45;
    for (;;)
    {
      if (i8 < i1)
      {
        localView5 = getVirtualChildAt(i8);
        if (localView5 == null)
        {
          this.mTotalLength += measureNullChild(i8);
          i8++;
        }
        else
        {
          if (localView5.getVisibility() == 8) {
            break label1626;
          }
          if (hasDividerBeforeChildAt(i8)) {
            this.mTotalLength += this.mDividerHeight;
          }
          LayoutParams localLayoutParams3 = (LayoutParams)localView5.getLayoutParams();
          f7 = f1 + localLayoutParams3.weight;
          if ((i3 == 1073741824) && (localLayoutParams3.height == 0) && (localLayoutParams3.weight > 0.0F))
          {
            int i62 = this.mTotalLength;
            this.mTotalLength = Math.max(i62, i62 + localLayoutParams3.topMargin + localLayoutParams3.bottomMargin);
            i5 = 1;
            if ((i6 >= 0) && (i6 == i8 + 1)) {
              this.mBaselineChildTop = this.mTotalLength;
            }
            if ((i8 < i6) && (localLayoutParams3.weight > 0.0F)) {
              throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
            }
          }
          else
          {
            int i46 = Integer.MIN_VALUE;
            if ((localLayoutParams3.height == 0) && (localLayoutParams3.weight > 0.0F))
            {
              i46 = 0;
              localLayoutParams3.height = -2;
            }
            int i47 = i46;
            if (f7 == 0.0F) {}
            for (int i48 = this.mTotalLength;; i48 = 0)
            {
              measureChildBeforeLayout(localView5, i8, paramInt1, 0, paramInt2, i48);
              if (i47 != Integer.MIN_VALUE) {
                localLayoutParams3.height = i47;
              }
              int i49 = localView5.getMeasuredHeight();
              int i50 = this.mTotalLength;
              this.mTotalLength = Math.max(i50, i50 + i49 + localLayoutParams3.topMargin + localLayoutParams3.bottomMargin + getNextLocationOffset(localView5));
              if (!bool) {
                break;
              }
              i7 = Math.max(i49, i7);
              break;
            }
          }
          if ((i2 == 1073741824) || (localLayoutParams3.width != -1)) {
            break label1616;
          }
          i51 = 1;
          i52 = 1;
          i53 = localLayoutParams3.leftMargin + localLayoutParams3.rightMargin;
          i54 = i53 + localView5.getMeasuredWidth();
          i55 = Math.max(i, i54);
          int i56 = ViewCompat.getMeasuredState(localView5);
          i57 = ViewUtils.combineMeasuredStates(j, i56);
          if ((n != 0) && (localLayoutParams3.width == -1))
          {
            i58 = 1;
            if (localLayoutParams3.weight <= 0.0F) {
              break label623;
            }
            if (i52 == 0) {
              break label616;
            }
            i60 = i53;
            int i61 = Math.max(m, i60);
            i40 = i58;
            i41 = i61;
            i42 = k;
            f6 = f7;
            i39 = i7;
            i44 = i57;
            i4 = i51;
            i43 = i5;
            i45 = i55;
          }
        }
      }
    }
    for (;;)
    {
      i8 += getChildrenSkipCount(localView5, i8);
      n = i40;
      m = i41;
      k = i42;
      j = i44;
      i = i45;
      i7 = i39;
      i5 = i43;
      f1 = f6;
      break;
      i58 = 0;
      break label499;
      label616:
      i60 = i54;
      break label518;
      label623:
      if (i52 != 0) {}
      for (;;)
      {
        int i59 = Math.max(k, i53);
        i40 = i58;
        i41 = m;
        i42 = i59;
        f6 = f7;
        i39 = i7;
        i44 = i57;
        i4 = i51;
        i43 = i5;
        i45 = i55;
        break;
        i53 = i54;
      }
      if ((this.mTotalLength > 0) && (hasDividerBeforeChildAt(i1))) {
        this.mTotalLength += this.mDividerHeight;
      }
      if ((bool) && ((i3 == Integer.MIN_VALUE) || (i3 == 0)))
      {
        this.mTotalLength = 0;
        int i36 = 0;
        if (i36 < i1)
        {
          View localView4 = getVirtualChildAt(i36);
          int i38;
          if (localView4 == null)
          {
            this.mTotalLength += measureNullChild(i36);
            i38 = i36;
          }
          for (;;)
          {
            i36 = i38 + 1;
            break;
            if (localView4.getVisibility() == 8)
            {
              i38 = i36 + getChildrenSkipCount(localView4, i36);
            }
            else
            {
              LayoutParams localLayoutParams2 = (LayoutParams)localView4.getLayoutParams();
              int i37 = this.mTotalLength;
              this.mTotalLength = Math.max(i37, i37 + i7 + localLayoutParams2.topMargin + localLayoutParams2.bottomMargin + getNextLocationOffset(localView4));
              i38 = i36;
            }
          }
        }
      }
      this.mTotalLength += getPaddingTop() + getPaddingBottom();
      int i9 = ViewCompat.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumHeight()), paramInt2, 0);
      int i10 = (0xFFFFFF & i9) - this.mTotalLength;
      int i11;
      int i12;
      int i13;
      int i14;
      int i15;
      float f2;
      int i16;
      View localView1;
      int i30;
      View localView2;
      label1124:
      int i22;
      int i23;
      float f3;
      label1166:
      int i24;
      int i25;
      int i27;
      label1217:
      label1222:
      int i19;
      label1248:
      int i20;
      int i21;
      if ((i5 != 0) || ((i10 != 0) && (f1 > 0.0F)))
      {
        if (this.mWeightSum > 0.0F) {
          f1 = this.mWeightSum;
        }
        this.mTotalLength = 0;
        i11 = n;
        i12 = k;
        i13 = j;
        i14 = i;
        i15 = 0;
        f2 = f1;
        i16 = i10;
        if (i15 < i1)
        {
          localView1 = getVirtualChildAt(i15);
          if (localView1.getVisibility() == 8) {
            break label1589;
          }
          LayoutParams localLayoutParams1 = (LayoutParams)localView1.getLayoutParams();
          float f4 = localLayoutParams1.weight;
          if (f4 <= 0.0F) {
            break label1574;
          }
          i30 = (int)(f4 * i16 / f2);
          float f5 = f2 - f4;
          int i31 = i16 - i30;
          int i32 = getChildMeasureSpec(paramInt1, getPaddingLeft() + getPaddingRight() + localLayoutParams1.leftMargin + localLayoutParams1.rightMargin, localLayoutParams1.width);
          if ((localLayoutParams1.height != 0) || (i3 != 1073741824))
          {
            i30 += localView1.getMeasuredHeight();
            if (i30 < 0) {
              i30 = 0;
            }
            localView2 = localView1;
            localView2.measure(i32, View.MeasureSpec.makeMeasureSpec(i30, 1073741824));
            int i33 = ViewUtils.combineMeasuredStates(i13, 0xFF00 & ViewCompat.getMeasuredState(localView1));
            i22 = i31;
            i23 = i33;
            f3 = f5;
            i24 = localLayoutParams1.leftMargin + localLayoutParams1.rightMargin;
            i25 = i24 + localView1.getMeasuredWidth();
            int i26 = Math.max(i14, i25);
            if ((i2 == 1073741824) || (localLayoutParams1.width != -1)) {
              break label1350;
            }
            i27 = 1;
            if (i27 == 0) {
              break label1356;
            }
            int i28 = Math.max(i12, i24);
            if ((i11 == 0) || (localLayoutParams1.width != -1)) {
              break label1363;
            }
            i19 = 1;
            int i29 = this.mTotalLength;
            this.mTotalLength = Math.max(i29, i29 + localView1.getMeasuredHeight() + localLayoutParams1.topMargin + localLayoutParams1.bottomMargin + getNextLocationOffset(localView1));
            i20 = i28;
            i21 = i26;
          }
        }
      }
      for (;;)
      {
        i15++;
        i11 = i19;
        i12 = i20;
        i13 = i23;
        i14 = i21;
        i16 = i22;
        f2 = f3;
        break;
        if (i30 > 0)
        {
          localView2 = localView1;
          break label1124;
        }
        localView2 = localView1;
        i30 = 0;
        break label1124;
        label1350:
        i27 = 0;
        break label1217;
        label1356:
        i24 = i25;
        break label1222;
        label1363:
        i19 = 0;
        break label1248;
        this.mTotalLength += getPaddingTop() + getPaddingBottom();
        n = i11;
        int i17 = i12;
        j = i13;
        for (int i18 = i14;; i18 = i)
        {
          if ((n == 0) && (i2 != 1073741824)) {}
          int i34;
          for (;;)
          {
            setMeasuredDimension(ViewCompat.resolveSizeAndState(Math.max(i17 + (getPaddingLeft() + getPaddingRight()), getSuggestedMinimumWidth()), paramInt1, j), i9);
            if (i4 != 0) {
              forceUniformWidth(i1, paramInt2);
            }
            return;
            i34 = Math.max(k, m);
            if ((!bool) || (i3 == 1073741824)) {
              break;
            }
            for (int i35 = 0; i35 < i1; i35++)
            {
              View localView3 = getVirtualChildAt(i35);
              if ((localView3 != null) && (localView3.getVisibility() != 8) && (((LayoutParams)localView3.getLayoutParams()).weight > 0.0F)) {
                localView3.measure(View.MeasureSpec.makeMeasureSpec(localView3.getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(i7, 1073741824));
              }
            }
            i17 = i18;
          }
          i17 = i34;
        }
        label1574:
        f3 = f2;
        i22 = i16;
        i23 = i13;
        break label1166;
        label1589:
        f3 = f2;
        i19 = i11;
        i20 = i12;
        i21 = i14;
        i22 = i16;
        i23 = i13;
      }
      label1616:
      i51 = i4;
      i52 = 0;
      break label435;
      label1626:
      i39 = i7;
      f6 = f1;
      i40 = n;
      i41 = m;
      i42 = k;
      i43 = i5;
      i44 = j;
      i45 = i;
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    if (this.mDivider == null) {
      return;
    }
    if (this.mOrientation == 1)
    {
      drawDividersVertical(paramCanvas);
      return;
    }
    drawDividersHorizontal(paramCanvas);
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
      paramAccessibilityEvent.setClassName(LinearLayoutCompat.class.getName());
    }
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
      paramAccessibilityNodeInfo.setClassName(LinearLayoutCompat.class.getName());
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.mOrientation == 1)
    {
      layoutVertical(paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    }
    layoutHorizontal(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    if (this.mOrientation == 1)
    {
      measureVertical(paramInt1, paramInt2);
      return;
    }
    measureHorizontal(paramInt1, paramInt2);
  }
  
  public void setBaselineAligned(boolean paramBoolean)
  {
    this.mBaselineAligned = paramBoolean;
  }
  
  public void setBaselineAlignedChildIndex(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= getChildCount())) {
      throw new IllegalArgumentException("base aligned child index out of range (0, " + getChildCount() + ")");
    }
    this.mBaselineAlignedChildIndex = paramInt;
  }
  
  public void setDividerDrawable(Drawable paramDrawable)
  {
    if (paramDrawable == this.mDivider) {
      return;
    }
    this.mDivider = paramDrawable;
    if (paramDrawable != null) {
      this.mDividerWidth = paramDrawable.getIntrinsicWidth();
    }
    for (this.mDividerHeight = paramDrawable.getIntrinsicHeight();; this.mDividerHeight = 0)
    {
      boolean bool = false;
      if (paramDrawable == null) {
        bool = true;
      }
      setWillNotDraw(bool);
      requestLayout();
      return;
      this.mDividerWidth = 0;
    }
  }
  
  public void setDividerPadding(int paramInt)
  {
    this.mDividerPadding = paramInt;
  }
  
  public void setGravity(int paramInt)
  {
    if (this.mGravity != paramInt) {
      if ((0x800007 & paramInt) != 0) {
        break label44;
      }
    }
    label44:
    for (int i = 0x800003 | paramInt;; i = paramInt)
    {
      if ((i & 0x70) == 0) {
        i |= 0x30;
      }
      this.mGravity = i;
      requestLayout();
      return;
    }
  }
  
  public void setHorizontalGravity(int paramInt)
  {
    int i = paramInt & 0x800007;
    if ((0x800007 & this.mGravity) != i)
    {
      this.mGravity = (i | 0xFF7FFFF8 & this.mGravity);
      requestLayout();
    }
  }
  
  public void setMeasureWithLargestChildEnabled(boolean paramBoolean)
  {
    this.mUseLargestChild = paramBoolean;
  }
  
  public void setOrientation(int paramInt)
  {
    if (this.mOrientation != paramInt)
    {
      this.mOrientation = paramInt;
      requestLayout();
    }
  }
  
  public void setShowDividers(int paramInt)
  {
    if (paramInt != this.mShowDividers) {
      requestLayout();
    }
    this.mShowDividers = paramInt;
  }
  
  public void setVerticalGravity(int paramInt)
  {
    int i = paramInt & 0x70;
    if ((0x70 & this.mGravity) != i)
    {
      this.mGravity = (i | 0xFFFFFF8F & this.mGravity);
      requestLayout();
    }
  }
  
  public void setWeightSum(float paramFloat)
  {
    this.mWeightSum = Math.max(0.0F, paramFloat);
  }
  
  public boolean shouldDelayChildPressedState()
  {
    return false;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  @IntDef(flag=true, value={0L, 1L, 2L, 4L})
  public static @interface DividerMode {}
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    public int gravity = -1;
    public float weight;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.weight = 0.0F;
    }
    
    public LayoutParams(int paramInt1, int paramInt2, float paramFloat)
    {
      super(paramInt2);
      this.weight = paramFloat;
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LinearLayoutCompat_Layout);
      this.weight = localTypedArray.getFloat(R.styleable.LinearLayoutCompat_Layout_android_layout_weight, 0.0F);
      this.gravity = localTypedArray.getInt(R.styleable.LinearLayoutCompat_Layout_android_layout_gravity, -1);
      localTypedArray.recycle();
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.weight = paramLayoutParams.weight;
      this.gravity = paramLayoutParams.gravity;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  @IntDef({0L, 1L})
  public static @interface OrientationMode {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/LinearLayoutCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */