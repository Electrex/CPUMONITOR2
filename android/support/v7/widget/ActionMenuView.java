package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.support.v7.internal.view.menu.ActionMenuItemView;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.support.v7.internal.view.menu.MenuBuilder.Callback;
import android.support.v7.internal.view.menu.MenuBuilder.ItemInvoker;
import android.support.v7.internal.view.menu.MenuItemImpl;
import android.support.v7.internal.view.menu.MenuPresenter.Callback;
import android.support.v7.internal.view.menu.MenuView;
import android.support.v7.internal.widget.ViewUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView
  extends LinearLayoutCompat
  implements MenuBuilder.ItemInvoker, MenuView
{
  static final int GENERATED_ITEM_PADDING = 4;
  static final int MIN_CELL_SIZE = 56;
  private static final String TAG = "ActionMenuView";
  private MenuPresenter.Callback mActionMenuPresenterCallback;
  private Context mContext;
  private boolean mFormatItems;
  private int mFormatItemsWidth;
  private int mGeneratedItemPadding;
  private MenuBuilder mMenu;
  private MenuBuilder.Callback mMenuBuilderCallback;
  private int mMinCellSize;
  private OnMenuItemClickListener mOnMenuItemClickListener;
  private Context mPopupContext;
  private int mPopupTheme;
  private ActionMenuPresenter mPresenter;
  private boolean mReserveOverflow;
  
  public ActionMenuView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ActionMenuView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
    setBaselineAligned(false);
    float f = paramContext.getResources().getDisplayMetrics().density;
    this.mMinCellSize = ((int)(56.0F * f));
    this.mGeneratedItemPadding = ((int)(f * 4.0F));
    this.mPopupContext = paramContext;
    this.mPopupTheme = 0;
  }
  
  static int measureChildForCells(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int i = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt3) - paramInt4, View.MeasureSpec.getMode(paramInt3));
    ActionMenuItemView localActionMenuItemView;
    int j;
    if ((paramView instanceof ActionMenuItemView))
    {
      localActionMenuItemView = (ActionMenuItemView)paramView;
      if ((localActionMenuItemView == null) || (!localActionMenuItemView.hasText())) {
        break label182;
      }
      j = 1;
      label54:
      if ((paramInt2 <= 0) || ((j != 0) && (paramInt2 < 2))) {
        break label188;
      }
      paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt1 * paramInt2, Integer.MIN_VALUE), i);
      int m = paramView.getMeasuredWidth();
      k = m / paramInt1;
      if (m % paramInt1 != 0) {
        k++;
      }
      if ((j == 0) || (k >= 2)) {}
    }
    label182:
    label188:
    for (int k = 2;; k = 0)
    {
      boolean bool1 = localLayoutParams.isOverflowButton;
      boolean bool2 = false;
      if (!bool1)
      {
        bool2 = false;
        if (j != 0) {
          bool2 = true;
        }
      }
      localLayoutParams.expandable = bool2;
      localLayoutParams.cellsUsed = k;
      paramView.measure(View.MeasureSpec.makeMeasureSpec(k * paramInt1, 1073741824), i);
      return k;
      localActionMenuItemView = null;
      break;
      j = 0;
      break label54;
    }
  }
  
  private void onMeasureExactFormat(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.getMode(paramInt2);
    int j = View.MeasureSpec.getSize(paramInt1);
    int k = View.MeasureSpec.getSize(paramInt2);
    int m = getPaddingLeft() + getPaddingRight();
    int n = getPaddingTop() + getPaddingBottom();
    int i1 = getChildMeasureSpec(paramInt2, n, -2);
    int i2 = j - m;
    int i3 = i2 / this.mMinCellSize;
    int i4 = i2 % this.mMinCellSize;
    if (i3 == 0)
    {
      setMeasuredDimension(i2, 0);
      return;
    }
    int i5 = this.mMinCellSize + i4 / i3;
    int i6 = 0;
    int i7 = 0;
    int i8 = 0;
    int i9 = 0;
    int i10 = 0;
    long l1 = 0L;
    int i11 = getChildCount();
    int i12 = 0;
    int i36;
    boolean bool2;
    label249:
    int i37;
    label267:
    int i39;
    int i40;
    label305:
    int i41;
    label316:
    int i42;
    int i44;
    int i33;
    int i34;
    long l6;
    int i35;
    int i32;
    if (i12 < i11)
    {
      View localView4 = getChildAt(i12);
      if (localView4.getVisibility() == 8) {
        break label1284;
      }
      boolean bool1 = localView4 instanceof ActionMenuItemView;
      i36 = i9 + 1;
      if (bool1) {
        localView4.setPadding(this.mGeneratedItemPadding, 0, this.mGeneratedItemPadding, 0);
      }
      LayoutParams localLayoutParams5 = (LayoutParams)localView4.getLayoutParams();
      localLayoutParams5.expanded = false;
      localLayoutParams5.extraPixels = 0;
      localLayoutParams5.cellsUsed = 0;
      localLayoutParams5.expandable = false;
      localLayoutParams5.leftMargin = 0;
      localLayoutParams5.rightMargin = 0;
      if ((bool1) && (((ActionMenuItemView)localView4).hasText()))
      {
        bool2 = true;
        localLayoutParams5.preventEdgeOffset = bool2;
        if (!localLayoutParams5.isOverflowButton) {
          break label415;
        }
        i37 = 1;
        int i38 = measureChildForCells(localView4, i5, i37, i1, n);
        i39 = Math.max(i7, i38);
        if (!localLayoutParams5.expandable) {
          break label1277;
        }
        i40 = i8 + 1;
        if (!localLayoutParams5.isOverflowButton) {
          break label1270;
        }
        i41 = 1;
        i42 = i3 - i38;
        int i43 = localView4.getMeasuredHeight();
        i44 = Math.max(i6, i43);
        if (i38 != 1) {
          break label1235;
        }
        long l8 = l1 | 1 << i12;
        i33 = i44;
        i34 = i42;
        i8 = i40;
        i10 = i41;
        l6 = l8;
        i35 = i39;
        i32 = i36;
      }
    }
    for (;;)
    {
      i12++;
      i7 = i35;
      i6 = i33;
      i3 = i34;
      l1 = l6;
      i9 = i32;
      break;
      bool2 = false;
      break label249;
      label415:
      i37 = i3;
      break label267;
      int i13;
      int i14;
      long l2;
      int i15;
      int i23;
      long l4;
      int i24;
      int i25;
      label470:
      LayoutParams localLayoutParams4;
      int i31;
      int i30;
      if ((i10 != 0) && (i9 == 2))
      {
        i13 = 1;
        i14 = 0;
        l2 = l1;
        i15 = i3;
        if ((i8 <= 0) || (i15 <= 0)) {
          break label760;
        }
        i23 = Integer.MAX_VALUE;
        l4 = 0L;
        i24 = 0;
        i25 = 0;
        if (i25 >= i11) {
          break label579;
        }
        localLayoutParams4 = (LayoutParams)getChildAt(i25).getLayoutParams();
        if (!localLayoutParams4.expandable) {
          break label1224;
        }
        if (localLayoutParams4.cellsUsed >= i23) {
          break label546;
        }
        i31 = localLayoutParams4.cellsUsed;
        l4 = 1 << i25;
        i30 = 1;
      }
      for (;;)
      {
        i25++;
        i23 = i31;
        i24 = i30;
        break label470;
        i13 = 0;
        break;
        label546:
        if (localLayoutParams4.cellsUsed == i23)
        {
          l4 |= 1 << i25;
          i30 = i24 + 1;
          i31 = i23;
          continue;
          label579:
          l2 |= l4;
          int i27;
          int i28;
          long l5;
          label610:
          View localView3;
          LayoutParams localLayoutParams3;
          int i29;
          if (i24 <= i15)
          {
            int i26 = i23 + 1;
            i27 = 0;
            i28 = i15;
            l5 = l2;
            if (i27 < i11)
            {
              localView3 = getChildAt(i27);
              localLayoutParams3 = (LayoutParams)localView3.getLayoutParams();
              if ((l4 & 1 << i27) == 0L)
              {
                if (localLayoutParams3.cellsUsed != i26) {
                  break label1217;
                }
                l5 |= 1 << i27;
                i29 = i28;
              }
            }
          }
          for (;;)
          {
            i27++;
            i28 = i29;
            break label610;
            if ((i13 != 0) && (localLayoutParams3.preventEdgeOffset) && (i28 == 1)) {
              localView3.setPadding(i5 + this.mGeneratedItemPadding, 0, this.mGeneratedItemPadding, 0);
            }
            localLayoutParams3.cellsUsed = (1 + localLayoutParams3.cellsUsed);
            localLayoutParams3.expanded = true;
            i29 = i28 - 1;
            continue;
            l2 = l5;
            i14 = 1;
            i15 = i28;
            break;
            label760:
            long l3 = l2;
            int i16;
            float f1;
            if ((i10 == 0) && (i9 == 1))
            {
              i16 = 1;
              if ((i15 <= 0) || (l3 == 0L)) {
                break label1111;
              }
              int i19 = i9 - 1;
              if ((i15 >= i19) && (i16 == 0) && (i7 <= 1)) {
                break label1111;
              }
              f1 = Long.bitCount(l3);
              if (i16 != 0) {
                break label1210;
              }
              if (((1L & l3) != 0L) && (!((LayoutParams)getChildAt(0).getLayoutParams()).preventEdgeOffset)) {
                f1 -= 0.5F;
              }
              if (((l3 & 1 << i11 - 1) == 0L) || (((LayoutParams)getChildAt(i11 - 1).getLayoutParams()).preventEdgeOffset)) {
                break label1210;
              }
            }
            label921:
            label928:
            label1035:
            label1104:
            label1111:
            label1115:
            label1210:
            for (float f2 = f1 - 0.5F;; f2 = f1)
            {
              int i20;
              int i21;
              LayoutParams localLayoutParams2;
              int i22;
              if (f2 > 0.0F)
              {
                i20 = (int)(i15 * i5 / f2);
                i21 = 0;
                i17 = i14;
                if (i21 >= i11) {
                  break label1115;
                }
                if ((l3 & 1 << i21) == 0L) {
                  break label1104;
                }
                View localView2 = getChildAt(i21);
                localLayoutParams2 = (LayoutParams)localView2.getLayoutParams();
                if (!(localView2 instanceof ActionMenuItemView)) {
                  break label1035;
                }
                localLayoutParams2.extraPixels = i20;
                localLayoutParams2.expanded = true;
                if ((i21 == 0) && (!localLayoutParams2.preventEdgeOffset)) {
                  localLayoutParams2.leftMargin = (-i20 / 2);
                }
                i22 = 1;
              }
              for (;;)
              {
                i21++;
                i17 = i22;
                break label928;
                i16 = 0;
                break;
                i20 = 0;
                break label921;
                if (localLayoutParams2.isOverflowButton)
                {
                  localLayoutParams2.extraPixels = i20;
                  localLayoutParams2.expanded = true;
                  localLayoutParams2.rightMargin = (-i20 / 2);
                  i22 = 1;
                }
                else
                {
                  if (i21 != 0) {
                    localLayoutParams2.leftMargin = (i20 / 2);
                  }
                  if (i21 != i11 - 1) {
                    localLayoutParams2.rightMargin = (i20 / 2);
                  }
                  i22 = i17;
                }
              }
              int i17 = i14;
              if (i17 != 0) {
                for (int i18 = 0; i18 < i11; i18++)
                {
                  View localView1 = getChildAt(i18);
                  LayoutParams localLayoutParams1 = (LayoutParams)localView1.getLayoutParams();
                  if (localLayoutParams1.expanded) {
                    localView1.measure(View.MeasureSpec.makeMeasureSpec(i5 * localLayoutParams1.cellsUsed + localLayoutParams1.extraPixels, 1073741824), i1);
                  }
                }
              }
              if (i != 1073741824) {}
              for (;;)
              {
                setMeasuredDimension(i2, i6);
                return;
                i6 = k;
              }
            }
            label1217:
            i29 = i28;
          }
        }
        else
        {
          label1224:
          i30 = i24;
          i31 = i23;
        }
      }
      label1235:
      i32 = i36;
      i35 = i39;
      long l7 = l1;
      i33 = i44;
      i34 = i42;
      i10 = i41;
      i8 = i40;
      l6 = l7;
      continue;
      label1270:
      i41 = i10;
      break label316;
      label1277:
      i40 = i8;
      break label305;
      label1284:
      i32 = i9;
      l6 = l1;
      i33 = i6;
      i34 = i3;
      i35 = i7;
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return (paramLayoutParams != null) && ((paramLayoutParams instanceof LayoutParams));
  }
  
  public void dismissPopupMenus()
  {
    if (this.mPresenter != null) {
      this.mPresenter.dismissPopupMenus();
    }
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    return false;
  }
  
  protected LayoutParams generateDefaultLayoutParams()
  {
    LayoutParams localLayoutParams = new LayoutParams(-2, -2);
    localLayoutParams.gravity = 16;
    return localLayoutParams;
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if (paramLayoutParams != null)
    {
      if ((paramLayoutParams instanceof LayoutParams)) {}
      for (LayoutParams localLayoutParams = new LayoutParams((LayoutParams)paramLayoutParams);; localLayoutParams = new LayoutParams(paramLayoutParams))
      {
        if (localLayoutParams.gravity <= 0) {
          localLayoutParams.gravity = 16;
        }
        return localLayoutParams;
      }
    }
    return generateDefaultLayoutParams();
  }
  
  public LayoutParams generateOverflowButtonLayoutParams()
  {
    LayoutParams localLayoutParams = generateDefaultLayoutParams();
    localLayoutParams.isOverflowButton = true;
    return localLayoutParams;
  }
  
  public Menu getMenu()
  {
    ActionMenuPresenter localActionMenuPresenter;
    if (this.mMenu == null)
    {
      Context localContext = getContext();
      this.mMenu = new MenuBuilder(localContext);
      this.mMenu.setCallback(new MenuBuilderCallback(null));
      this.mPresenter = new ActionMenuPresenter(localContext);
      this.mPresenter.setReserveOverflow(true);
      localActionMenuPresenter = this.mPresenter;
      if (this.mActionMenuPresenterCallback == null) {
        break label110;
      }
    }
    label110:
    for (Object localObject = this.mActionMenuPresenterCallback;; localObject = new ActionMenuPresenterCallback(null))
    {
      localActionMenuPresenter.setCallback((MenuPresenter.Callback)localObject);
      this.mMenu.addMenuPresenter(this.mPresenter, this.mPopupContext);
      this.mPresenter.setMenuView(this);
      return this.mMenu;
    }
  }
  
  public int getPopupTheme()
  {
    return this.mPopupTheme;
  }
  
  public int getWindowAnimations()
  {
    return 0;
  }
  
  protected boolean hasSupportDividerBeforeChildAt(int paramInt)
  {
    if (paramInt == 0) {
      return false;
    }
    View localView1 = getChildAt(paramInt - 1);
    View localView2 = getChildAt(paramInt);
    int i = getChildCount();
    boolean bool1 = false;
    if (paramInt < i)
    {
      boolean bool2 = localView1 instanceof ActionMenuChildView;
      bool1 = false;
      if (bool2) {
        bool1 = false | ((ActionMenuChildView)localView1).needsDividerAfter();
      }
    }
    if ((paramInt > 0) && ((localView2 instanceof ActionMenuChildView))) {
      return bool1 | ((ActionMenuChildView)localView2).needsDividerBefore();
    }
    return bool1;
  }
  
  public boolean hideOverflowMenu()
  {
    return (this.mPresenter != null) && (this.mPresenter.hideOverflowMenu());
  }
  
  public void initialize(MenuBuilder paramMenuBuilder)
  {
    this.mMenu = paramMenuBuilder;
  }
  
  public boolean invokeItem(MenuItemImpl paramMenuItemImpl)
  {
    return this.mMenu.performItemAction(paramMenuItemImpl, 0);
  }
  
  public boolean isOverflowMenuShowPending()
  {
    return (this.mPresenter != null) && (this.mPresenter.isOverflowMenuShowPending());
  }
  
  public boolean isOverflowMenuShowing()
  {
    return (this.mPresenter != null) && (this.mPresenter.isOverflowMenuShowing());
  }
  
  public boolean isOverflowReserved()
  {
    return this.mReserveOverflow;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (Build.VERSION.SDK_INT >= 8) {
      super.onConfigurationChanged(paramConfiguration);
    }
    this.mPresenter.updateMenuView(false);
    if ((this.mPresenter != null) && (this.mPresenter.isOverflowMenuShowing()))
    {
      this.mPresenter.hideOverflowMenu();
      this.mPresenter.showOverflowMenu();
    }
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    dismissPopupMenus();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (!this.mFormatItems)
    {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    }
    int i = getChildCount();
    int j = (paramInt4 - paramInt2) / 2;
    int k = getDividerWidth();
    int m = 0;
    int n = paramInt3 - paramInt1 - getPaddingRight() - getPaddingLeft();
    int i1 = 0;
    boolean bool = ViewUtils.isLayoutRtl(this);
    int i2 = 0;
    label70:
    View localView4;
    LayoutParams localLayoutParams3;
    int i27;
    int i30;
    int i29;
    label167:
    int i25;
    int i26;
    if (i2 < i)
    {
      localView4 = getChildAt(i2);
      if (localView4.getVisibility() == 8) {
        break label690;
      }
      localLayoutParams3 = (LayoutParams)localView4.getLayoutParams();
      if (localLayoutParams3.isOverflowButton)
      {
        i27 = localView4.getMeasuredWidth();
        if (hasSupportDividerBeforeChildAt(i2)) {
          i27 += k;
        }
        int i28 = localView4.getMeasuredHeight();
        if (bool)
        {
          i30 = getPaddingLeft() + localLayoutParams3.leftMargin;
          i29 = i30 + i27;
          int i31 = j - i28 / 2;
          localView4.layout(i30, i31, i29, i28 + i31);
          i25 = n - i27;
          i1 = 1;
          i26 = m;
        }
      }
    }
    for (;;)
    {
      i2++;
      m = i26;
      n = i25;
      break label70;
      i29 = getWidth() - getPaddingRight() - localLayoutParams3.rightMargin;
      i30 = i29 - i27;
      break label167;
      i25 = n - (localView4.getMeasuredWidth() + localLayoutParams3.leftMargin + localLayoutParams3.rightMargin);
      hasSupportDividerBeforeChildAt(i2);
      i26 = m + 1;
      continue;
      if ((i == 1) && (i1 == 0))
      {
        View localView3 = getChildAt(0);
        int i21 = localView3.getMeasuredWidth();
        int i22 = localView3.getMeasuredHeight();
        int i23 = (paramInt3 - paramInt1) / 2 - i21 / 2;
        int i24 = j - i22 / 2;
        localView3.layout(i23, i24, i21 + i23, i22 + i24);
        return;
      }
      int i3;
      label367:
      int i5;
      label386:
      int i6;
      int i14;
      int i15;
      label413:
      LayoutParams localLayoutParams2;
      int i17;
      int i18;
      if (i1 != 0)
      {
        i3 = 0;
        int i4 = m - i3;
        if (i4 <= 0) {
          break label540;
        }
        i5 = n / i4;
        i6 = Math.max(0, i5);
        if (!bool) {
          break label546;
        }
        i14 = getWidth() - getPaddingRight();
        i15 = 0;
        if (i15 < i)
        {
          View localView2 = getChildAt(i15);
          localLayoutParams2 = (LayoutParams)localView2.getLayoutParams();
          if ((localView2.getVisibility() == 8) || (localLayoutParams2.isOverflowButton)) {
            break label683;
          }
          i17 = i14 - localLayoutParams2.rightMargin;
          i18 = localView2.getMeasuredWidth();
          int i19 = localView2.getMeasuredHeight();
          int i20 = j - i19 / 2;
          localView2.layout(i17 - i18, i20, i17, i19 + i20);
        }
      }
      label540:
      label546:
      label555:
      label676:
      label683:
      for (int i16 = i17 - (i6 + (i18 + localLayoutParams2.leftMargin));; i16 = i14)
      {
        i15++;
        i14 = i16;
        break label413;
        break;
        i3 = 1;
        break label367;
        i5 = 0;
        break label386;
        int i7 = getPaddingLeft();
        int i8 = 0;
        LayoutParams localLayoutParams1;
        int i10;
        int i11;
        if (i8 < i)
        {
          View localView1 = getChildAt(i8);
          localLayoutParams1 = (LayoutParams)localView1.getLayoutParams();
          if ((localView1.getVisibility() == 8) || (localLayoutParams1.isOverflowButton)) {
            break label676;
          }
          i10 = i7 + localLayoutParams1.leftMargin;
          i11 = localView1.getMeasuredWidth();
          int i12 = localView1.getMeasuredHeight();
          int i13 = j - i12 / 2;
          localView1.layout(i10, i13, i10 + i11, i12 + i13);
        }
        for (int i9 = i10 + (i6 + (i11 + localLayoutParams1.rightMargin));; i9 = i7)
        {
          i8++;
          i7 = i9;
          break label555;
          break;
        }
      }
      label690:
      i25 = n;
      i26 = m;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    boolean bool1 = this.mFormatItems;
    if (View.MeasureSpec.getMode(paramInt1) == 1073741824) {}
    int j;
    for (boolean bool2 = true;; bool2 = false)
    {
      this.mFormatItems = bool2;
      if (bool1 != this.mFormatItems) {
        this.mFormatItemsWidth = 0;
      }
      int i = View.MeasureSpec.getSize(paramInt1);
      if ((this.mFormatItems) && (this.mMenu != null) && (i != this.mFormatItemsWidth))
      {
        this.mFormatItemsWidth = i;
        this.mMenu.onItemsChanged(true);
      }
      j = getChildCount();
      if ((!this.mFormatItems) || (j <= 0)) {
        break;
      }
      onMeasureExactFormat(paramInt1, paramInt2);
      return;
    }
    for (int k = 0; k < j; k++)
    {
      LayoutParams localLayoutParams = (LayoutParams)getChildAt(k).getLayoutParams();
      localLayoutParams.rightMargin = 0;
      localLayoutParams.leftMargin = 0;
    }
    super.onMeasure(paramInt1, paramInt2);
  }
  
  public MenuBuilder peekMenu()
  {
    return this.mMenu;
  }
  
  public void setExpandedActionViewsExclusive(boolean paramBoolean)
  {
    this.mPresenter.setExpandedActionViewsExclusive(paramBoolean);
  }
  
  public void setMenuCallbacks(MenuPresenter.Callback paramCallback, MenuBuilder.Callback paramCallback1)
  {
    this.mActionMenuPresenterCallback = paramCallback;
    this.mMenuBuilderCallback = paramCallback1;
  }
  
  public void setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener)
  {
    this.mOnMenuItemClickListener = paramOnMenuItemClickListener;
  }
  
  public void setOverflowReserved(boolean paramBoolean)
  {
    this.mReserveOverflow = paramBoolean;
  }
  
  public void setPopupTheme(int paramInt)
  {
    if (this.mPopupTheme != paramInt)
    {
      this.mPopupTheme = paramInt;
      if (paramInt == 0) {
        this.mPopupContext = this.mContext;
      }
    }
    else
    {
      return;
    }
    this.mPopupContext = new ContextThemeWrapper(this.mContext, paramInt);
  }
  
  public void setPresenter(ActionMenuPresenter paramActionMenuPresenter)
  {
    this.mPresenter = paramActionMenuPresenter;
    this.mPresenter.setMenuView(this);
  }
  
  public boolean showOverflowMenu()
  {
    return (this.mPresenter != null) && (this.mPresenter.showOverflowMenu());
  }
  
  public static abstract interface ActionMenuChildView
  {
    public abstract boolean needsDividerAfter();
    
    public abstract boolean needsDividerBefore();
  }
  
  private class ActionMenuPresenterCallback
    implements MenuPresenter.Callback
  {
    private ActionMenuPresenterCallback() {}
    
    public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean) {}
    
    public boolean onOpenSubMenu(MenuBuilder paramMenuBuilder)
    {
      return false;
    }
  }
  
  public static class LayoutParams
    extends LinearLayoutCompat.LayoutParams
  {
    @ViewDebug.ExportedProperty
    public int cellsUsed;
    @ViewDebug.ExportedProperty
    public boolean expandable;
    boolean expanded;
    @ViewDebug.ExportedProperty
    public int extraPixels;
    @ViewDebug.ExportedProperty
    public boolean isOverflowButton;
    @ViewDebug.ExportedProperty
    public boolean preventEdgeOffset;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.isOverflowButton = false;
    }
    
    LayoutParams(int paramInt1, int paramInt2, boolean paramBoolean)
    {
      super(paramInt2);
      this.isOverflowButton = paramBoolean;
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.isOverflowButton = paramLayoutParams.isOverflowButton;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
  }
  
  private class MenuBuilderCallback
    implements MenuBuilder.Callback
  {
    private MenuBuilderCallback() {}
    
    public boolean onMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
    {
      return (ActionMenuView.this.mOnMenuItemClickListener != null) && (ActionMenuView.this.mOnMenuItemClickListener.onMenuItemClick(paramMenuItem));
    }
    
    public void onMenuModeChange(MenuBuilder paramMenuBuilder)
    {
      if (ActionMenuView.this.mMenuBuilderCallback != null) {
        ActionMenuView.this.mMenuBuilderCallback.onMenuModeChange(paramMenuBuilder);
      }
    }
  }
  
  public static abstract interface OnMenuItemClickListener
  {
    public abstract boolean onMenuItemClick(MenuItem paramMenuItem);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/ActionMenuView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */