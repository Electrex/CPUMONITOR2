package android.support.v4.view;

import android.view.View;

public abstract interface ViewPropertyAnimatorUpdateListener
{
  public abstract void onAnimationUpdate(View paramView);
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/ViewPropertyAnimatorUpdateListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */