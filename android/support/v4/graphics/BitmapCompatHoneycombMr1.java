package android.support.v4.graphics;

import android.graphics.Bitmap;

class BitmapCompatHoneycombMr1
{
  static int getAllocationByteCount(Bitmap paramBitmap)
  {
    return paramBitmap.getByteCount();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/graphics/BitmapCompatHoneycombMr1.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */