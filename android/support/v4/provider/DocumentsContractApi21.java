package android.support.v4.provider;

import android.content.Context;
import android.net.Uri;
import android.provider.DocumentsContract;

class DocumentsContractApi21
{
  private static final String TAG = "DocumentFile";
  
  private static void closeQuietly(AutoCloseable paramAutoCloseable)
  {
    if (paramAutoCloseable != null) {}
    try
    {
      paramAutoCloseable.close();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      throw localRuntimeException;
    }
    catch (Exception localException) {}
  }
  
  public static Uri createDirectory(Context paramContext, Uri paramUri, String paramString)
  {
    return createFile(paramContext, paramUri, "vnd.android.document/directory", paramString);
  }
  
  public static Uri createFile(Context paramContext, Uri paramUri, String paramString1, String paramString2)
  {
    return DocumentsContract.createDocument(paramContext.getContentResolver(), paramUri, paramString1, paramString2);
  }
  
  /* Error */
  public static Uri[] listFiles(Context paramContext, Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 37	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   4: astore_2
    //   5: aload_1
    //   6: aload_1
    //   7: invokestatic 49	android/provider/DocumentsContract:getDocumentId	(Landroid/net/Uri;)Ljava/lang/String;
    //   10: invokestatic 53	android/provider/DocumentsContract:buildChildDocumentsUriUsingTree	(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    //   13: astore_3
    //   14: new 55	java/util/ArrayList
    //   17: dup
    //   18: invokespecial 56	java/util/ArrayList:<init>	()V
    //   21: astore 4
    //   23: aload_2
    //   24: aload_3
    //   25: iconst_1
    //   26: anewarray 58	java/lang/String
    //   29: dup
    //   30: iconst_0
    //   31: ldc 60
    //   33: aastore
    //   34: aconst_null
    //   35: aconst_null
    //   36: aconst_null
    //   37: invokevirtual 66	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   40: astore 9
    //   42: aload 9
    //   44: astore 6
    //   46: aload 6
    //   48: invokeinterface 72 1 0
    //   53: ifeq +63 -> 116
    //   56: aload 4
    //   58: aload_1
    //   59: aload 6
    //   61: iconst_0
    //   62: invokeinterface 76 2 0
    //   67: invokestatic 79	android/provider/DocumentsContract:buildDocumentUriUsingTree	(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    //   70: invokevirtual 83	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   73: pop
    //   74: goto -28 -> 46
    //   77: astore 5
    //   79: new 85	java/lang/StringBuilder
    //   82: dup
    //   83: ldc 87
    //   85: invokespecial 90	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   88: aload 5
    //   90: invokevirtual 94	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   93: pop
    //   94: aload 6
    //   96: invokestatic 96	android/support/v4/provider/DocumentsContractApi21:closeQuietly	(Ljava/lang/AutoCloseable;)V
    //   99: aload 4
    //   101: aload 4
    //   103: invokevirtual 100	java/util/ArrayList:size	()I
    //   106: anewarray 102	android/net/Uri
    //   109: invokevirtual 106	java/util/ArrayList:toArray	([Ljava/lang/Object;)[Ljava/lang/Object;
    //   112: checkcast 108	[Landroid/net/Uri;
    //   115: areturn
    //   116: aload 6
    //   118: invokestatic 96	android/support/v4/provider/DocumentsContractApi21:closeQuietly	(Ljava/lang/AutoCloseable;)V
    //   121: goto -22 -> 99
    //   124: astore 7
    //   126: aconst_null
    //   127: astore 6
    //   129: aload 6
    //   131: invokestatic 96	android/support/v4/provider/DocumentsContractApi21:closeQuietly	(Ljava/lang/AutoCloseable;)V
    //   134: aload 7
    //   136: athrow
    //   137: astore 7
    //   139: goto -10 -> 129
    //   142: astore 5
    //   144: aconst_null
    //   145: astore 6
    //   147: goto -68 -> 79
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	150	0	paramContext	Context
    //   0	150	1	paramUri	Uri
    //   4	20	2	localContentResolver	android.content.ContentResolver
    //   13	12	3	localUri	Uri
    //   21	81	4	localArrayList	java.util.ArrayList
    //   77	12	5	localException1	Exception
    //   142	1	5	localException2	Exception
    //   44	102	6	localCursor1	android.database.Cursor
    //   124	11	7	localObject1	Object
    //   137	1	7	localObject2	Object
    //   40	3	9	localCursor2	android.database.Cursor
    // Exception table:
    //   from	to	target	type
    //   46	74	77	java/lang/Exception
    //   23	42	124	finally
    //   46	74	137	finally
    //   79	94	137	finally
    //   23	42	142	java/lang/Exception
  }
  
  public static Uri prepareTreeUri(Uri paramUri)
  {
    return DocumentsContract.buildDocumentUriUsingTree(paramUri, DocumentsContract.getTreeDocumentId(paramUri));
  }
  
  public static Uri renameTo(Context paramContext, Uri paramUri, String paramString)
  {
    return DocumentsContract.renameDocument(paramContext.getContentResolver(), paramUri, paramString);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/provider/DocumentsContractApi21.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */