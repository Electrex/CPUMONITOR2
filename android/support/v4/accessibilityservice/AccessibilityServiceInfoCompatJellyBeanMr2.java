package android.support.v4.accessibilityservice;

import android.accessibilityservice.AccessibilityServiceInfo;

class AccessibilityServiceInfoCompatJellyBeanMr2
{
  public static int getCapabilities(AccessibilityServiceInfo paramAccessibilityServiceInfo)
  {
    return paramAccessibilityServiceInfo.getCapabilities();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/accessibilityservice/AccessibilityServiceInfoCompatJellyBeanMr2.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */