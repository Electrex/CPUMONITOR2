package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.support.v4.content.Loader.OnLoadCompleteListener;
import android.support.v4.util.DebugUtils;
import android.support.v4.util.SparseArrayCompat;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

class LoaderManagerImpl
  extends LoaderManager
{
  static boolean DEBUG = false;
  static final String TAG = "LoaderManager";
  FragmentActivity mActivity;
  boolean mCreatingLoader;
  final SparseArrayCompat<LoaderInfo> mInactiveLoaders = new SparseArrayCompat();
  final SparseArrayCompat<LoaderInfo> mLoaders = new SparseArrayCompat();
  boolean mRetaining;
  boolean mRetainingStarted;
  boolean mStarted;
  final String mWho;
  
  LoaderManagerImpl(String paramString, FragmentActivity paramFragmentActivity, boolean paramBoolean)
  {
    this.mWho = paramString;
    this.mActivity = paramFragmentActivity;
    this.mStarted = paramBoolean;
  }
  
  private LoaderInfo createAndInstallLoader(int paramInt, Bundle paramBundle, LoaderManager.LoaderCallbacks<Object> paramLoaderCallbacks)
  {
    try
    {
      this.mCreatingLoader = true;
      LoaderInfo localLoaderInfo = createLoader(paramInt, paramBundle, paramLoaderCallbacks);
      installLoader(localLoaderInfo);
      return localLoaderInfo;
    }
    finally
    {
      this.mCreatingLoader = false;
    }
  }
  
  private LoaderInfo createLoader(int paramInt, Bundle paramBundle, LoaderManager.LoaderCallbacks<Object> paramLoaderCallbacks)
  {
    LoaderInfo localLoaderInfo = new LoaderInfo(paramInt, paramBundle, paramLoaderCallbacks);
    localLoaderInfo.mLoader = paramLoaderCallbacks.onCreateLoader(paramInt, paramBundle);
    return localLoaderInfo;
  }
  
  public void destroyLoader(int paramInt)
  {
    if (this.mCreatingLoader) {
      throw new IllegalStateException("Called while creating a loader");
    }
    if (DEBUG) {
      new StringBuilder("destroyLoader in ").append(this).append(" of ").append(paramInt);
    }
    int i = this.mLoaders.indexOfKey(paramInt);
    if (i >= 0)
    {
      LoaderInfo localLoaderInfo2 = (LoaderInfo)this.mLoaders.valueAt(i);
      this.mLoaders.removeAt(i);
      localLoaderInfo2.destroy();
    }
    int j = this.mInactiveLoaders.indexOfKey(paramInt);
    if (j >= 0)
    {
      LoaderInfo localLoaderInfo1 = (LoaderInfo)this.mInactiveLoaders.valueAt(j);
      this.mInactiveLoaders.removeAt(j);
      localLoaderInfo1.destroy();
    }
    if ((this.mActivity != null) && (!hasRunningLoaders())) {
      this.mActivity.mFragments.startPendingDeferredFragments();
    }
  }
  
  void doDestroy()
  {
    if (!this.mRetaining)
    {
      if (DEBUG) {
        new StringBuilder("Destroying Active in ").append(this);
      }
      for (int j = -1 + this.mLoaders.size(); j >= 0; j--) {
        ((LoaderInfo)this.mLoaders.valueAt(j)).destroy();
      }
      this.mLoaders.clear();
    }
    if (DEBUG) {
      new StringBuilder("Destroying Inactive in ").append(this);
    }
    for (int i = -1 + this.mInactiveLoaders.size(); i >= 0; i--) {
      ((LoaderInfo)this.mInactiveLoaders.valueAt(i)).destroy();
    }
    this.mInactiveLoaders.clear();
  }
  
  void doReportNextStart()
  {
    for (int i = -1 + this.mLoaders.size(); i >= 0; i--) {
      ((LoaderInfo)this.mLoaders.valueAt(i)).mReportNextStart = true;
    }
  }
  
  void doReportStart()
  {
    for (int i = -1 + this.mLoaders.size(); i >= 0; i--) {
      ((LoaderInfo)this.mLoaders.valueAt(i)).reportStart();
    }
  }
  
  void doRetain()
  {
    if (DEBUG) {
      new StringBuilder("Retaining in ").append(this);
    }
    if (!this.mStarted)
    {
      new RuntimeException("here").fillInStackTrace();
      new StringBuilder("Called doRetain when not started: ").append(this);
    }
    for (;;)
    {
      return;
      this.mRetaining = true;
      this.mStarted = false;
      for (int i = -1 + this.mLoaders.size(); i >= 0; i--) {
        ((LoaderInfo)this.mLoaders.valueAt(i)).retain();
      }
    }
  }
  
  void doStart()
  {
    if (DEBUG) {
      new StringBuilder("Starting in ").append(this);
    }
    if (this.mStarted)
    {
      new RuntimeException("here").fillInStackTrace();
      new StringBuilder("Called doStart when already started: ").append(this);
    }
    for (;;)
    {
      return;
      this.mStarted = true;
      for (int i = -1 + this.mLoaders.size(); i >= 0; i--) {
        ((LoaderInfo)this.mLoaders.valueAt(i)).start();
      }
    }
  }
  
  void doStop()
  {
    if (DEBUG) {
      new StringBuilder("Stopping in ").append(this);
    }
    if (!this.mStarted)
    {
      new RuntimeException("here").fillInStackTrace();
      new StringBuilder("Called doStop when not started: ").append(this);
      return;
    }
    for (int i = -1 + this.mLoaders.size(); i >= 0; i--) {
      ((LoaderInfo)this.mLoaders.valueAt(i)).stop();
    }
    this.mStarted = false;
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    int i = 0;
    if (this.mLoaders.size() > 0)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("Active Loaders:");
      String str2 = paramString + "    ";
      for (int j = 0; j < this.mLoaders.size(); j++)
      {
        LoaderInfo localLoaderInfo2 = (LoaderInfo)this.mLoaders.valueAt(j);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("  #");
        paramPrintWriter.print(this.mLoaders.keyAt(j));
        paramPrintWriter.print(": ");
        paramPrintWriter.println(localLoaderInfo2.toString());
        localLoaderInfo2.dump(str2, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
      }
    }
    if (this.mInactiveLoaders.size() > 0)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("Inactive Loaders:");
      String str1 = paramString + "    ";
      while (i < this.mInactiveLoaders.size())
      {
        LoaderInfo localLoaderInfo1 = (LoaderInfo)this.mInactiveLoaders.valueAt(i);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("  #");
        paramPrintWriter.print(this.mInactiveLoaders.keyAt(i));
        paramPrintWriter.print(": ");
        paramPrintWriter.println(localLoaderInfo1.toString());
        localLoaderInfo1.dump(str1, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        i++;
      }
    }
  }
  
  void finishRetain()
  {
    if (this.mRetaining)
    {
      if (DEBUG) {
        new StringBuilder("Finished Retaining in ").append(this);
      }
      this.mRetaining = false;
      for (int i = -1 + this.mLoaders.size(); i >= 0; i--) {
        ((LoaderInfo)this.mLoaders.valueAt(i)).finishRetain();
      }
    }
  }
  
  public <D> Loader<D> getLoader(int paramInt)
  {
    if (this.mCreatingLoader) {
      throw new IllegalStateException("Called while creating a loader");
    }
    LoaderInfo localLoaderInfo = (LoaderInfo)this.mLoaders.get(paramInt);
    if (localLoaderInfo != null)
    {
      if (localLoaderInfo.mPendingLoader != null) {
        return localLoaderInfo.mPendingLoader.mLoader;
      }
      return localLoaderInfo.mLoader;
    }
    return null;
  }
  
  public boolean hasRunningLoaders()
  {
    int i = this.mLoaders.size();
    int j = 0;
    boolean bool1 = false;
    if (j < i)
    {
      LoaderInfo localLoaderInfo = (LoaderInfo)this.mLoaders.valueAt(j);
      if ((localLoaderInfo.mStarted) && (!localLoaderInfo.mDeliveredData)) {}
      for (boolean bool2 = true;; bool2 = false)
      {
        bool1 |= bool2;
        j++;
        break;
      }
    }
    return bool1;
  }
  
  public <D> Loader<D> initLoader(int paramInt, Bundle paramBundle, LoaderManager.LoaderCallbacks<D> paramLoaderCallbacks)
  {
    if (this.mCreatingLoader) {
      throw new IllegalStateException("Called while creating a loader");
    }
    LoaderInfo localLoaderInfo = (LoaderInfo)this.mLoaders.get(paramInt);
    if (DEBUG) {
      new StringBuilder("initLoader in ").append(this).append(": args=").append(paramBundle);
    }
    if (localLoaderInfo == null)
    {
      localLoaderInfo = createAndInstallLoader(paramInt, paramBundle, paramLoaderCallbacks);
      if (DEBUG) {
        new StringBuilder("  Created new loader ").append(localLoaderInfo);
      }
    }
    for (;;)
    {
      if ((localLoaderInfo.mHaveData) && (this.mStarted)) {
        localLoaderInfo.callOnLoadFinished(localLoaderInfo.mLoader, localLoaderInfo.mData);
      }
      return localLoaderInfo.mLoader;
      if (DEBUG) {
        new StringBuilder("  Re-using existing loader ").append(localLoaderInfo);
      }
      localLoaderInfo.mCallbacks = paramLoaderCallbacks;
    }
  }
  
  void installLoader(LoaderInfo paramLoaderInfo)
  {
    this.mLoaders.put(paramLoaderInfo.mId, paramLoaderInfo);
    if (this.mStarted) {
      paramLoaderInfo.start();
    }
  }
  
  public <D> Loader<D> restartLoader(int paramInt, Bundle paramBundle, LoaderManager.LoaderCallbacks<D> paramLoaderCallbacks)
  {
    if (this.mCreatingLoader) {
      throw new IllegalStateException("Called while creating a loader");
    }
    LoaderInfo localLoaderInfo1 = (LoaderInfo)this.mLoaders.get(paramInt);
    if (DEBUG) {
      new StringBuilder("restartLoader in ").append(this).append(": args=").append(paramBundle);
    }
    if (localLoaderInfo1 != null)
    {
      LoaderInfo localLoaderInfo2 = (LoaderInfo)this.mInactiveLoaders.get(paramInt);
      if (localLoaderInfo2 == null) {
        break label246;
      }
      if (!localLoaderInfo1.mHaveData) {
        break label153;
      }
      if (DEBUG) {
        new StringBuilder("  Removing last inactive loader: ").append(localLoaderInfo1);
      }
      localLoaderInfo2.mDeliveredData = false;
      localLoaderInfo2.destroy();
    }
    for (;;)
    {
      localLoaderInfo1.mLoader.abandon();
      this.mInactiveLoaders.put(paramInt, localLoaderInfo1);
      for (;;)
      {
        return createAndInstallLoader(paramInt, paramBundle, paramLoaderCallbacks).mLoader;
        label153:
        if (localLoaderInfo1.mStarted) {
          break;
        }
        this.mLoaders.put(paramInt, null);
        localLoaderInfo1.destroy();
      }
      if (localLoaderInfo1.mPendingLoader != null)
      {
        if (DEBUG) {
          new StringBuilder("  Removing pending loader: ").append(localLoaderInfo1.mPendingLoader);
        }
        localLoaderInfo1.mPendingLoader.destroy();
        localLoaderInfo1.mPendingLoader = null;
      }
      localLoaderInfo1.mPendingLoader = createLoader(paramInt, paramBundle, paramLoaderCallbacks);
      return localLoaderInfo1.mPendingLoader.mLoader;
      label246:
      if (DEBUG) {
        new StringBuilder("  Making last loader inactive: ").append(localLoaderInfo1);
      }
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(128);
    localStringBuilder.append("LoaderManager{");
    localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    localStringBuilder.append(" in ");
    DebugUtils.buildShortClassTag(this.mActivity, localStringBuilder);
    localStringBuilder.append("}}");
    return localStringBuilder.toString();
  }
  
  void updateActivity(FragmentActivity paramFragmentActivity)
  {
    this.mActivity = paramFragmentActivity;
  }
  
  final class LoaderInfo
    implements Loader.OnLoadCompleteListener<Object>
  {
    final Bundle mArgs;
    LoaderManager.LoaderCallbacks<Object> mCallbacks;
    Object mData;
    boolean mDeliveredData;
    boolean mDestroyed;
    boolean mHaveData;
    final int mId;
    boolean mListenerRegistered;
    Loader<Object> mLoader;
    LoaderInfo mPendingLoader;
    boolean mReportNextStart;
    boolean mRetaining;
    boolean mRetainingStarted;
    boolean mStarted;
    
    public LoaderInfo(Bundle paramBundle, LoaderManager.LoaderCallbacks<Object> paramLoaderCallbacks)
    {
      this.mId = paramBundle;
      this.mArgs = paramLoaderCallbacks;
      LoaderManager.LoaderCallbacks localLoaderCallbacks;
      this.mCallbacks = localLoaderCallbacks;
    }
    
    final void callOnLoadFinished(Loader<Object> paramLoader, Object paramObject)
    {
      String str2;
      if (this.mCallbacks != null)
      {
        if (LoaderManagerImpl.this.mActivity == null) {
          break label153;
        }
        str2 = LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause;
        LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = "onLoadFinished";
      }
      label153:
      for (String str1 = str2;; str1 = null) {
        try
        {
          if (LoaderManagerImpl.DEBUG) {
            new StringBuilder("  onLoadFinished in ").append(paramLoader).append(": ").append(paramLoader.dataToString(paramObject));
          }
          this.mCallbacks.onLoadFinished(paramLoader, paramObject);
          if (LoaderManagerImpl.this.mActivity != null) {
            LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = str1;
          }
          this.mDeliveredData = true;
          return;
        }
        finally
        {
          if (LoaderManagerImpl.this.mActivity != null) {
            LoaderManagerImpl.this.mActivity.mFragments.mNoTransactionsBecause = str1;
          }
        }
      }
    }
    
    /* Error */
    final void destroy()
    {
      // Byte code:
      //   0: getstatic 70	android/support/v4/app/LoaderManagerImpl:DEBUG	Z
      //   3: ifeq +17 -> 20
      //   6: new 72	java/lang/StringBuilder
      //   9: dup
      //   10: ldc 101
      //   12: invokespecial 77	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   15: aload_0
      //   16: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   19: pop
      //   20: aload_0
      //   21: iconst_1
      //   22: putfield 103	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mDestroyed	Z
      //   25: aload_0
      //   26: getfield 98	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mDeliveredData	Z
      //   29: istore_1
      //   30: aload_0
      //   31: iconst_0
      //   32: putfield 98	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mDeliveredData	Z
      //   35: aload_0
      //   36: getfield 45	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mCallbacks	Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
      //   39: ifnull +121 -> 160
      //   42: aload_0
      //   43: getfield 105	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mLoader	Landroid/support/v4/content/Loader;
      //   46: ifnull +114 -> 160
      //   49: aload_0
      //   50: getfield 107	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mHaveData	Z
      //   53: ifeq +107 -> 160
      //   56: iload_1
      //   57: ifeq +103 -> 160
      //   60: getstatic 70	android/support/v4/app/LoaderManagerImpl:DEBUG	Z
      //   63: ifeq +17 -> 80
      //   66: new 72	java/lang/StringBuilder
      //   69: dup
      //   70: ldc 109
      //   72: invokespecial 77	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
      //   75: aload_0
      //   76: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   79: pop
      //   80: aload_0
      //   81: getfield 36	android/support/v4/app/LoaderManagerImpl$LoaderInfo:this$0	Landroid/support/v4/app/LoaderManagerImpl;
      //   84: getfield 53	android/support/v4/app/LoaderManagerImpl:mActivity	Landroid/support/v4/app/FragmentActivity;
      //   87: ifnull +165 -> 252
      //   90: aload_0
      //   91: getfield 36	android/support/v4/app/LoaderManagerImpl$LoaderInfo:this$0	Landroid/support/v4/app/LoaderManagerImpl;
      //   94: getfield 53	android/support/v4/app/LoaderManagerImpl:mActivity	Landroid/support/v4/app/FragmentActivity;
      //   97: getfield 59	android/support/v4/app/FragmentActivity:mFragments	Landroid/support/v4/app/FragmentManagerImpl;
      //   100: getfield 65	android/support/v4/app/FragmentManagerImpl:mNoTransactionsBecause	Ljava/lang/String;
      //   103: astore 4
      //   105: aload_0
      //   106: getfield 36	android/support/v4/app/LoaderManagerImpl$LoaderInfo:this$0	Landroid/support/v4/app/LoaderManagerImpl;
      //   109: getfield 53	android/support/v4/app/LoaderManagerImpl:mActivity	Landroid/support/v4/app/FragmentActivity;
      //   112: getfield 59	android/support/v4/app/FragmentActivity:mFragments	Landroid/support/v4/app/FragmentManagerImpl;
      //   115: ldc 111
      //   117: putfield 65	android/support/v4/app/FragmentManagerImpl:mNoTransactionsBecause	Ljava/lang/String;
      //   120: aload 4
      //   122: astore_2
      //   123: aload_0
      //   124: getfield 45	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mCallbacks	Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
      //   127: aload_0
      //   128: getfield 105	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mLoader	Landroid/support/v4/content/Loader;
      //   131: invokeinterface 114 2 0
      //   136: aload_0
      //   137: getfield 36	android/support/v4/app/LoaderManagerImpl$LoaderInfo:this$0	Landroid/support/v4/app/LoaderManagerImpl;
      //   140: getfield 53	android/support/v4/app/LoaderManagerImpl:mActivity	Landroid/support/v4/app/FragmentActivity;
      //   143: ifnull +17 -> 160
      //   146: aload_0
      //   147: getfield 36	android/support/v4/app/LoaderManagerImpl$LoaderInfo:this$0	Landroid/support/v4/app/LoaderManagerImpl;
      //   150: getfield 53	android/support/v4/app/LoaderManagerImpl:mActivity	Landroid/support/v4/app/FragmentActivity;
      //   153: getfield 59	android/support/v4/app/FragmentActivity:mFragments	Landroid/support/v4/app/FragmentManagerImpl;
      //   156: aload_2
      //   157: putfield 65	android/support/v4/app/FragmentManagerImpl:mNoTransactionsBecause	Ljava/lang/String;
      //   160: aload_0
      //   161: aconst_null
      //   162: putfield 45	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mCallbacks	Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
      //   165: aload_0
      //   166: aconst_null
      //   167: putfield 116	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mData	Ljava/lang/Object;
      //   170: aload_0
      //   171: iconst_0
      //   172: putfield 107	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mHaveData	Z
      //   175: aload_0
      //   176: getfield 105	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mLoader	Landroid/support/v4/content/Loader;
      //   179: ifnull +30 -> 209
      //   182: aload_0
      //   183: getfield 118	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mListenerRegistered	Z
      //   186: ifeq +16 -> 202
      //   189: aload_0
      //   190: iconst_0
      //   191: putfield 118	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mListenerRegistered	Z
      //   194: aload_0
      //   195: getfield 105	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mLoader	Landroid/support/v4/content/Loader;
      //   198: aload_0
      //   199: invokevirtual 122	android/support/v4/content/Loader:unregisterListener	(Landroid/support/v4/content/Loader$OnLoadCompleteListener;)V
      //   202: aload_0
      //   203: getfield 105	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mLoader	Landroid/support/v4/content/Loader;
      //   206: invokevirtual 125	android/support/v4/content/Loader:reset	()V
      //   209: aload_0
      //   210: getfield 127	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mPendingLoader	Landroid/support/v4/app/LoaderManagerImpl$LoaderInfo;
      //   213: ifnull +38 -> 251
      //   216: aload_0
      //   217: getfield 127	android/support/v4/app/LoaderManagerImpl$LoaderInfo:mPendingLoader	Landroid/support/v4/app/LoaderManagerImpl$LoaderInfo;
      //   220: astore_0
      //   221: goto -221 -> 0
      //   224: astore_3
      //   225: aload_0
      //   226: getfield 36	android/support/v4/app/LoaderManagerImpl$LoaderInfo:this$0	Landroid/support/v4/app/LoaderManagerImpl;
      //   229: getfield 53	android/support/v4/app/LoaderManagerImpl:mActivity	Landroid/support/v4/app/FragmentActivity;
      //   232: ifnull +17 -> 249
      //   235: aload_0
      //   236: getfield 36	android/support/v4/app/LoaderManagerImpl$LoaderInfo:this$0	Landroid/support/v4/app/LoaderManagerImpl;
      //   239: getfield 53	android/support/v4/app/LoaderManagerImpl:mActivity	Landroid/support/v4/app/FragmentActivity;
      //   242: getfield 59	android/support/v4/app/FragmentActivity:mFragments	Landroid/support/v4/app/FragmentManagerImpl;
      //   245: aload_2
      //   246: putfield 65	android/support/v4/app/FragmentManagerImpl:mNoTransactionsBecause	Ljava/lang/String;
      //   249: aload_3
      //   250: athrow
      //   251: return
      //   252: aconst_null
      //   253: astore_2
      //   254: goto -131 -> 123
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	257	0	this	LoaderInfo
      //   29	28	1	bool	boolean
      //   122	132	2	str1	String
      //   224	26	3	localObject	Object
      //   103	18	4	str2	String
      // Exception table:
      //   from	to	target	type
      //   123	136	224	finally
    }
    
    public final void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
    {
      for (;;)
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mId=");
        paramPrintWriter.print(this.mId);
        paramPrintWriter.print(" mArgs=");
        paramPrintWriter.println(this.mArgs);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mCallbacks=");
        paramPrintWriter.println(this.mCallbacks);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mLoader=");
        paramPrintWriter.println(this.mLoader);
        if (this.mLoader != null) {
          this.mLoader.dump(paramString + "  ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
        }
        if ((this.mHaveData) || (this.mDeliveredData))
        {
          paramPrintWriter.print(paramString);
          paramPrintWriter.print("mHaveData=");
          paramPrintWriter.print(this.mHaveData);
          paramPrintWriter.print("  mDeliveredData=");
          paramPrintWriter.println(this.mDeliveredData);
          paramPrintWriter.print(paramString);
          paramPrintWriter.print("mData=");
          paramPrintWriter.println(this.mData);
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mStarted=");
        paramPrintWriter.print(this.mStarted);
        paramPrintWriter.print(" mReportNextStart=");
        paramPrintWriter.print(this.mReportNextStart);
        paramPrintWriter.print(" mDestroyed=");
        paramPrintWriter.println(this.mDestroyed);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mRetaining=");
        paramPrintWriter.print(this.mRetaining);
        paramPrintWriter.print(" mRetainingStarted=");
        paramPrintWriter.print(this.mRetainingStarted);
        paramPrintWriter.print(" mListenerRegistered=");
        paramPrintWriter.println(this.mListenerRegistered);
        if (this.mPendingLoader == null) {
          break;
        }
        paramPrintWriter.print(paramString);
        paramPrintWriter.println("Pending Loader ");
        paramPrintWriter.print(this.mPendingLoader);
        paramPrintWriter.println(":");
        this = this.mPendingLoader;
        paramString = paramString + "  ";
      }
    }
    
    final void finishRetain()
    {
      if (this.mRetaining)
      {
        if (LoaderManagerImpl.DEBUG) {
          new StringBuilder("  Finished Retaining: ").append(this);
        }
        this.mRetaining = false;
        if ((this.mStarted != this.mRetainingStarted) && (!this.mStarted)) {
          stop();
        }
      }
      if ((this.mStarted) && (this.mHaveData) && (!this.mReportNextStart)) {
        callOnLoadFinished(this.mLoader, this.mData);
      }
    }
    
    public final void onLoadComplete(Loader<Object> paramLoader, Object paramObject)
    {
      if (LoaderManagerImpl.DEBUG) {
        new StringBuilder("onLoadComplete: ").append(this);
      }
      if (this.mDestroyed) {}
      do
      {
        return;
        if (LoaderManagerImpl.this.mLoaders.get(this.mId) != this) {
          return;
        }
        LoaderInfo localLoaderInfo1 = this.mPendingLoader;
        if (localLoaderInfo1 != null)
        {
          if (LoaderManagerImpl.DEBUG) {
            new StringBuilder("  Switching to pending loader: ").append(localLoaderInfo1);
          }
          this.mPendingLoader = null;
          LoaderManagerImpl.this.mLoaders.put(this.mId, null);
          destroy();
          LoaderManagerImpl.this.installLoader(localLoaderInfo1);
          return;
        }
        if ((this.mData != paramObject) || (!this.mHaveData))
        {
          this.mData = paramObject;
          this.mHaveData = true;
          if (this.mStarted) {
            callOnLoadFinished(paramLoader, paramObject);
          }
        }
        LoaderInfo localLoaderInfo2 = (LoaderInfo)LoaderManagerImpl.this.mInactiveLoaders.get(this.mId);
        if ((localLoaderInfo2 != null) && (localLoaderInfo2 != this))
        {
          localLoaderInfo2.mDeliveredData = false;
          localLoaderInfo2.destroy();
          LoaderManagerImpl.this.mInactiveLoaders.remove(this.mId);
        }
      } while ((LoaderManagerImpl.this.mActivity == null) || (LoaderManagerImpl.this.hasRunningLoaders()));
      LoaderManagerImpl.this.mActivity.mFragments.startPendingDeferredFragments();
    }
    
    final void reportStart()
    {
      if ((this.mStarted) && (this.mReportNextStart))
      {
        this.mReportNextStart = false;
        if (this.mHaveData) {
          callOnLoadFinished(this.mLoader, this.mData);
        }
      }
    }
    
    final void retain()
    {
      if (LoaderManagerImpl.DEBUG) {
        new StringBuilder("  Retaining: ").append(this);
      }
      this.mRetaining = true;
      this.mRetainingStarted = this.mStarted;
      this.mStarted = false;
      this.mCallbacks = null;
    }
    
    final void start()
    {
      if ((this.mRetaining) && (this.mRetainingStarted)) {
        this.mStarted = true;
      }
      do
      {
        do
        {
          return;
        } while (this.mStarted);
        this.mStarted = true;
        if (LoaderManagerImpl.DEBUG) {
          new StringBuilder("  Starting: ").append(this);
        }
        if ((this.mLoader == null) && (this.mCallbacks != null)) {
          this.mLoader = this.mCallbacks.onCreateLoader(this.mId, this.mArgs);
        }
      } while (this.mLoader == null);
      if ((this.mLoader.getClass().isMemberClass()) && (!Modifier.isStatic(this.mLoader.getClass().getModifiers()))) {
        throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + this.mLoader);
      }
      if (!this.mListenerRegistered)
      {
        this.mLoader.registerListener(this.mId, this);
        this.mListenerRegistered = true;
      }
      this.mLoader.startLoading();
    }
    
    final void stop()
    {
      if (LoaderManagerImpl.DEBUG) {
        new StringBuilder("  Stopping: ").append(this);
      }
      this.mStarted = false;
      if ((!this.mRetaining) && (this.mLoader != null) && (this.mListenerRegistered))
      {
        this.mListenerRegistered = false;
        this.mLoader.unregisterListener(this);
        this.mLoader.stopLoading();
      }
    }
    
    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(64);
      localStringBuilder.append("LoaderInfo{");
      localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      localStringBuilder.append(" #");
      localStringBuilder.append(this.mId);
      localStringBuilder.append(" : ");
      DebugUtils.buildShortClassTag(this.mLoader, localStringBuilder);
      localStringBuilder.append("}}");
      return localStringBuilder.toString();
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/LoaderManagerImpl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */