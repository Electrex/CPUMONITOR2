package android.support.v4.print;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.CancellationSignal.OnCancelListener;
import android.print.PrintAttributes;
import android.print.PrintAttributes.Builder;
import android.print.PrintAttributes.MediaSize;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentAdapter.LayoutResultCallback;
import android.print.PrintDocumentInfo;
import android.print.PrintDocumentInfo.Builder;
import android.print.PrintManager;
import java.io.FileNotFoundException;

class PrintHelperKitkat
{
  public static final int COLOR_MODE_COLOR = 2;
  public static final int COLOR_MODE_MONOCHROME = 1;
  private static final String LOG_TAG = "PrintHelperKitkat";
  private static final int MAX_PRINT_SIZE = 3500;
  public static final int ORIENTATION_LANDSCAPE = 1;
  public static final int ORIENTATION_PORTRAIT = 2;
  public static final int SCALE_MODE_FILL = 2;
  public static final int SCALE_MODE_FIT = 1;
  int mColorMode = 2;
  final Context mContext;
  BitmapFactory.Options mDecodeOptions = null;
  private final Object mLock = new Object();
  int mOrientation = 1;
  int mScaleMode = 2;
  
  PrintHelperKitkat(Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  private Matrix getMatrix(int paramInt1, int paramInt2, RectF paramRectF, int paramInt3)
  {
    Matrix localMatrix = new Matrix();
    float f1 = paramRectF.width() / paramInt1;
    if (paramInt3 == 2) {}
    for (float f2 = Math.max(f1, paramRectF.height() / paramInt2);; f2 = Math.min(f1, paramRectF.height() / paramInt2))
    {
      localMatrix.postScale(f2, f2);
      localMatrix.postTranslate((paramRectF.width() - f2 * paramInt1) / 2.0F, (paramRectF.height() - f2 * paramInt2) / 2.0F);
      return localMatrix;
    }
  }
  
  /* Error */
  private Bitmap loadBitmap(Uri paramUri, BitmapFactory.Options paramOptions)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_1
    //   3: ifnull +10 -> 13
    //   6: aload_0
    //   7: getfield 45	android/support/v4/print/PrintHelperKitkat:mContext	Landroid/content/Context;
    //   10: ifnonnull +13 -> 23
    //   13: new 93	java/lang/IllegalArgumentException
    //   16: dup
    //   17: ldc 95
    //   19: invokespecial 98	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   22: athrow
    //   23: aload_0
    //   24: getfield 45	android/support/v4/print/PrintHelperKitkat:mContext	Landroid/content/Context;
    //   27: invokevirtual 104	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   30: aload_1
    //   31: invokevirtual 110	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   34: astore_3
    //   35: aload_3
    //   36: aconst_null
    //   37: aload_2
    //   38: invokestatic 116	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   41: astore 6
    //   43: aload_3
    //   44: ifnull +7 -> 51
    //   47: aload_3
    //   48: invokevirtual 121	java/io/InputStream:close	()V
    //   51: aload 6
    //   53: areturn
    //   54: astore 4
    //   56: aload_3
    //   57: ifnull +7 -> 64
    //   60: aload_3
    //   61: invokevirtual 121	java/io/InputStream:close	()V
    //   64: aload 4
    //   66: athrow
    //   67: astore 7
    //   69: aload 6
    //   71: areturn
    //   72: astore 5
    //   74: goto -10 -> 64
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	77	0	this	PrintHelperKitkat
    //   0	77	1	paramUri	Uri
    //   0	77	2	paramOptions	BitmapFactory.Options
    //   1	60	3	localInputStream	java.io.InputStream
    //   54	11	4	localObject	Object
    //   72	1	5	localIOException1	java.io.IOException
    //   41	29	6	localBitmap	Bitmap
    //   67	1	7	localIOException2	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   23	43	54	finally
    //   47	51	67	java/io/IOException
    //   60	64	72	java/io/IOException
  }
  
  private Bitmap loadConstrainedBitmap(Uri paramUri, int paramInt)
  {
    int i = 1;
    if ((paramInt <= 0) || (paramUri == null) || (this.mContext == null)) {
      throw new IllegalArgumentException("bad argument to getScaledBitmap");
    }
    BitmapFactory.Options localOptions1 = new BitmapFactory.Options();
    localOptions1.inJustDecodeBounds = i;
    loadBitmap(paramUri, localOptions1);
    int j = localOptions1.outWidth;
    int k = localOptions1.outHeight;
    if ((j <= 0) || (k <= 0)) {}
    do
    {
      return null;
      int m = Math.max(j, k);
      while (m > paramInt)
      {
        m >>>= 1;
        i <<= 1;
      }
    } while ((i <= 0) || (Math.min(j, k) / i <= 0));
    BitmapFactory.Options localOptions2;
    synchronized (this.mLock)
    {
      this.mDecodeOptions = new BitmapFactory.Options();
      this.mDecodeOptions.inMutable = true;
      this.mDecodeOptions.inSampleSize = i;
      localOptions2 = this.mDecodeOptions;
    }
    try
    {
      Bitmap localBitmap = loadBitmap(paramUri, localOptions2);
      synchronized (this.mLock)
      {
        this.mDecodeOptions = null;
        return localBitmap;
      }
      localObject2 = finally;
      throw ((Throwable)localObject2);
    }
    finally {}
  }
  
  public int getColorMode()
  {
    return this.mColorMode;
  }
  
  public int getOrientation()
  {
    return this.mOrientation;
  }
  
  public int getScaleMode()
  {
    return this.mScaleMode;
  }
  
  public void printBitmap(final String paramString, final Bitmap paramBitmap, final OnPrintFinishCallback paramOnPrintFinishCallback)
  {
    if (paramBitmap == null) {
      return;
    }
    final int i = this.mScaleMode;
    PrintManager localPrintManager = (PrintManager)this.mContext.getSystemService("print");
    PrintAttributes.MediaSize localMediaSize = PrintAttributes.MediaSize.UNKNOWN_PORTRAIT;
    if (paramBitmap.getWidth() > paramBitmap.getHeight()) {
      localMediaSize = PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE;
    }
    PrintAttributes localPrintAttributes = new PrintAttributes.Builder().setMediaSize(localMediaSize).setColorMode(this.mColorMode).build();
    localPrintManager.print(paramString, new PrintDocumentAdapter()
    {
      private PrintAttributes mAttributes;
      
      public void onFinish()
      {
        if (paramOnPrintFinishCallback != null) {
          paramOnPrintFinishCallback.onFinish();
        }
      }
      
      public void onLayout(PrintAttributes paramAnonymousPrintAttributes1, PrintAttributes paramAnonymousPrintAttributes2, CancellationSignal paramAnonymousCancellationSignal, PrintDocumentAdapter.LayoutResultCallback paramAnonymousLayoutResultCallback, Bundle paramAnonymousBundle)
      {
        int i = 1;
        this.mAttributes = paramAnonymousPrintAttributes2;
        PrintDocumentInfo localPrintDocumentInfo = new PrintDocumentInfo.Builder(paramString).setContentType(i).setPageCount(i).build();
        if (!paramAnonymousPrintAttributes2.equals(paramAnonymousPrintAttributes1)) {}
        for (;;)
        {
          paramAnonymousLayoutResultCallback.onLayoutFinished(localPrintDocumentInfo, i);
          return;
          int j = 0;
        }
      }
      
      /* Error */
      public void onWrite(android.print.PageRange[] paramAnonymousArrayOfPageRange, android.os.ParcelFileDescriptor paramAnonymousParcelFileDescriptor, CancellationSignal paramAnonymousCancellationSignal, android.print.PrintDocumentAdapter.WriteResultCallback paramAnonymousWriteResultCallback)
      {
        // Byte code:
        //   0: new 79	android/print/pdf/PrintedPdfDocument
        //   3: dup
        //   4: aload_0
        //   5: getfield 25	android/support/v4/print/PrintHelperKitkat$1:this$0	Landroid/support/v4/print/PrintHelperKitkat;
        //   8: getfield 83	android/support/v4/print/PrintHelperKitkat:mContext	Landroid/content/Context;
        //   11: aload_0
        //   12: getfield 45	android/support/v4/print/PrintHelperKitkat$1:mAttributes	Landroid/print/PrintAttributes;
        //   15: invokespecial 86	android/print/pdf/PrintedPdfDocument:<init>	(Landroid/content/Context;Landroid/print/PrintAttributes;)V
        //   18: astore 5
        //   20: aload 5
        //   22: iconst_1
        //   23: invokevirtual 90	android/print/pdf/PrintedPdfDocument:startPage	(I)Landroid/graphics/pdf/PdfDocument$Page;
        //   26: astore 8
        //   28: new 92	android/graphics/RectF
        //   31: dup
        //   32: aload 8
        //   34: invokevirtual 98	android/graphics/pdf/PdfDocument$Page:getInfo	()Landroid/graphics/pdf/PdfDocument$PageInfo;
        //   37: invokevirtual 104	android/graphics/pdf/PdfDocument$PageInfo:getContentRect	()Landroid/graphics/Rect;
        //   40: invokespecial 107	android/graphics/RectF:<init>	(Landroid/graphics/Rect;)V
        //   43: astore 9
        //   45: aload_0
        //   46: getfield 25	android/support/v4/print/PrintHelperKitkat$1:this$0	Landroid/support/v4/print/PrintHelperKitkat;
        //   49: aload_0
        //   50: getfield 29	android/support/v4/print/PrintHelperKitkat$1:val$bitmap	Landroid/graphics/Bitmap;
        //   53: invokevirtual 113	android/graphics/Bitmap:getWidth	()I
        //   56: aload_0
        //   57: getfield 29	android/support/v4/print/PrintHelperKitkat$1:val$bitmap	Landroid/graphics/Bitmap;
        //   60: invokevirtual 116	android/graphics/Bitmap:getHeight	()I
        //   63: aload 9
        //   65: aload_0
        //   66: getfield 31	android/support/v4/print/PrintHelperKitkat$1:val$fittingMode	I
        //   69: invokestatic 120	android/support/v4/print/PrintHelperKitkat:access$000	(Landroid/support/v4/print/PrintHelperKitkat;IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
        //   72: astore 10
        //   74: aload 8
        //   76: invokevirtual 124	android/graphics/pdf/PdfDocument$Page:getCanvas	()Landroid/graphics/Canvas;
        //   79: aload_0
        //   80: getfield 29	android/support/v4/print/PrintHelperKitkat$1:val$bitmap	Landroid/graphics/Bitmap;
        //   83: aload 10
        //   85: aconst_null
        //   86: invokevirtual 130	android/graphics/Canvas:drawBitmap	(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
        //   89: aload 5
        //   91: aload 8
        //   93: invokevirtual 134	android/print/pdf/PrintedPdfDocument:finishPage	(Landroid/graphics/pdf/PdfDocument$Page;)V
        //   96: aload 5
        //   98: new 136	java/io/FileOutputStream
        //   101: dup
        //   102: aload_2
        //   103: invokevirtual 142	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //   106: invokespecial 145	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
        //   109: invokevirtual 149	android/print/pdf/PrintedPdfDocument:writeTo	(Ljava/io/OutputStream;)V
        //   112: iconst_1
        //   113: anewarray 151	android/print/PageRange
        //   116: astore 13
        //   118: aload 13
        //   120: iconst_0
        //   121: getstatic 155	android/print/PageRange:ALL_PAGES	Landroid/print/PageRange;
        //   124: aastore
        //   125: aload 4
        //   127: aload 13
        //   129: invokevirtual 161	android/print/PrintDocumentAdapter$WriteResultCallback:onWriteFinished	([Landroid/print/PageRange;)V
        //   132: aload 5
        //   134: invokevirtual 164	android/print/pdf/PrintedPdfDocument:close	()V
        //   137: aload_2
        //   138: ifnull +7 -> 145
        //   141: aload_2
        //   142: invokevirtual 165	android/os/ParcelFileDescriptor:close	()V
        //   145: return
        //   146: astore 11
        //   148: aload 4
        //   150: aconst_null
        //   151: invokevirtual 169	android/print/PrintDocumentAdapter$WriteResultCallback:onWriteFailed	(Ljava/lang/CharSequence;)V
        //   154: goto -22 -> 132
        //   157: astore 6
        //   159: aload 5
        //   161: invokevirtual 164	android/print/pdf/PrintedPdfDocument:close	()V
        //   164: aload_2
        //   165: ifnull +7 -> 172
        //   168: aload_2
        //   169: invokevirtual 165	android/os/ParcelFileDescriptor:close	()V
        //   172: aload 6
        //   174: athrow
        //   175: astore 12
        //   177: return
        //   178: astore 7
        //   180: goto -8 -> 172
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	183	0	this	1
        //   0	183	1	paramAnonymousArrayOfPageRange	android.print.PageRange[]
        //   0	183	2	paramAnonymousParcelFileDescriptor	android.os.ParcelFileDescriptor
        //   0	183	3	paramAnonymousCancellationSignal	CancellationSignal
        //   0	183	4	paramAnonymousWriteResultCallback	android.print.PrintDocumentAdapter.WriteResultCallback
        //   18	142	5	localPrintedPdfDocument	android.print.pdf.PrintedPdfDocument
        //   157	16	6	localObject	Object
        //   178	1	7	localIOException1	java.io.IOException
        //   26	66	8	localPage	android.graphics.pdf.PdfDocument.Page
        //   43	21	9	localRectF	RectF
        //   72	12	10	localMatrix	Matrix
        //   146	1	11	localIOException2	java.io.IOException
        //   175	1	12	localIOException3	java.io.IOException
        //   116	12	13	arrayOfPageRange	android.print.PageRange[]
        // Exception table:
        //   from	to	target	type
        //   96	132	146	java/io/IOException
        //   20	96	157	finally
        //   96	132	157	finally
        //   148	154	157	finally
        //   141	145	175	java/io/IOException
        //   168	172	178	java/io/IOException
      }
    }, localPrintAttributes);
  }
  
  public void printBitmap(final String paramString, final Uri paramUri, final OnPrintFinishCallback paramOnPrintFinishCallback)
  {
    PrintDocumentAdapter local2 = new PrintDocumentAdapter()
    {
      AsyncTask<Uri, Boolean, Bitmap> loadBitmap;
      private PrintAttributes mAttributes;
      Bitmap mBitmap = null;
      
      private void cancelLoad()
      {
        synchronized (PrintHelperKitkat.this.mLock)
        {
          if (PrintHelperKitkat.this.mDecodeOptions != null)
          {
            PrintHelperKitkat.this.mDecodeOptions.requestCancelDecode();
            PrintHelperKitkat.this.mDecodeOptions = null;
          }
          return;
        }
      }
      
      public void onFinish()
      {
        super.onFinish();
        cancelLoad();
        this.loadBitmap.cancel(true);
        if (paramOnPrintFinishCallback != null) {
          paramOnPrintFinishCallback.onFinish();
        }
      }
      
      public void onLayout(final PrintAttributes paramAnonymousPrintAttributes1, final PrintAttributes paramAnonymousPrintAttributes2, final CancellationSignal paramAnonymousCancellationSignal, final PrintDocumentAdapter.LayoutResultCallback paramAnonymousLayoutResultCallback, Bundle paramAnonymousBundle)
      {
        int i = 1;
        if (paramAnonymousCancellationSignal.isCanceled())
        {
          paramAnonymousLayoutResultCallback.onLayoutCancelled();
          this.mAttributes = paramAnonymousPrintAttributes2;
          return;
        }
        if (this.mBitmap != null)
        {
          PrintDocumentInfo localPrintDocumentInfo = new PrintDocumentInfo.Builder(paramString).setContentType(i).setPageCount(i).build();
          if (!paramAnonymousPrintAttributes2.equals(paramAnonymousPrintAttributes1)) {}
          for (;;)
          {
            paramAnonymousLayoutResultCallback.onLayoutFinished(localPrintDocumentInfo, i);
            return;
            int j = 0;
          }
        }
        this.loadBitmap = new AsyncTask()
        {
          protected Bitmap doInBackground(Uri... paramAnonymous2VarArgs)
          {
            try
            {
              Bitmap localBitmap = PrintHelperKitkat.this.loadConstrainedBitmap(PrintHelperKitkat.2.this.val$imageFile, 3500);
              return localBitmap;
            }
            catch (FileNotFoundException localFileNotFoundException) {}
            return null;
          }
          
          protected void onCancelled(Bitmap paramAnonymous2Bitmap)
          {
            paramAnonymousLayoutResultCallback.onLayoutCancelled();
          }
          
          protected void onPostExecute(Bitmap paramAnonymous2Bitmap)
          {
            int i = 1;
            super.onPostExecute(paramAnonymous2Bitmap);
            PrintHelperKitkat.2.this.mBitmap = paramAnonymous2Bitmap;
            if (paramAnonymous2Bitmap != null)
            {
              PrintDocumentInfo localPrintDocumentInfo = new PrintDocumentInfo.Builder(PrintHelperKitkat.2.this.val$jobName).setContentType(i).setPageCount(i).build();
              if (!paramAnonymousPrintAttributes2.equals(paramAnonymousPrintAttributes1)) {}
              for (;;)
              {
                paramAnonymousLayoutResultCallback.onLayoutFinished(localPrintDocumentInfo, i);
                return;
                int j = 0;
              }
            }
            paramAnonymousLayoutResultCallback.onLayoutFailed(null);
          }
          
          protected void onPreExecute()
          {
            paramAnonymousCancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener()
            {
              public void onCancel()
              {
                PrintHelperKitkat.2.this.cancelLoad();
                PrintHelperKitkat.2.1.this.cancel(false);
              }
            });
          }
        };
        this.loadBitmap.execute(new Uri[0]);
        this.mAttributes = paramAnonymousPrintAttributes2;
      }
      
      /* Error */
      public void onWrite(android.print.PageRange[] paramAnonymousArrayOfPageRange, android.os.ParcelFileDescriptor paramAnonymousParcelFileDescriptor, CancellationSignal paramAnonymousCancellationSignal, android.print.PrintDocumentAdapter.WriteResultCallback paramAnonymousWriteResultCallback)
      {
        // Byte code:
        //   0: new 133	android/print/pdf/PrintedPdfDocument
        //   3: dup
        //   4: aload_0
        //   5: getfield 30	android/support/v4/print/PrintHelperKitkat$2:this$0	Landroid/support/v4/print/PrintHelperKitkat;
        //   8: getfield 137	android/support/v4/print/PrintHelperKitkat:mContext	Landroid/content/Context;
        //   11: aload_0
        //   12: getfield 90	android/support/v4/print/PrintHelperKitkat$2:mAttributes	Landroid/print/PrintAttributes;
        //   15: invokespecial 140	android/print/pdf/PrintedPdfDocument:<init>	(Landroid/content/Context;Landroid/print/PrintAttributes;)V
        //   18: astore 5
        //   20: aload 5
        //   22: iconst_1
        //   23: invokevirtual 144	android/print/pdf/PrintedPdfDocument:startPage	(I)Landroid/graphics/pdf/PdfDocument$Page;
        //   26: astore 8
        //   28: new 146	android/graphics/RectF
        //   31: dup
        //   32: aload 8
        //   34: invokevirtual 152	android/graphics/pdf/PdfDocument$Page:getInfo	()Landroid/graphics/pdf/PdfDocument$PageInfo;
        //   37: invokevirtual 158	android/graphics/pdf/PdfDocument$PageInfo:getContentRect	()Landroid/graphics/Rect;
        //   40: invokespecial 161	android/graphics/RectF:<init>	(Landroid/graphics/Rect;)V
        //   43: astore 9
        //   45: aload_0
        //   46: getfield 30	android/support/v4/print/PrintHelperKitkat$2:this$0	Landroid/support/v4/print/PrintHelperKitkat;
        //   49: aload_0
        //   50: getfield 43	android/support/v4/print/PrintHelperKitkat$2:mBitmap	Landroid/graphics/Bitmap;
        //   53: invokevirtual 167	android/graphics/Bitmap:getWidth	()I
        //   56: aload_0
        //   57: getfield 43	android/support/v4/print/PrintHelperKitkat$2:mBitmap	Landroid/graphics/Bitmap;
        //   60: invokevirtual 170	android/graphics/Bitmap:getHeight	()I
        //   63: aload 9
        //   65: aload_0
        //   66: getfield 38	android/support/v4/print/PrintHelperKitkat$2:val$fittingMode	I
        //   69: invokestatic 174	android/support/v4/print/PrintHelperKitkat:access$000	(Landroid/support/v4/print/PrintHelperKitkat;IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
        //   72: astore 10
        //   74: aload 8
        //   76: invokevirtual 178	android/graphics/pdf/PdfDocument$Page:getCanvas	()Landroid/graphics/Canvas;
        //   79: aload_0
        //   80: getfield 43	android/support/v4/print/PrintHelperKitkat$2:mBitmap	Landroid/graphics/Bitmap;
        //   83: aload 10
        //   85: aconst_null
        //   86: invokevirtual 184	android/graphics/Canvas:drawBitmap	(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
        //   89: aload 5
        //   91: aload 8
        //   93: invokevirtual 188	android/print/pdf/PrintedPdfDocument:finishPage	(Landroid/graphics/pdf/PdfDocument$Page;)V
        //   96: aload 5
        //   98: new 190	java/io/FileOutputStream
        //   101: dup
        //   102: aload_2
        //   103: invokevirtual 196	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //   106: invokespecial 199	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
        //   109: invokevirtual 203	android/print/pdf/PrintedPdfDocument:writeTo	(Ljava/io/OutputStream;)V
        //   112: iconst_1
        //   113: anewarray 205	android/print/PageRange
        //   116: astore 13
        //   118: aload 13
        //   120: iconst_0
        //   121: getstatic 209	android/print/PageRange:ALL_PAGES	Landroid/print/PageRange;
        //   124: aastore
        //   125: aload 4
        //   127: aload 13
        //   129: invokevirtual 215	android/print/PrintDocumentAdapter$WriteResultCallback:onWriteFinished	([Landroid/print/PageRange;)V
        //   132: aload 5
        //   134: invokevirtual 218	android/print/pdf/PrintedPdfDocument:close	()V
        //   137: aload_2
        //   138: ifnull +7 -> 145
        //   141: aload_2
        //   142: invokevirtual 219	android/os/ParcelFileDescriptor:close	()V
        //   145: return
        //   146: astore 11
        //   148: aload 4
        //   150: aconst_null
        //   151: invokevirtual 223	android/print/PrintDocumentAdapter$WriteResultCallback:onWriteFailed	(Ljava/lang/CharSequence;)V
        //   154: goto -22 -> 132
        //   157: astore 6
        //   159: aload 5
        //   161: invokevirtual 218	android/print/pdf/PrintedPdfDocument:close	()V
        //   164: aload_2
        //   165: ifnull +7 -> 172
        //   168: aload_2
        //   169: invokevirtual 219	android/os/ParcelFileDescriptor:close	()V
        //   172: aload 6
        //   174: athrow
        //   175: astore 12
        //   177: return
        //   178: astore 7
        //   180: goto -8 -> 172
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	183	0	this	2
        //   0	183	1	paramAnonymousArrayOfPageRange	android.print.PageRange[]
        //   0	183	2	paramAnonymousParcelFileDescriptor	android.os.ParcelFileDescriptor
        //   0	183	3	paramAnonymousCancellationSignal	CancellationSignal
        //   0	183	4	paramAnonymousWriteResultCallback	android.print.PrintDocumentAdapter.WriteResultCallback
        //   18	142	5	localPrintedPdfDocument	android.print.pdf.PrintedPdfDocument
        //   157	16	6	localObject	Object
        //   178	1	7	localIOException1	java.io.IOException
        //   26	66	8	localPage	android.graphics.pdf.PdfDocument.Page
        //   43	21	9	localRectF	RectF
        //   72	12	10	localMatrix	Matrix
        //   146	1	11	localIOException2	java.io.IOException
        //   175	1	12	localIOException3	java.io.IOException
        //   116	12	13	arrayOfPageRange	android.print.PageRange[]
        // Exception table:
        //   from	to	target	type
        //   96	132	146	java/io/IOException
        //   20	96	157	finally
        //   96	132	157	finally
        //   148	154	157	finally
        //   141	145	175	java/io/IOException
        //   168	172	178	java/io/IOException
      }
    };
    PrintManager localPrintManager = (PrintManager)this.mContext.getSystemService("print");
    PrintAttributes.Builder localBuilder = new PrintAttributes.Builder();
    localBuilder.setColorMode(this.mColorMode);
    if (this.mOrientation == 1) {
      localBuilder.setMediaSize(PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE);
    }
    for (;;)
    {
      localPrintManager.print(paramString, local2, localBuilder.build());
      return;
      if (this.mOrientation == 2) {
        localBuilder.setMediaSize(PrintAttributes.MediaSize.UNKNOWN_PORTRAIT);
      }
    }
  }
  
  public void setColorMode(int paramInt)
  {
    this.mColorMode = paramInt;
  }
  
  public void setOrientation(int paramInt)
  {
    this.mOrientation = paramInt;
  }
  
  public void setScaleMode(int paramInt)
  {
    this.mScaleMode = paramInt;
  }
  
  public static abstract interface OnPrintFinishCallback
  {
    public abstract void onFinish();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/print/PrintHelperKitkat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */